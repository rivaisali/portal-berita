var mnmanIsSaved = true;
var mnmanItemDeleted = 0;

function generateMenumanItem(obj) {
    obj.count = !obj.count ? '' : obj.count;
    obj.grup = !obj.grup ? '' : obj.grup;
    obj.icon = !obj.icon ? '' : obj.icon;
    obj.form = !obj.form ? '1' : obj.form;
    var txt = '<li class="dd-item" data-judul="'+obj.judul+'" data-type="'+obj.type+'" data-icon="'+obj.icon+'" '+
        'data-count="'+obj.count+'" data-grup="'+obj.grup+'" data-form="'+obj.form+'"><div class="dd-handle">';
    if (obj.icon != '') txt+= '<i class="glyphicon glyphicon-'+obj.icon+'"></i> ';
    txt+= obj.type;
    if (obj.judul != '') txt+= ': <small>'+obj.judul+'</small>';
    txt+= '</div><div class="menuman-item-btns">'+
        '<a href="#" class="menu-edit" title="Edit" data-toggle="tooltip"><i class="fa fa-edit"></i></a>'+
        '<a href="#" class="menu-delete" title="Hapus" data-toggle="tooltip"><i class="fa fa-trash-o"></i></a></div>';
    if ($.type(obj.children) === 'array') {
        txt+= '<ol class="dd-list">';
        $.each(obj.children, function(idx, itm){
            txt+= generateMenumanItem(itm);
        });
        txt+= '</ol>';
    }
    txt+= '</li>';
    return txt;
}
var menuitemLiToEdit = null;
var currBtnAct = null;
var sideItemFormType = 1;
var sideItemName = '';
$('#widget-items > button').attr('title', 'Klik untuk menambahkan')
.on('click', function(e){
    e.preventDefault();
    sideItemFormType = $(this).attr('data-form') || 1;
    sideItemName = $(this).attr('data-name') || '';
    var areaId = '#frontpage-form-inline-'+sideItemFormType;
    var area = $(areaId);
    area.find('input[name=judul]').val(sideItemName);
    area.find('input[name=icon]').val('');
    $.colorbox({
        href: areaId,
        inline: true,
        width: 500,
        maxWidth: '95%',
        maxHeight: '95%',
        transition: 'none',
        onComplete: function(){
            area.find(':text,select,textarea').filter(':first').focus();
        }
    });
});

/* Update Config */
$('.colorbox-form').on('click', ':submit', function(e){
    e.preventDefault();
    var area = $('#frontpage-form-inline-'+sideItemFormType);
    var vgrup = area.find('input,select,textarea').filter('[name=grup]').val() || '';
    var objItem = {
        judul: area.find('input[name=judul]').val(),
        type: sideItemName,
        icon: area.find('input,select,textarea').filter('[name=icon]').val(),
        grup: encodeURIComponent(vgrup),
        count: area.find('input,select,textarea').filter('[name=count]').val() || '0',
        form: sideItemFormType
    };
    var txt = generateMenumanItem(objItem);
    if (menuitemLiToEdit == null) {
        $('#frontpage-items > ol').append(txt);
    } else {
        menuitemLiToEdit.after(txt);
        menuitemLiToEdit.remove(); 
    }    
    $('#frontpage-items').trigger('change');   
    $.colorbox.close();    
    mnmanIsSaved = false;
    menuitemLiToEdit = null;
});

/* Simpan */
$("#btn-save-frontpage").on('click', function(e){
    e.preventDefault();
    var btn = $(this);
    var pdata = { 'items': $('#serialized-item').val() };
    btn.prop('disabled', true);
    $('.alert').remove();
    $.post(site_root+'ajax/frontpageitem/save', pdata, function(res){
        btn.prop('disabled', false);  
        if (res.ok) {
            $('.main-area').prepend('<div class="alert alert-success alert-dismissable" style="margin-bottom:10px">'+
                '<i class="glyphicon glyphicon-info-sign"></i> Pengaturan disimpan...'+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
            mnmanIsSaved = true;
        } 
    }, 'json');
});

/* Edit hapus */
$('#frontpage-items').on('click', '.menu-delete', function(e){
    e.preventDefault();
    var par = $(this).closest('#frontpage-items');
    if (mnmanItemDeleted < 2) // jika hapus lebih dari 2x, tidak perlu lagi confirm
        if ( !confirm('Hapus item ini?') ) return false;
    $(this).closest('.dd-item').remove();
    par.trigger('change');
    mnmanIsSaved = false;
    mnmanItemDeleted++;    
}).on('click', '.menu-edit', function(e){
    e.preventDefault();
    var li = $(this).closest('li');
    menuitemLiToEdit = li;
    sideItemFormType = li.attr('data-form');
    sideItemName = li.attr('data-type');
    var areaId = '#frontpage-form-inline-'+sideItemFormType;
    $(areaId).find(':text,select,textarea').each(function(){
        var nm = $(this).attr('name');
        var val = li.attr('data-'+nm);
        if (nm == 'grup')
            val = decodeURIComponent(val);
        $(this).val(val);
    });
    $.colorbox({
        href: areaId,
        inline: true,
        width: 500,
        maxWidth: '95%',
        maxHeight: '95%',
        transition: 'none',
        onOpen: function(){
            if (sideItemName == 'Banner') {
                var banid = li.attr('data-grup'); 
                var banurl = $('#tab-bann-plh select:first option[value='+banid+']').attr('data-url');
                banurl = site_root + 'uploads/banner/' + banurl; 
                $('#img-bann-fp-area').html('<span class="valign-helper"></span><img src="'+banurl+'">');
            }
        },
        onComplete: function(){
            var area = $(areaId);
            area.find(':text,select,textarea').filter(':first').focus();
        },
        onClosed: function(){
            menuitemLiToEdit = null;
        }
    });    
});

if (jQuery().ajaxForm) {
    var bannFrm = $('#bann-fp-form');
    var bannfrProgress = bannFrm.next('.progress');
    var bannfrProgressBar = bannfrProgress.find('.progress-bar');
    var bannfileField = bannFrm.find('input:file');
    bannFrm.find('.btn-uplfpban').on('click', function(e){
    	if (bannFrm.find('[name=judul]').val()=='' || bannFrm.find('[name=file]').val()=='') {
    		alert('Judul / file masih kosong!');
    	} else {
	        bannFrm.ajaxSubmit({
	            dataType: 'json',
	            beforeSubmit: function(arr, frm, options) {            	
	                bannfrProgressBar.css('width', '0'); 
	                bannfrProgress.show();
	                frm.find('input, button').prop('disabled', true);
	            },
	            uploadProgress: function(event, position, total, percentComplete) {
	                bannfrProgressBar.css('width', percentComplete+'%');
	            },
	            success: function(res, statusText, xhr, frm) {
	                bannfrProgress.hide();
	                bannfrProgressBar.css('width', '0');        
	                frm.find('input, button').prop('disabled', false);   
	                bannFrm.find('input').val('');      
	                if (res.ok) {
	                    var large_url = site_root + 'uploads/banner/' + res.img;
	                    $('#img-bann-fp-area').html('<span class="valign-helper"></span><img src="'+large_url+'">');
	                    $('#url-img-banner').val(res.id);
	                    $('#tab-bann-plh select:first option:first').after('<option data-url="'+res.img+'" data-link="" '+
	                        'data-grup="Halaman Depan" value="'+res.id+'">'+res.judul+'</option>');
	                }
	            }
	        });
       }
    });
}
$('#tab-bann-plh select:first').on('change', function() {
    if ($(this).val() == '') {
        $('#img-bann-fp-area').html('');
        $('#url-img-banner').val('');
    } else {
        var sltd = $(this).find('option:selected');
        $('#img-bann-fp-area').html('<span class="valign-helper"></span><img src="'+site_root + 'uploads/banner/'+sltd.attr('data-url')+'">');
        $('#url-img-banner').val(sltd.val());
    }
});

/* Nestable */
$('#frontpage-items').nestable({
    expandBtnHTML: '',
    collapseBtnHTML: '',
    maxDepth: 1
}).on('change', function(){
    var serz = $(this).nestable('serialize');
    $(this).next('textarea').val(JSON.stringify(serz));
    mnmanIsSaved = false;
});

window.onbeforeunload = function() {
    if (mnmanIsSaved == false)
        return 'Perubahan yang anda lakukan belum disimpan';
}