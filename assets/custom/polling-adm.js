$('#poll-opsi-area').on('click', '.btn-add-opsi', function(e){
    e.preventDefault();
    var jum = $('.poll-opsi-group').length;
    if (jum >= 10) {
        alert('Jumlah pilihan maksimal 10');
        return false;
    }
    var grup = $(this).closest('.poll-opsi-group');
    var cloned = grup.clone();
    cloned.find(':text').val('').attr('name', 'opsi_baru[]').removeClass('poll-opsi-lama');
    cloned.insertAfter(grup).find(':text').focus();
}).on('click', '.btn-del-opsi', function(e){
    e.preventDefault();
    var jum = $('.poll-opsi-group').length;
    if (jum <=1) return false;
    var grup = $(this).closest('.poll-opsi-group');
    var txt = grup.find(':text');
    if (txt.val().trim() != '') {
        if (!confirm('Hapus pilihan "'+txt.val()+'"?')) {
            return false;
        }
    }    
    if (txt.hasClass('poll-opsi-lama')) {
        var toDelOpsi = $('#opsi_to_delete');
        var valArr = toDelOpsi.val().split(',');
        valArr.push(txt.attr('data-id'));
        toDelOpsi.val(valArr.join(','));
    }   
    grup.remove();
});