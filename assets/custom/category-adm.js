var categoryForm  = $('#category-form');
var categoryTable = $('#cats-table');
var categoryFormTitle = $('#category-form-title');

function loadCategoryTable() {
    var ptype = $('#post-type').val(); 
    categoryTable.find('tbody').html('<tr><td style="text-align:center;padding:20px 0" colspan="3">'+
        '<img src="'+site_root+'assets/img/ajax-loader1.gif"></td></tr>');
    $.get(site_root+'ajax/category-table/'+ptype, function(res){
        categoryTable.find('tbody').html(res.html);
    }, 'json');   
    $.get(site_root+'ajax/loadopts/category?t='+ptype, function(res){
        $('#iparent').html(res.html);
    }, 'json');     
}
loadCategoryTable();

// Tabel
categoryTable.on('mouseenter', 'tr', function(e){
    $(this).find('.cat-actions').show();
}).on('mouseleave', 'tr', function(e){
    $(this).find('.cat-actions').hide();
}).on('click', '.btn-del-cat', function(e){
    e.preventDefault();
    if (confirm('Hapus kategori ini?')) {
        var tr = $(this).closest('tr');
        var url = $(this).attr('href');
        $.get(url, function(res){
            if (res.ok) {
                var warnaAsli = tr.css('background-color');
                tr.children('td').addClass('deleted');
                tr.animate({
                    opacity: 0.4
                    }, 400, function(){
                        $(this).children('td').removeClass('deleted');
                        $(this).css({ 'opacity': 1 });
                });
                tr.fadeOut(200, function(){ 
                    $(this).remove();
                    loadCategoryTable();
                });
                categoryForm.find(':reset').trigger('click');  
            }    
        }, 'json');
    }
}).on('click', '.btn-edit-cat', function(e){
    e.preventDefault();
    var id = $(this).attr('data-id');
    $('.category-alert').remove();
    categoryFormTitle.html('<i class="fa fa-check-square"></i> Edit Kategori');
    categoryForm.find(':submit').html('<i class="glyphicon glyphicon-ok"></i> Update Kategori');
    categoryForm.attr('action', site_root+'action/post/update-category?id='+id);
    var tr = $(this).closest('tr');
    categoryForm.find('#inama').val(tr.find('td:first > span').text());
    categoryForm.find('#idescription').val(tr.find('td:eq(1)').text());
    categoryForm.find('#iparent').val(tr.attr('data-parent'));
    $("html, body").animate({ scrollTop: 0 });
    categoryForm.find(':text:first').focus();
});

// Form
categoryForm.on('submit', function(e){
    e.preventDefault();
    var frm = $(this);
    var nmkat = frm.find('#inama');
    $('.category-alert').remove();
    if (nmkat.val().trim() == '') {
        nmkat.val('').focus();
    } else {
        var pdata = $(this).serialize();
        frm.find(':text, textarea, select, button').prop('disabled', true); 
        $.post(frm.attr('action'), pdata, function(res){
            frm.find(':text, textarea, select, button').prop('disabled', false);
            frm.before('<div class="alert category-alert alert-success alert-dismissable" style="margin-bottom:10px">'+
                '<i class="glyphicon glyphicon-info-sign"></i> '+res.msg+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
            if (res.ok) {
                frm.find(':reset').trigger('click');   
                loadCategoryTable();
            }
        }, 'json');
    }
}).on('click', ':reset', function(e){
    categoryFormTitle.html(categoryFormTitle.data('html'));
    categoryForm.find(':submit').html(categoryForm.find(':submit').data('html'));
    categoryForm.attr('action', categoryForm.data('action'));   
    categoryForm.find(':text:first').focus(); 
});

// Save default properties
categoryFormTitle.data('html', categoryFormTitle.html());
categoryForm.find(':submit').data('html', categoryForm.find(':submit').html());
categoryForm.data('action', categoryForm.attr('action'));