var mnmanIsSaved = true;
var mnmanItemDeleted = 0;

function generateMenumanItem(obj) {
    var txt = '<li class="dd-item" data-label="'+obj.label+'" data-type="'+obj.type+'" data-icon="'+obj.icon+'" data-url="'+obj.url+'" data-newtab="'+obj.newtab+'" data-classes="'+obj.classes+'" data-title="'+obj.title+'"><div class="dd-handle">';
    if (obj.icon != '') txt+= '<i class="glyphicon glyphicon-'+obj.icon+'"></i> ';
    txt+= obj.label+' <small>('+obj.type+')</small></div><div class="menuman-item-btns">'+
        '<a href="#" class="menu-edit" title="Edit" data-toggle="tooltip"><i class="fa fa-edit"></i></a>'+
        '<a href="#" class="menu-delete" title="Hapus" data-toggle="tooltip"><i class="fa fa-trash-o"></i></a></div>';
    if ($.type(obj.children) === 'array' && obj.children.length > 0) {
        txt+= '<ol class="dd-list">';
        $.each(obj.children, function(idx, itm){
            txt+= generateMenumanItem(itm);
        });
        txt+= '</ol>';
    }
    txt+= '</li>';
    return txt;
}

// dropdown daftar menu
$('#menu-id').on('change', function(e){
    var val = $(this).val();
    $('.menuact-btn').prop('disabled', true);
    $('.menuman-msg').hide();
    $('.menuman-loader').show();
    if (val == '') {
        $('.menuman-msg').show();
        $('.menuman-loader').hide();
    } else {
        $('#menuman-item > ol:first').html('');
        $.get(site_root+'ajax/menuman/load?id='+val, function(res){
            $('.menuact-btn').prop('disabled', false);
            $('.menuman-loader').hide();
            $('#menuman-item').show();
            if (val == 1) // Menu utama tidak boleh dihapus
                $('.delmenu-btn, .editmenu-btn').prop('disabled', true);
            if (res.ok && $.type(res.val) === 'array') { 
                var txt = ''; 
                $.each(res.val, function(idx, item){ 
                    txt+= generateMenumanItem(item);    
                });
                $('#menuman-item > ol:first').html(txt);                
            } 
            $('#menuman-item').trigger('change'); 
            if (history.pushState) {
			  window.history.pushState('', '', addParameter(location.href, 'id', val));
			}
        }, 'json');        
    }
}).trigger('change');

// Tombol tambah menu baru
$('.addmenu-btn').on('click', function(e){
    e.preventDefault();
    var frm = $('#menu-form-inline form');
    frm.find('[name=nm_menu]').val('');
    frm.find('[name=menu_id]').val('0');
    frm.find('[name=action]').val('insert');
    frm.find(':submit').text('Tambahkan Menu');
    $.colorbox({
        inline: true,
        href: '#menu-form-inline',
        width: 500,
        transition: 'none',
        title: '&nbsp; Tambah Menu ',
        maxWidth: '95%',
        maxHeight: '95%',
        onComplete: function(){ frm.find(':text:first').focus(); }
    });
});

// Form tambah/edit nama menu
$('#menu-form-inline').on('submit', 'form', function(e){
    e.preventDefault();
    var frm = $(this);
    var fnm = frm.find(':text');
    if (fnm.val().trim() == '') {
        fnm.focus();
        return false;
    }
    var selmenu = $('select#menu-id');
    var pdata = $(this).serialize();
    var actn = $(this).attr('action');
    frm.find('input, select, button').prop('disabled', true);
    selmenu.prop('disabled', true);
    $('.addmenu-btn').prop('disabled', true);
    $.post(actn, pdata, function(res){
        frm.find('input, select, button').prop('disabled', false);
        selmenu.prop('disabled', false);
        $('.addmenu-btn').prop('disabled', false);
        if (res.ok) {
            if (res.action == 'insert') {
                selmenu.append('<option value="'+res.id+'">'+res.val+'</option>').val(res.id).trigger('change');
            } else {
                selmenu.find('option:selected').text(res.val);
            }
            $.colorbox.close();
        } else {
            alert(res.msg);
        }            
    }, 'json');
});

// Tombol hapus, edit dan simpan menu
$('.menubtn-area').on('click', '.delmenu-btn', function(e){
    e.preventDefault();
    var selmenu = $('#menu-id');
    var idmenu = selmenu.val();
    var nmmenu = selmenu.find('option:selected').text();
    if (confirm('Hapus menu "'+nmmenu+'"?')) {
        $.get(site_root+'ajax/menuman/delete?id='+idmenu, function(res){
            if (res.ok) {
                selmenu.find('option:selected').remove();
                selmenu.trigger('change');
            }
        }, 'json');        
    }
}).on('click', '.editmenu-btn', function(e){  // ubah nama menu
    e.preventDefault();
    var selmenu = $('#menu-id');
    var idmenu = selmenu.val();
    var nmmenu = selmenu.find('option:selected').text();
    var frm = $('#menu-form-inline form');
    frm.find('[name=nm_menu]').val(nmmenu);
    frm.find('[name=menu_id]').val(idmenu);
    frm.find('[name=action]').val('update');
    frm.find(':submit').text('Update Menu');
    $.colorbox({
        inline: true,
        href: '#menu-form-inline',
        width: 500,
        transition: 'none',
        title: '&nbsp; Edit Nama Menu ',
        maxWidth: '95%',
        maxHeight: '95%',
        onComplete: function(){ frm.find(':text:first').focus(); }
    });    
}).on('click', '.savemenu-btn', function(e){ // Simpan pengaturan menu
    e.preventDefault();
    var btn = $(this);
    var mnid = $('#menu-id').val();
    var mnitm = $('#serialized-menu').val();
    if (mnid != '') {
        btn.prop('disabled', true);
        $.post(site_root+'ajax/menuman/save-item', {'menuid':mnid, 'menuitem':mnitm}, function(res){
            btn.prop('disabled', false);
            if (res.ok) {
                $('<div class="alert alert-success alert-dismissable menuman-saved-msg" style="margin-bottom:10px;display:none"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="glyphicon glyphicon-info-sign"></i> Pengaturan menu disimpan..</div>')
                    .prependTo('#menuman-item-area').slideDown(200).delay(5000)
                    .slideUp(200, function(){ $(this).remove(); });
                mnmanIsSaved = true;
            }
        }, 'json');
    }
});

var menuitemLiToEdit = null;
// sortable
$('#menuman-item').nestable({
    expandBtnHTML: '',
    collapseBtnHTML: '',
    maxDepth: 4
}).on('change', function(){
    var serz = $(this).nestable('serialize');
    $('#serialized-menu').val(JSON.stringify(serz));
}).on('click', '.menu-delete', function(e){
    e.preventDefault();
    if (mnmanItemDeleted < 2) // jika hapus lebih dari 2x, tidak perlu lagi confirm
        if ( !confirm('Hapus item ini?') ) return false;
    $(this).closest('.dd-item').remove();
    $('#menuman-item').trigger('change');
    mnmanIsSaved = false;
    mnmanItemDeleted++;
    
}).on('click', '.menu-edit', function(e){
    e.preventDefault();
    var frm = $('#menuitem-form-inline');
    var li = $(this).closest('li');
    menuitemLiToEdit = li;
    frm.find('[name=label]').val( li.attr('data-label') );
    frm.find('[name=url]').val( li.attr('data-url') );
    frm.find('[name=title]').val( li.attr('data-title') );
    frm.find('[name=icon]').val( li.attr('data-icon') );
    frm.find('[name=classes]').val( li.attr('data-classes') );
    frm.find(':checkbox[name=newtab]').prop('checked', (li.attr('data-newtab') == 'yes'));
    $.colorbox({
        inline: true,
        href: frm,
        width: 500,
        transition: 'none',
        title: '&nbsp; Edit Menu Item ',
        maxWidth: '95%',
        maxHeight: '95%',
        onComplete: function(){ frm.find(':text:first').focus(); }
    });
});

function generateDDItemObj(parentElm) {
	var nChild = [];
	parentElm.children('ol').children('li').each(function(){ 
		var cObj = {
    		label: $(this).attr('data-label'),
    		type: $(this).attr('data-type'),
    		icon: $(this).attr('data-icon'),
    		title: $(this).attr('data-title'),
    		url: $(this).attr('data-url'),
    		classes: $(this).attr('data-classes'),
    		newtab: $(this).attr('data-newtab'),
    		children:[] };
    	if ($(this).children('ol').length) {
    		var subChild = generateDDItemObj($(this));	
    		cObj.children = subChild;
    	}
        nChild.push(cObj);	
	});
	return nChild;
}

// Update menu item
$('#update-menuitem-btn').on('click', function(e){
    e.preventDefault();
    var frm = $('#menuitem-form-inline');
    if (menuitemLiToEdit != null) {
        var nLabel = frm.find('[name=label]').val();
        var nType = menuitemLiToEdit.attr('data-type');
        var nIcon = frm.find('[name=icon]').val();
        var ntab = frm.find(':checkbox[name=newtab]').prop('checked') == true ? 'yes':'no';     
        var nObj = {
            label: nLabel,
            type: nType,
            icon: nIcon,
            title: frm.find('[name=title]').val(),
            url: frm.find('[name=url]').val(),
            classes: frm.find('[name=classes]').val(),
            newtab: ntab,
            children: generateDDItemObj(menuitemLiToEdit)
        }; 
        var txt = generateMenumanItem(nObj);
        menuitemLiToEdit.after(txt);
        menuitemLiToEdit.remove();        
        mnmanIsSaved = false;
        $('#menuman-item').trigger('change');
    }
    $.colorbox.close();
});

// Tombol tambah ke menu
$('.btn-addtomenu').on('click', function(e){
    e.preventDefault(); 
    if ( $('#menu-id').val() == '' ) {
        alert('Menu belum dipilih!');
        return false;
    }
    var areaId = $(this).attr('data-rel');  
    $(areaId).find(':checkbox:checked').each(function(){
        var objItem = {
            label: $(this).attr('data-label'),
            type: $(this).attr('data-type'),
            icon: $(this).attr('data-glyphicon'),
            url: $(this).val(),
            title: '',
            classes: '',
            newtab: 'no'
        };
        var txt = generateMenumanItem(objItem);
        $('#menuman-item > ol:first').append(txt);
        $(this).prop('checked', false);
    });  
    mnmanIsSaved = false;
    $('#menuman-item').trigger('change');
});
// tombol tambahkan ke menu khusus link
$('.btn-addtomenu-link').on('click', function(e){
    e.preventDefault();
    var area = $('#menutype-link');
    var linkTxt = area.find('input[name=link]').val().trim();
    var labelTxt = area.find('input[name=label]').val().trim();
    if (linkTxt == '')
        area.find(':text[name=link]').focus(); else
    if (labelTxt == '')
        area.find(':text[name=label]').focus(); 
    else {
        var objItem = {
            label: labelTxt,
            type: 'Link',
            icon: '',
            url: linkTxt,
            title: '',
            classes: '',
            newtab: 'no'
        };
        var txt = generateMenumanItem(objItem);
        area.find(':text').val('');
        $('#menuman-item > ol:first').append(txt);
        $('#menuman-item').trigger('change');
    }
});

window.onbeforeunload = function() {
    if (mnmanIsSaved == false)
        return 'Perubahan yang anda lakukan belum disimpan';
}