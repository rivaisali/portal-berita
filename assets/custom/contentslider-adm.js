if (jQuery().cropit) {

    var imgCropper = $('#image-cropper'),
        browseClicked = false;

    imgCropper.cropit({
        smallImage: 'stretch', 
        onImageLoaded: function() {
            imgCropper.find('.cropit-preview').css('cursor', 'move');
            if (browseClicked)
                $('.main-form').find('[name=newimg]').val('yes');
        }
    });

    var prevImgSrc = $('.main-form').find('[name=prev_imgsrc]:first').val();
    if ( prevImgSrc != '' ){
        imgCropper.cropit('imageSrc', site_root+'uploads/content-slider/'+prevImgSrc);
    }

    $('.cropit-select-btn').on('click', function() {
        imgCropper.find('.cropit-image-input').trigger('click');
        browseClicked = true;
    });

    $('.main-form').dsAjaxForm({
        resultDiv : '#post-result',
        clearOnOk : $('.main-form:first').find('[name=id]:first').val() == '',
        beforeSerialize : function(frm) {
            var imgWidth = imgCropper.cropit('previewSize').width;
            if (imgWidth < 1170) {
                var expZoom = 1170 / imgWidth;
                imgCropper.cropit('exportZoom', expZoom);
            }
            var imageData = imgCropper.cropit('export', {
                type: 'image/jpeg',
                quality: .9,
                originalSize: false
            });
            $('[name=foto_dataurl]', frm).val(imageData);
        },
        onOk : function(res, frm) { 
            if (res.type == 'add') {
                imgCropper.find('.cropit-preview-image').attr('src', '');
                // imgCropper.cropit('reenable');
                imgCropper.cropit('zoom', 1);
                frm.find('textarea, :text').val('');
            }
            browseClicked = false;
            $('.main-form').find('[name=newimg]').val('');
            $('html, body').animate({scrollTop: 0 }, 200);
        }
    });
}

if (jQuery().sortable) {

    var sortableWrap = $('#contentslider-sortable');
    var sortableSaveBtn = $('#btn-savesort');

    sortableWrap.sortable({
        // placeholder : 'sortable-placeholder col-sm-6 col-md-4',
        update : function(e, ui) {
            $('#btn-savesort').show();
        }
    }).on('click', '.btn-delete', function(e) {
        e.preventDefault();
        if (!confirm('Hapus data ini?')) return false;
        var btn = $(this),
            item = $(this).closest('.sortable-item'),
            itemId = item.attr('data-id'),
            itemFn = item.attr('data-filename');
        $.post(site_root+'ajax/contentslider/delete', { id:itemId,fn:itemFn }, function(res) {
            if (res.ok) {
                btn.closest('.sortable-item').remove();
            }
        }, 'json');
    });

    sortableSaveBtn.on('click', function(e) {
        var pdata = { items : [] };
        $('.sortable-item[data-id]', sortableWrap).each(function() {
            pdata.items.push( parseInt($(this).attr('data-id')) );
        });
        console.log(pdata);
        $.post(site_root+'ajax/contentslider/sort', pdata, function(res) {
            alert(res.msg);
            sortableSaveBtn.hide();
            if (res.ok) {
                $('html, body').animate({scrollTop: 0 }, 200);
            }
        }, 'json');
    });

}
