$('.colorbox').colorbox({ maxWidth:'90%', maxHeight:'90%', rel:'colorbox' });

$('.header-img-others:first').on('click', '.btn-use-headerimg', function(e){
    e.preventDefault();
    if (!confirm('Gunakan gambar ini sebagai header?')) return false;
    var btn = $(this);
    var itm = btn.closest('.item');
    var img = itm.attr('data-img');
    var thumb = itm.attr('data-thumb');
    btn.prop('disabled', true);   
    $.post(site_root+'ajax/headerimg/use', { 'img':img }, function(res){
        btn.prop('disabled', false); 
        if (res.ok) {
            var usdimg = $('#used-headerimg');
            itm.attr({
                'data-img': usdimg.attr('data-img'),
                'data-thumb': usdimg.attr('data-thumb')
            });
            itm.find('a:first').attr('href', site_root+'uploads/header-img/'+usdimg.attr('data-img'))
             .find('img:first').attr('src', site_root+'uploads/header-img/'+usdimg.attr('data-thumb'));
            usdimg.attr({
                'src': site_root+'uploads/header-img/'+img,
                'data-img': img,
                'data-thumb': thumb
            });            
        }
    }, 'json');     
}).on('click', '.btn-del-headerimg', function(e){
    e.preventDefault();
    if (!confirm('Hapus gambar ini?')) return false;
    var btn = $(this);
    var itm = btn.closest('.item');
    var img = itm.attr('data-img');
    btn.prop('disabled', true);
    $.post(site_root+'ajax/headerimg/delete', { 'img':img }, function(res){
        btn.prop('disabled', false); 
        if (res.ok) {
            itm.fadeOut(200, function(){
                $(this).remove();
            });                   
        }
    }, 'json');  
});

var frProgress = $('#headerimg-form .progress');
var frProgressBar = $('#headerimg-form .progress-bar');
var fileField = $('#headerimg-form input:file');
fileField.on('change', function(){
    $('#headerimg-form > form').ajaxSubmit({
        dataType: 'json',
        beforeSubmit: function(arr, frm, options) {
            frProgressBar.css('width', '0');
            frProgress.show();
            frm.find('input, button').prop('disabled', true);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            frProgressBar.css('width', percentComplete+'%');
        },
        success: function(res, statusText, xhr, frm) {
            frProgress.hide();
            frProgressBar.css('width', '0');        
            frm.find('input, button').prop('disabled', false);   
            fileField.val('');      
            var large_url = site_root + 'uploads/header-img/' + res.img;
            var thumb_url = site_root + 'uploads/header-img/' + res.thumb;
            var txt = '<div class="item" data-img="'+res.img+'" data-thumb="'+res.thumb+'">'+
                '<a href="'+large_url+'" class="colorbox"><img title="Klik untuk preview" src="'+thumb_url+'"></a>'+
                '<button type="button" class="btn btn-xs btn-success btn-use-headerimg">'+
                '<i class="glyphicon glyphicon-ok"></i> Gunakan</button> '+
                '<button type="button" class="btn btn-xs btn-danger btn-del-headerimg">'+
                '<i class="glyphicon glyphicon-trash"></i> Hapus</button></div>';
            $('.header-img-others:first').append(txt);    
            $('.header-img-others .colorbox').colorbox({ maxWidth:'90%', maxHeight:'90%', rel:'colorbox' });
        }
    });
});