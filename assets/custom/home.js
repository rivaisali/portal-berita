(function($) {  
    if (jQuery().owlCarousel) {       
       $('.content-slider').owlCarousel({ 
            navigation : false, 
            autoPlay: 6000,
    		stopOnHover: true,
            lazyLoad : true,
            slideSpeed: 700,
            pagination: true,
            paginationSpeed : 400,
            singleItem: true,
            responsiveRefreshRate: 500,
            transitionStyle: 'fadeUp',
            afterInit: function(elm) {
                elm.find('.item').show();
            }
        });
        $('.photo-slider > .slider').owlCarousel({ 
            items: 3,
            navigation : false, 
            stopOnHover: true,
            lazyLoad : true,
            slideSpeed: 700,
            pagination: true,
            paginationSpeed : 400,
            autoPlay: 5000,
            afterInit: function(elm) {
                elm.find('.item').show();
            }
        }).on('mouseenter', '.item', function(){
            $(this).find('.img-title').slideDown(100);
        }).on('mouseleave', '.item', function(){
            $(this).find('.img-title').slideUp(100);
        });
    }    
})(jQuery);
