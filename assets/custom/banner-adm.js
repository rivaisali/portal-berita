var frBanner = $('#banner-form-inline form');
var frProgress = $('#banner-form-inline .progress');
var frProgressBar = $('#banner-form-inline .progress-bar');
var adaYgDiubah = false;

$('.add-banner').on('click', function(e){
    e.preventDefault();
    frBanner.find('input, select').val('');
    frBanner.find('#iaction').val('insert');
    frBanner.find('#iban_id').val('0');
    frBanner.find(':submit').text('Tambahkan Banner');
    frProgressBar.css('width', '0');
    frBanner.find('input:file').show();
    adaYgDiubah = false;
    $.colorbox({
        inline: true,
        href: '#banner-form-inline',
        width: 500,
        title: '&nbsp; Tambah Banner ',
        maxWidth: '98%',
        maxHeight: '95%',
        onComplete: function(){ frBanner.find('input:first').focus(); },
        onClosed: function(){ 
            if (adaYgDiubah)
                $('#grup_filter').trigger('change') 
        }
    });
});
frBanner.ajaxForm({
    dataType: 'json',
    beforeSubmit: function(arr, frm, options) {
        if (frBanner.find('input:file').val()=='' &&
            frBanner.find('#iaction').val()=='insert') {
            alert('File belum dipilih!');
            return false;
        }
        frProgressBar.css('width', '0');
        frProgress.css('visibility', 'visible');
        frBanner.find('input, select, button').prop('disabled', true);
    },
    uploadProgress: function(event, position, total, percentComplete) {
        frProgressBar.css('width', percentComplete+'%');
    },
    success: function(res, statusText, xhr, frm) {
        adaYgDiubah = true;
        frProgress.css('visibility', 'hidden');
        frProgressBar.css('width', '0');        
        frBanner.find('input, select, button').prop('disabled', false);               
        alert(res.msg);
        if (frBanner.find('#iaction').val()=='update') 
            $.colorbox.close(); else frBanner.find(':text, :file, select').val(''); 
    }
});
$('select#banner_grup').on('change', function(e){
    e.preventDefault();
    var sel = $(this);
    var val = $(this).val();
    if (val == '-[add_new_banner]-') {
        var newgroup = prompt('Buat Grup Banner Baru', '', 'Buat Grup');
        if (newgroup) {
            var addOpt = sel.find('option:last');
            var newOpt = '<option value=\"'+newgroup+'\">'+newgroup+'</option>';
            $(newOpt).insertBefore(addOpt);
            $('#grup_filter').append(newOpt);
            sel.val(newgroup);
        } else {
            sel.val('');
        }
    }
});
$('#grup_filter').on('change', function(e){
    var grup = $(this).val().replace(/ /g, '+');
    $('#banner-area').html('<div class=\"well\"><img src=\"'+site_root+'assets/img/loader-31-black.gif\"></div>');
    $.get(site_root+'ajax/load-banner/'+grup, function(res){
        $('#banner-area').html(res.html);
        if (typeof(history.pushState) !== 'undefined') {
            var url =  grup != '' ? '?grup=' + grup : '?';
            history.pushState({ url: url }, 'Banner', url);
        }
    }, 'json');
}).trigger('change');
$('#banner-area').on('mouseenter', '.banner-list > li', function(){
   $(this).find('.banner-btns').show();
}).on('mouseleave', '.banner-list > li', function(){
   $(this).find('.banner-btns').hide();
}).on('click', '.btn-del-banner', function(e){
    e.preventDefault();
    var ban_id = $(this).attr('data-id');
    var url = $(this).attr('data-url');
    var li = $(this).closest('li');
    var pdata = { 'ban_id':ban_id, 'url':url };
    $.post(site_root+'ajax/deletebanner', pdata, function(res){
        if (res.ok) {
            li.fadeOut(100, function(){ $(this).remove(); });
        }
    }, 'json');
}).on('click', '.btn-edit-banner', function(e){
    e.preventDefault();
    var ban_id = $(this).attr('data-id');
    var grup = $(this).attr('data-grup');
    var judul = $(this).attr('data-judul');
    var link = $(this).attr('data-link');
    var li = $(this).closest('li');
    frBanner.find('#ijudul').val(judul);
    frBanner.find('#banner_grup').val(grup);
    frBanner.find('#ilink').val(link);
    frBanner.find('#iaction').val('update');
    frBanner.find('#iban_id').val(ban_id);
    frBanner.find('input:file').hide();
    frBanner.find(':submit').text('Update Banner');
    adaYgDiubah = false;
    $.colorbox({
        inline: true,
        href: '#banner-form-inline',
        width: 500,
        title: '&nbsp; Edit Banner',
        maxWidth: '98%',
        maxHeight: '95%',
        onComplete: function(){ frBanner.find('input:first').focus(); },
        onClosed: function(){ 
            if (adaYgDiubah)
                $('#grup_filter').trigger('change') 
        }
    });
});