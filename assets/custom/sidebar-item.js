var mnmanIsSaved = true;
var mnmanItemDeleted = 0;

function generateMenumanItem(obj) {
    obj.count = !obj.count ? '' : obj.count;
    obj.grup = !obj.grup ? '' : obj.grup;
    obj.icon = !obj.icon ? '' : obj.icon;
    obj.form = !obj.form ? '1' : obj.form;
    var txt = '<li class="dd-item" data-judul="'+obj.judul+'" data-type="'+obj.type+'" data-icon="'+obj.icon+'" '+
        'data-count="'+obj.count+'" data-grup="'+obj.grup+'" data-form="'+obj.form+'"><div class="dd-handle">';
    if (obj.icon != '') txt+= '<i class="glyphicon glyphicon-'+obj.icon+'"></i> ';
    txt+= obj.type;
    if (obj.judul != '') txt+= ': <small>'+obj.judul+'</small>';
    txt+= '</div><div class="menuman-item-btns">'+
        '<a href="#" class="menu-edit" title="Edit" data-toggle="tooltip"><i class="fa fa-edit"></i></a>'+
        '<a href="#" class="menu-delete" title="Hapus" data-toggle="tooltip"><i class="fa fa-trash-o"></i></a></div>';
    if ($.type(obj.children) === 'array') {
        txt+= '<ol class="dd-list">';
        $.each(obj.children, function(idx, itm){
            txt+= generateMenumanItem(itm);
        });
        txt+= '</ol>';
    }
    txt+= '</li>';
    return txt;
}

var currBtnAct = null;
var sideItemFormType = 1;
var sideItemName = '';
$('#widget-items > button').attr('title', 'Klik untuk menambahkan')
.on('click', function(e){
    e.preventDefault();
    currBtnAct = $(this);
    if ( $(this).hasClass('btn-default')) {
        $(this).removeClass('btn-default').addClass('btn-success').siblings().prop('disabled', true);
        $(this).after('<div style="margin:10px 0 20px">'+
            // '<label>Tambahkan ke :</label><br />'+
            // '<button type="button" class="btn btn-info btn-sm addto-btn" data-des="left">Kiri</button> '+ 
            '<button type="button" class="btn btn-info btn-sm addto-btn" data-des="right">Tambahkan ke Sidebar</button> '+ 
            '<button type="button" class="btn btn-danger btn-sm addto-reset">Batal</button></div>');
        sideItemFormType = $(this).attr('data-form') || 1;
        sideItemName = $(this).attr('data-name') || '';
    } else {
        $(this).removeClass('btn-success').addClass('btn-default').siblings().prop('disabled', false);
        $(this).next('div').remove();
    }
});

var sideItemDes = 'left';
$('#widget-items').on('click', '.addto-reset', function(e){
    e.preventDefault();
    $(this).parent().prev('button').trigger('click');
}).on('click', '.addto-btn', function(e){
    e.preventDefault();
    sideItemDes = $(this).attr('data-des') || 'left';
    var areaId = '#sideitem-form-inline-'+sideItemFormType;
    var area = $(areaId);
    area.find('input[name=judul]').val(sideItemName);
    area.find('input[name=icon]').val('');
    $.colorbox({
        href: areaId,
        inline: true,
        width: 500,
        maxWidth: '95%',
        maxHeight: '95%',
        transition: 'none',
        onComplete: function(){
            area.find(':text,select,textarea').filter(':first').focus();
        }
    });
});

var menuitemLiToEdit = null;
$('.side-items').on('click', '.menu-delete', function(e){
    e.preventDefault();
    var par = $(this).closest('.side-items');
    if (mnmanItemDeleted < 2) // jika hapus lebih dari 2x, tidak perlu lagi confirm
        if ( !confirm('Hapus item ini?') ) return false;
    $(this).closest('.dd-item').remove();
    par.trigger('change');
    mnmanIsSaved = false;
    mnmanItemDeleted++;    
}).on('click', '.menu-edit', function(e){
    e.preventDefault();
    var li = $(this).closest('li');
    menuitemLiToEdit = li;
    sideItemFormType = li.attr('data-form');
    sideItemName = li.attr('data-type');
    var areaId = '#sideitem-form-inline-'+sideItemFormType;
    $(areaId).find(':text,select,textarea').each(function(){
        var nm = $(this).attr('name');
        var val = li.attr('data-'+nm);
        if (nm == 'grup')
            val = decodeURIComponent(val);
        $(this).val(val);
    });
    $.colorbox({
        href: areaId,
        inline: true,
        width: 500,
        maxWidth: '95%',
        maxHeight: '95%',
        transition: 'none',
        onComplete: function(){
            var area = $(areaId);
            area.find(':text,select,textarea').filter(':first').focus();
        },
        onClosed: function(){
            menuitemLiToEdit = null;
        }
    });    
});

/* Update Config */
$('.colorbox-form').on('click', ':submit', function(e){
    e.preventDefault();
    var area = $('#sideitem-form-inline-'+sideItemFormType);
    var vgrup = area.find('input,select,textarea').filter('[name=grup]').val() || '';
    var objItem = {
        judul: area.find(':text[name=judul]').val(),
        type: sideItemName,
        icon: area.find(':text[name=icon]').val(),
        grup: encodeURIComponent(vgrup),
        count: area.find('input,select,textarea').filter('[name=count]').val() || '0',
        form: sideItemFormType
    };
    var txt = generateMenumanItem(objItem);
    if (menuitemLiToEdit == null) {
        $('#side-items-'+sideItemDes+' > ol').append(txt);
        currBtnAct.trigger('click');
    } else {
        menuitemLiToEdit.after(txt);
        menuitemLiToEdit.remove(); 
    }    
    $('.side-items').trigger('change');   
    $.colorbox.close();    
    mnmanIsSaved = false;
    menuitemLiToEdit = null;
});

/* Simpan */
$("#btn-save-sideitems").on('click', function(e){
    e.preventDefault();
    var btn = $(this);
    var pdata = {
        'left': $('#serialized-item-left').val(),
        'right': $('#serialized-item-right').val()
    };
    btn.prop('disabled', true);
    $('.alert').remove();
    $.post(site_root+'ajax/sidebaritem/save', pdata, function(res){
        btn.prop('disabled', false);  
        if (res.ok) {
            $('.main-area').prepend('<div class="alert alert-success alert-dismissable" style="margin-bottom:10px">'+
                '<i class="glyphicon glyphicon-info-sign"></i> Pengaturan disimpan...'+
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
            mnmanIsSaved = true;
        } 
    }, 'json');
});

/* Nestable */
$('.side-items').nestable({
    expandBtnHTML: '',
    collapseBtnHTML: '',
    maxDepth: 1
}).on('change', function(){
    var serz = $(this).nestable('serialize');
    $(this).next('textarea').val(JSON.stringify(serz));
    mnmanIsSaved = false;
});

window.onbeforeunload = function() {
    if (mnmanIsSaved == false)
        return 'Perubahan yang anda lakukan belum disimpan';
}