$(function(){
    var postElms = { 
        // Edit slug area ----------
        'editSlugArea': $('#post-slug-edit-area'),
        'editSlug': $('#btn-edit-slug'), // tombol edit slug
        'cancelEditSlug': $('#btn-cancel-edit-slug'), // tombol batal edit slug
        'slugText': $('#slug-text'), // text slug yg tampil
        'slugVal': $('#post-slug'), // editbox edit slug    
        // Box pilih kategori ------
        'addCat': $('#btn-add-cat'), // tombol tambah kategori baru  
        'cancelAddCat': $('#btn-cancel-add-cat'), // tombol batal tambah kategori
        'newCatArea': $('#new-cat-area'), // area field untuk tambah kategori
        'newCatVal': $("#new-cat"), // editbox kategori baru
        'newCatParentVal': $('#new-cat-parent'), // dropdown pilih kategori induk        
        'catCheckArea': $('.cat-checkbox-area'), // area heckbox pilih kategori
        // Box tambah tags -------
        'addTag': $('#btn-add-tags'), // tombol tambah tags
        'newTagVal': $('#new-tags'), // edit box tags baru
        'tagsVal': $('#post-tags'), // value semua tags
        'tagsArea': $('.tags-area'), // area daftar tags
        'newTagArea': $('#new-tags-area'), // area form tags baru
        // Box post menu -----
        'editPostDate': $('#btn-edit-post-date'), // tombol edit tanggal post
        'postDateArea': $('#post-date-area'),
        'postDateVal': $('#post-date'),
        'postDateVal1': $('#post-date1'),
        'postDateText': $('#post-date-text'),
        'postDateEdit': $('#post-date-edit'), // datetime picker
        'postDateEdit1': $('#post-date-edit1'), // datetime picker
        'publishPost': $('#btn-publish-post'),
        'savePost': $('#btn-save-post'),
        'previewPost': $('#btn-preview-post'),
        'deletePost': $('#btn-delete-post'),
        // Post content field ---
        'postForm': $('#post-form'),
        'postTitleVal': $('#post-title'), // judul post
        'postTypeVal': $('#post-type'), // input hidden post type
        'postStatusVal': $('#post-status'),
        'postIdVal': $('#post-id'),
        'postTable': $('#post-table')
    };    
    // tombol tambah kategori baru  
    postElms.addCat.on('click', function(e){
        e.preventDefault();
        if (postElms.newCatArea.is(':hidden')) {
            postElms.cancelAddCat.show();
            postElms.newCatArea.show(100, function(){
                $(this).find('input, select, button').prop('disabled', false);
                $(this).find(':text:first').focus();            
            });
        } else {
            var val = postElms.newCatVal.val().trim(); 
            if (val == '') {
                postElms.newCatVal.val('').focus();
            } else {                
                var pdata = {
                    'cat_name': val,
                    'cat_parent': postElms.newCatParentVal.val(),
                    'post_type': postElms.postTypeVal.val()     
                };
                postElms.newCatArea.find('input, select, button').prop('disabled', true); 
                $.post(site_root + 'action/post/add-category', pdata, function(res){ 
                    postElms.newCatArea.find('input, select, button').prop('disabled', false); 
                    if (res.ok) {                         
                        // Tambah item baru checkbox pilih kategori ------------
                        var chkVal = postElms.catCheckArea.find(':checkbox[value="'+res.id+'"]');
                        if (!chkVal.length) {
                            var chkPar = postElms.catCheckArea.find(':checkbox[value="'+res.code+'"]'); // cek parent
                            if (chkPar.length) {
                                var cloned = chkPar.closest('.checkbox').clone();
                                cloned.prepend('&nbsp;&nbsp;&nbsp;&nbsp;')
                                    .find(':checkbox').val(res.id).prop('checked', true).next('span')
                                    .text(res.val); 
                                cloned.insertAfter(chkPar.closest('.checkbox'));
                            } else {
                                var lastChk = postElms.catCheckArea.find('.checkbox:last');
                                var cloned  = lastChk.clone()
                                cloned.find(':checkbox').val(res.id).prop('checked', true).next('span').text(res.val); 
                                cloned.insertAfter(lastChk);
                            }                            
                        } else {
                            chkVal.prop('checked', true);
                        } 
                        // Tambah item baru dropdown kategori induk -----------
                        var slctVal = postElms.newCatParentVal.find('option[value="'+res.id+'"]'); 
                        if (!slctVal.length) {
                            var chkPar2 = postElms.newCatParentVal.find('option[value="'+res.code+'"]'); // cek parent
                            if (chkPar2.length) {
                                var cloned2 = chkPar2.clone();
                                cloned2.text(res.val).prepend('&nbsp;&nbsp;&nbsp;&nbsp;').val(res.id); 
                                cloned2.insertAfter(chkPar2);
                            } else {
                                var lastChk2 = postElms.newCatParentVal.find('option:last');
                                var cloned2  = lastChk2.clone()
                                cloned2.val(res.id).text(res.val); 
                                cloned2.insertAfter(lastChk2);
                            }  
                        } 
                        postElms.newCatArea.find('input').val('');
                        postElms.newCatArea.find('select').val('0');
                    }
                }, 'json');
            }
        }
    });
     // tombol batal tambah kategori
    postElms.cancelAddCat.on('click', function(e){
        e.preventDefault();
        postElms.cancelAddCat.hide();
        postElms.newCatArea.hide(100);
    });    
    // isi field post-tags ex: didin,sino
    function fillPostTagsInput() {
        var arr = postElms.tagsArea.find('.new-post-tag').map(function(){
                     return $(this).text();
                  }).get();
        postElms.tagsVal.val(arr.join(','));
    }
    // Tombol tambah tag
    postElms.addTag.on('click', function(e){
        e.preventDefault(); 
        var vals = postElms.newTagVal.val().trim();
        if (vals != '') { 
            $.each(vals.split(','), function(idx, tag){
               tag = tag.trim();
               if (postElms.tagsArea.find('.new-post-tag').filter(function() {
                  return $(this).text().toLowerCase() == tag.toLowerCase();
                }).length) return;
               postElms.tagsArea.append('<span class="label label-default new-post-tag" title="Klik untuk menghapus">'+tag+'</span> '); 
            });    
        }
        fillPostTagsInput();
        postElms.newTagVal.val('').focus();  
    });
    postElms.newTagVal.on('keypress', function(e){ 
       if (e.keyCode == 13) {
            e.preventDefault();
            postElms.addTag.trigger('click');
       }
    });
    // Hapus tag
    postElms.tagsArea.on('click', '.new-post-tag', function(e){
        e.preventDefault();
        $(this).fadeOut(150, function(){
            $(this).remove();
            fillPostTagsInput();
        });
    });
    // tombol edit post date
    postElms.editPostDate.on('click', function(e){
        e.preventDefault(); 
        if ($(this).text().toLowerCase() == 'edit') {
            postElms.postDateArea.fadeIn(100);
            $(this).text('Reset');
        } else {
            postElms.postDateArea.fadeOut(100);
            $(this).text('Edit');
            postElms.postDateVal.val( $('#default-post-date').val() );
            postElms.postDateText.text( $('#default-post-date-text').val() );
        }
    });    
    // Datetimepicker di post menu box
    if (jQuery().datetimepicker) {
        postElms.postDateEdit.datetimepicker({ 
            className:'datetimepicker', 
            format: 'j M Y - H:i', 
            yearStart: 2000, 
            yearEnd: 2040, 
            step: 30,
            onChangeDateTime: function(ct) { 
                var xDate  = ('0' + ct.getDate()).slice(-2);
                var xMonth = ('0' + (ct.getMonth()+1)).slice(-2);
                var xHours = ('0' + ct.getHours()).slice(-2);
                var xMinut = ('0' + ct.getMinutes()).slice(-2);
                var ymdhis = ct.getFullYear()+'-'+xMonth+'-'+xDate+' '+xHours+':'+xMinut+':00';
                postElms.postDateVal.val(ymdhis);            
            } 
        });
		postElms.postDateEdit1.datetimepicker({ 
            className:'datetimepicker', 
            format: 'j M Y - H:i', 
            yearStart: 2000, 
            yearEnd: 2040, 
            step: 30,
            onChangeDateTime: function(ct) { 
                var xDate  = ('0' + ct.getDate()).slice(-2);
                var xMonth = ('0' + (ct.getMonth()+1)).slice(-2);
                var xHours = ('0' + ct.getHours()).slice(-2);
                var xMinut = ('0' + ct.getMinutes()).slice(-2);
                var ymdhis = ct.getFullYear()+'-'+xMonth+'-'+xDate+' '+xHours+':'+xMinut+':00';
                postElms.postDateVal1.val(ymdhis);            
            } 
        });
        $('#btn-set-post-date').on('click', function(e){
            e.preventDefault();
            postElms.postDateText.text(postElms.postDateEdit.val());
            postElms.editPostDate.text('Edit');
            postElms.postDateArea.fadeOut(100);
        });
    }
    
    var slugChanged = false;    
    // mendapatkan post slug, via ajax
    function generatePostSlugVal(titleText) {
        var postID = postElms.postIdVal.val();
        var uriGet = 'action/post/generate-slug?post-title=' + titleText + '&post-id=' + postID;
        $.get(site_root + uriGet, function(res){
            postElms.slugVal.val(res.val);
            postElms.slugText.text(res.val);   
            if (postElms.editSlugArea.is(':hidden'))
                postElms.editSlugArea.fadeIn(100);                    
        }, 'json'); 
    }
    // otomatis generate slug saat judul diganti
    postElms.postTitleVal.on('blur', function(){
        var postTit = $(this).val().trim();
        if (postTit != '' && !slugChanged) {
            generatePostSlugVal(postTit);          
        }        
    });
    
    // tombol edit slug    
    postElms.editSlug.on('click', function(e){ 
        e.preventDefault();
        if ($(this).text().toLowerCase() == 'edit') {          
            postElms.slugText.hide();
            postElms.slugVal.val( postElms.slugText.text() ).show().focus();
            postElms.cancelEditSlug.show();
            $(this).text('OK');
        } else {
            var newSlug = postElms.slugVal.val();
            postElms.slugVal.hide();
            postElms.slugText.text(newSlug).show();
            postElms.cancelEditSlug.hide();
            generatePostSlugVal(newSlug);
            slugChanged = true;
            $(this).text('Edit');
        }
    }); 
    // tombol batal edit slug
    postElms.cancelEditSlug.on('click', function(e){
        e.preventDefault();
        postElms.slugText.show();
        postElms.slugVal.val( postElms.slugText.text() ).hide();
        postElms.editSlug.text('Edit');        
        $(this).hide();
    });    
    // editbox edit slug
    postElms.slugVal.on('keypress', function(e){        
        if (e.keyCode == 13) {
            e.preventDefault();
            postElms.editSlug.trigger('click');
        } else if (e.keyCode == 27) {
            e.preventDefault();
            postElms.cancelEditSlug.trigger('click');
        }
    });    
    // Post thumbnail
    $('#btn-add-thumb').openPopup(site_root + 'filemanager/dialog.php?type=1&field_id=post-thumb&popup=1', 900, 550,
        function(){
            var imgsrc = $('#post-thumb').val();
            if (imgsrc != '') {
                $('.msg-empty-humb').hide();
                $('#post-thumb-preview').attr('src', imgsrc).show(100);
                $('#btn-add-thumb').hide();
                $('#btn-del-thumb').show();
            }
    });
    $('#btn-del-thumb').on('click', function(e){
       e.preventDefault();
       $('#post-thumb').val('');
       $('#post-thumb-preview').removeAttr('src').hide();
       $('.msg-empty-humb').show();
       $(this).hide();
       $('#btn-add-thumb').show();
    });    
    // Preview post
    $('#btn-preview-post').on('click', function(e){
       e.preventDefault(); 
       tinyMCE.triggerSave();
       var pt = $(this).attr('data-posttype');       
       $.post(site_root+'action/post/preview-post', postElms.postForm.serialize(), function(res){ 
       		if (res.ok) {
       			var w = window.open('', 'post-preview-window');
       			setTimeout(function() {
				    w.location.href = site_root+pt+'/preview?id='+res.id;
				}, 100);
       		}		
       }, 'json');
    }); 
    // Tabel data
    postElms.postTable.on('mouseenter', 'tbody > tr', function(e){ 
       $(this).find('.post-actions').show(); 
       $(this).find('td').addClass('hover');
    }).on('mouseleave', 'tbody > tr', function(e){ 
       $(this).find('.post-actions').hide(); 
       $(this).find('td').removeClass('hover');
    });  
});