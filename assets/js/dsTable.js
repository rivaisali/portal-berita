(function($) {
    var methods = {
        init: function(options) {
            var tableElm = this.eq(0);
            var settings = tableElm.data('dsTable'); 
            if (typeof(settings) == 'undefined') {
                var defaults = {
                    dataName            : 'maintable',
                    dataSource          : null, // url sumber data via ajax
                    perPage             : 20,
                    page                : 1,
                    columns             : [], // array json: [{name,label,align,prefix,suffix,
                                              // format[money,date,datetime, function(id){ {namaCol} } )]
                                              // sort:[true|false],hide:[true|false]}]
                    showHeader          : true,
                    showFooter          : true,
                    tableClass          : '',
                    wrapperClass        : '',
                    searchField         : [],    // array json: [{name:'',label:''}]
                    orderBy             : null,  // sql order: id_mhs DESC
                    filter              : {},    // { nim: 'nim = 123', nm_mhs: 'nm_mhs like didin'  }     
                                                 // format: 'key[spasi][operator(=|%)][spasi]value'             
                    editAction          : null,  // editAct,delAct,detAct,rowClick bisa function, bisa link.
                    deleteAction        : null,  // function(id, tr, tableElm) --> return id row
                    detailAction        : null,  // link ex: action/edit?id=
                    customActions       : [],
                    rowClickAction      : null,
                    deleteMsg           : 'Hapus data ini?',
                    maxPageBtn          : 10,     // jumlah maksimal tombol halaman di pagination
                    centerTh            : false, // memaksa <th> align center
                    thActionWidth       : 30,
                    valign              : 'top',
                    emptyValue          : '',
                    useHash             : true,  // url hash sebagai halama
                    showOnInit          : true,  // Langsung tampilkan data saat inisiasi
                    onInit              : null,  // function(tableElm)
                    onLoad              : null,  // function(tableElm) -> saat loadData
                    onChangePerpage     : null   // function(tableElm, oldPerpage, newPerpage)
                };
                settings = $.extend({}, defaults, options); 
				tableElm.data('dsTable', settings);
            } else {
				settings = $.extend({}, settings, options);
			}
            // jika pake hash, ambil nilai sebagai halaman aktif
            if (settings.useHash) {
                var hash = location.hash;
                hash = parseInt(hash.substring(1)); 
                if ( !isNaN(hash) ) settings.page = hash;
            }      
            // ambil setting 'perpage' yg disimpan di localStorage        
            if (methods._setting(tableElm, 'perpage') != null)  
                settings.perPage = methods._setting(tableElm, 'perpage');   
            settings.colCount = settings.columns.length; // Jumlah kolom data  
            settings.actionBtnCount = 0; 
            if (settings.detailAction != null) settings.actionBtnCount++;    
            if (settings.editAction != null) settings.actionBtnCount++; 
            if (settings.deleteAction != null) settings.actionBtnCount++;  
            if ($.type(settings.customActions) !== 'array') 
                settings.customActions = [settings.customActions];
            settings.actionBtnCount += settings.customActions.length;        
            //settings.hasBtn   = (settings.detailAction != null || settings.editAction != null || settings.deleteAction != null);
            settings.colspan  = settings.actionBtnCount + settings.colCount;
            var content = ''; 
            var wrapClass = (settings.wrapperClass + ' dstable-wrapper').trim();
            tableElm.addClass('dstable '+settings.tableClass).wrap('<div class="' +wrapClass+ '"></div>'); 
            // Table title row --------------------------    
            var tableTitle = '<tr>';       
            $.each(settings.columns, function(idxCol, col) {     
                var thAttr   = 'data-column-name="' + col.name + '" ';
                tableTitle+= '<th ' + thAttr;                    
                tableTitle+= 'style="';
                if (col.width) 
                    tableTitle+= 'width:'+col.width+'px;';
                if (col.minWidth) 
                    tableTitle+= 'min-width:'+col.minWidth+'px;';
                if ( settings.centerTh ) 
                    tableTitle+= 'text-align:center;'; else
                if (col.align) 
                    tableTitle+= 'text-align:'+col.align+';';
                if (col.hide) tableTitle+= 'display:none;';
                tableTitle+= '">' + col.label + '</th>';
            });    
            if (settings.actionBtnCount > 0) 
                tableTitle+= '<th colspan="'+settings.actionBtnCount+'"></th>';                 
            tableTitle+= '</tr>'; 
            // -----------------------------------------
            if (settings.showHeader) content+= '<thead>' +tableTitle+ '</thead>';  
            content+= '<tbody><tr><td colspan="' +settings.colspan+ '" class="dstable-nodata"></td></tr></tbody>';
            if (settings.showFooter) content+= '<tfoot>' +tableTitle+ '</tfoot>'; 
            tableElm.append(content);
            // table bottom menu --------------
            var bottomContent = '<div class="dstable-bottom-menu clearfix">'+
                '<ul class="pagination pull-right" style="display:none"></ul>'+
                '<div class="dstable-data-pos">Menampilkan: <span class="dstable-data-from">0</span> - '+
                '<span class="dstable-data-to">0</span> dari total <span class="dstable-data-total">0</span>'+
                '</div></div>'+
                '<div class="dstable-loader" style="display:none"><img src="'+site_root+'assets/img/ajax-loader1.gif"></div>';
            tableElm.after(bottomContent);
            if (settings.showOnInit) { 
                tableElm.show();
                methods._loadData(tableElm, settings);
            }
            // Callback onInit
            if ($.type(settings.onInit) === 'function')  settings.onInit(tableElm);
            // Events tabel ------------------
            tableElm.on('click', '.dstable-delete-btn', function(e){
                e.preventDefault();
                var id = $(this).attr('data-id');
                if ($.type(settings.deleteAction) === 'function') {   
                    var tr = $(this).closest('tr');
                    settings.deleteAction(id, tr, tableElm);
                } else {
                    if (confirm(settings.deleteMsg)) {
                        var tr = $(this).closest('tr');                        
                        var rowNo = tableElm.find('td[data-column-name=rowNumber]:first').text();
                        $.get(settings.deleteAction + id, function(respon){ 
                            if (respon.ok) {
                                tr.addClass('deleted');
                                tr.animate({
                                    opacity: 0.4
                                    }, 200, function(){
                                    tr.css({ 'opacity': 1 }).remove(); 
                                    tableElm.find('tr:odd').removeClass('alt');
                                    tableElm.find('tr:even').addClass('alt');
                                    tableElm.find('td[data-column-name=rowNumber]').each(function(){
                                       $(this).text(rowNo); 
                                       rowNo++;
                                    });         
                                }); 
                            } else {
                                alert(respon.msg);
                            }
                        },'json');
                    }
                }
            }).on('click', '.dstable-edit-btn', function(e){         
                if ($.type(settings.editAction) === 'function') {
                    e.preventDefault();
                    var id = $(this).attr('data-id');
                    var tr = $(this).closest('tr');
                    settings.editAction(id, tr, tableElm); 
                }
            }).on('click', '.dstable-detail-btn', function(e){         
                if ($.type(settings.detailAction) === 'function') {
                    e.preventDefault();
                    var id = $(this).attr('data-id');
                    var tr = $(this).closest('tr');
                    settings.detailAction(id, tr, tableElm); 
                }
            }).on('click', 'tbody > tr > td:not(.dstable-action, :has(button, input, select))', function(e){         
                if ($.type(settings.rowClickAction) === 'function') {
                    e.preventDefault();
                    var tr = $(this).closest('tr');
                    var id = tr.attr('data-id');                    
                    settings.rowClickAction(id, tr, tableElm); 
                }
            });
            // Events bottom menu ------------
            $('.dstable-bottom-menu').on('click', '.pagination > li:not(.disabled) > a', function(e) {
                e.preventDefault(); 
                var toPage = $(this).attr('href').substring(1);
             	settings.page = isNaN( parseInt(toPage) ) ? 1 : parseInt(toPage);
                methods._loadData(tableElm, settings, function(){
                    if (settings.useHash) location.hash = settings.page;
                }); 
            });
        }, // End methods.init ---------------
        
        // Reload data tabel
        refresh: function(callBack, tableElm) { 
            if (!tableElm) tableElm = this.eq(0);
            var settings = tableElm.data('dsTable');              
            methods._loadData(tableElm, settings, callBack);
        },
        
        addFilter: function(key, value, tableElm) {
            if (!tableElm) tableElm = this.eq(0);
            var settings = tableElm.data('dsTable');            
            settings.filter[key] = value;
        },
        removeFilter: function(key, tableElm) {
            if (!tableElm) tableElm = this.eq(0);
            var settings = tableElm.data('dsTable');            
            delete settings.filter[key];
        },
        clearFilter: function(key, value, tableElm) {
            if (!tableElm) tableElm = this.eq(0);
            var settings = tableElm.data('dsTable');            
            settings.filter = {};
        },
        gotoPage: function(pageNum, tableElm) {
            if (!tableElm) tableElm = this.eq(0);
            var settings = tableElm.data('dsTable'); 
            settings.page = pageNum;
            methods._loadData(tableElm, settings, function(){
                if (settings.useHash) location.hash = settings.page;
            });
        },
        
        showLoader: function(tableElm) {
            if (!tableElm) tableElm = this.eq(0);
            var tbodyElm = tableElm.find('tbody:first'); 
            $('.dstable-loader').css({
                'left': tbodyElm.position().left + 1, 
                'top': tbodyElm.position().top + 1,
                'right': tbodyElm.position().left + 1, 
                'bottom': tbodyElm.position().top + 1,
                'padding-top': $('.dstable-loader').height()/2
            }).show();
        },
        
        _loadData: function(tableElm, settings, callBack) {
            var content  = '';  
            var loaderElm = $('.dstable-loader');
            var postdata = {
                page    : settings.page,
                perpage : settings.perPage,
                filters : settings.filter,
                orderby : settings.orderBy
            };  
            if (loaderElm.is(':hidden'))
                methods.showLoader(tableElm);           
            $.post(settings.dataSource,  postdata, function(respon) {
                // callback setelah data berhasil di-load
                loaderElm.fadeOut(200);
                if (callBack && $.type(callBack) === 'function') callBack();
                if ($.type(settings.onLoad) === 'function')  settings.onLoad(tableElm);
                var rowCount = respon.rows.length;         
                var no = respon.data_from;                   
                if (rowCount == 0) {
                    // jika data kosong
                    content+= '<tr><td class="dstable-nodata" colspan="'+settings.colspan+'">Data tidak ditemukan!</td></tr>';
                    methods._generateDataStatus.apply(tableElm, [0, 0, 0]);
                } else { 
                    $.each(respon.rows, function(idxRow, row) { 
                        if (idxRow % 2 != 0)
                            content+= '<tr data-id="' +row.id+ '" class="alt"'; else 
                            content+= '<tr data-id="' +row.id+ '"'; 
                        if (settings.rowClickAction != null)
                            content+= ' style="cursor:pointer"';
                        content+= '>'; 
                        // Looping kolom tabel, sesuai options.columns
                        $.each(settings.columns, function(idxCol, col) {
                            if (col.name == 'rowNumber') {
                                var colValue = no; 
                            } else {
                                var colValue = row[col.name]; 
                                if (colValue == null) colValue = settings.emptyValue;
                                if (col.prefix) colValue = col.prefix + colValue;
                                if (col.suffix) colValue+= col.suffix;
                                if (col.format) {
                                    if ($.type(col.format) === 'function') {
                                        colValue = col.format(row);
                                    } else {                                    
                                        switch (col.format) {
                                            case 'money':
                                                while ( /(\d+)(\d{3})/.test(colValue.toString()) ) {
                                                  colValue = colValue.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
                                                }
                                            break;
                                            case 'date':
                                                var exp = /(\d{4})\-(\d{2})\-(\d{2})/;
                                                colValue = colValue.replace(exp, "$3/$2/$1"); 
                                            break;
                                            case 'datetime':
                                                var exp = /(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
                                                colValue = colValue.replace(exp, "$3/$2/$1 - $4:$5"); 
                                            break;
                                        }
                                    }
                                } 
                            }
                            content+= '<td data-column-name="' +col.name+ '"';                            
                            content+= ' style="vertical-align:' +settings.valign+ ';';
                            if (col.align) content+= 'text-align:' +col.align+ ';';
                            if (col.hide) content+= 'display:none';
                            content+= '">'+colValue+'</td>';  
                        }); // End each.col
                        // jika ada tombol (edit, detail, delete)
                        if (settings.actionBtnCount > 0) {
                            var tdActHtml = '<td class="dstable-action" style="vertical-align:'+settings.valign+'">'; 
                            $.each(settings.customActions, function(i, item){
                                var def = { name:'', action:'', label:'', title:'', color:'default', icon:'wrench', attr:'' };
                                item = $.extend({}, def, item);
                                if (item.attr != '') item.attr = ' '+item.attr;
                                if (item.title == '') item.title = item.label;
                                item.icon = item.icon.substring(0, 3) == 'fa-' ? 'fa '+item.icon : 'glyphicon glyphicon-'+item.icon;
                                var customLink = $.type(item.action) === 'function' ? '#' : item.action + row.id;
                                content+= tdActHtml+'<a class="btn btn-xs btn-'+item.color+' dstable-custom-btn '+item.name+'" title="'+item.title+'" href="'+customLink+'" data-id="'+row.id+'"'+item.attr+'><i class="'+item.icon+'"></i></a></td>'; 
                            });  
                            if (settings.detailAction != null) {      
                                var detailLink = $.type(settings.detailAction) === 'function' ? '#' : settings.detailAction + row.id;
                                content+= tdActHtml+'<a class="btn btn-xs btn-warning dstable-detail-btn" title="Lihat Detail" href="' +detailLink+ '" data-id="' +row.id+ '"><i class="glyphicon glyphicon-eye-open"></i></a></td>';   
                            }
                            if ( settings.editAction != null ) {     
                                var editLink = $.type(settings.editAction) === 'function' ? '#' : settings.editAction + row.id;
                                content+= tdActHtml+'<a class="btn btn-xs btn-success dstable-edit-btn" title="Edit" href="' +editLink+ '" data-id="' +row.id+ '"><i class="glyphicon glyphicon-edit"></i></a></td>';
                            }
                            if (settings.deleteAction != null) {
                                var deleteLink = $.type(settings.deleteAction) === 'function' ? '#' : settings.deleteAction + row.id;
                                content+= tdActHtml+'<a class="btn btn-xs btn-danger dstable-delete-btn" title="Hapus" href="' +deleteLink+ '" data-id="' +row.id+ '"><i class="glyphicon glyphicon-trash"></i></a></td>';
                            }
                            content+= '</td>';
                        }
                        content+= '</tr>';                         
                        no++; 
                    });  // End each.row
                    methods._generatePageMenu.apply(tableElm, [respon]);
                }
                tableElm.find('tbody').hide().html(content).show(100);              
            }, 'json');
        }, // End methods._loadData
        
        _generatePageMenu: function(respon) { 
            var tableElm = this;
            var settings = tableElm.data('dsTable'); 
            var bottMenu = $('.dstable-bottom-menu');
            methods._generateDataStatus.apply(tableElm, [respon.data_from, respon.data_to, respon.data_total]);
            if (respon.page_total > 1) {                
                var prevPage = respon.page <= 1 ? 1 : parseInt(respon.page) - 1;
                var nextPage = respon.page < respon.page_total ? parseInt(respon.page) + 1 : respon.page_total;
                var liItem   = []; 
                var disClass = respon.page <= 1 ? ' class="disabled"' : '';
                liItem.push('<li' +disClass+ '><a href="#' +prevPage+ '" title="Sebelumnya"><i class="glyphicon glyphicon-arrow-left"></i></a></li>');
                for (i=1; i<=respon.page_total; i++) {
                    if (i > settings.maxPageBtn) break;
                    var activeClass = respon.page == i ? ' class="active"' : '';
                    liItem.push('<li' +activeClass+ '><a href="#' +i+ '">'+i+'</a></li>');                    
                }
                disClass = respon.page >= respon.page_total ? ' class="disabled"' : '';
                liItem.push('<li' +disClass+ '><a href="#' +nextPage+ '" title="Berikut"><i class="glyphicon glyphicon-arrow-right"></i></a></li>');            
                bottMenu.find('.pagination').html(liItem.join('')).show();
            } else {
                bottMenu.find('.pagination').hide();
            }
        },
        
        _generateDataStatus: function(dataFrom, dataTo, dataTotal) {
            var bottMenu = $('.dstable-bottom-menu');
            bottMenu.find('.dstable-data-from').text(dataFrom);
            bottMenu.find('.dstable-data-to').text(dataTo);
            bottMenu.find('.dstable-data-total').text(dataTotal); 
        },
        
        // simpan/get setting localStorage.
        _setting: function(tableElm, key, value) {
            var settings = tableElm.data('dsTable'); 
            if (typeof(Storage) !== "undefined") {
                key = settings.dataName + '_' + key;
                if (value)
                    return localStorage.setItem(key, value); else
                    return localStorage.getItem(key);                               
            }  
        }
    }; 
    
    $.fn.dsTable = function() {
		var method = arguments[0]; 
		if (methods[method] && method.substring(0,1) != '_') {
			method = methods[method];
			arguments = Array.prototype.slice.call(arguments, 1);
		} else if( typeof(method) == 'object' || !method ) {
			method = methods.init;
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.dsTable' );
			return this;
		} 
		return method.apply(this, arguments); 
	}; 
})(jQuery);