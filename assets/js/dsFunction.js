function setDecimalPlace(num, dec) {
    if (!dec) dec = 2;
    num = num.toFixed(dec).replace('.', ',');
    var dec = num.split(',')[1];
    if (dec == '00') 
        return num.replace(',00', ''); else
        return num;
}

function addThousandSep(num) { 
    num = parseFloat(num);
    num = num.toFixed(2); 
    num = num.toString().replace('.', ',');
    return num.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function addParameter(url, param, value) {
    var val = new RegExp('(\\?|\\&)' + param + '=.*?(?=(&|$))'),
        parts = url.toString().split('#'),
        url = parts[0],
        hash = parts[1]
        qstring = /\?.+$/,
        newURL = url;
    if (val.test(url)) {
        newURL = url.replace(val, '$1' + param + '=' + value);
    }
    else if (qstring.test(url)) {
        newURL = url + '&' + param + '=' + value;
    }
    else {
        newURL = url + '?' + param + '=' + value;
    }
    if (hash) {
        newURL += '#' + hash;
    }
    return newURL;
}

function removeParam(sourceURL, key) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function checkChildWindow(win, onclose) {
    var w = win;
    var cb = onclose;
    var t = setTimeout(function() { checkChildWindow(w, cb); }, 500);
    var closing = false;
    try {
        if (win.closed || win.top == null) 
        closing = true;        
    } catch (e) {       
        closing = true;
    }
    if (closing) {
        clearTimeout(t);
        onclose();
    }
}

$.fn.popupLink = function(options) { 
    var defaults = {
        width: 800,
        height: 450,
        wname: '' 
    };
    var options = $.extend(defaults, options); 
    var left = (screen.width / 2) - (options.width / 2);
    var top = (screen.height / 2) - (options.height / 2);
    top -= 20;
    return this.each(function() {
        var elm = $(this);              
        elm.on('click', function(e){
            var url = elm.attr('href') || document.URL; 
            if ( options.url ) {
                if ( $.type(options.url) === 'function' )
                    url = options.url(elm); else url = options.url;
            }
            e.preventDefault(); 
            window.open(url, options.wname, "location=no,directories=no,copyhistory=no,scrollbars=yes,menubar=no,width="+options.width+",height="+options.height+",left="+left+",top="+top+",toolbar=no");
        });      
    });        
};

/*  memanggil popup window */
$.fn.openPopup = function(url, w ,h, onClose, wname) { 
    var width = w || 800;
    var height = h || 450;
    var left = (screen.width / 2) - (width / 2);
    var top = (screen.height / 2) - (height / 2);
    if (!wname) wname = 'mypopup';
    top -= 20;
    return this.each(function() {
        var elm = $(this);    
        elm.on('click', function(e){
            e.preventDefault(); 
            var wopen = window.open(url, wname, "location=no,directories=no,copyhistory=no,scrollbars=yes,menubar=no,width="+width+",height="+height+",left="+left+",top="+top+",toolbar=no");
            if (onClose != null)
                checkChildWindow(wopen, onClose);
            return wopen;
        });      
    });        
};

/*  post tanpa form. ex: postAndGo(action.php, ['nama=didin','nim=1234']) */
$.postToNewTab = function(url, objVal) {
    var field = '';
    $.each(objVal, function(fName, fValue){
        field += '<textarea name="' +fName+ '">' +fValue+ '</textarea>';
    });
    $('body').append('<form id="dstmpfrm253x" action="'+url+'" style="display:none" target="dstmpfrm253x_window" method="post">'+field+'</form>');
    $('#dstmpfrm253x').on('submit', function(){
        return window.open(url, 'dstmpfrm253x_window');
    }).trigger('submit').remove();
};

/* submit post ddalam popup window. 
   set form attribut 'target'; 
   event onClick: return postPopup(wname=[form target]) */
$.postPopup = function(wname, width, height) {
    var top = (screen.height-height)/2-30;
    var left = (screen.width-width)/2;
    var win = window.open('', wname, 'width='+width+',height='+height+',top='+top+',left='+left);
    return true;
};
       
$.fn.toggleAttr = function(attr, attr_val1, attr_val2) {
    return this.each(function() {
        var $elm = $(this);
        var $attr = $elm.attr(attr);
        var $hasAttr = (typeof $attr !== 'undefined' && $attr !== false);
        if (!attr_val2) {
            if ($hasAttr) $elm.removeAttr(attr); else $elm.attr(attr, attr_val1);    
        } else {
            if ($attr == attr_val1) $elm.attr(attr, attr_val2); else
            if ($attr == attr_val2) $elm.attr(attr, attr_val1);
        }
    });
};

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$.fn.dsAjaxForm = function(options) { 
    var defaults = {  
        data            : {},   // extra data to post
        loader          : '',   // loader element: #loader
        resultDiv       : '',   // post result element: #post-result            
        clearOnOk       : true, // kosongkan semua field jika sukses
        noClear         : [],   // field yg tidak dikosongkan jika sukses
        exclude         : [],   // field to exclude
        okMsg           : '',   // overwrite ok msg from server
        errorMsg        : '',   // overwrite error msg from server
        beforeSerialize : function(){ },
        beforePost      : function(){  },
        afterPost       : function(){   },
        onError         : function(res){    },
        onOk            : function(res){     },
        errorResultTpl  : '<div class="alert alert-dismissable alert-danger">'+
            '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>'+
            '<span class="glyphicon glyphicon-exclamation-sign"></span> &nbsp;'+
            '<span class="post-result-text">%s</span></div>',
        okResultTpl     : '<div class="alert alert-dismissable alert-success">'+
            '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>'+
            '<span class="glyphicon glyphicon-ok-sign"></span> &nbsp;'+
            '<span class="post-result-text">%s</span></div>'            
    };  
    var options = $.extend(defaults, options); 
    return this.each(function() {
        var $frm = $(this);
        $frm.on('submit', function(e){ 
            e.preventDefault(); 
            var $action = $frm.attr('action');
            options.beforeSerialize($frm);
            var $pdata_json = $frm.serializeObject();
            $pdata_json = $.extend($pdata_json, options.data); 
            if (options.exclude.length > 0) {                
                for (i=0; i<options.exclude.length; i++) {
                    var $key = options.exclude[i];
                    delete $pdata_json[$key];
                }
            }
            var $pdata = $.param($pdata_json); 
            $frm.find('input, select, button, textarea').prop('disabled', true); 
            if (options.loader != '') $(options.loader).show();
            if (options.resultDiv != '') $(options.resultDiv).html('');
            options.beforePost($frm);              
            $.post($action, $pdata, function(res){
                if (options.loader != '') $(options.loader).hide();                    
                $frm.find("input, select, button, textarea").prop("disabled", false); 
                options.afterPost(res, $frm);                                       
                if (res.ok) {
                    if (options.okMsg != '') res.msg = options.okMsg;
                    if (options.resultDiv != '') {
                        var $msg = options.okResultTpl.replace('%s', res.msg);
                        $(options.resultDiv).hide().html($msg).slideDown(200);
                    }
                    if (options.clearOnOk) {
                        options.noClear.push(':button',':submit',':reset','.no-clear');
                        var $not = options.noClear.join(',');
                        $frm.find('input:not('+$not+'), select:not('+$not+'), textarea:not('+$not+')').val('');
                        $frm.find(':checkbox:not('+$not+'), :radio:not('+$not+')').removeAttr('checked');
                    }
                    options.onOk(res, $frm);
                } else {
                    if (options.errorMsg != '') res.msg = options.errorMsg;
                    if (options.resultDiv != '') {
                        var $msg = options.errorResultTpl.replace('%s', res.msg);
                        $(options.resultDiv).hide().html($msg).slideDown(200);
                    }
                    options.onError(res, $frm);
                }
                if (options.resultDiv != '') {
                    $('html, body').animate({scrollTop: $(options.resultDiv).offset().top -100 }, 200);
                }                    
            }, 'json');
        });
    });
}