(function($) {

	 // DOM Ready
	$(function() {

		// Binding a click event
		// From jQuery v.1.7.0 use .on() instead of .bind()
		//$('#my-button').bind('click', function(e) {
		 $(document).ready(function(e) {

			// Prevents the default action to be triggered. 
			// e.preventDefault();

			// Triggering bPopup when click event is fired
			$('#element_to_pop_up').bPopup({
			    //modalClose: false,
                //opacity: 0.6,
                //positionStyle: 'fixed' //'fixed' or 'absolute'
                content:'image', //'ajax', 'iframe' or 'image'
                contentContainer:'.content',
                loadUrl:'http://gorontalokota.go.id/assets/img/baner.jpg'
			});

		});

	});

})(jQuery);