(function($){  
 $.fn.limitchar = function(options) {  
  
  var defaults = {  
    alpha: true,
    numeric: true,
    space: true,  
    charCase: 'both', 
    special: '',
    onError: function(c){}
  };  
  
  var options = $.extend(defaults, options);  
  
  var allowed = [8, 0, 13];
  var alphaUpper = [65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90]; 
  var alphaLower = [97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122];   
  var numeric = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57];
  var spec_len = options.special.length;
  if(options.special != '') {
    for(var i=0; i<spec_len; i++) {
        allowed.push(options.special.charCodeAt(i));
    }
  }
  return this.each(function() {  
    obj = $(this);
    obj.removeAttr('readonly');
    if(options.space == true) {
        allowed.push(32);
    }
    if(options.alpha == true) {
        $.merge(allowed, alphaLower); 
        $.merge(allowed, alphaUpper);        
    }     
    if(options.numeric == true) {
        $.merge(allowed, numeric);
    }
    
    $.merge(allowed, options.special);
    
    obj.keypress(function(e){         
        if($.inArray(e.which, allowed) < 0) {
            var $c = String.fromCharCode(e.which);
            options.onError($c);
            e.preventDefault();
        } else {
            if($.inArray(e.which, alphaLower) >= 0 || $.inArray(e.which, alphaUpper) >= 0) {
                var $char = $(this).val() + String.fromCharCode(e.which);
                if(options.charCase.toLowerCase() == 'upper') {
                    $(this).val($char.toUpperCase());
                } else
                if(options.charCase.toLowerCase() == 'lower') {
                    $(this).val($char.toLowerCase());
                } else {
                    $(this).val($char);
                }
                e.preventDefault();
            }
        }      
    });    
    
  }); 
   
 };  
})(jQuery);