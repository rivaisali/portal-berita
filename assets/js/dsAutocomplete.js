(function($){ 
    var methods = {
        init: function(options) { 
            var reservedKey = [9, 13, 16, 17, 18, 20, 37, 38, 39, 40];
            return this.each(function() { 
                var $this = $(this);
                var settings = $this.data('dsAutocomplete'); 
                if ($.type(settings) === 'undefined') {
                    var defaults = {
                        minChar     : 1,
                        source      : [],
                        listClass   : '',
                        theme       : 'white' // [white|black]
                    };
                    settings = $.extend({}, defaults, options); 
				    $this.data('dsAutocomplete', settings);
                } else {
    				settings = $.extend({}, settings, options);
    			} 
                if ( !$('#dsAutocomplete-list').length ) {
                    var classAttr = 'dsAutocomplete dsAutocomplete-'+settings.theme;
                    if (settings.listClass != '') {
                        classAttr = classAttr + ' ' + settings.listClass;
                    }
                    $('body').append('<ul id="dsAutocomplete-list" class="'+classAttr+'" '+
                        'style="display:none;position:absolute;z-index:999"></ul>');
                } 
                var listElm = $('#dsAutocomplete-list');  
                listElm.on('click', 'li', function(e){ 
                    listElm.data('input').val( $(this).text() );
                    listElm.hide();
                }).on('mouseenter', 'li', function(e){
                    $(this).addClass('selected').siblings().removeClass('selected');
                }).on('mouseleave', 'li', function(e){
                    $(this).removeClass('selected');
                });  
                /* tombol lookup. beri class lookup-btn, 1 parent dgn editbox */
                var lookupBtn = $this.parent().find('.lookup-btn:first');
                if (lookupBtn.length) {
                    lookupBtn.css('cursor', 'pointer').on('click', function(e){
                        e.stopPropagation();
                        e.preventDefault();  
                        if ($('#dsAutocomplete-list').is(':hidden'))
                        	methods.lookup($this, null); else
                        	methods._animateHide(settings);               
                    });
                }                   
                $this.attr('autocomplete', 'off').on('keyup', function(e) { 
                    if ($.inArray(e.which, reservedKey) > -1) return;                     
                    methods._delay(function(){
                        var q = $this.val(); 
                        if (q.length >= settings.minChar) { 
                            methods.lookup($this, q);
                        } else {
                            methods._animateHide(settings);
                        }    
                    }, 200);                    
                }).on('keydown', function(e) {
                    if (listElm.is(':visible')) { 
                        switch (e.which) {
                            case 38: 
                                e.preventDefault();
                                var liSel = listElm.children('li.selected');
                                if ( !liSel.length ) {
                                    if (listElm.hasClass('list-top'))
                                        listElm.children('li:last').addClass('selected'); 
                                } else {                                    
                                    liSel.removeClass('selected').prev('li').addClass('selected');                                    
                                } 
                            break;
                            case 40:
                                e.preventDefault();
                                var liSel = listElm.children('li.selected');
                                if ( !liSel.length ) {
                                    if (!listElm.hasClass('list-top'))
                                        listElm.children('li:first').addClass('selected'); 
                                } else {                                    
                                    liSel.removeClass('selected').next('li').addClass('selected'); 
                                }                              
                            break;
                            case 13:
                                e.preventDefault();
                                listElm.children('li.selected').trigger('click');
                            break;
                        }   
                    }                    
                });
            });     
        },
        lookup: function(elm, q) {
            var settings = elm.data('dsAutocomplete');
            var resultCache = elm.data('result_cache') || {};
            var listElm = $('#dsAutocomplete-list');
            var results = settings.source;
            if (q != null) {
                if (q in resultCache) {
                    results = resultCache[q];
                } else {
                    results = $.grep(settings.source, function(item){
                        return item.search(RegExp(q, "i")) != -1;
                    });
                    resultCache[q] = results;
                    elm.data('result_cache', resultCache);
                }  
            }
            if ( !$.isEmptyObject(results) ) {                            
                // jika query cocok
                listElm.empty();
                for(term in results){
                    listElm.append('<li>' + results[term] + '</li>');
                }                             
                methods._animateShow(elm, settings);
                $('body').one('click', function(){
                    methods._animateHide(settings);
                });
            } else {
                // jika query tidak cocok
                methods._animateHide(settings);
            }
        },
        _animateShow: function(elm, settings) {
            var listElm = $('#dsAutocomplete-list');
            var elmOff = elm.offset();
            var elmPosTop = elmOff.top - $(window).scrollTop();
            var wHe = $(window).height(); 
            if ( elmPosTop > ((wHe/2)+30) ) {
                listElm.css('max-height', (elmPosTop - 10) + 'px');
                listElm.css({   
                    'left': elmOff.left, 
                    'top': elmOff.top - listElm.outerHeight(),
                    'width': elm.outerWidth()                    
                }).removeClass('list-bottom').addClass('list-top');
            } else {
                listElm.css({                                
                    'left': elmOff.left, 
                    'top': elmOff.top + elm.outerHeight(),
                    'width': elm.outerWidth(),
                    'max-height': (wHe - elmPosTop - elm.outerHeight() - 10) + 'px'
                }).removeClass('list-top').addClass('list-bottom');
            }
            listElm.show().data('input', elm);
            if (listElm.hasClass('list-top'))
                listElm.scrollTop(listElm.height()); else
                listElm.scrollTop(0);
        },
        _animateHide: function(settings) {
            $('#dsAutocomplete-list').hide();
        },
        _delay: (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })()
    };
    
    $.fn.dsAutocomplete = function(options) {
        var method = arguments[0]; 
        if (methods[method] && method.substring(0,1) != '_') {
        	method = methods[method];
        	arguments = Array.prototype.slice.call(arguments, 1);
        } else if( typeof(method) == 'object' || !method ) {
        	method = methods.init;
        } else {
        	$.error( 'Method ' +  method + ' does not exist on jQuery.dsAutocomplete' );
        	return this;
        } 
        return method.apply(this, arguments); 
    };
})(jQuery);