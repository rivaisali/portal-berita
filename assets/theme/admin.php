<?php header_no_cache(); ?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo page_title() ?></title>
    <meta name="robots" content="nofollow"/>
    <meta name="robots" content="noindex"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta http-equiv="content-type" content="text/html"/>
    <meta charset="<?php echo config_item('charset') ?>"/>
    <meta name="description" content="<?php echo config_item('site_desc') ?>"/>
	<meta name="author" content="<?php echo config_item('app_author') ?>"/>
    <link rel="shortcut icon" href="<?php echo img_path('logo-gorontalokota.png'); ?>" type="image/x-icon"/>
    <link rel="icon" href="<?php echo img_path('logo-gorontalokota.png'); ?>" type="image/x-icon"/>
<?php if (ENVIRONMENT == 'production'): ?>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,400italic&subset=latin,cyrillic" rel="stylesheet" type="text/css">
 <?php endif; ?>
    <script>var site_root = '<?php echo base_url() ?>';</script> 
    <?php 
    include_assets('bootstrap');
    enqueue_css('adminlte', 'assets/theme/inc/adminlte.css', 'bootstrap,font-awesome');
    enqueue_css('font-awesome');
    enqueue_js('adminlte', 'assets/theme/inc/adminlte.js', 'jquery,bootstrap');   
    enqueue_css('admincustom', 'assets/theme/inc/admin.css', 'adminlte');   
    head_content();
    ?>	
</head>
<BODY class="skin-<?php echo config_item('admin_skin_color') ?>">
    <header class="header">
        <a href="<?php echo base_url($user_type) ?>" class="logo"><i class="fa fa-spin fa-cog"></i>&nbsp; Kota Gorontalo</a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <?php include_view('admin_navbar') ?>
        </nav>
    </header>  
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <?php include_view('admin_sidebar') ?>
        <aside class="right-side">   
            <section class="content-header">
                <h1><?php echo page_header() ?></h1>
                <?php echo get_breadcumb($user_type, '<i class="fa fa-dashboard"></i> Home'); ?>
            </section>
            <section class="content">
                <div class="row">
                    <?php echo $main_content; ?>
                </div> 
            </section>
        </aside>
    </div> <!-- /.wrapper row-offcanvas row-offcanvas-left --> 
    
    <?php foot_content() ?>
</BODY>
</html>