<?php
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", FALSE);
header("Pragma: no-cache");
?>
<!DOCTYPE HTML>
<html class="bg-black">
<head>
    <title><?php echo config_item('site_title') ?> &raquo; Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta http-equiv="content-type" content="text/html"/>
    <meta charset="<?php echo config_item('charset') ?>"/>
    <meta name="robots" content="noindex"/>
    <link rel="shortcut icon" href="<?php echo img_path('logo-gorontalokota.png'); ?>" type="image/x-icon"/>
    <link rel="icon" href="<?php echo img_path('logo-gorontalokota.png'); ?>" type="image/x-icon"/>
    <script>var site_root = '<?php echo base_url() ?>';</script> 
<?php if (ENVIRONMENT == 'production'): ?>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,400italic&subset=latin,cyrillic" rel="stylesheet" type="text/css">
 <?php endif; ?>
    <link rel="stylesheet" href="<?php echo base_path('assets/css/login.css') ?>"/>
</head>
<BODY class="bg-black">
    <div class="form-box" id="login-box">
        <?php $action = get_value('a', 'login');
        switch ($action): case 'new-password': 
            if ( !$this->input->cookie('rstp') ) {
                redirect(add_qstring(full_url(), 'a=reset-password'));
                exit;
            }
        ?>
            
        <div class="header">
            <i class="glyphicon glyphicon-lock"></i> Reset Password
            <h3><?php echo config_item('site_title') ?></h3>
        </div> 
        <form action="<?php echo base_path("action/resetpass"); ?>" method="post" class="reset-form">
            <div class="body bg-gray">
                <div id="post-result" style="display:none"></div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password Baru" autofocus required>
                </div>
                <div class="form-group">
                    <input type="password" name="password2" class="form-control" placeholder="Ulangi Password" required>
                </div>  
                <div class="form-group">
                    <input type="text" name="kode" class="form-control" placeholder="Kode Verifikasi" required>
                    <span class="help-block">(Kode yang telah dikirim ke email anda)</span>
                </div>  
            </div>
            <div class="footer">                                                               
                <button type="submit" class="btn bg-olive btn-block"><i class="glyphicon glyphicon-ok"></i> Ubah Password</button>                
                <p><a href="<?php echo add_qstring(full_url(), 'a=reset-password') ?>">
                    <i class="glyphicon glyphicon-key"></i> Kirim ulang kode verifiaksi</a></p>
            </div>
            <input type="hidden" name="redirect" value="<?php echo get_value('redirect', '') ?>">
        </form>
        
        <?php
        add_script("var oldSubmtText = '';
            $('.reset-form').dsAjaxForm({
                resultDiv: '#post-result',
                okResultTpl: '<p class=\"ok\">%s</p>',
                errorResultTpl: '<p class=\"error\">%s</p>',
                beforePost: function(frm) {
                    oldSubmtText = frm.find(':submit').html();
                    frm.find(':submit').html('<i class=\"glyphicon glyphicon-ban-circle\"></i> Loading...');
                },
                afterPost: function(res, frm) {
                    frm.find(':submit').html(oldSubmtText);
                },
                onOk: function(res, frm) {
                    setTimeout(function(){
                        window.location = '".remove_qstring(full_url(), 'a')."';
                    }, 2000);
                }
            });");
        break; case 'reset-password': ?>
        
        <div class="header">
            <i class="glyphicon glyphicon-lock"></i> Reset Password
            <h3><?php echo config_item('site_title') ?></h3>
        </div> 
        <form action="<?php echo base_path("action/resetpass/verification"); ?>" method="post" class="verf-form">            
            <div class="body bg-gray">
                <div id="post-result" style="display:none"></div>
                <p>Masukkan email anda dan kode verifikasi akan dikirim ke email tersebut.</p>
                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="Email" autocomplete="off" autofocus required>
                </div>
            </div>
            <div class="footer">                                                               
                <button type="submit" class="btn bg-olive btn-block">
                    <i class="glyphicon glyphicon-ok"></i> Kirim Kode Verifikasi</button>                
                <p><a href="<?php echo remove_qstring(full_url(), 'a') ?>">
                    <i class="glyphicon glyphicon-key"></i> Ke halaman Login</a></p>
            </div>
            <input type="hidden" name="verf-url" value="<?php echo add_qstring(full_url(), 'a=new-password') ?>">            
        </form>
        
        <?php 
        add_script("var oldSubmtText = '';
            $('.verf-form').dsAjaxForm({
                resultDiv: '#post-result',
                okResultTpl: '<p class=\"ok\">%s</p>',
                errorResultTpl: '<p class=\"error\">%s</p>',
                beforePost: function(frm) {
                    oldSubmtText = frm.find(':submit').html();
                    frm.find(':submit').html('<i class=\"glyphicon glyphicon-ban-circle\"></i> Loading...');
                },
                afterPost: function(res, frm) {
                    frm.find(':submit').html(oldSubmtText);
                },
                onOk: function(res, frm) {
                    setTimeout(function(){
                        window.location = '".add_qstring(full_url(), 'a=new-password')."';
                    }, 2000);                    
                }
            });");
        break; default: ?>
      
        <div class="header">
            <i class="glyphicon glyphicon-lock"></i> Login User
            <h3><?php echo config_item('site_title') ?></h3>
        </div>        
        <form action="<?php echo base_path("action/login/{$user_type}"); ?>" method="post" class="login-form">            
            <div class="body bg-gray">
                <div id="post-result" style="display:none"></div>
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Username" autocomplete="off" autofocus required>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                </div>          
                <div class="form-group">
                    <input type="checkbox" name="remember"/> Ingat Saya
                </div>
            </div>
            <div class="footer">                                                               
                <button type="submit" class="btn bg-olive btn-block"><i class="glyphicon glyphicon-ok"></i> Masuk</button>                
                <p><a href="<?php echo add_qstring(full_url(), 'a=reset-password') ?>">
                    <i class="glyphicon glyphicon-key"></i> Lupa password?</a></p>
            </div>
            <input type="hidden" name="redirect" value="<?php echo get_value('redirect', '') ?>">
        </form>
        
        <?php 
        add_script("var oldSubmtText = '';
            $('.login-form').dsAjaxForm({
                resultDiv: '#post-result',
                okResultTpl: '<p class=\"ok\">%s</p>',
                errorResultTpl: '<p class=\"error\">%s</p>',
                beforePost: function(frm) {
                    oldSubmtText = frm.find(':submit').html();
                    frm.find(':submit').html('<i class=\"glyphicon glyphicon-ban-circle\"></i> Loading...');
                },
                afterPost: function(res, frm) {
                    frm.find(':submit').html(oldSubmtText);
                },
                onOk: function(res, frm) {
                    window.location = addParameter(res.redirect, 'log-time', new Date().getTime());
                }
            });");
        endswitch; 

        ?>
                    
        <div class="margin text-center">
            <a href="<?php echo base_url() ?>">&laquo; Kembali ke halaman utama</a>
        </div>
    </div>  
    <script type="text/javascript" src="<?php echo base_path('assets/js/login.js') ?>"></script>
    <?php foot_content(); ?>
</BODY>
</html>