<?php header_no_cache(); ?>
<!DOCTYPE HTML>
<html>

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="<?php echo config_item('charset') ?>" />
	<title><?php echo page_title() ?></title>
	<meta name="description" content="<?php echo page_description() ?>" />
	<meta name="author" content="<?php echo config_item('app_author') ?>" />
	<meta name="keywords" content="<?php echo config_item('site_keywords') ?>" />
	<?php
	$tmp_short       = explode(' | ', page_title());
	$pagetitle_short = array_shift($tmp_short);
	?>
	<!-- Open Graph data -->
	<meta property="og:title" content="<?php echo $pagetitle_short; ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo current_url() ?>" />
	<meta property="og:image" content="<?php echo page_imageurl() ?>" />
	<meta property="og:image:width" content="640" />
	<meta property="og:image:height" content="342" />
	<meta property="og:description" content="<?php echo page_description() ?>" />
	<meta property="og:site_name" content="<?php echo config_item('site_title') ?>" />
	<meta property="fb:app_id" content="<?php echo config_item('facebook_appid') ?>" />
	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="<?php echo $pagetitle_short; ?>">
	<meta name="twitter:description" content="<?php echo page_description() ?>">
	<meta name="twitter:image" content="<?php echo page_imageurl() ?>">

	<link rel="shortcut icon" href="<?php echo img_path('logo-gorontalokota.png');; ?>" type="image/x-icon" />
	<link rel="icon" href="<?php echo img_path('logo-gorontalokota.png'); ?>" type="image/x-icon" />
	<link rel="stylesheet" href="<?php echo theme_path('font-awesome/css/font-awesome.min.css'); ?>" />

	<!-- Stylesheets -->
	<!-- Mobile Specific Metas
	  ================================================== -->
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<!-- CSS 
	================================================== -->
	<link href="<?php echo theme_path('native/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo theme_path('native/plugins/mediaelement/mediaelementplayer.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo theme_path('native/css/style.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo theme_path('native/plugins/prettyphoto/css/prettyPhoto.css'); ?>" rel="stylesheet" type="text/css">

	<!-- Warna -->
	<link class="alt" href="<?php echo theme_path('native/colors/color3.css'); ?>" rel="stylesheet" type="text/css">

	<!-- Nivo Slider Styles -->
	<link rel="stylesheet" href="<?php echo theme_path('native/plugins/nivoslider/themes/default/default.css'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo theme_path('native/plugins/nivoslider/nivo-slider.css'); ?>" type="text/css" media="screen" />

	<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?php echo theme_path('native/css/extralayers.css'); ?>" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo theme_path('native/plugins/rs-plugin/css/settings.css'); ?>" media="screen" />

	<!-- SCRIPTS 
	================================================== -->
	<script src="<?php echo theme_path('native/js/modernizr.js'); ?>"></script><!-- Modernizr -->
	<!-- Responsive -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<!-- css lama -->
	<link rel="stylesheet" href="<?php echo theme_path('owl-carousel/owl.carousel.all.css'); ?>" />
	<link rel="stylesheet" href="<?php echo assets_path("css/animate.css"); ?>" />
	<link rel="stylesheet" href="<?php echo theme_path("inc/slider.css"); ?>" />

	<script type="text/javascript" src=https://widget.kominfo.go.id/gpr-widget-kominfo.min.js> </script> <?php
																											/*
	<link rel="stylesheet" href="<?php echo theme_path("inc/post.css"); ?>"/> 
	<script type="text/javascript" src="<?php echo theme_path('inc/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo assets_path('js/bpopup.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo assets_path('js/popup.js'); ?>"></script>
	*/
																											?> <!--[if lt IE 9]>
		< script src = "<?php echo theme_path('inc/html5shiv-respond'); ?>" >
	</script>
	<![endif]-->
	<script>
		var site_root = '<?php echo base_url() ?>';
	</script>
	<?php
	head_content();
	?>
</head>

<body>
	<div class="body">
		<header class="site-header">
			<div class="topbar">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-8">
							<h1 class="logo"> <a href="index-2.html"><img src="<?php echo base_url(); ?>assets/theme/img/logo-web-kota.png" alt="Logo"></a> </h1>
						</div>
						<div class="col-md-8 col-sm-6 col-xs-4">
							<!--ul class="top-navigation hidden-sm hidden-xs">
				  <li><a href="plan-visit.html">Plan your visit</a></li>
				  <li><a href="events-calendar.html">Calendar</a></li>
				  <li><a href="donate.html">Donate Now</a></li>
				</ul-->
							<a href="#" class="visible-sm visible-xs menu-toggle"><i class="fa fa-bars"></i></a> </div>
					</div>
				</div>
			</div>
			<div class="main-menu-wrapper">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<nav class="navigation">

								<?php generate_menu_user(get_menu_items(1), array('class' => 'sf-menu'), array('submenu_key' => 'children')); ?>

							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>

		<?php
		// Jika halaman yang terbuka adalah halaman beranda
		if (uri_string() == '') {
		?>

			<!--Main Slider-->

			<div class="slider-rev-cont">
				<div class="tp-banner-container">
					<div class="tp-banner">
						<ul style="display:none;">
							<?php
							$cntslr = list_content_slider(0, array('aktif' => 'Y'));
							$i	=	0;
							foreach ($cntslr as $j => $c) {
							?>

								<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide <?php echo $i; ?>">
									<img src="<?php echo $base_url . 'uploads/content-slider/' . $c['nama_file']; ?>" alt="<?php echo $c['judul']; ?>" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

									<!-- LAYER NR. 2 -->
									<div class="tp-caption boldwide_small_white randomrotate customout tp-resizeme" data-x="center" data-hoffset="0" data-y="center" data-voffset="38" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="3000" data-easing="Power3.easeInOut" data-splitin="chars" data-splitout="chars" data-elementdelay="0.08" data-endelementdelay="0.08" data-end="6500" data-endspeed="500" style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"><?php echo $c['judul']; ?>
									</div>
								</li>
							<?php
								$i++;
							}
							?>
						</ul>
						<div class="tp-bannertimer" style="display:none;"></div>
					</div>
				</div>
			</div>

			<!-- Start Notice Bar -->
			<?php
			$event_next = list_event(1);
			if (!empty($event_next));
			foreach ($event_next as $en) {
			?>
				<div class="notice-bar">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-6 col-xs-6 notice-bar-title"> <span class="notice-bar-title-icon hidden-xs"><i class="fa fa-calendar fa-3x"></i></span> <span class="title-note">Next</span> <strong>Upcoming Event</strong> </div>
							<div class="col-md-3 col-sm-6 col-xs-6 notice-bar-event-title">
								<h5><a href="single-event.html"><?php echo $en->judul; ?></a></h5>
								<?php
								$tgl_mulai	=	date('F d, Y', strtotime(substr($en->waktu_mulai, 0, 10)));
								?>
								<span class="meta-data"><?php echo hari_tanggal($en->waktu_mulai); ?></span>
							</div>
							<div id="counter" class="col-md-4 col-sm-6 col-xs-12 counter" data-date="<?php echo $tgl_mulai; ?>">
								<div class="timer-col"> <span id="days"></span> <span class="timer-type">days</span> </div>
								<div class="timer-col"> <span id="hours"></span> <span class="timer-type">hrs</span> </div>
								<div class="timer-col"> <span id="minutes"></span> <span class="timer-type">mins</span> </div>
								<div class="timer-col"> <span id="seconds"></span> <span class="timer-type">secs</span> </div>
							</div>
							<div class="col-md-2 col-sm-6 hidden-xs"> <a href="event" class="btn btn-primary btn-lg btn-block">All Events</a> </div>
						</div>
					</div>
				</div>
			<?php
			}
			?>

		<?php
		} else {
		?>
			<div class="nav-backed-header parallax" style="background-image:url(<?php echo theme_path('img/baner-header.jpg'); ?>);">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li><a href="<?php echo site_url(); ?>">Home</a></li>
								<li class="active"><?php echo $pagetitle_short ?></li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		<?php
		} // end header
		?>

		<?php
		if (uri_string() == '') { // body website
		?>
			<div class="main" role="main">
				<div id="content" class="content full">
					<div class="container">
						<?php
						echo $main_content;
						?>

						<!-- EVENT -->
						<div class="row">
							<div class="col-md-8 col-sm-12">
								<div class="listing post-listing">
									<header class="listing-header">
										<h3>Berita Terbaru</h3>
									</header>
									<section class="listing-cont">
										<ul>
											<?php
											$where	=	array("post_type" => "post", "post_status" => "publish");
											$berita	=	list_berita(9, $where);
											$batas	=	3;
											$i		=	1;
											foreach ($berita as $ber) {
												if ($i <= $batas) {
													$i++;
													continue;
												}
												$post_summary = character_limiter(strip_tags($ber->post_content), 150);
											?>
												<li class="item post">
													<div class="row">
														<div class="col-md-4">
															<a href="#" class="media-box">
																<img src="<?php echo get_post_thumb_url_user($ber->post_thumb); ?>" alt="" class="img-thumbnail">
															</a>
														</div>
														<div class="col-md-8">
															<div class="post-title">
																<h2><a href="<?= $base_url.$ber->post_type.'/'.$ber->post_slug; ?>"><?php echo $ber->post_title; ?></a></h2>
																<span class="meta-data"><i class="fa fa-calendar"></i> <?php echo xtime($ber->post_date); ?></span>
															</div>
															<p>
																<?php echo $post_summary; ?>
															</p>
														</div>
													</div>
												</li>
											<?php
												$i++;
											}
											?>
										</ul>
									</section>
								</div>

								<div class="spacer-30"></div>

								<!-- Events Listing -->
								<div class="listing events-listing">
									<header class="listing-header">
										<h3>Event Yang Akan Datang</h3>
									</header>
									<section class="listing-cont">
										<ul>
											<?php
											// $where	=	array("event_id" => "30");
											$event	=	list_event(5);
											if (!empty($event)) {
												foreach ($event as $ev) {
													$bulan	=	nama_bulan($ev->waktu_mulai, TRUE);
													$tgl	=	substr($ev->waktu_mulai, 8, 2);
													$hari	=	nama_hari($ev->waktu_mulai);
													$jam	=	jam_saja($ev->waktu_mulai);
											?>
													<li class="item event-item">
														<div class="event-date"> <span class="date"><?php echo $tgl; ?></span> <span class="month"><?php echo $bulan; ?></span> </div>
														<div class="event-detail">
															<h4><a href="?aksi=detail&id=<?php echo $ev->event_id; ?>"><?php echo $ev->judul; ?></a></h4>
															<span class="event-dayntime meta-data"><?= $hari; ?> | <?= $jam; ?></span>
														</div>
														<div class="to-event-url">
															<div><a href="?aksi=detail&id=<?php echo $ev->event_id; ?>" class="btn btn-default btn-sm">Details</a></div>
														</div>
													</li>
											<?php
												}
											} else {
												echo '<li class="item event-item">';
												echo '<div class="alert alert-primary">Belum ada event</div>';
												echo '</li>';
											}
											?>
										</ul>
									</section>
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<?php include_view('sidebar_right'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		} else {
		?>
			<div class="main" role="main">
				<div id="content" class="content full">
					<div class="container">
						<div class="row">
							<div class="col-md-9">
								<?php
								echo $main_content;
								?>
							</div>
							<div class="col-md-3">
								<?php include_view('sidebar_right'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>

		<!--Footer Footer-->
		<footer class="site-footer-bottom">
			<div class="container">
				<div class="row">
					<div class="copyrights-col-left col-md-6 col-sm-6">
						&copy;Copyright 2020 <?php echo config_item('site_title') ?>.
					</div>
					<!-- <div class="copyrights-col-right col-md-6 col-sm-6">
						<a href="http://sarondetech.com" class="pull-right">sarondetech.com</a>
					</div> -->
				</div>
			</div>
		</footer>
		<!--End Footer Footer-->


	</div>
	<!--End page wrapper-->

	<script src="<?php echo theme_path('native/js/jquery-2.0.0.min.js'); ?>"></script> <!-- Jquery Library Call -->
	<script src="<?php echo theme_path('native/plugins/prettyphoto/js/prettyphoto.js'); ?>"></script> <!-- PrettyPhoto Plugin -->
	<script src="<?php echo theme_path('native/js/helper-plugins.js'); ?>"></script> <!-- Plugins -->
	<script src="<?php echo theme_path('native/js/bootstrap.js'); ?>"></script> <!-- UI -->
	<script src="<?php echo theme_path('native/js/waypoints.js'); ?>"></script> <!-- Waypoints -->
	<script src="<?php echo theme_path('native/plugins/mediaelement/mediaelement-and-player.min.js'); ?>"></script> <!-- MediaElements -->
	<script src="<?php echo theme_path('native/plugins/nivoslider/jquery.nivo.slider.js'); ?>"></script> <!-- NivoSlider -->
	<script src="<?php echo theme_path('native/js/init.js'); ?>"></script> <!-- All Scripts -->
	<script src="<?php echo theme_path('native/plugins/flexslider/js/jquery.flexslider.js'); ?>"></script> <!-- FlexSlider -->
	<script src="<?php echo theme_path('native/plugins/countdown/js/jquery.countdown.min.js'); ?>"></script> <!-- Jquery Timer -->

	<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="<?php echo theme_path('native/plugins/rs-plugin/js/jquery.themepunch.tools.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo theme_path('native/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js'); ?>"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('.tp-banner').show().revolution({
				dottedOverlay: "none",
				delay: 9000,
				startwidth: 1060,
				startheight: 500,
				hideThumbs: 200,

				thumbWidth: 100,
				thumbHeight: 50,
				thumbAmount: 5,

				navigationType: "none",
				navigationArrows: "solo",
				navigationStyle: "preview2",

				touchenabled: "on",
				onHoverStop: "on",

				swipe_velocity: 0.7,
				swipe_min_touches: 1,
				swipe_max_touches: 1,
				drag_block_vertical: false,


				keyboardNavigation: "on",

				navigationHAlign: "center",
				navigationVAlign: "bottom",
				navigationHOffset: 0,
				navigationVOffset: 20,

				soloArrowLeftHalign: "left",
				soloArrowLeftValign: "center",
				soloArrowLeftHOffset: 20,
				soloArrowLeftVOffset: 0,

				soloArrowRightHalign: "right",
				soloArrowRightValign: "center",
				soloArrowRightHOffset: 20,
				soloArrowRightVOffset: 0,

				shadow: 0,
				fullWidth: "on",
				fullScreen: "off",

				spinner: "spinner0",

				stopLoop: "off",
				stopAfterLoops: -1,
				stopAtSlide: -1,

				shuffle: "off",

				autoHeight: "off",
				forceFullWidth: "off",



				hideThumbsOnMobile: "off",
				hideNavDelayOnMobile: 1500,
				hideBulletsOnMobile: "off",
				hideArrowsOnMobile: "off",
				hideThumbsUnderResolution: 0,

				hideSliderAtLimit: 0,
				hideCaptionAtLimit: 0,
				hideAllCaptionAtLilmit: 0,
				startWithSlide: 0
			});
		}); //ready
	</script>

	<!--JS Lama-->
	<script type="text/javascript" src="<?php echo theme_path('inc/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo theme_path('owl-carousel/owl.carousel.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_path('assets/js/imgLiquid-min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_path('assets/js/dsEndlessScroll.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo theme_path("inc/main.{$jscss_version}.js"); ?>"></script>
	<?php
	/*
	*/
	?>
	<?php foot_content() ?>

</body>

<!-- Mirrored from html.tonatheme.com/2018assets/theme/orenburg/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Jul 2018 05:13:59 GMT -->

</html>