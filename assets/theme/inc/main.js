//var $btnBackTop = $('<button type="button" id="btn-back-top" title="Kembali ke atas"><i class="fa fa-arrow-up"></i></button>').appendTo('body');

if ( $('#page-pre-loader').length) {
    var preloadTimer = 0;
    var preloadIntval = setInterval(function() {
        if (preloadTimer > 15) {
            clearInterval(preloadIntval);
            $('#page-pre-loader').fadeOut(700, function() {
                $(this).remove();
            });
        } 
        preloadTimer++;
    }, 1000);
    $(window).load(function() {
        preloadTimer = 16;
    });
}

(function($) {

    $('#main-menu .navbar-nav').on('mouseenter', '> li.dropdown', function() {
        if (screenSizeType() == 'xs') return;
        $(this).children('ul').css({
            'opacity': 0,
            'margin-top': -15
        }).show().animate({
            'margin-top': 0,
            'opacity': 1
        }, 200);
    }).on('mouseleave', '> li.dropdown', function(){
        if (screenSizeType() == 'xs') return;
        $(this).children('ul').fadeOut(200, function() {
            $(this).hide();
        });
    }).on('click', 'a[href=#]', function(e){
        e.preventDefault();
    })
    // dropdown submenu
    .on('mouseenter', 'li.dropdown li.dropdown', function() {
        if (screenSizeType() == 'xs') return;
        $(this).children('ul').css({
            'opacity': 0,
            'margin-left': -30
        }).show().animate({
            'margin-left': 0,
            'opacity': 1
        }, 200);
    }).on('mouseleave', 'li.dropdown li.dropdown', function() {
        if (screenSizeType() == 'xs') return;
        $(this).children('ul').fadeOut(200, function() {
            $(this).hide();
        });
    });

    $('#main-menu .navbar-nav').on('click', 'li.dropdown > a', function(e) {
        if (screenSizeType() != 'xs') return;
        $(this).closest('li').children('ul').toggle().end().toggleClass('open');
        // $(this).closest('li').children('ul').slideToggle(100);
    });

        
    $('.polling-widget').on('submit', 'form', function(e){
        e.preventDefault(); 
        var frm = $(this);
        var pdata = frm.serialize();
        if (frm.find(':radio[name=pilihan]:checked').length < 1) {
            alert('Anda belum memilih salah satu!');
            return false;
        }
        frm.find('input, button').prop('disabled', true);
        $.post(frm.attr('action'), pdata, function(res){
            frm.find('input, button').prop('disabled', false);
            if (res.ok) {
                alert('Terima kasih telah memberikan suara dalam polling ini..');
                location.reload();
            }
        },'json');
    }).on('click', ':reset', function(e){
        $(this).closest('form').hide().prev('.polling-result').fadeIn(200);
    }).on('click', '.btn-view-pollop', function(e){
        $(this).closest('.polling-result').hide().next('form').fadeIn(200);
    });
    
    $('.search-form').on('submit', function(e){ 
       if ( $(this).find(':text[name=q]').val().trim() == '' ) {
            e.preventDefault();
       } 
    });

    var homeSliderElm = $('#home-slider');
    homeSliderElm.on('animate.caption', function(e, elm) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        setTimeout(function() {
            elm.find('.animated-caption:first').addClass('animated bounceIn').one(animationEnd, function() {
                $(this).next('.animated-caption').addClass('animated fadeInDown');
            });
        }, 500);
    }).on('slid.bs.carousel', function(e) {
        $(this).find('.animated-caption').removeClass('animated bounceIn fadeInDown').end()
            .trigger('animate.caption', [$(e.relatedTarget)]);
    }).trigger('animate.caption', [$('.item.active', homeSliderElm)]);

    
    $('.single-item-slider').owlCarousel({
        navigation : true, 
        autoPlay: 6000,
        stopOnHover: true,
        lazyLoad : false,
        slideSpeed: 700,
        pagination: true,
        paginationSpeed : 400,
        singleItem: true,
        responsiveRefreshRate: 500,
        transitionStyle: 'fadeUp',
        navigationText: ['<i class="fa fa-chevron-left"></i> PREV','NEXT <i class="fa fa-chevron-right"></i>'],
        afterInit: function(elm) { 
            elm.find('.item').show();
        }
        /*afterLazyLoad: function() {
            if (jQuery().imgLiquid) {
                $('.single-item-slider').find('.img-container').imgLiquid({ fill:true });
            }
        }*/
    });
    $('#topbar-menu-toggle').on('click', function(e) {
        e.stopPropagation();
        $('#topbar-menu').slideToggle(50).one('click', function(ee) {
            ee.stopPropagation();
        });
        $('body').one('click', function() {
            $('#topbar-menu').hide();
        });
    });
        
    $('[data-toggle=tooltip]').tooltip({container:'body'}); 

    if (jQuery().imgLiquid) {
        $('.img-container').imgLiquid({ fill:true });
    }

    $('.sidebar-slider').owlCarousel({
        navigation : false, 
        autoPlay: 6000,
        stopOnHover: true,
        lazyLoad : false,
        slideSpeed: 700,
        pagination: false,
        paginationSpeed : 400,
        singleItem: true,
    });
	
    $('.baner-slider').owlCarousel({
        navigation : false, 
        autoPlay: 6000,
        stopOnHover: true,
        lazyLoad : false,
        slideSpeed: 700,
        pagination: false,
        paginationSpeed : 400,
        singleItem: false,
    });


    // Back to top button
    // $btnBackTop.on('click', function() {
        // $('html, body').animate({ scrollTop: 0 }, 400);
    // });

})(jQuery);


// Scroll based animation
(function($) {

    var $animationElements = $('.allow-animation .animation-item'), animationApplied = false;
    var $window = $(window);
    var isResizing = false, isScrolling = false;
    
    function posInViewport($element) {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
        return (element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position);
    }

    function checkIfInView() {
        $.each($animationElements, function() {
            var $element = $(this);
            if ( posInViewport($element) ) {
                var animationType = $element.data('animationType') || 'fadeIn';
                $element.addClass('animated '+animationType).removeClass('animation-item');
            } 
        });
        animationApplied = true;
    }

    function onResizeEnd() {
        isResizing = false;
        checkIfInView();
    }

    // function onScrollEnd() {
        // isScrolling = false;
        // checkIfInView();
        // Back to top button
        // if ( $window.scrollTop() > 300 ) {
            // $btnBackTop.fadeIn(200);
        // } else {
            // $btnBackTop.fadeOut(200);
        // }
    // }

    $window.resize(function() {
        if ( ! isResizing) {
          isResizing = true;
          setTimeout(onResizeEnd, 100);
        }
    }).scroll(function() {
        if ( ! isScrolling) {
          isScrolling = true;
          setTimeout(onScrollEnd, 100);
        }
    });

    if (animationApplied == false) checkIfInView();

})(jQuery);

function screenSizeType() {
  var ww = $(window).width();
  switch(true) {
    case (ww < 768): return 'xs'; break;
    case (ww < 992): return 'sm'; break;
    case (ww < 1200): return 'md'; break;
    default: return 'lg';
  }
}