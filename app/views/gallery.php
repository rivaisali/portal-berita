<?php
if ($album = get_value('album', FALSE)) {    
    if ($photo = get_value('photo', FALSE)) {
        // Single photo
        page_title('Galeri Foto | '.config_item('site_title'));
        $qw = $this->db->get_where('foto', array('foto_id'=>$photo), 1);
        echo '<div class="page-header"><a class="pull-right btn btn-xs btn-warning" href="?album='.$album.'">
        <i class="fa fa-chevron-left"></i> Kembali</a>
            <h2>Galeri Foto</h2></div>';        
        if ($qw->num_rows() > 0) {
            $row = $qw->row();
            if ($row->judul == '') $row->judul = '(Tanpa Judul)';
            echo '<div class="img-container img-rounded">
                <img src="'.base_url('uploads/gallery/'.$row->url).'" alt="'.$row->judul.'"></div>';
            echo '<div class="space-15"></div><div class="single-img-info">'.$row->judul.'<br>
                <small class="text-muted"><i class="fa fa-clock-o"></i> Di unggah <strong>'.xtime($row->waktu).'
                </strong></small></div>';
        }
    } else {
        if ($nm_lbum = get_where_value('album', 'nm_album', array('album_id'=>$album), FALSE)) {
        echo '
			  <div class="section-title">
				<h2> Album '.$nm_lbum.'</h2>
			  </div>
					<a class="btn btn- btn-warning" href="gallery"><i class="fa fa-chevron-left"></i> Kembali</a><br><br>
			  ';
            page_title('Album '.$nm_lbum.' | '.config_item('site_title'));
            // echo '<h4 class="album-title well" style="font-size:14px;margin-bottom:15px">'.$nm_lbum.'</h4>';
				
            $qw = $this->db->order_by('foto_id DESC')->get_where('foto', array('album_id'=>$album));
            if ($qw->num_rows() > 0) {
                include_assets('colorbox');
                add_script("
                $('.img-container').colorbox({ 
                    href: function(){ return $(this).attr('data-href'); },
                    title: function(){
                      var url = $(this).attr('href');
                      var tit = $(this).attr('data-title');
                      return '<a style=\"color:#ebebeb\" href=\"' + url + '\">'+tit+'</a>';
                    },
                    rel:'img-container',maxWidth:'90%',maxHeight:'90%' 
                });");
                echo '<div class="row">';
                foreach($qw->result() as $row) {
                    $jdlfoto = $row->judul == '' ? '(Tanpa Judul)' : character_limiter($row->judul, 50);
                    $large_url = base_url('uploads/gallery/'.$row->url);
                    $thumb_url = base_url('uploads/gallery/thumbs/'.$row->url);
                    echo '<div class="col-sm-3 col-md-4">
                        <a class="img-container img-stretch img-ratio-43 img-rounded" data-title="'.$row->judul.'" data-href="'.$large_url.'" href="?album='.$row->album_id.'&photo='.$row->foto_id.'">
                            <img src="'.$thumb_url.'" alt"'.$row->judul.'" class="img-rounded">
                            <div class="img-caption text-semibold">'.$jdlfoto.'</div>
                        </a><div class="space-20"></div>
                    </div>';
                }
                echo '</div>';
            }
        } else {
            echo '<div class="alert alert-danger">Album tidak ditemukan!</div>';        
        }    
    }
} else {
    // List album
    page_title('Galeri Foto | '.config_item('site_title'));
    echo '<div class="section-title"><h2>Galeri Foto</h2></div>';
    $qw = $this->db->get('valbum');
    if ($qw->num_rows() == 0) {
        echo '<div class="alert alert-info">Belum ada album di galeri foto</div>';
    } else {
        echo '<div class="row">';
        foreach($qw->result() as $row) {
            $jdlalbum = character_limiter($row->nm_album, 50);
            echo '<div class="col-sm-6 col-md-4">
                <a class="img-container img-stretch img-ratio-43 img-rounded" data-title="'.$row->nm_album.'" href="?album='.$row->album_id.'">
                    <img src="'.base_url('uploads/gallery/thumbs/'.$row->url).'" alt"'.$row->nm_album.'" class="img-rounded">
                    <div class="img-caption text-semibold">'.$jdlalbum.' ('.$row->jum_foto.')</div>
                </a><div class="space-20"></div>
            </div>';
        }
        echo '</div>';
    }
}
?>
