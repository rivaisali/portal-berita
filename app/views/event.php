<?php
	$aksi 	= get_value("aksi");
	$id 	= get_value("id");
	switch($aksi){
		case "detail": // detail event
		$where	=	array("event_id" => $id);
		$event	=	list_event(0,$where,"row");
		if(empty($event)){
			echo '<div class="alert alert-danger">Belum ada event mendatang</div>';
		}else{
			$hari	=	nama_hari($event->waktu_mulai);
			$tgl	=	tanggal($event->waktu_mulai);
			$jam	=	jam_saja($event->waktu_mulai);
			
?>
	<header class="single-post-header clearfix">
		<h2 class="post-title"><?php echo $event->judul; ?></h2>
	</header>
	<article class="post-content">
	  <div class="event-description"> <img src="<?php echo base_url("uploads/event/".$event->gambar); ?>" class="img-responsive">
		<div class="spacer-20"></div>
		<div class="row">
		  <div class="col-md-8">
			<div class="panel panel-default">
			  <div class="panel-heading">
				<h3 class="panel-title">Event details</h3>
			  </div>
			  <div class="panel-body">
				<ul class="info-table">
				  <li><i class="fa fa-calendar"></i> <strong><?= $hari; ?></strong> | <?= $tgl; ?></li>
				  <li><i class="fa fa-clock-o"></i> <?= $jam ?></li>
				  <li><i class="fa fa-map-marker"></i> <?= $event->tempat; ?></li>
				  <li><i class="fa fa-phone"></i> <?= $event->cp; ?></li>
				</ul>
			  </div>
			</div>
		  </div>
		  <div class="col-md-4">
			<a href="<?php echo site_url("event"); ?>" class="btn btn-primary">
				Kembali
			</a>
		  </div>
		</div>
		<p>
			<?php echo $event->deskripsi; ?>
		</p>
	  </div>
	</article>
<?php			
		}
		break;
		
		default:
			$where	=	array("waktu_mulai >=" => date("Y-m-d H:i:s"));
			$event	=	list_event(0,$where);
			if(empty($event)){
				echo '<div class="alert alert-danger">Belum ada event mendatang</div>';
			}else{
?>
				<div class="listing events-listing">
				  <header class="listing-header">
					<div class="row">
					  <div class="col-md-6 col-sm-6">
						<h3>All events</h3>
					  </div>
					</div>
				  </header>
				  <section class="listing-cont">
					<ul>
						<?php 
							foreach($event as $ev){ 
								$bulan	=	nama_bulan($ev->waktu_mulai,TRUE);
								$tgl	=	substr($ev->waktu_mulai,8,2);
								$hari	=	nama_hari($ev->waktu_mulai);
								$jam	=	jam_saja($ev->waktu_mulai);
						?>
					  <li class="item event-item">
						<div class="event-date"> <span class="date"><?php echo $tgl; ?></span> <span class="month"><?php echo $bulan; ?></span> </div>
						<div class="event-detail">
						  <h4><a href="?aksi=detail&id=<?php echo $ev->event_id; ?>"><?php echo $ev->judul; ?></a></h4>
						  <span class="event-dayntime meta-data"><?= $hari; ?> | <?= $jam; ?></span> </div>
						<div class="to-event-url">
						  <div><a href="?aksi=detail&id=<?php echo $ev->event_id; ?>" class="btn btn-default btn-sm">Details</a></div>
						</div>
					  </li>
					    <?php } ?>
					</ul>
				  </section>
				</div>
<?php
			}
		break;
	}
?>