<?php page_title('Kontak Kami | '.config_item('site_title')); ?>
<div class="page-header">
    <h2>Kontak Kami</h2>
</div>
<div class="post-body">
    <p><strong>Dinas Pertanian, Tanaman Pangan dan Holtikultura Provinsi Gorontalo</strong><br />
    <?php echo config_item('owner_addr') ?><br>
    Telp. : <?php echo config_item('owner_phone') ?> &nbsp;-&nbsp; Fax : <?php echo config_item('owner_fax') ?><br>
    Email: <?php echo config_item('owner_email') ?></p>
</div>
<?php
$this->load->helper('form');
if ($resultkontak = get_flashdata('result', array())) {
    $res_type = $resultkontak['ok'] ? 'success' : 'danger';
    $res_msg = $resultkontak['ok'] ? 'Pesan anda telah disimpan dan otomatis masuk ke email kami.<br> 
        Jawaban dari kami akan dikirim ke alamat email yang telah anda masukkan.<br>Terima kasih atas partisipasinya.' : $resultkontak['msg'];
    echo '<div class="alert alert-'.$res_type.' alert-dismissable" style="margin-top:20px">'.
        $res_msg.'</div>';
}
?>
<?php if (empty($resultkontak) OR !$resultkontak['ok']) : ?>
<div class="space-20"></div>
<form method="post" action="<?php echo base_path('action/crud/insert?n=kontak') ?>" class="kontak-form">
    <p>
        <label>Nama Lengkap</label>
        <span class="form-input">
            <input type="text" name="nama" class="form-control" value="<?php echo post_data('nama') ?>" required>
        </span>
    </p>
    <p>
        <label>Email</label>
        <span class="form-input">
            <input type="text" name="email" class="form-control" value="<?php echo post_data('email') ?>" required>
        </span>
    </p>
    <p>
        <label>Subjek</label>
        <span class="form-input">
            <input type="text" name="subjek" class="form-control" value="<?php echo post_data('subjek') ?>" required>
        </span>
    </p>
    <p>
        <label>Isi Pesan</label>
        <span class="form-input">
            <textarea name="pesan" class="form-control" rows="7" maxlength="4000" required><?php echo post_data('pesan') ?></textarea>
        </span>
    </p>
    <p style="margin:15px 0 0 0">
        <span class="form-input">
            <button type="submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim Pesan</button>&nbsp;
            <button type="reset" class="btn btn-danger"><i class="fa fa-ban"></i> Batal</button>
        </span>
    </p>
    <input name="empty-field" type="text" class="hidden" value="">
    <input name="random-field" type="text" class="hidden" value="<?php echo config_item('my_random_char') ?>">
</form>
<?php endif; ?>