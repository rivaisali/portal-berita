<?php
enqueue_js('home-js', "assets/custom/home.{$jscss_version}.js", 'jquery,bootstrap,imgLiquid,owl-carousel,dsEndlessScroll2');
$fpagcnt = json_decode(config_item('front_page'), TRUE);
echo generate_frontpage_content($fpagcnt);
?>