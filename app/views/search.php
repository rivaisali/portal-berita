<?php
$kword = get_value('q', '');
page_title('Hasil Pencarian "' . $kword . '" | ' . config_item('site_title'));
add_script("
function loadHasilPencarian(tipe, hal, elm) {
    $(elm).html('<p style=\"text-align:center;padding:20px 0\"><img src=\"'+site_root+'assets/img/ajax-loader1.gif\"></p>');
    $.get(site_root+'ajax/pencarian/'+tipe+'?k=" . urlencode($kword) . "&p='+hal, function(res){
        $(elm).html(res.html);
        $('#search-total-'+tipe).text('('+res.total+')');
    }, 'json');
}
loadHasilPencarian('post', 1, '#tab-search-news');
");
?>
<div class="page-header">
    <h2>Hasil Pencarian</h2>
</div>
<div class="tab-pane active post-list post-list-1 relative-pos" id="tab-search-news" data-type="news">

</div>