<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta charset="UTF-8" />
	<title>Upload Foto</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/dropzone.css') ?>"/>
    <style>
    html, body { margin:0;padding:0;width:100%;height:100%; }
    #uploader { width:100%;height:100%; }
    </style>
</head>
<body>
	<div id="uploader" class="dropzone"></div>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/dropzone.js') ?>"></script>
    <script type="text/javascript">
    <!--
    	var myDropzone = new Dropzone('div#uploader', { 
            url: '<?php echo base_url('action/upload/gallery?album='.get_value('album', 0)) ?>',
            maxFilesize: 7,
            acceptedFiles: 'image/*'
        });
    -->
    </script>
</body>
</html>