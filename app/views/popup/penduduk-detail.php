<?php 
if ( !$this->input->is_ajax_request() || !get_value('id') ) exit; 
$row = $this->db->get_where('personil', array('pers_id'=>get_value('id')))->row();
?>
<div class="detail-wrap">
    <h2>Detail Personil</h2>
    <img src="<?php echo foto_thumb_personil_url($row->foto) ?>">
    <p>
        <label>NIP</label>
        <span><?php echo $row->nip ?></span>
    </p><p>
        <label>Nama</label>
        <span><?php echo $row->nama ?></span>
    </p><p>
        <label>Tempat, Tanggal Lahir</label>
        <span><?php echo $row->tmp_lahir.', '.tanggal($row->tgl_lahir) ?></span>
    </p><p>
        <label>Jabatan Sekarang</label>
        <span><?php echo $row->jabatan ?></span>
    </p><p>
        <label>Riwayat Jabatan</label>
        <span><?php echo nl2br($row->riw_jab) ?></span>
    </p><p>
        <label>Pendidikan Terakhir</label>
        <span><?php echo $row->pend_jenj.' '.$row->pend_jur.' '.$row->pend_univ.' '.$row->pend_thn ?></span>
    </p><p>
        <label>Pangkat / Golongan</label>
        <span><?php echo $row->pangkat_gol ?></span>
    </p><p>
        <label>Penghargaan</label>
        <span><?php echo $row->penghargaan ?></span>
    </p>
</div>