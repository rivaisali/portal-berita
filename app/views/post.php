<?php 
add_style('.input-group-btn .btn { border-width:1px; }');
$type_opts = $this->post->get_registered_types($post_type);
if (isset($post_slug)) :
if ($post_slug == '') {
    // indeks berita
    if ($type_opts['has_archive']) {
        enqueue_js('moment', NULL, 'jquery');
        enqueue_js('daterangepicker', NULL, 'moment');
        enqueue_css('daterangepicker', NULL, 'bootstrap');
        add_script("$('#filter-by-date').daterangepicker(
            {
              ranges: {
                 'Hari Ini': [moment(), moment()],
                 'Kemarin': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 '7 Hari Terakhir': [moment().subtract('days', 6), moment()],
                 'Sebulan Terakhir': [moment().subtract('days', 29), moment()],
                 'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                 'Bulan Lalu': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              format: 'DD/MM/YYYY', showDropdowns: true,
              startDate: moment().subtract('days', 29),
              endDate: moment()
            }
        );");
        $idx_page = get_value('p', 1);
        $pparems = array('perpage'=>20, 'page'=>$idx_page);
        if ($cat = get_value('cat')) 
            $pparems['cat_slug'] = $cat;
        $fdate = get_value('date', '');
        if (preg_match('/([0-9]{2})\/([0-9]{2})\/([0-9]{4})\ \-\ ([0-9]{2})\/([0-9]{2})\/([0-9]{4})/', 
            urldecode($fdate), $match)) {
            $date1 = $match[3].'-'.$match[2].'-'.$match[1]; 
            $date2 = $match[6].'-'.$match[5].'-'.$match[4]; 
            $pparems['date'] = array($date1, $date2);
        }        
        page_title('Daftar '.$type_opts['label'].' | '.config_item('site_title'));
        echo '<div class="section-title"><h2>Daftar '.$type_opts['label'].'</h2></div>';
        echo '<form method="get" action="" id="post-filter-form" class="row">
            <div class="col-sm-6">'.
                categories_dropdown($post_type, get_value('cat', ''), array('class'=>'form-control',
                    'first_label'=>'&mdash; Pilih Kategori &mdash;', 'name'=>'cat', 'id'=>'filter-by-cat',
                    'value_field'=>'slug')).'
                <div class="space-10 visible-xs"></div>
            </div><div class="col-sm-6">
                <div class="input-group">
                          <input type="text" name="date" id="filter-by-date" class="form-control" 
                          placeholder="&mdash; Rentang waktu &mdash;" value="'.get_value('date', '').'" readonly>
                          <div class="input-group-btn">
                              <button type="submit" class="btn btn-primary"><i class="fa fa-filter"></i> Filter</button>
                          </div>
                        </div>
            </div>';
        echo '</form><div class="space-25"></div><br><br>';
        $posts = $this->post->get_result($post_type, $pparems);
        if ($posts['data_total'] == 0) {
            echo '<div class="alert alert-info"><i class="fa fa-info-sign"></i> '.
                $type_opts['label'].' tidak ditemukan!</div>';
        } else { 
            //echo '<div class="post-list post-list-1 clearfix"><ul class="media-list">';
            foreach ($posts['rows'] as $post) {
                $post_url = "{$base_url}{$post['post_type']}/{$post['post_slug']}";
                $post_summary = character_limiter(strip_tags($post['post_content']), 150);
                echo '<div class="posts-archive">';
                echo '<article class="post">';
				echo '<div class="row">';
				echo '<div class="col-md-4 col-sm-4">';
                if ( !empty($post['post_thumb']) ) {
                    echo '<a href="'.$post_url.'">
                            <img src="'.get_post_thumb_url_user($post['post_thumb']).'" class="img-thumbnail">
                        </a>';
                }
				echo '</div>';
				echo '<div class="col-md-8 col-sm-8">';
				echo '<h5>'.$post['post_title'].'</h5>';
				echo '<span class="post-meta meta-data"><span><i class="fa fa-clock-o"></i> '.xtime($post['post_date']).'</span>';
				echo '<span><i class="fa fa-user"></i> '.$post['namauser'].'</span></span>';
				echo '<p class="post-summary" style="margin-bottom:5px">'.$post_summary.'</p>';
				echo '<p><a href="'.$post_url.'" class="btn btn-primary">Continue reading <i class="fa fa-long-arrow-right"></i></a></p>';
				echo '</div>';
				echo '</div>';
				echo '</article>';
				echo '</div>';
            }
            //echo '</ul></div>';
            echo generate_paginiation_link_user($posts['page'], $posts['page_total'], 7);
        }
    } else {
        include_view('_404');
    }
} elseif ($post_slug == 'preview') {
    if (!empty($admin)) {
        add_style('.page-header h2 { line-height:1.3; } .page-header small { text-transform:none; }');
        if ($cache_id = get_value('id')) {
           $ci = &get_instance();
           $ci->load->driver('cache');
           if ($post = $ci->cache->file->get('postprev-'.$cache_id)) {
                page_title('(Preview) '.$post['post_title'].' | '.config_item('site_title'));
                echo '<div class="page-header"><h2>'.$post['post_title'].'</h2></div>';
                if ( !empty($post['post_thumb']) ) {
                    echo '<div class="img-container img-rounded post-thumbnail"><img src="'.$post['post_thumb'].'"></div>';
                    echo '<div class="space-20"></div>';
                }
                echo '<div class="post-body">'.$post['post_content'].'</div>';
                add_content('foot', '<div style="position:fixed;top:0;left:0;color:#fff;background:#E64A4A;
                    padding:10px 25px;font-size:24px"><i class="fa fa-search"></i> Preview</div>');
                add_script("$('a, input, button').on('click', function(e){ e.preventDefault(); });");
           } else {
               include_view('_404');
           }              
        }        
    } else {
        include_view('_404');
    }
} else {
    add_style('.page-header h2 { line-height:1.3; } .page-header small { text-transform:none; }
        .post-body ol, .post-body ul { padding-left:15px;margin:0;line-height:1.6; }
        .post-body h4 { margin:15px 0 10px;font-size:15px; }');
    $post = $this->post->get_row($post_slug);
    if (!$post) {
        include_view('_404');
    } else { 
        $allow_comment = in_array($post['post_type'], config_item('post_type_comment'));
        $post_summary = character_limiter(strip_tags($post['post_content']), 150, '');
        page_title($post['post_title'].' | '.config_item('site_title'));
        page_description($post_summary);
		echo '<header class="single-post-header clearfix">';
			echo '<h2 class="post-title">'.$post['post_title'].'</h2>';
				// echo '<img src="'.$post['post_thumb'].'" alt="'.$post['post_title'].'">';
		echo '</header>';
		// echo '<div class="blog-item-one">';
			// if ( ! empty($post['post_thumb'])) {
				// echo '<div class="image-box"><figure>';
				// echo '<img src="'.$post['post_thumb'].'" alt="'.$post['post_title'].'">';
				// echo '</figure></div>';
			// }
        echo '<article class="post-content">';
        if ($type_opts['has_archive']) {
			echo '<span class="post-meta meta-data">
					<span><i class="fa fa-clock-o"></i>'.hari_tanggal_jam($post['post_date']).'</span> 
					<span><i class="fa fa-user"></i>'.$post['namauser'].'</span>
				  </span>';
			if ( ! empty($post['post_thumb'])) {
				echo '<div class="featured-image">';
				echo '<img src="'.$post['post_thumb'].'" alt="'.$post['post_title'].'">';
				echo '</div>';
			}
        }
		echo $post['post_content'];
        echo '</article>';
        // if ( ! empty($post['post_thumb'])) {
            // echo '<div class="img-container img-rounded animation-item" data-animation-type="zoomIn"><img src="'.$post['post_thumb'].'" alt="'.$post['post_title'].'"></div>';
            // echo '<div class="space-15"></div>';
            // page_imageurl($post['post_thumb']);
        // }
        echo '<!-- AddToAny BEGIN -->
            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
            <a class="a2a_dd" href="https://www.addtoany.com/share?linkurl='.urlencode(current_url()).'&amp;linkname='.urlencode($post['post_title']).'"></a>
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
            <a class="a2a_button_google_plus"></a>
            </div>
            <script>
            var a2a_config = a2a_config || {};
            a2a_config.linkname = "'.$post['post_title'].'";
            a2a_config.linkurl = "'.current_url().'";
            a2a_config.locale = "id";
            a2a_config.prioritize = ["whatsapp", "email", "google_gmail", "line", "telegram", "yahoo_mail"];
            </script>
            <script async src="https://static.addtoany.com/menu/page.js"></script>
            <!-- AddToAny END -->';
        if ($allow_comment) {
            echo '<div class="space-20"></div>
                <div class="fb-comments" data-href="'.current_url().'" data-width="100%" data-numposts="10"></div>
                <div id="fb-root"></div>';
        }
        $this->db->where('post_slug', $post_slug);
        $this->db->set('views_count', 'views_count+1', FALSE);
        $this->db->update('post');
        if ($allow_comment) {
            add_script("(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = \"//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.7&appId=".config_item('facebook_appid')."\";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));");
        }
    }
}

endif;