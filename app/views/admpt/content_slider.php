<?php
if (!has_akses('cntslider-view')) :
    include_view('admin_noaccess');
else:
    $action = get_value('a');
    $id = (int) get_value('id');
    if ($action === 'add' OR $action === 'edit') {
        if (!has_akses('cntslider-'.$action)) {
            include_view('admin_noaccess');
            return;
        }
        $action_label ='Tambah';
        $submit_label = 'Tambahkan';
        $row = array('judul'=>'','deskripsi'=>'','link'=>'');
        $prev_imgsrc = '';
        if ($action === 'edit') {
            $action_label = 'Edit';
            $submit_label = 'Update Data';
            $row = $this->db->get_where('content_slider', array('ID'=>$id))->row_array();
            $prev_imgsrc = $row['nama_file'];
        }
        set_breadcumb('content-slider|Slider Konten','add|'.$action_label);
        page_header('<i class="fa fa-picture-o"></i> '.$action_label.' Slider Konten <a href="content-slider" class="btn-add">Lihat Data</a>');
        include_assets('cropit'); 
        enqueue_js('dsFunction', NULL, 'bootstrap');
        enqueue_js('contentslider-adm', 'assets/custom/contentslider-adm.js', 'cropit,dsFunction');
        echo '<div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div id="post-result" style="display:none"></div>
                    <form method="post" action="'.base_url('ajax/contentslider/'.$action).'" class="main-form">
                    <input type="hidden" name="id" value="'.$id.'">
                    <input type="hidden" name="newimg" value="">
                    <input type="hidden" name="prev_imgsrc" value="'.$prev_imgsrc.'">
                    <div id="image-cropper">
                        <div class="cropit-preview-container"><div class="cropit-preview"></div></div>  
                        <div class="row" style="margin:10px -15px 0">
                            <div class="col-xs-6 col-sm-4 col-md-2">
                                <button type="button" class="cropit-select-btn btn-success btn-sm btn btn-block">
                                    <i class="fa fa-picture-o"></i> Pilih Foto</button>  
                            </div><div class="col-xs-6 col-sm-8 col-md-10">
                                <input type="range" class="cropit-image-zoom-input" />
                            </div>
                        </div>            
                        <input type="file" name="foto" class="cropit-image-input" />
                        <textarea name="foto_dataurl" style="display:none"></textarea>
                    </div>
                    <div style="margin:20px 7px 7px">
                        <div class="row">
                            <div class="col-sm-3 col-lg-2">Judul</div>
                            <div class="col-sm-9 col-lg-10">
                            <textarea name="judul" class="form-control" rows="3" style="resize:vertical">'.$row['judul'].'</textarea>
                            </div>
                        </div>
                        <div class="row mtop-10">
                            <div class="col-sm-3 col-lg-2">Deskripsi</div>
                            <div class="col-sm-9 col-lg-10">
                                <textarea name="deskripsi" class="form-control" rows="4" style="resize:vertical">'.$row['deskripsi'].'</textarea>
                            </div>
                        </div>
                        <div class="row mtop-10">
                            <div class="col-sm-3 col-lg-2">Link</div>
                            <div class="col-sm-9 col-lg-10">
                                <input type="text" name="link" class="form-control" value="'.$row['link'].'">
                            </div>
                        </div>
                        <div class="row mtop-10">
                            <div class="col-sm-offset-3 col-lg-offset-2 col-sm-9 col-lg-10">
                                <button class="btn btn-primary btn-lg" type="submit" style="margin-top:10px">
                                    <i class="fa fa-check"></i> '.$submit_label.'</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>';
        
    } else {
        $this->load->helper('file');
        set_breadcumb('content-slider|Slider Konten');
        enqueue_js('sortable', 'assets/js/sortable.min.js', 'bootstrap');   
        enqueue_js('contentslider-adm', 'assets/custom/contentslider-adm.js', 'sortable');

        page_header('<i class="fa fa-picture-o"></i> Slider Konten <a href="?a=add" class="btn-add">Tambah Data</a>');
        echo '<div class="col-xs-12"><div class="box box-primary"><div class="box-body" style="padding:20px 17px 7px">';
        $rows = list_content_slider();
        if (count($rows) > 0) {
            $imgdir = base_url('uploads/content-slider').'/';
            echo '<div class="row sortable" id="contentslider-sortable">';
            foreach ($rows as $row) {
                echo '<div class="col-sm-6 col-md-4 sortable-item" data-id="'.$row['ID'].'" data-filename="'.$row['nama_file'].'">
                    <div class="sortable-item-wrap">';
                echo '<div class="pull-right btns-area">
                        <!--<a href="#" class="btn btn-sm btn-info btn-preview"><i class="fa fa-search"></i> Preview</a>-->
                        <a href="?a=edit&id='.$row['ID'].'" class="btn btn-sm btn-warning btn-edit"><i class="fa fa-edit"></i> Edit</a>
                        <a href="#" class="btn btn-sm btn-danger btn-delete"><i class="fa fa-trash-o"></i> Hapus</a>
                    </div>';
                echo '<img src="'.$imgdir.add_filename_prefix($row['nama_file'],'_thumb').'" class="img-responsive"/>';
                echo '</div></div>';
            }
            echo '</div>';
            echo '<button class="btn btn-success mtop-5 mbottom-10" style="display:none" type="button" id="btn-savesort">
                <i class="fa fa-floppy-o"></i> Simpan Perubahan</button>';
        }
        echo '</div></div></div>';
    }
 endif; 