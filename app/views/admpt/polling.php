<?php
if (!has_akses('polling-view')) {
    include_view('admin_noaccess');
} else {
    
$action = get_value('a');
switch($action):
case 'add': case 'edit':
    if (!has_akses('polling-add') && $action=='add') {
        include_view('admin_noaccess'); 
        break;
    }  
    if (!has_akses('polling-edit') && $action=='edit') {
        include_view('admin_noaccess'); 
        break;
    }       
    enqueue_js('polling-adm', 'assets/custom/polling-adm.js', 'jquery,bootstrap');
    $opsi_items = '<div class="input-group poll-opsi-group">
        <input type="text" name="opsi_baru[]" class="form-control poll-opsi">    
        <div class="input-group-btn">    
            <button type="button" class="btn btn-danger btn-del-opsi" title="Hapus Pilihan"><i class="glyphicon glyphicon-remove"></i></button>   
            <button type="button" class="btn btn-info btn-add-opsi" title="Tambah Pilihan"><i class="glyphicon glyphicon-plus"></i></button>
        </div></div>';
    if ($action == 'add') {
        $submit_action = 'insert?n=polling';  
        $submit_label = 'ok#Tambahkan Polling';  
        page_header('<i class="fa fa-bar-chart-o"></i> Tambah Polling <a href="polling" class="btn-add">Lihat Daftar</a>');
        set_breadcumb('polling|Polling', 'tambah|Tambah baru');        
        $row = array('poll_id'=>'0', 'pertanyaan'=>'');        
    } else { 
        page_header('<i class="fa fa-bar-chart-o"></i> Edit Polling <a href="polling" class="btn-add">Lihat Daftar</a>');
        set_breadcumb('polling|Polling', 'edit|Edit');  
        $submit_label = 'ok#Update Polling'; 
        if ($poll_id = get_value('id')) {
            $submit_action = 'update?n=polling&id='.$poll_id;
            $qw = $this->db->get_where('polling', array('poll_id'=>$poll_id), 1);
            if ($qw->num_rows() == 0) redirect($user_type.'/polling');
            $row = $qw->row_array();
            $get_opsi = $this->db->get_where('polling_opsi', array('poll_id'=>$poll_id));
            $opsi_items = '';
            foreach ($get_opsi->result() as $dbopsi) {
                $opsi_items.= '<div class="input-group poll-opsi-group">
                    <input type="text" name="opsi_lama['.$dbopsi->opsi_id.']" value="'.$dbopsi->opsi.'" 
                       data-id="'.$dbopsi->opsi_id.'" class="form-control poll-opsi poll-opsi-lama">    
                    <div class="input-group-btn">    
                        <button type="button" class="btn btn-danger btn-del-opsi" title="Hapus Pilihan"><i class="glyphicon glyphicon-remove"></i></button>   
                        <button type="button" class="btn btn-info btn-add-opsi" title="Tambah Pilihan"><i class="glyphicon glyphicon-plus"></i></button>
                    </div></div>';
            }
        }
    }
    if ($post_data = get_flashdata('post_data')) $row = array_merge($row, $post_data);
?>
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body table-responsive">
    <?php
    if ($result = get_flashdata('result')) {
        $res_type = $result['ok'] ? 'success' : 'danger';
        echo '<div class="alert alert-'.$res_type.' alert-dismissable" style="margin:15px 15px 0">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
            $result['msg'].'</div>';
    }
    $this->form->open('action/crud/'.$submit_action,'class="form-horizontal data-form"')
    ->ftextarea('pertanyaan', $row['pertanyaan'], 'Pertanyaan', array('style'=>'width:600px;max-width:100%;resize:none'))
    ->add_row('opsi', 'Pilihan', '<div id="poll-opsi-area">'.$opsi_items.'</div>')
    ->hidden('poll_id', $row['poll_id'])
    ->hidden('opsi_to_delete', '')
    ->fsubmit('simpan', $submit_label)    
    ->close();   
    ?>
            </div>
        </div>
    </div>
<?php
break;
default:
    $headertxt = '<i class="fa fa-bar-chart-o"></i> Polling';
    if (has_akses('polling-add'))
        $headertxt.= ' <a href="?a=add" class="btn-add">Tambah Polling</a>';
    page_header($headertxt);
    set_breadcumb('poll|Polling');
    include_assets('colorbox','dsTable');
    add_style('#banner-polling-inline input, #polling-form-inline textarea { margin-bottom:15px; }');
    enqueue_js('polling-adm', 'assets/custom/polling-adm.js', 'jquery,bootstrap,colorbox,dsTable');
    $editaction = has_akses('polling-edit') ? "editAction: '?a=edit&id='," : '';
    $delaction = has_akses('polling-del') ? "deleteAction: site_root + 'action/crud/delete?n=polling&id='," : '';
    $thactwi = $delaction && $editaction ? 70 : 35;
    add_script("var mainTable = $('#data-table');
    if (jQuery().dsTable) {
        mainTable.dsTable({
            dataSource: site_root+'ajax/dstable/polling',
            columns: [
                { name:'rowNumber', label:'No.', width:45, align:'center' },
                { name:'pertanyaan', label:'Pertanyaan Polling' },
                { name:'total_suara', label:'Total Suara', width:120, align:'center' }
            ],
            {$editaction} {$delaction}
            thActionWidth: {$thactwi},
            showFooter: false,
            perPage: 20,
            orderBy: 'poll_id DESC'
        });
    }");
    ?>
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body table-responsive">
                <table id="data-table" class="table table-bordered table-striped"></table>
            </div>
        </div>
    </div>
<?php
endswitch;

}
?>