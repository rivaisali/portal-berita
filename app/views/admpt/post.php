<?php
function post_status_label($post_status = 'draft') {
    $bgcolor = $post_status == 'publish' ? 'green' : 'yellow';
    $label = $post_status == 'publish' ? 'Terbit' : 'Draft';
    return '<i class="fa fa-dot-circle-o"></i> Status: <span class="badge bg-'.$bgcolor.'">'.$label.'</span>';    
}
$curr_type  = get_value('t', 'unknown');
$action     = get_value('a');
$post_type  = get_post_types($curr_type);
if (!$post_type) {
    include_view('admin_404');
} else { 
    switch($action):
    case 'add': case 'edit':
    $curr_year = date('Y'); 
    $next_year = $curr_year + 1;
    enqueue_js('tinymce', 'assets/tinymce/tinymce.min.js');
    add_script("tinymce.init({
        selector: '#post-content',
    	menubar: false,
    	relative_urls: false,
    	remove_script_host: false,
    	image_advtab: true,
        document_base_url: '{$base_url}',            
        external_filemanager_path: '{$base_url}filemanager/',
        filemanager_title: 'File Manager',
        external_plugins: { 'filemanager' : '{$base_url}filemanager/plugin.min.js'},
    	plugins: 'paste advlist charmap textcolor hr print image media searchreplace table contextmenu fullscreen preview code link pagebreak wordcount emoticons',
    	toolbar: 'styleselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | link unlink | bullist numlist | image media | pagebreak table hr | charmap emoticons | searchreplace  print | undo redo | fullscreen preview code',
     });");
    include_assets('datetimepicker');
    enqueue_js('dsFunction', NULL, 'jquery,bootstrap');
    enqueue_js('post-lib', NULL, 'jquery,bootstrap,tinymce,dsFunction');  
    if ($action == 'add') {
        page_header('<i class="fa '.$post_type['icon'].'"></i> '.$post_type['add_new_label']);
        set_breadcumb('post?t='.$post_type['name'].'|'.$post_type['label'], 'add|Tambah');
        add_style('#post-slug-edit-area, #btn-delete-post {display:none}');
        $publish_label = 'Terbitkan';
        $reset_label = '<i class="fa fa-ban"></i> Batal';
        $form_data = array('post_title'=>'', 'post_content'=>'', 'post_slug'=>'', 'post_date'=>'Otomatis',
            'post_type'=>$curr_type, 'post_status'=>'draft', 'post_id'=>0, 'post_thumb'=>'', 'category_ids'=>array()); 
    } else {
        $headertxt = '<i class="fa '.$post_type['icon'].'"></i> '.$post_type['edit_label'];
        if ( (has_akses('berita-add') && $curr_type == 'post') || 
             (has_akses('halaman-add') && $curr_type == 'page') ||
             (has_akses('pengumuman-add') && $curr_type == 'pengumuman') ) {
            $headertxt.= ' <a href="post?t='.$curr_type.'&a=add" class="btn-add">Tambah Baru</a>';
        }
        page_header($headertxt);
        set_breadcumb('post?t='.$post_type['name'].'|'.$post_type['label'], 'edit|Edit');        
        $post_id = get_value('id');
        $form_data = $this->post->get_row($post_id, "type={$curr_type}&status=all"); 
        $publish_label = $form_data['post_status'] == 'draft' ? 'Terbitkan' : 'Update';
        $reset_label = '<i class="fa fa-trash-o"></i> Hapus';
    }
    $post_date_text = $form_data['post_date']=='Otomatis' ? 'Otomatis' : 
        date('j M Y - H:i', strtotime($form_data['post_date'])); 
    if ($result = get_flashdata('post_submit_result')) {
        echo '<div class="alert alert-success alert-dismissable" style="margin-left:15px;margin-right:15px">
            <i class="glyphicon glyphicon-info-sign"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
            $result['msg'].'</div>';
    }
?>
    <form id="post-form" method="post" action="<?php echo base_path("action/post/submit-post") ?>" enctype="multipart/form-data">
        <div class="col-lg-9">
            <!-- post title, slug & content fields -->
            <div class="box box-primary">
                <div class="box-body pad">
                    <input type="text" class="form-control input-lg" name="post-title" id="post-title" value="<?php echo $form_data['post_title'] ?>" 
                        placeholder="Judul <?php echo $post_type['label'] ?>" autocomplete="off" style="margin-bottom:15px" required>
                    <div id="post-slug-edit-area" class="permalink-edit" style="margin:-5px 0 15px 3px;font-size:13px">
                        <strong>Link: </strong>
                        <span class="text-muted permalink-text">
                            <?php echo "{$base_url}{$curr_type}" ?>/<span id="slug-text"><?php echo $form_data['post_slug'] ?></span>
                        </span>
                        <input type="text" name="post-slug" id="post-slug" value="<?php echo $form_data['post_slug'] ?>" style="width:200px;display:none">            
                        <button type="button" class="btn btn-xs btn-default" id="btn-edit-slug">Edit</button>
                        <button type="button" class="btn btn-xs btn-default" id="btn-cancel-edit-slug" style="display:none">Batal</button>
                    </div>
                    <textarea name="post-content" id="post-content" style="width:100%;height:450px"><?php echo $form_data['post_content'] ?></textarea>
                    <input type="hidden" name="post-type" id="post-type" value="<?php echo $curr_type ?>">
                    <input type="hidden" name="post-status" id="post-status" value="<?php echo $form_data['post_status'] ?>">
                    <input type="hidden" name="post-id" id="post-id" value="<?php echo $form_data['post_id'] ?>">
                </div>
            </div>
        </div>
        <div class="col-lg-3 right-panel">
            <!-- post menu box -->
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h3 class="box-title box-title-sm"><i class="fa fa-bars"></i> Menu</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-primary btn-sm" data-widget="collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="post-menu-item">
                        <?php echo post_status_label($form_data['post_status']) ?>
                    </div>
                    <div class="post-menu-item">
                        <i class="fa fa-calendar"></i> Waktu:
                        <strong id="post-date-text">
                            <?php echo $post_date_text; ?>
                        </strong>
                        <button id="btn-edit-post-date" type="button" class="btn btn-xs btn-default">Edit</button>  
                        <div id="post-date-area" class="input-group input-group-sm" style="margin-top:10px;display:none">
                            <input id="post-date-edit" type="text" value="<?php echo date('j M Y - H:i') ?>" class="form-control" style="cursor:pointer" readonly>                            
                            <span class="input-group-btn">
                                <button id="btn-set-post-date" class="btn btn-info btn-flat" type="button">OK</button>
                            </span>                            
                        </div>  
                        <input type="hidden" id="default-post-date-text" value="<?php echo $post_date_text; ?>">
                        <input type="hidden" name="post-date" id="post-date" value="<?php echo $form_data['post_date'] ?>">  
                        <input type="hidden" id="default-post-date" value="<?php echo $form_data['post_date'] ?>">
                    </div>
                </div>
                <div class="box-footer" id="post-menu-btn-area">
                    <div class="btn-group">
                        <button name="btn-submit-post" id="btn-publish-post" value="publish" type="submit" class="btn btn-success">
                            <i class="fa fa-check"></i> <?php echo $publish_label ?></button>
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu dropdown-btn" role="menu">
                            <li><button id="btn-save-post" name="btn-submit-post" value="draft" type="submit">
                                <i class="fa fa-save"></i> Simpan Draft</button></li>
                            <li><button data-posttype="<?php echo $curr_type ?>" id="btn-preview-post" name="btn-preview-post" type="button">
                                <i class="fa fa-search"></i> Preview</button></li>
                        </ul>
                    </div>    
                    <a id="btn-delete-post" class="btn btn-danger" href="<?php echo base_url('action/post/trash-post?id='.
                        $form_data['post_id'].'&redirect='.uri_string()."?t={$curr_type}") ?>">
                        <i class="fa fa-trash-o"></i> Hapus</a>
                </div>
            </div>
        <?php if ($post_type['cat_support']): ?>
            <!-- box pilih kategori post (jika support) -->
            <div class="box box-solid box-warning">
                <div class="box-header">
                    <h3 class="box-title box-title-sm"><i class="fa fa-check-square-o"></i> <?php echo $post_type['cat_label'] ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-warning btn-sm" data-widget="collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body cat-checkbox-area">
                    <div class="box-area">
                        <?php echo categories_checkbox($curr_type, $form_data['category_ids'], array('name'=>'post-cats[]')) ?>
                    </div>
                </div>
                <div class="box-footer" id="new-tags-area">
                    <div id="new-cat-area" style="display:none">
                        <p>
                            <input class="form-control input-sm" name="new-cat" id="new-cat">
                        </p><p>
                            <?php echo categories_dropdown($curr_type, '', 
                                array('class'       => 'form-control',
                                      'first_label' => '&mdash; Kategori Induk &mdash;',
                                      'name'        => 'new-cat-parent',
                                      'id'          => 'new-cat-parent',
                                      'exclude_def' => TRUE)) ?>    
                        </p>
                    </div>
                    <button type="button" class="btn btn-sm btn-default" id="btn-add-cat">
                        <i class="fa fa-plus"></i> Tambah Kategori</button>     
                    <button type="button" class="btn btn-danger btn-sm" id="btn-cancel-add-cat" style="display:none">
                        <i class="fa fa-ban"></i> Batal</button>   
                </div>
            </div>
        
        <?php endif;  if ($post_type['tag_support']): ?>   
            <!-- box pilih tag post (jika support) -->
            <div class="box box-solid box-success">
                <div class="box-header">
                    <h3 class="box-title box-title-sm"><i class="fa fa-tags"></i> <?php echo $post_type['tag_label'] ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-success btn-sm" data-widget="collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body tags-area">
                    <input type="hidden" name="post-tags" id="post-tags" value="">
                </div>
                <div class="box-footer">
                    <div class="input-group input-group-sm">
                        <input name="new-tags" id="new-tags" type="text" class="form-control" placeholder="Tags..">
                        <span class="input-group-btn">
                            <button name="btn-add-tags" id="btn-add-tags" class="btn btn-default btn-flat" type="button">
                                <i class="fa fa-plus"></i> Tambah</button>
                        </span>
                    </div>
                    <em class="help-block">Batasi tiap tag dengan koma</em>
                </div>
            </div>
        <?php endif; if ($post_type['thumb_support']): ?>
            <!-- box tambah thumbnail post (jika support) -->
            <div class="box box-solid box-danger">
                <div class="box-header">
                    <h3 class="box-title box-title-sm"><i class="fa fa-picture-o"></i> Gambar</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-danger btn-sm" data-widget="collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body post-thumb-area">
                    <?php
					echo $form_data['post_title'];
                    $msg_thumb_empty = $form_data['post_thumb'] == '' ? '' : ' style="display:none"';
                    echo '<p class="text-center text-muted msg-empty-humb"'.$msg_thumb_empty.'><em>(Pilih gambar yang mewakili post)</em></p>';
                    $thumb_preview = $form_data['post_thumb'] != '' ? '' : ' style="display:none"';
                    echo '<img id="post-thumb-preview" src="'.$form_data['post_thumb'].'" class="img-responsive"'.$thumb_preview.'>';
                    ?>    
                    <input type="hidden" name="post-thumb" id="post-thumb" value="<?php echo $form_data['post_thumb'] ?>">
                </div>
                <div class="box-footer">
                    <button<?php if($form_data['post_thumb']!='') echo ' style="display:none"';?>
                        id="btn-add-thumb" type="button" class="btn btn-sm btn-default">
                        <i class="fa fa-plus"></i> Pilih Foto</button>
                    <button<?php if($form_data['post_thumb']=='') echo ' style="display:none"';?>
                        id="btn-del-thumb" type="button" class="btn btn-sm btn-danger">
                        <i class="fa fa-trash-o"></i> Hapus Foto</button>
                </div>
            </div>
        <?php endif; ?>
        </div>
        <input type="hidden" name="post-type-label" value="<?php echo $post_type['label'] ?>">
        <input type="hidden" name="redirect" value="<?php echo current_url()."?t={$curr_type}" ?>">
    </form>
<?php 
    break;
    case 'category':
        page_header('<i class="fa '.$post_type['icon'].'"></i> Kategori '.$post_type['label']);
        set_breadcumb('post?t='.$curr_type.'|'.$post_type['label'], 'category|Kategori');
        enqueue_js('category-adm', 'assets/custom/category-adm.js', 'bootstrap');
?>
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-body table-responsive">
                <table id="cats-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width:270px;min-width:250px">Nama Kategori</th>
                            <th style="min-width:200px">Deskripsi</th>
                            <th style="text-align:center;width:65px"><?php echo $post_type['label'] ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>       
        </div>
    </div>
    <div class="col-md-4 right-panel">
        <div class="box box-solid box-warning">
            <div class="box-header">
                <h3 id="category-form-title" class="box-title box-title-sm">
                    <i class="fa fa-check-square"></i> Tambah Kategori Baru</h3>
            </div>
            <div class="box-body">                
                <form method="post" action="<?php echo base_url('action/post/add-category') ?>" id="category-form">
                    <p>
                        <label for="inama">Nama Kategori</label>
                        <input name="cat_name" id="inama" type="text" class="form-control" required>
                    </p><p>
                        <label for="idescription">Deskripsi</label>
                        <textarea name="description" id="idescription" class="form-control" rows="4" style="resize:vertical"></textarea>
                    </p><p>
                        <label for="iparent">Kategori Induk</label>
                        <select name="cat_parent" id="iparent" class="form-control"></select> 
                    </p><p style="padding-top:10px">
                        <input type="hidden" name="post_type" value="<?php echo $curr_type ?>">
                        <button type="submit" class="btn btn-success">
                            <i class="glyphicon glyphicon-plus"></i> Tambahkan Kategori</button>
                        <button type="reset" class="btn btn-danger">
                            <i class="glyphicon glyphicon-ban-circle"></i> Batal</button>
                    </p>
                    <input type="hidden" name="post-type" id="post-type" value="<?php echo $curr_type ?>">
                </form>
            </div>
        </div>
    </div>
<?php
    break;
    default:
        $headertxt = '<i class="fa '.$post_type['icon'].'"></i> '.$post_type['label'];
        if ( (has_akses('berita-add') && $curr_type == 'post') || 
             (has_akses('halaman-add') && $curr_type == 'page') ||
             (has_akses('pengumuman-add') && $curr_type == 'pengumuman') ) {
            $headertxt.= ' <a href="post?t='.$curr_type.'&a=add" class="btn-add">Tambah Baru</a>';
        }
        page_header($headertxt);
        set_breadcumb('view|'.$post_type['label']);
        enqueue_js('dsFunction', NULL, 'jquery,bootstrap');
        enqueue_js('post-lib', NULL, 'jquery,bootstrap,dsFunction');  
?>
    <div class="col-xs-12">
    <div class="box box-primary">
    <?php      
    function is_acttabstat($stat = '') {
        $fpost_status = get_value('st', 'all');  
        if ($fpost_status == $stat)
            echo ' class="active"';
    }
    ?>
        <ul class="nav nav-pills box-tab box-tab-small data-switch" style="border-bottom:solid 1px #dcdcdc">
          <li<?php is_acttabstat('all') ?>><a href="?t=<?php echo $curr_type ?>">
                <i class="fa fa-list-alt"></i>Semua (<?php echo get_post_count($curr_type, 'all') ?>)</a></li>        
          <?php
          $published_count = get_post_count($curr_type, 'publish');
          if ($published_count > 0) {
             echo '<li'; is_acttabstat('publish');
             echo '><a href="?t='.$curr_type.'&st=publish">
                <i class="fa fa-check-square-o"></i>Terbit ('.get_post_count($curr_type, 'publish').')</a></li>';
          }
          $draft_count = get_post_count($curr_type, 'draft');
          if ($draft_count > 0) {
             echo '<li'; is_acttabstat('draft');
             echo '><a href="?t='.$curr_type.'&st=draft">
                <i class="fa fa-save"></i>Draft ('.get_post_count($curr_type, 'draft').')</a></li>';
          }
          $trashed_count = get_post_count($curr_type, 'trash');
          if ($trashed_count > 0) {
             echo '<li'; is_acttabstat('trash');
             echo '><a href="?t='.$curr_type.'&st=trash">
                <i class="fa fa-trash-o"></i>Terhapus ('.get_post_count($curr_type, 'trash').')</a></li>';
          }
          $fpost_status = get_value('st', 'all');  
          ?>          
        </ul>
        <div class="box-body">
            <form method="get" action="">
            <div class="row">
                <div class="col-sm-8 post-table-filter">                    
                    <?php 
                    if ($post_type['cat_support']) {
                        echo categories_dropdown($curr_type, get_value('c', ''), array(
                            'first_label' => 'Semua Kategori',
                            'class' => 'form-control input-sm',
                            'name' => 'c',
                            'id' => 'filter-by-cat'
                        )).' ';
                    }
                    $this->form->select('r', get_value('r', 'all'), list_postfilter_byrange(), 
                        array('class'=>'input-sm', 'id'=>'filter-by-range'), FALSE);
                    ?>
                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-filter"></i> Filter</button>
                </div>
                <div class="col-sm-4">
                    <div class="text-right">
                        <div class="input-group">                                                            
                            <input value="<?php echo get_value('q', '') ?>" type="text" class="form-control input-sm"
                                name="q" placeholder="Search" autocomplete="off">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </div>                                                     
                    </div>
                </div>
            </div>
            <input type="hidden" name="st" value="<?php echo $fpost_status ?>">
            <input type="hidden" name="t" value="<?php echo $curr_type ?>">
            </form>
            <?php
            if ($result = get_flashdata('result')) {
                echo '<div class="alert alert-success alert-dismissable" style="margin:15px 0 0">
                    <i class="glyphicon glyphicon-info-sign"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                    $result['msg'].'</div>';
            }  
            ?>
            <div class="table-responsive" style="margin:10px 0">
            <table id="post-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="min-width:300px">Judul <?php echo $post_type['label'] ?></th>
                        <?php if ($post_type['cat_support']): ?>
                            <th style="width:180px">Kategori</th>
                        <?php endif; ?>
                        <th style="width:140px">Waktu</th>
                        <th style="width:140px">Penulis</th>
                        <th style="text-align:center;width:50px" title="Dilihat">
                            <i class="glyphicon glyphicon-eye-open"></i></th>              
                    </tr>
                </thead>
                <tbody>
        <?php        
        $idx_page = get_value('p', 1);
        $params = array('status'=>$fpost_status, 'perpage'=>15, 'page'=>$idx_page);
        if ($fpost_status == 'trash') 
            $params['trash'] = 1;
        if ($qsearch = get_value('q'))
            $params['search'] = $qsearch;
        if ($cat_id = get_value('c'))
            $params['cat_id'] = $cat_id;
        if ($drange = get_value('r')) {
            if ($drange != 'all')
            $params['date'] = $drange;
        }
        $posts = $this->post->get_result($curr_type, $params);
        if ( $posts['data_total'] == 0 ) {
            echo '<tr><td colspan="5" style="text-align:center;padding:20px 0">Tidak ada data</td></tr>';
        } else { 
            foreach ($posts['rows'] as $post) {
                $draft_label = $post['post_status'] == 'draft' ? '<span class="label label-warning">Draft</span>' : '';
                echo '<tr>';
                echo '<td class="large">'.$post['post_title'].' '.$draft_label;
                echo '<div class="post-actions" style="display:none">';
                if ($post['trash'] == 0) {
                    if ( (has_akses('berita-edit') && $curr_type == 'post') || 
                         (has_akses('halaman-edit') && $curr_type == 'page') ||
                         (has_akses('pengumuman-edit') && $curr_type == 'pengumuman') ) {  
                        echo '<a href="?t='.$curr_type.'&a=edit&id='.$post['post_id'].'" class="btn btn-xs btn-success btn-edit-post" 
                            title="Edit"><i class="glyphicon glyphicon-edit"></i> Edit</a> ';
                    }   
                    if ( (has_akses('berita-del') && $curr_type == 'post') || 
                         (has_akses('halaman-del') && $curr_type == 'page') ||
                         (has_akses('pengumuman-del') && $curr_type == 'pengumuman') ) {  
                        echo '<a href="'.base_url('action/post/trash-post?id='.$post['post_id']).'" class="btn btn-xs btn-danger btn-del-post" 
                            title="Hapus"><i class="glyphicon glyphicon-trash"></i> Hapus</a> ';
                    }
                    if ($post['post_status'] == 'draft') {
                        if ( (has_akses('berita-edit') && $curr_type == 'post') || 
                             (has_akses('halaman-edit') && $curr_type == 'page') ||
                             (has_akses('pengumuman-edit') && $curr_type == 'pengumuman') ) { 
                                echo '<a href="'.base_url('action/post/publish-post?id='.$post['post_id']).'" class="btn btn-xs btn-primary btn-publish-post" 
                                title="Terbitkan"><i class="glyphicon glyphicon-ok"></i> Terbitkan</a> ';
                        }
                        echo '<a href="'.base_url($curr_type.'/preview?id='.$post['post_id']).'" class="btn btn-xs btn-warning btn-preview-post" 
                            title="Preview"><i class="glyphicon glyphicon-search"></i> Preview</a> ';
                    } else {
                        if ( (has_akses('berita-edit') && $curr_type == 'post') || 
                             (has_akses('halaman-edit') && $curr_type == 'page') ||
                             (has_akses('pengumuman-edit') && $curr_type == 'pengumuman') ) {
                                echo '<a href="'.base_url('action/post/draft-post?id='.$post['post_id']).'" class="btn btn-xs btn-warning btn-publish-post" 
                                title="Terbitkan"><i class="glyphicon glyphicon-floppy-disk"></i> Jadikan Draft</a> ';
                        }
                        echo '<a href="'.base_url($curr_type.'/'.$post['post_slug']).'" class="btn btn-xs btn-info btn-view-post" 
                            title="Lihat"><i class="glyphicon glyphicon-eye-open"></i> Lihat</a> ';
                    }
                } else {
                    echo '<a href="'.base_url('action/post/restore-post?id='.$post['post_id']).'" class="btn btn-xs btn-success btn-edit-post" 
                        title="Kembalikan"><i class="glyphicon glyphicon-repeat"></i> Kembalikan</a> ';
                    echo '<a href="'.base_url('action/post/delete-post?id='.$post['post_id']).'" class="btn btn-xs btn-danger btn-del-post" 
                        title="Hapus permanen"><i class="glyphicon glyphicon-remove"></i> Hapus Selamanya</a> ';
                }
                echo '</div>';
                echo '</td>';
                if ($post_type['cat_support'])
                    echo '<td>'.$post['categories'].'</td>';
                echo '<td>'.xtime($post['post_date']).'</td>';
                echo '<td>'.$post['namauser'].'</td>';                
                //echo '<td style="text-align:center">'.$post['comments_count'].'</td>';
                echo '<td style="text-align:center">'.$post['views_count'].'</td>';
                echo '</tr>';
            }
        }
        ?>           
                </tbody>
            </table>
            </div> <!-- .table-reponsive -->
            <?php echo generate_paginiation_link($posts['page'], $posts['page_total'], 9); ?>
        </div>
    </div>
    </div>
<?php        
    endswitch;
}
?>   