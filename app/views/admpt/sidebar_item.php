<?php
page_header('<i class="fa fa-list"></i> Sidebar Item');
set_breadcumb('sibar|Sidebar Item');
if (!has_akses('setting-sidebar')) {
    include_view('admin_noaccess');
} else {
    
include_assets('colorbox');
enqueue_css('icon-picker', NULL, 'bootstrap');
enqueue_js('nestable', NULL, 'bootstrap');
enqueue_js('icon-picker', NULL, 'jquery,bootstrap');
enqueue_js('sidebar-js', 'assets/custom/sidebar-item.js', 'nestable,icon-picker,colorbox');   
add_style(".btn-block {text-align:left;padding-left:10px}");
add_script("$('.icon-picker').iconPicker();"); 
?>
<div class="col-md-8">
    <div class="box box-primary">
        <div class="box-body main-area" style="min-height:180px">            
            <div id="side-items-right" class="dd side-items" data-des="right">
                <ol class="dd-list">
                <?php
                $sright = json_decode(config_item('sidebar_right'), TRUE);
                echo generate_nestable_items($sright);
                ?>
                </ol>
            </div> 
            <textarea id="serialized-item-right" cols="60" rows="5" style="display:none"><?php echo config_item('sidebar_right') ?></textarea>   
        </div>
        <div class="box-footer">
            <button type="button" id="btn-save-sideitems" class="btn btn-success">
                <i class="fa fa-save"></i> Simpan Pengaturan</button>
        </div>
    </div>
</div>
<div class="col-md-4 right-panel">
    <div class="box box-solid box-warning">
        <div class="box-header">
            <h3 class="box-title box-title-sm"><i class="fa fa-th-large"></i> Widget Tersedia</h3>
        </div>
        <div class="box-body" id="widget-items">
            <button class="btn btn-block btn-default" data-name="Menu" data-form="menu"><i class="fa fa-indent"></i> Menu</button>            
            <button class="btn btn-block btn-default" data-name="Berita" data-form="news"><i class="fa fa-list-alt"></i> Berita Terbaru</button>
            <button class="btn btn-block btn-default" data-name="Pengumuman" data-form="pengumuman"><i class="fa fa-bullhorn"></i> Pengumuman</button> 
            <button class="btn btn-block btn-default" data-name="Profil Kepala Daerah"><i class="fa fa-user"></i> Profil Kepala Daerah</button> 
            <button class="btn btn-block btn-default" data-name="Halaman" data-form="page"><i class="fa fa-file-o"></i> Halaman</button>
            <button class="btn btn-block btn-default" data-name="Polling" data-form="polling"><i class="fa fa-bar-chart-o"></i> Polling</button>
            <button class="btn btn-block btn-default" data-name="Banner" data-form="banner"><i class="fa fa-flag"></i> Banner</button>
            <button class="btn btn-block btn-default" data-name="Galeri" data-form="gallery"><i class="fa fa-picture-o"></i> Galeri Foto</button>
            <!-- <button class="btn btn-block btn-default" data-name="Personil"><i class="fa fa-user"></i> Menu Profil Personil</button> -->
            <!-- <button class="btn btn-block btn-default" data-name="Perkara"><i class="fa fa-book"></i> Menu Data Perkara</button> -->
            <button class="btn btn-block btn-default" data-name="Pengunjung"><i class="fa fa-female"></i> Statistik Pengunjung</button>
            <button class="btn btn-block btn-default" data-name="HTML" data-form="html"><i class="fa fa-code"></i> Text HTML</button>
        </div>
    </div>
</div>
<div style="display:none">
    <div id="sideitem-form-inline-1" class="colorbox-form" style="padding:20px">
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div> 
        <div class="form-group">
            <label>Icon</label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>        
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="sideitem-form-inline-menu" class="colorbox-form" style="padding:20px">
        <div class="form-group">
            <label>Nama Menu</label>
            <?php
            $this->form->select('grup', 1, list_menu(), array('id'=>'optsist-menu'), FALSE);
            ?>
        </div>
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div> 
        <div class="form-group">
            <label>Icon</label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>        
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="sideitem-form-inline-banner" class="colorbox-form" style="padding:20px">
        <div class="form-group">
            <label>Grup Banner</label>
            <?php
            $this->form->select('grup', '', list_grup_banner(TRUE), array('id'=>'optslist-banner'), FALSE);
            ?>
        </div>
        <div class="form-group">
            <label>Jumlah Maksimal <small>(0 = tanpa batas)</small></label>
            <input type="text" name="count" class="form-control" value="0">
        </div> 
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div> 
        <div class="form-group">
            <label>Icon</label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>        
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="sideitem-form-inline-gallery" class="colorbox-form" style="padding:20px">
        <div class="form-group">
            <label>Album Foto</label>
            <?php
            $this->form->select('grup', '', list_album(TRUE), array('id'=>'optslist-gallery'), FALSE);
            ?>
        </div>
        <div class="form-group">
            <label>Jumlah Maksimal</label>
            <input type="text" name="count" class="form-control" value="5">
        </div> 
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div> 
        <div class="form-group">
            <label>Icon</label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>        
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="sideitem-form-inline-news" class="colorbox-form" style="padding:20px">         
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div> 
        <div class="form-group">
            <label>Kategori</label>
            <?php
            echo categories_dropdown('news', '', array('id'=>'optslist-news','name'=>'grup',
                'first_label'=>'&mdash; Semua &mdash;','class'=>'form-control'));
            ?>
        </div>
        <div class="form-group">
            <label>Jumlah Maksimal</label>
            <input type="text" name="count" class="form-control" value="5">
        </div>
        <div class="form-group">
            <label>Icon</label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>        
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="sideitem-form-inline-page" class="colorbox-form" style="padding:20px">         
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div> 
        <div class="form-group">
            <label>Kategori</label>
            <?php
            echo categories_dropdown('page', '', array('id'=>'optslist-news','name'=>'grup',
                'first_label'=>'&mdash; Semua &mdash;','class'=>'form-control'));
            ?>
        </div>
        <div class="form-group">
            <label>Jumlah Maksimal</label>
            <input type="text" name="count" class="form-control" value="5">
        </div>
        <div class="form-group">
            <label>Icon</label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>        
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="sideitem-form-inline-pengumuman" class="colorbox-form" style="padding:20px">
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div> 
        <div class="form-group">
            <label>Jumlah Maksimal</label>
            <input type="text" name="count" class="form-control" value="5">
        </div>         
        <div class="form-group">
            <label>Icon</label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>        
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="sideitem-form-inline-polling" class="colorbox-form" style="padding:20px">
        <div class="form-group">
            <label>Pilih Polling</label>
            <?php
            $this->form->select('grup', '', list_polling(), array('id'=>'optslist-polling'), FALSE);
            ?>
        </div> 
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div> 
        <div class="form-group">
            <label>Icon</label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>        
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="sideitem-form-inline-html" class="colorbox-form" style="padding:20px">         
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div> 
        <div class="form-group">
            <label>Icon</label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>    
        <div class="form-group">
            <label>HTML</label>
            <textarea name="grup" class="form-control" rows="9" style="resize:vertical"></textarea>
        </div>     
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
</div>

<?php } ?>