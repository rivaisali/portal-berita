<?php
$action = get_value('a');
switch($action):
case 'add':
    page_header('Buat Halaman Baru');
    set_breadcumb('page|Halaman', 'page?a=add|Tambah');
    echo '<form method="post" action="'.base_path('action/post').'" enctype="multipart/form-data">';
    echo '<div class="col-lg-9">
        <div class="box box-primary">
            <div class="box-body pad">'.
                $this->admin->post_title_permalink('page').
                $this->admin->wyswyg_editor().   
            '</div>
        </div>
    </div><div class="col-lg-3">'.
        $this->admin->post_setting_box().        
        $this->admin->post_category_box().
        $this->admin->post_image_box().
    '</div>';
    echo '</form>';
break;
default:
    page_header('Halaman <a href="?a=add" class="btn-add">Tambah Baru</a>');
    set_breadcumb('page|Halaman');
endswitch;
?>       

    <!-- <div class="box">
        <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th>Engine version</th>
                        <th>CSS grade</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Trident</td>
                        <td>Internet
                            Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>Trident</td>
                        <td>Internet
                            Explorer 5.0</td>
                        <td>Win 95+</td>
                        <td>5</td>
                        <td>C</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th>Engine version</th>
                        <th>CSS grade</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div> /.box -->
