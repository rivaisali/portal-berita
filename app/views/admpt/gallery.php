<?php
$action = get_value('a');
switch($action):
case 'album':
    $headertxt = '<i class="fa fa-picture-o"></i> Album &raquo; Galeri Foto';
    if (has_akses('galeri-add'))
        $headertxt.= ' <a href="#" class="btn-add add-album">Tambah Album</a>';
    page_header($headertxt);
    set_breadcumb('gallery|Galeri Foto', 'album|Album');
    include_assets('dsTable', 'colorbox');
    $editaction = has_akses('galeri-edit') ? "editAction: function(id, tr, tbl){  
            var frm = $('#album-form-inline form');
            frm.find('#new_album_name').val(tr.find('td[data-column-name=nm_album]').text());
            frm.find('#new_album_id').val(id);
            frm.find('#new_album_action').val('update');
            frm.find(':submit').text('Update Album');
            $.colorbox({
                inline: true,
                href: '#album-form-inline',
                width: 500,
                title: '&nbsp; Edit Album ',
                maxWidth: '98%',
                maxHeight: '95%',
                onComplete: function(){ frm.find(':text').focus(); }
            });
        }," : '';
    $delaction = has_akses('galeri-del') ? "deleteAction: site_root + 'action/crud/delete?n=album&id='," : '';
    $thactwi = $delaction && $editaction ? 70 : 35;
    add_script("var mainTable = $('#data-table');
    mainTable.dsTable({
        dataSource: site_root+'ajax/dstable/album',
        columns: [
            { name:'rowNumber', label:'No.', width:40, align:'center' },
            { name:'nm_album', label:'Nama Album' },
            { name:'jum_foto', label:'Jumlah Foto', width:150, align:'center' }
        ], {$editaction} {$delaction}
        deleteMsg: 'Foto-foto yang ada di album ini juga akan dihapus. Lanjutkan?',
        thActionWidth: {$thactwi},
        showFooter: false,
        perPage: 20,
        orderBy: 'album_id DESC'
    });
    $('.add-album').on('click', function(e){
        e.preventDefault();
        var frm = $('#album-form-inline form');
        frm.find('#new_album_name').val('');
        frm.find('#new_album_id').val('0');
        frm.find('#new_album_action').val('insert');
        $.colorbox({
            inline: true,
            href: '#album-form-inline',
            width: 500,
            title: '&nbsp; Tambah Album ',
            maxWidth: '98%',
            maxHeight: '95%',
            onComplete: function(){ frm.find(':text').focus(); }
        });
    });
    $('#album-form-inline').on('submit', 'form', function(e){
        e.preventDefault();
        var frm = $(this);
        var fnm = frm.find(':text');
        if (fnm.val().trim() == '') {
            fnm.focus();
            return false;
        }
        var pdata = $(this).serialize();
        var actn = $(this).attr('action');
        frm.find('input, select, button').prop('disabled', true);
        $.post(actn, pdata, function(res){
            frm.find('input, select, button').prop('disabled', false);
            if (res.ok) {
                $.colorbox.close();
                mainTable.dsTable('refresh');
            } else {
                alert(res.msg);
            }            
        }, 'json');
    });");
?>
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body table-responsive">
                <table id="data-table" class="table table-bordered table-striped"></table>
            </div>
        </div>
    </div>
    <div style="display:none">
        <div id="album-form-inline" style="padding:25px 20px">
            <form method="post" action="<?php echo base_url('ajax/savealbum') ?>">
                <input type="text" name="new_album_name" id="new_album_name" placeholder="Nama album" 
                    class="form-control" style="margin-bottom:15px" value=""> 
                <button type="submit" class="btn btn-success">Tambahkan Album</button>
                <input type="hidden" name="new_album_id" id="new_album_id" value="0">
                <input type="hidden" name="new_album_action" id="new_album_action" value="insert">
            </form>
        </div>
    </div>
<?php
break;
default:
    $headertxt = '<i class="fa fa-picture-o"></i> Galeri Foto';
    if (has_akses('galeri-add'))
        $headertxt.= ' <a href="#" class="btn-add">Upload Foto</a>';
    page_header($headertxt);
    set_breadcumb('gallery|Galeri Foto');
    include_assets('colorbox', 'dropzone');
    add_script("$('.btn-add').on('click', function(e){
        e.preventDefault();
        var alid = $('#album_id').val();
        var alnm = $('#album_id').find('option:selected').text();
        if (alid == '') {
            alert('Album belum dipilih!');   
            return false;
        }
        $.colorbox({
            iframe: true,
            href: site_root + 'popup/upload-gallery?album='+alid,
            width: 800,
            title: ' Upload foto di Album '+alnm,
            height: 480,
            maxWidth: '98%',
            maxHeight: '98%',
            onClosed: function(){
               $('#album_id').trigger('change'); 
            }
        });
    });
    $('#album_id').on('change', function(e){
        var albm = $(this).val();
        if (albm == '') {
            $('#foto-area').html('<div class=\"well\">( Pilih album )</div>');
        } else {
            $('#foto-area').html('<div class=\"well\"><img src=\"'+site_root+'assets/img/loader-31-black.gif\"></div>');
            $.get(site_root+'ajax/load-gallery/'+albm, function(res){
                $('#foto-area').html(res.html);
                $('.img-container').colorbox({ rel:'img-container',maxWidth: '98%', maxHeight: '90%' });
                if (typeof(history.pushState) !== 'undefined') {
                    var url = '?album=' + albm;
                    history.pushState({ url: url }, 'Gallery', url);
                }
            }, 'json');
        }
    }).trigger('change');
    $('#foto-area').on('mouseenter', '.foto-list > li', function(){
       $(this).find('.foto-title, .foto-btns').slideDown(100);
    }).on('mouseleave', '.foto-list > li', function(){
       $(this).find('.foto-title, .foto-btns').slideUp(100);
    }).on('click', '.btn-del-foto', function(e){
        e.preventDefault();
        var foto_id = $(this).attr('data-id');
        var album_id = $(this).attr('data-album-id');
        var url = $(this).attr('data-url');
        var li = $(this).closest('li');
        var pdata = { 'foto_id':foto_id, 'album_id':album_id, 'url':url };
        $.post(site_root+'ajax/deletefoto', pdata, function(res){
            if (res.ok) {
                li.fadeOut(100, function(){ $(this).remove(); });
            }
        }, 'json');
    }).on('click', '.btn-edit-foto', function(e){
        e.preventDefault();
        var foto_id = $(this).attr('data-id');
        var album_id = $(this).attr('data-album-id');
        var judul = $(this).attr('data-judul');
        var li = $(this).closest('li');
        var frm = $('#edit-foto-form');
        frm.find('#new_judul').val(judul);
        frm.find('#new_album_id').val(album_id);
        frm.find('#edit_foto_id').val(foto_id);
        $.colorbox({
            inline: true,
            href: '#edit-foto-inline',
            width: 500,
            title: '&nbsp; Edit Foto',
            maxWidth: '98%',
            maxHeight: '95%'
        });
    });
    $('#edit-foto-form').on('submit', function(e){
        e.preventDefault();
        var frm = $(this);
        var pdata = $(this).serialize();
        var actn = $(this).attr('action');
        var fotid = $(this).find('#edit_foto_id').val();
        var fotarea = $('#foto-area');
        var curr_album = $('#album_id').val();
        frm.find('input, select, button').prop('disabled', true);
        $.post(actn, pdata, function(res){
            frm.find('input, select, button').prop('disabled', false);
            if (res.ok) {
                var fotli = fotarea.find('li[data-id='+fotid+']');
                if (res.album_id != curr_album) {
                    fotli.remove();
                } else {
                    fotli.find('.foto-title').remove();
                    fotli.append('<span class=\"foto-title\">'+res.judul+'</span>');
                    fotli.find('.img-container').attr('title', res.judul);
                    fotli.find('.foto-btns > a').attr('data-judul', res.judul);
                }
                $.colorbox.close();
            }
        }, 'json');
    });");
    echo '<div class="col-xs-12"><div class="box box-warning">';
    $this->form->layout = '';
    $list_album = list_album();
    echo '<div class="dttool">Album : '.$this->form->select('album_id', get_value('album', ''), 
        $list_album, array('style'=>'width:auto;max-width:320px;min-width:100px'), TRUE, TRUE).'</div>';   
    echo '<div id="foto-area"><div class="well">( Pilih album )</div></div>';         
    echo '</div></div>';
    echo '<div style="display:none"><div id="edit-foto-inline" style="padding:15px">
        <form method="post" action="'.base_url('ajax/updatefoto').'" id="edit-foto-form">Pindah Album :<br>'.
        $this->form->select('new_album_id', get_value('album', ''), $list_album, array('class'=>'form-control'), FALSE, TRUE).'
        <input name="new_judul" id="new_judul" placeholder="Keterangan foto.." class="form-control" style="margin:15px 0">
        <button type="submit" id="btn-update-foto" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Update Foto</button>
        <input type="hidden" name="edit_foto_id" id="edit_foto_id" value="0"></form>
    </div></div>';
endswitch;