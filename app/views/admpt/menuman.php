<?php
page_header('<i class="fa fa-indent"></i> Manajemen Menu');
set_breadcumb('menu|Manajemen Menu');
if (!has_akses('setting-menu')) {
    include_view('admin_noaccess');
} else {
    
include_assets('colorbox');
enqueue_css('icon-picker', NULL, 'bootstrap');
enqueue_js('nestable', NULL, 'jquery,bootstrap');
enqueue_js('icon-picker', NULL, 'jquery,bootstrap');
enqueue_js('dsFunction', NULL, 'jquery,bootstrap');
enqueue_js('menuman-js', 'assets/custom/menuman.js', 'nestable,icon-picker,dsFunction');   
add_script("$('.icon-picker').iconPicker();"); 
add_style('
    .right-panel .checkbox { font-size:12px; padding-left:20px; position:relative; }
    .right-panel .checkbox input[type=checkbox] { margin-left:-20px; margin-top:-1px; position:absolute; }
');
?>    
<div class="col-lg-9">
    <div class="box box-primary">
        <div class="box-tool">
            <label for="menu-id">Menu</label>
            <?php
            $list_menu = list_menu();
            $this->form->layout = '';
            $this->form->select('menu-id', get_value('id', 1), $list_menu, '', FALSE);
            ?>
            &nbsp;<button type="button" class="btn btn-sm btn-primary addmenu-btn">
                <i class="fa fa-plus"></i> Tambah Menu</button>
        </div>
        <div id="menuman-item-area" class="box-body pad" style="min-height:100px">            
            <div class="alert alert-info menuman-msg" style="margin:25px 10px">Pilih salah satu menu</div>
            <p class="text-center menuman-loader" style="display:none">
                <img src="<?php echo base_url('assets/img/ajax-loader1.gif') ?>">
            </p>                    
            <div id="menuman-item" class="dd" style="display:none"><ol class="dd-list"></ol></div>    
            <textarea id="serialized-menu" cols="90" rows="5" style="display:none"></textarea>           
        </div>
        <div class="box-footer clearfix menubtn-area">                
            <button class="pull-right btn btn-success menuact-btn savemenu-btn" type="button" disabled>
                <i class="fa fa-save"></i> Simpan Menu</button>
            <button class="pull-right btn btn-default menuact-btn editmenu-btn" type="button" style="margin:0 10px" disabled>
                <i class="fa fa-edit"></i> Ubah Nama Menu</button>
            <button class="pull-right btn btn-danger menuact-btn delmenu-btn" type="button" disabled>
                <i class="fa fa-trash-o"></i> Hapus</button>
        </div>
    </div>
</div>
<div class="col-lg-3 right-panel">  
    <div class="panel-group" id="panel-accordion">
        <!-- halaman -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                <a class="btn-collapse" data-toggle="collapse" data-parent="#panel-accordion" href="#collapse-page">
                  <i class="fa fa-files-o"></i> Halaman
                </a></h4>
            </div>
            <div id="collapse-page" class="panel-collapse collapse in">
                <div class="panel-body" id="menutype-halaman">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-page-1" data-toggle="tab">Custom</a></li>
                        <li><a href="#tab-page-2" data-toggle="tab">Tetap</a></li>
                    </ul>
                    <div class="tab-content box-area">
                        <div class="tab-pane active" id="tab-page-1">
                            <?php
                            $qw = $this->db->select('post_slug,post_title')->order_by('post_id DESC')
                                ->get_where('post', array('post_type'=>'page','post_status'=>'publish','trash'=>0));
                            foreach ($qw->result() as $row) {
                                echo '<div class="checkbox"><label>
                                    <input type="checkbox" data-type="Halaman" data-glyphicon="" data-label="'.
                                        $row->post_title.'" value="page/'.$row->post_slug.'">'.$row->post_title.'
                                </label></div>';
                            }
                            ?>
                        </div>
                        <div class="tab-pane" id="tab-page-2">
                            <div class="checkbox"><label>
                                <input type="checkbox" data-type="Halaman" data-glyphicon="home" data-label="Beranda" value=""> 
                                <i class="glyphicon glyphicon-home"></i> Beranda
                            </label></div>
                            <div class="checkbox"><label>
                                <input type="checkbox" data-type="Halaman" data-label="Daftar Tulisan" value="post"> 
                                Daftar Tulisan
                            </label></div>
                            <div class="checkbox"><label>
                                <input type="checkbox" data-type="Halaman" data-label="Pengumuman" value="pengumuman"> 
                                Pengumuman
                            </label></div>
                            <!-- <div class="checkbox"><label>
                                <input type="checkbox" data-type="Halaman" data-glyphicon="file" data-label="Data Perkara" value="perkara"> 
                                <i class="glyphicon glyphicon-file"></i> Data Perkara
                            </label></div> -->
                            <!-- <div class="checkbox"><label>
                                <input type="checkbox" data-type="Halaman" data-glyphicon="signal" data-label="Grafik Perkara" value="grafik-perkara"> 
                                <i class="glyphicon glyphicon-signal"></i> Grafik Perkara
                            </label></div> -->
                            <div class="checkbox"><label>
                                <input type="checkbox" data-type="Halaman" data-label="Galeri Foto" value="gallery"> 
                                Galeri Foto
                            </label></div>
                            <div class="checkbox"><label>
                                <input type="checkbox" data-type="Halaman" data-label="Harga Komoditi" value="harga-komoditi"> 
                                Harga Komoditi
                            </label></div>
                            <!-- <div class="checkbox"><label>
                                <input type="checkbox" data-type="Halaman" data-glyphicon="earphone" data-label="Pengaduan" value="pengaduan"> 
                                <i class="glyphicon glyphicon-earphone"></i> Pengaduan
                            </label></div> -->
                            <div class="checkbox"><label>
                                <input type="checkbox" data-type="Halaman" data-label="Kontak Kami" value="kontak"> 
                                Kontak Kami
                            </label></div>                        
                        </div>
                    </div>
                </div>
                <div class="panel-footer clearfix">
                    <button type="button" class="btn btn-default btn-sm btn-addtomenu" data-rel="#menutype-halaman">
                        <i class="fa fa-plus"></i> Tambahkan ke menu</button>
                </div>
            </div>
        </div>  
        
        <!-- Kategori -->
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                <a class="btn-collapse" data-toggle="collapse" data-parent="#panel-accordion" href="#collapse-kategori">
                  <i class="fa fa-folder-o"></i> Kategori
                </a></h4>
            </div>
            <div id="collapse-kategori" class="panel-collapse collapse">
                <div class="panel-body" id="menutype-kategori">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-kategori-1" data-toggle="tab">Berita</a></li>
                        <li><a href="#tab-kategori-2" data-toggle="tab">Halaman</a></li>
                    </ul>
                    <div class="tab-content box-area">
                        <div class="tab-pane active" id="tab-kategori-1">
                            <?php
                            $cats = get_categories('post');
                            foreach ($cats as $key => $val) {
                                echo '<div class="checkbox">';
                                for ($i=1; $i<=($val['level']-1); $i++) echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                                echo '<label>
                                    <input type="checkbox" data-type="Kategori" data-glyphicon="" data-label="'.
                                        $val['name'].'" value="post?cat='.$val['slug'].'"> '.$val['name'].'
                                </label></div>';
                            }
                            ?>
                        </div>
                        <div class="tab-pane" id="tab-kategori-2">
                            <?php
                            $cats = get_categories('page');
                            foreach ($cats as $key => $val) {
                                echo '<div class="checkbox">';
                                for ($i=1; $i<=($val['level']-1); $i++) echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                                echo '<label>
                                    <input type="checkbox" data-type="Kategori" data-glyphicon="" data-label="'.
                                        $val['name'].'" value="post?cat='.$val['slug'].'"> '.$val['name'].'
                                </label></div>';
                            }
                            ?>                           
                        </div>
                    </div>
                </div>
                <div class="panel-footer clearfix">
                    <button type="button" class="btn btn-default btn-sm btn-addtomenu" data-rel="#menutype-kategori">
                        <i class="fa fa-plus"></i> Tambahkan ke menu</button>
                </div>
            </div>
        </div>  
        
        <!-- Kategori          
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h4 class="panel-title">
                <a class="btn-collapse" data-toggle="collapse" data-parent="#panel-accordion" href="#collapse-kategori">
                  <i class="fa fa-folder-o"></i> Kategori
                </a></h4>
            </div>
            <div id="collapse-kategori" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="box-area" id="menutype-kategori">
                    <?php
                    $cats = get_categories('post');
                    foreach ($cats as $key => $val) {
                        echo '<div class="checkbox">';
                        for ($i=1; $i<=($val['level']-1); $i++) echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                        echo '<label>
                            <input type="checkbox" data-type="Kategori" data-glyphicon="" data-label="'.
                                $val['name'].'" value="post?cat='.$val['slug'].'"> '.$val['name'].'
                        </label></div>';
                    }
                    ?>
                    </div>
                </div>
                <div class="panel-footer clearfix">
                    <button type="button" class="btn btn-default btn-sm btn-addtomenu" data-rel="#menutype-kategori">
                        <i class="fa fa-plus"></i> Tambahkan ke menu</button>
                </div>
            </div>
        </div>--> 
        
        <!-- Link -->
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4 class="panel-title">
                <a class="btn-collapse" data-toggle="collapse" data-parent="#panel-accordion" href="#collapse-link">
                  <i class="fa fa-link"></i> Link
                </a></h4>
            </div>
            <div id="collapse-link" class="panel-collapse collapse">
                <div class="panel-body" id="menutype-link">
                    <input name="link" type="text" class="form-control" placeholder="http://" style="margin-bottom:10px"> 
                    <input name="label" type="text" class="form-control" placeholder="Label Menu"> 
                </div>
                <div class="panel-footer clearfix">
                    <button type="button" class="btn btn-default btn-sm btn-addtomenu-link">
                        <i class="fa fa-plus"></i> Tambahkan ke menu</button>
                </div>
            </div>
        </div>
    </div>    
</div>

<div style="display:none">
    <div id="menu-form-inline" style="padding:25px 20px">
        <form method="post" action="<?php echo base_url('ajax/menuman/save') ?>">
            <input type="text" name="nm_menu" placeholder="Nama Menu" 
                class="form-control" style="margin-bottom:15px" value=""> 
            <button type="submit" class="btn btn-success">Tambahkan Menu</button>
            <input type="hidden" name="menu_id" value="0">
            <input type="hidden" name="action" value="insert">
        </form>
    </div>
    <div id="menuitem-form-inline" class="colorbox-form" style="padding:25px 20px">
        <div class="form-group">
            <label>Label Menu</label>
            <input type="text" name="label" class="form-control" placeholder="Label Menu">
        </div>
        <div class="form-group">
            <label>URL</label>
            <input type="text" name="url" class="form-control" placeholder="http://">
        </div>  
        <div class="form-group">
            <label>Icon <small>(opsional)</small></label>
            <input type="text" name="icon" class="form-control icon-picker">
        </div>          
        <div class="form-group">
            <label>Attribut Judul <small>(opsional)</small></label>
            <input type="text" name="title" class="form-control">
        </div>            
        <div class="form-group">
            <label>CSS Class <small>(opsional)</small></label>
            <input type="text" name="classes" class="form-control">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="newtab"> Buka link di jendela/tab baru</label>
        </div>
        <button type="submit" class="btn btn-success" id="update-menuitem-btn">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
</div>

<?php } ?>