<?php
$action = get_value('a');
switch($action):
case 'add': case 'edit': 
    if (!has_akses('user-'.$action)) {
        include_view('admin_noaccess');
        return;
    }
	include_assets('datetimepicker');
	enqueue_js('dsFunction', NULL, 'jquery,bootstrap');
	enqueue_js('event-lib', NULL, 'jquery,bootstrap,tinymce,dsFunction');
    if ($action == 'add') {
        page_header('<i class="fa fa-calendar"></i> Tambah Event Baru');
        set_breadcumb('event|Data Event', 'add|Tambah');
        $submit_label = 'ok#Tambahkan';
        $submit_action = 'insert?n=event';    
		$row = array('waktu_selesai'=>'','tempat'=>'','cp'=>'','judul'=>'','deskripsi'=>'','waktu_mulai'=>'');
    } else {
        page_header('<i class="fa fa-users"></i> Edit Data Event');
        set_breadcumb('event|Data Event', 'edit|Edit');
        $submit_label = 'ok#Update Data';
        if ($uid = get_value('id')) {
            $submit_action = 'update?n=event&id='.$uid;
            $qw = $this->db->get_where('event', array('event_id'=>$uid), 1);
            if ($qw->num_rows() == 0) redirect($user_type.'/event');
            $row = $qw->row_array();
        }
    }
    if ($post_data = get_flashdata('post_data'))         
        $row = array_merge($row, $post_data);
    echo '<div class="col-xs-12"><div class="box box-primary">';
    if ($result = get_flashdata('result')) {
        $res_type = $result['ok'] ? 'success' : 'danger';
        echo '<div class="alert alert-'.$res_type.' alert-dismissable" style="margin:15px 15px 0">
            <i class="glyphicon glyphicon-info-sign"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
            $result['msg'].'</div>';
    }
	$this->form->upload = TRUE;
    $this->form->open('action/crud/'.$submit_action,'class="form-horizontal data-form" autocomplete="off"');
    $this->form->ftext('judul', $row['judul'], 'Judul', array('size'=>'50','maxlength'=>'100','required'=>'required'));
?>
	<div class="form-group clearfix">
		<label for="waktu_mulai" class="col-lg-2 col-md-3 control-label">Waktu Mulai</label>
		<div class="col-lg-10 col-md-9">
			<div id="post-date-area" class="input-group input-group-sm" style="margin-top:10px;">
				<input id="post-date-edit" type="text" name="waktu_mulai" value="<?php echo date('j M Y - H:i') ?>" class="form-control" style="cursor:pointer" readonly>                            
			</div>  
			<input type="hidden" name="waktu_mulai" id="post-date" value="<?php echo $row['waktu_mulai'] ?>">  
		</div>
	</div>
	<div class="form-group clearfix">
		<label for="waktu_mulai" class="col-lg-2 col-md-3 control-label">Waktu Selesai</label>
		<div class="col-lg-10 col-md-9">
			<div id="post-date-area" class="input-group input-group-sm" style="margin-top:10px;">
				<input id="post-date-edit1" name="waktu_selesai" type="text" value="<?php echo date('j M Y - H:i') ?>" class="form-control" style="cursor:pointer" readonly>                            
			</div>  
			<input type="hidden" name="waktu_selesai" id="post-date1" value="<?php echo $row['waktu_selesai'] ?>">  
		</div>
	</div>
<?php
    $this->form->ftext('tempat', $row['tempat'], 'Tempat Kegiatan', array('size'=>'60','maxlength'=>'100','required'=>'required'));
    $this->form->ftext('cp', $row['cp'], 'Contact Person', array('size'=>'20','maxlength'=>'100','required'=>'required'));
    $this->form->ftextarea('deskripsi', $row['deskripsi'], 'Deskripsi', array('rows'=>'3','cols'=>'100','required'=>'required'));
	// $this->form->add_row('gambar', 'Gambar', '<input type="file" name="gambar" accept="image/*">'); 
	$this->form->add_row('fileupl', 'Foto', '<input type="file" name="file" accept="image/*">'); 
	if (!empty($row['gambar'])) {
		$this->form->add_row('fotoprev', '&nbsp;', '<img src="../uploads/event/'.$row['gambar'].'">');        
	}
    $this->form->fsubmit('simpan', $submit_label)    
    ->close();    
    echo '</div></div>';
break;

default:
    if (!has_akses('user-view')) {
        include_view('admin_noaccess');
        return;
    }
    $headertxt = '<i class="fa fa-calendar"></i> Data Event';
    if (has_akses('user-add'))
        $headertxt.= ' <a href="?a=add" class="btn-add add-banner">Tambah Baru</a>';
    page_header($headertxt);
    set_breadcumb('event|Data Event');
    include_assets('dsTable');
    $editaction = has_akses('user-edit') ? "editAction: '?a=edit&id='," : '';
    $delaction = has_akses('user-del') ? "deleteAction: site_root + 'action/crud/delete?n=event&id='," : '';
    $thactwi = $delaction && $editaction ? 70 : 35;
    add_script("if (jQuery().dsTable) {
        var mainTable = $('#data-table');
        mainTable.dsTable({
            dataSource: site_root+'ajax/dstable/event',
            columns: [
                { name:'rowNumber', label:'No.', width:40, align:'center' },
                { name:'judul', label:'Nama', width:200, minWidth:200 },
                { name:'waktu_mulai', label:'Waktu Kegiatan', minWidth:180, minWidth:160 },
                { name:'tempat', label:'Lokasi', width:140, minWidth:140 }
            ],
            {$editaction} {$delaction}
            thActionWidth: {$thactwi},
            perPage: 30,
            showFooter: false
        });
    }");
?>
    <div class="col-xs-12">
        <div class="box box-primary">            
            <div class="box-body">
                <div class="table-responsive" style="margin:10px 0">
                    <table id="data-table" class="table table-bordered table-striped"></table>
                </div>
            </div>
        </div>
    </div>
<?php
endswitch;