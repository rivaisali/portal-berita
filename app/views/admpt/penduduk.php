<?php
$curr_type  = get_value('t', 'unknown');
$action     = get_value('a');
switch($action):
case 'add': case 'edit':    
    enqueue_js('dsAutocomplete', NULL, 'bootstrap');
    enqueue_css('dsAutocomplete', NULL, 'bootstrap');
    enqueue_js('penduduk-adm', 'assets/custom/penduduk-adm.js', 'dsAutocomplete');
    if ($action == 'add') {
        page_header('<i class="fa fa-users"></i> Tambah Data Penduduk');
        set_breadcumb('penduduk|Data Penduduk', 'add|Tambah');
        $submit_label = 'ok#Tambahkan';
        $submi_action = 'insert?n=penduduk';
        $row = array('ID_pend'=>'0','gender'=>'L','nik'=>'','nama'=>'','tmp_lahir'=>'','tgl_lahir'=>'','agama'=>'Islam',
            'gol_darah'=>'','pendidikan'=>'','pekerjaan'=>'');
    } else {
        page_header('<i class="fa fa-users"></i> Edit Data Penduduk 
            <a href="penduduk?a=add" class="btn-add">Tambah Baru</a>');
        set_breadcumb('penduduk|Data Penduduk', 'add|Edit');
        $submit_label = 'ok#Update Data';        
        if ($ID_pend = get_value('id')) {
            $submi_action = 'update?n=penduduk&id='.$ID_pend;
            $qw = $this->db->get_where('penduduk', array('ID_pend'=>$ID_pend), 1);
            if ($qw->num_rows() == 0) redirect($user_type.'/penduduk');
            $row = $qw->row_array();
        }
    }
    if ($post_data = get_flashdata('post_data'))         
        $row = array_merge($row, $post_data);
    echo '<div class="col-xs-12"><div class="box box-primary">';
    if ($result = get_flashdata('result')) {
        $res_type = $result['ok'] ? 'success' : 'danger';
        echo '<div class="alert alert-'.$res_type.' alert-dismissable" style="margin:15px 15px 0">
            <i class="glyphicon glyphicon-info-sign"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
            $result['msg'].'</div>';
    }
    add_style(".data-form label {font-weight:normal}");
    $this->form->upload = TRUE;
    $this->form->set_input_addon(array('tgl_lahir'=>'<i class="glyphicon glyphicon-calendar"></i>',
        'pangkat_gol'=>'<button type="button" class="btn lookup-btn btn-default"><i class="glyphicon glyphicon-chevron-down"></i></button>'), 'right');
    $this->form->open('action/crud/'.$submi_action,'class="form-horizontal data-form"')
    ->ftext('nik', $row['nik'], 'N I K', array('size'=>'30','maxlength'=>'50'))    
    ->ftext('nama', $row['nama'], 'Nama Lengkap', array('size'=>'80','maxlength'=>'300','required'=>'required'))
    ->fradio('gender', $row['gender'], 'Jenis Kelamin', array('L'=>'Laki-laki','P'=>'Perempuan'))
    ->ftext('tmp_lahir', $row['tmp_lahir'], 'Tempat Lahir', array('size'=>'30','maxlength'=>'100')) 
    ->ftext('tgl_lahir', ddmmy($row['tgl_lahir']), 'Tanggal Lahir', array('size'=>'12','maxlength'=>'12',
        'placeholder'=>'dd/mm/yyyy','width'=>'150')) 
    ->fselect('pendidikan', $row['pendidikan'], 'Pendidikan Terakhir', list_jenjang())    
    ->ftext('pekerjaan', $row['pekerjaan'], 'Pekerjaan', array('size'=>'40'))
    ->fselect('agama', $row['agama'], 'Agama', list_agama())   
    ->fselect('gol_darah', $row['gol_darah'], 'Golongan Darah', list_goldarah());    
    $this->form->hidden('ID_pend', $row['ID_pend'])
    ->fsubmit('simpan', $submit_label)    
    ->close();    
    echo '</div></div>';
break;

default:
    add_style('.dstable th { font-weight:600;vertical-align:middle !important; } .dstable { font-size:13px; }');
    $headertxt = '<i class="fa fa-users"></i> Data Penduduk';
    if (has_akses('penduduk-add'))
        $headertxt.= ' <a href="penduduk?a=add" class="btn-add">Tambah Baru</a>';
    page_header($headertxt);
    set_breadcumb('penduduk|Data Penduduk');
    include_assets('dsTable');
    $editaction = has_akses('penduduk-edit') ? "editAction: '?a=edit&id='," : '';
    $delaction = has_akses('penduduk-del') ? "deleteAction: site_root + 'action/crud/delete?n=penduduk&id='," : '';
    $thactwi = $delaction && $editaction ? 70 : 35;
    add_script("if (jQuery().dsTable) {
        var mainTable = $('#data-table');
        mainTable.dsTable({
            dataSource: site_root+'ajax/dstable/penduduk',
            columns: [
                { name:'rowNumber', label:'No.', width:40, align:'center' },
                { name:'nik', label:'NIK', width:120 },
                { name:'nama', label:'Nama', width:200, minWidth:200 },
                { name:'gender', label:'L/P', width:40, align:'center' },
                { name:'tmp_tgl_lahir', label:'Tempat, Tgl. Lahir', minWidth:120 },
                { name:'pekerjaan', label:'Pekerjaan', width:110},
                { name:'pendidikan', label:'Pendidikan<br/>Terakhir', width:80, align:'center' },
                { name:'menikah', label:'Menikah', width:70, align:'center' },
                { name:'agama', label:'Agama', width:70, align:'center' },
                { name:'gol_darah', label:'Gol. Darah', width:70, align:'center' }
            ],
            {$editaction} {$delaction}
            thActionWidth: {$thactwi},
            orderBy: 'nik,nama',
            perPage: 20
        });
        $('#btn-search-pers').on('click', function(e){
            e.preventDefault();
            var q = $('#filter-by-query').val().trim();
            if (q == '') 
                $('#btn-reset-search-pers').trigger('click');
            else {
                $('#btn-reset-search-pers').show();
                mainTable.dsTable('addFilter', 'nama', 'nama % '+q);
                mainTable.dsTable('refresh');
            }  
        });
        $('#btn-reset-search-pers').on('click', function(e){
            e.preventDefault();
            $('#filter-by-query').val('');
            mainTable.dsTable('removeFilter', 'nama');
            mainTable.dsTable('refresh');
            $(this).hide();
        });
        $('#filter-by-query').on('keypress', function(e){
            if (e.which == 13) {
                e.preventDefault();
                $('#btn-search-pers').trigger('click');
            } 
        });    
    }");
?>
    <div class="col-xs-12">
        <div class="box box-primary">            
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-offset-8 col-sm-4">
                        <div class="text-right">
                            <div class="input-group">                                                            
                                <input value="<?php echo get_value('q', '') ?>" type="text" class="form-control input-sm"
                                    name="q" id="filter-by-query" placeholder="Search" autocomplete="off">
                                <div class="input-group-btn">
                                    <button id="btn-reset-search-pers" type="buttton" class="btn btn-sm btn-danger"
                                        title="Reset pencarian" data-toggle="tooltip" style="display:none">
                                        <i class="fa fa-times"></i></button>
                                    <button id="btn-search-pers" type="buttton" class="btn btn-sm btn-primary">
                                        <i class="fa fa-search"></i></button>
                                </div>
                            </div>                                                     
                        </div>
                    </div>
                </div>
                <div class="table-responsive" style="margin:10px 0">
                    <table id="data-table" class="table table-bordered table-striped"></table>
                </div>
            </div>
        </div>
    </div>
<?php
endswitch;
