<?php
$this->load->helper('social');
page_header('<i class="fa fa-envelope"></i> Pesan Masuk');
set_breadcumb('menu|Pesan Masuk');
include_assets('colorbox');
add_script("$('.del-pesan').on('click', function(e){
    if (!confirm('Hapus pesan ini?') ) e.preventDefault();
});
var rplyfrmarea = $('#reply-form-inline');
$('.reply-pesan').colorbox({
    inline: true,
    title: ' <i class=\"fa fa-mail-reply\"></i> Balas Pesan',
    width: 600,
    maxWidth: '95%',
    maxHeight: '95%',
    onOpen: function(){
        var repmail = $(this).attr('data-email');
        rplyfrmarea.find('.reply-to-text').text($(this).attr('data-nama')+' <'+repmail+'>');
        rplyfrmarea.find('.reply-to-val').val(repmail);
        rplyfrmarea.find('.reply-to-id').val($(this).attr('data-id'));
    },
    onComplete: function(){
        rplyfrmarea.find('textarea:first').focus();
    }
});
$('.reply-form').on('submit', function(e){
   e.preventDefault();
   var frm = $(this);
   var pdata = frm.serialize();
   frm.find('textarea, button').prop('disabled', true);
   $.post(frm.attr('action'), pdata, function(res){
        frm.find('textarea, button').prop('disabled', false); 
        alert(res.msg);   
        if (res.ok) {
            $.colorbox.close();
            location.reload();
        }
   }, 'json'); 
});");
?>
<div class="col-lg-12">
    <div class="box box-primary">
        <div class="box-body pesan-list">
        <?php                
        if ($result = get_flashdata('result')) {
            echo '<div class="alert alert-info alert-dismissable">
                <i class="glyphicon glyphicon-trash"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>1 Pesan
                berhasil dihapus..</div>';
        }
        if (get_value('id', 0) > 0)
            $this->db->where('kontak_id', get_value('id'));    
        $qw = $this->db->order_by('waktu DESC')->get('kontak', 50);
        if ($qw->num_rows() == 0) {
            echo '<div class="text-muted text-center" style="padding:20px 0">Belum ada pesan masuk</div>';
        } else {
            foreach ($qw->result() as $row) {
                echo '<div class="media">                    
                    <a class="pull-left">
                        <img src="'.get_gravatar($row->email, 50).'" class="media-object img-circle" alt=""/>
                    </a>
                    <div class="media-body">
                        <div class="pull-right">
                            <a href="#reply-form-inline" class="btn btn-info btn-sm reply-pesan" 
                                data-id="'.$row->kontak_id.'" data-nama="'.$row->nama.'" data-email="'.$row->email.'" title="Balas">
                                <i class="fa fa-mail-reply"></i></a>
                            <a href="'.$base_url.'action/crud/delete?n=kontak&id='.$row->kontak_id.'" class="btn btn-danger btn-sm del-pesan" title="Hapus"><i class="fa fa-trash-o"></i></a>
                        </div>
                        <h4 class="media-heading">'.$row->subjek.'</h4>
                        <div class="msg-user"><i class="glyphicon glyphicon-user"></i> '.$row->nama.' &nbsp;|&nbsp; 
                        <i class="glyphicon glyphicon-envelope"></i> '.$row->email.'</div>
                        <p>'.$row->pesan.'</p>         
                        <small class="msg-time"><i class="glyphicon glyphicon-time"></i> '.xtime($row->waktu).'</small>'; 
                    $qw2 = $this->db->select('balasan.*, user.nama')
                        ->order_by('waktu')->join('user', 'user.user_id=balasan.user_id')
                        ->get_where('balasan', array('tipe'=>'kontak','obj_id'=>$row->kontak_id));
                    foreach ($qw2->result() as $row2) {
                        echo '<div class="media-reply">
                            <strong>Balasan : &nbsp;</strong> <i class="glyphicon glyphicon-user"></i> '.$row2->nama.'                             
                            <p>'.$row2->pesan.'</p>
                            <small class="msg-time"><i class="glyphicon glyphicon-time"></i> '.xtime($row2->waktu).'</small> 
                        </div>';    
                    }                 
                echo '</div></div>';
            }
            if (get_value('id', 0) > 0)                 
                $this->db->where('kontak_id', get_value('id')); 
            $this->db->update('kontak', array('dibaca'=>1), array('dibaca'=>0));
        }
        if (get_value('id', 0) > 0) {
            echo anchor($user_type.'/kontak', '<i class="glyphicon glyphicon-list-alt"></i> Lihat semua pesan', 
                array('class'=>'btn btn-warning btn-sm','style'=>'margin-top:20px'));
        }
        ?> 
        </div>
    </div>
</div>
<div style="display:none">
    <div id="reply-form-inline" style="padding:10px">
        <form class="reply-form" method="post" action="<?php echo $base_url.'action/reply-msg/kontak'; ?>">
        <p>
            Balas ke: <strong class="reply-to-text">user@gmail.com</strong>
            <input type="hidden" name="email" class="reply-to-val" value="">
            <input type="hidden" name="obj_id" class="reply-to-id" value="0">
        </p><p>
            <textarea name="pesan" class="form-control" placeholder="Isi pesan..." style="resize:none" rows="8"></textarea>
        </p><p>
            <button type="submit" class="btn btn-success">
                <i class="glyphicon glyphicon-ok"></i> Kirim Balasan
            </button>
        </p>
        </form>
    </div>
</div>