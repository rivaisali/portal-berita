<?php
page_header('<i class="fa fa-user"></i> Edit Profil</a>');
set_breadcumb('banner|Edit Profil');
echo '<div class="col-md-12"><div class="box box-primary"><div class="box-body">';
if ($result = get_flashdata('result')) {
    $res_type = $result['ok'] ? 'success' : 'danger';
    echo '<div class="alert alert-'.$res_type.' alert-dismissable" style="margin:15px 15px 0">
        <i class="glyphicon glyphicon-info-sign"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
        $result['msg'].'</div>';
}
$this->form->upload = TRUE;
$this->form->set_input_addon(array('tgl_lahir'=>'<i class="glyphicon glyphicon-calendar"></i>',
   'pangkat_gol'=>'<button type="button" class="btn lookup-btn btn-default"><i class="glyphicon glyphicon-chevron-down"></i></button>'), 'right');
$this->form->set_help_text('password', '<em>( Kosongkan jika tidak ingin diganti )</em>');
if ($admin['user_id'] == 1) {
    $this->form->open('action/crud/update?n=user2&id='.$admin['user_id'], 'class="form-horizontal data-form"');
} else {
    enqueue_js('dsAutocomplete', NULL, 'bootstrap');
    enqueue_css('dsAutocomplete', NULL, 'bootstrap');
    enqueue_js('personil-adm', 'assets/custom/personil-adm.js', 'dsAutocomplete');
    $qw = $this->db->get_where('personil', array('nama'=>$admin['nama']), 1);
    $row = $qw->row_array();
    $pers_id = $row['pers_id'];
    $this->form->open('action/crud/update?n=personiluser&id='.$pers_id, 'class="form-horizontal data-form"')
    ->fselect('kategori', $row['kategori'], 'Kategori', list_kategori_pesonil(), '', '', '', FALSE)
    ->fselect('aktif', $row['aktif'], 'Aktif', array('Ya'=>'Ya','Tidak'=>'Tidak'), '', '', '', FALSE)
    ->ftext('nip', $row['nip'], 'NIP', array('size'=>'30','maxlength'=>'50'))    
    ->ftext('nama', $row['nama'], 'Nama Lengkap', array('size'=>'80','maxlength'=>'300','required'=>'required'))
    ->ftext('jabatan', $row['jabatan'], 'Jabatan Sekarang', array('size'=>'50','maxlength'=>'100')) 
    ->ftextarea('riw_jab', $row['riw_jab'], 'Riwayat Jabatan', array('cols'=>'60','rows'=>'3'))    
    ->fselect('gender', $row['gender'], 'Jenis Kelamin', array('L'=>'Laki-laki','P'=>'Perempuan'), array('required'=>'required'))
    ->ftext('tmp_lahir', $row['tmp_lahir'], 'Tempat Lahir', array('size'=>'30','maxlength'=>'100')) 
    ->ftext('tgl_lahir', ddmmy($row['tgl_lahir']), 'Tanggal Lahir', array('size'=>'12','maxlength'=>'12',
        'placeholder'=>'dd/mm/yyyy','width'=>'150')) 
    ->fselect('pend_jenj', $row['pend_jenj'], 'Pendidikan Terakhir', list_jenjang(), array('style'=>'display:inline-block'), '', 
        ' '.$this->form->text('pend_jur', $row['pend_jur'], array('size'=>'20','placeholder'=>'Jurusan',
        'style'=>'display:inline-block'), TRUE).' '.$this->form->text('pend_univ', $row['pend_univ'], array('size'=>'30','placeholder'=>'Nama sekolah/universitas',
        'style'=>'display:inline-block'), TRUE).' '.$this->form->text('pend_thn', $row['pend_thn'], array('size'=>'4',
            'placeholder'=>'Tahun','maxlength'=>'4','style'=>'display:inline-block'), TRUE))    
    ->ftext('pangkat_gol', $row['pangkat_gol'], 'Pangkat / Gol.', array('width'=>'280'))   
    //->fselect('pangkat_gol', $row['pangkat_gol'], 'Pangkat / Gol.', list_pangkatgol())  
    ->ftextarea('penghargaan', $row['penghargaan'], '=', array('cols'=>'60','rows'=>'3'));
    $this->form->hidden('pers_id', $pers_id);
    echo '<hr/>';
}
$this->form->ftext('username', $admin['username'], 'Username', array('size'=>'30','maxlength'=>'50','readonly'=>'readonly')); 
if ($admin['user_id'] == 1) 
    $this->form->ftext('nama', $admin['nama'], 'Nama Lengkap', array('size'=>'50','maxlength'=>'50','class'=>'focus'));
$this->form->ftext('email', $admin['email'], 'Email', array('size'=>'50','maxlength'=>'150')); 
$this->form->fpass('password', '', 'Password', array('size'=>'30','maxlength'=>'50'));
$this->form->add_row('password2', '&nbsp;', $this->form->pass('password2', '', array('size'=>'30','maxlength'=>'50',
    'placeholder'=>'Konfirmasi password'), TRUE) );
$this->form->add_row('fileupl', 'Foto', '<input type="file" name="file" accept="image/*">'); 
if (!empty($row['foto'])) {
    $this->form->add_row('fotoprev', '&nbsp;', '<img src="../uploads/personil/thumbs/'.$row['foto'].'">');        
}
echo '<div class="space-10"></div>';
$this->form->hidden('user_id', $admin['user_id']);
$this->form->hidden('level', $admin['level'], array('class'=>'no-clear'));
$this->form->fsubmit('simpan', 'ok#Update Profil');    
$this->form->close(); 
echo '</div></div></div>';
?>