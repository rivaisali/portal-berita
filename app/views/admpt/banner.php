<?php
if (!has_akses('banner-view')) {
    include_view('admin_noaccess');
} else {
    
$action = get_value('a');
switch($action):
default:
    $headertxt = '<i class="fa fa-flag"></i> Banner';
    if (has_akses('banner-add'))
        $headertxt.= ' <a href="#" class="btn-add add-banner">Tambah Banner</a>';
    page_header($headertxt);
    set_breadcumb('banner|Banner');
    include_assets('colorbox');
    enqueue_js('jquery-form', NULL, 'jquery');
    add_style('#banner-form-inline input, #banner-form-inline select { margin-bottom:15px; }
        #banner-form-inline .progress { margin:-7px 0 7px; }');
    enqueue_js('banner-adm', 'assets/custom/banner-adm.js', 'jquery,bootstrap,jquery-form,colorbox');
    echo '<div class="col-md-12"><div class="box box-primary">';
    $this->form->layout = '';
    $list_bann_grup = list_grup_banner();
    $list_bann_1 = array('' => '&mdash; Semua &mdash;') + $list_bann_grup;
    echo '<div class="dttool">Grup : '.$this->form->select('grup_filter', get_value('grup', ''), $list_bann_1, 
        array('style'=>'width:auto;max-width:320px;min-width:100px'), FALSE, TRUE).'</div>';   
    echo '<div id="banner-area" style="padding:5px 15px 15px"></div>'; 
    echo '</div></div>';
?>
    <div style="display:none">
        <div id="banner-form-inline" style="padding:25px 20px">
            <form method="post" action="<?php echo base_url('action/upload/banner') ?>" enctype="multipart/form-data">
                <input type="text" name="judul" id="ijudul" placeholder="Judul Banner" 
                    class="form-control" value="" required> 
                <input type="text" name="link" id="ilink" placeholder="http://" 
                    class="form-control" value=""> 
                <input type="file" name="file" style="width:100%" accept="image/*">
            <?php  
            $this->form->layout = 'bootstrap';
            $list_bann_grup = array('' => '(Pilih grup banner)') + $list_bann_grup;
            $list_bann_grup['-[add_new_banner]-'] = '&mdash; Buat grup baru &mdash;';
            $this->form->select('banner_grup', '', $list_bann_grup, '', FALSE);
            ?>
                <div class="progress xs progress-striped active" style="visibility:hidden">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" 
                        aria-valuemin="0" aria-valuemax="100" style="width:0">
                        <span class="sr-only">20% Complete</span>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Tambahkan Banner</button>
                <input type="hidden" name="ban_id" id="iban_id" value="0">
                <input type="hidden" name="action" id="iaction" value="insert">
            </form>            
        </div>
    </div>
<?php
endswitch;

}