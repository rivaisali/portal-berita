<?php
page_header('<i class="fa fa-picture-o"></i> Gambar Header');
set_breadcumb('header|Gambar Header');
if (!has_akses('setting-head')) {
    include_view('admin_noaccess');
} else {
    
include_assets('colorbox'); 
enqueue_js('jquery-form', NULL, 'jquery');
enqueue_js('header-img', 'assets/custom/header-img.js', 'colorbox,jquery-form');   
$curr_img = config_item('header_img'); 
$curr_path = base_path('uploads/header-img/'.$curr_img);
$curr_nm = pathinfo($curr_img, PATHINFO_FILENAME); 
$curr_ext = pathinfo($curr_img, PATHINFO_EXTENSION);
$curr_thumb = "{$curr_nm}_thumb.{$curr_ext}";
$this->load->helper('file');
$allowed_exts = array('jpg','jpeg','png','gif');
?>    
<div class="col-lg-9 col-md-8">
    <div class="box box-primary"><div class="box-body">
        <div style="padding:15px">
            <img data-img="<?php echo $curr_img ?>" data-thumb="<?php echo $curr_thumb ?>" 
                id="used-headerimg" src="<?php echo $curr_path ?>" class="img-responsive">
        </div>        
    </div></div>
</div>
<div class="col-lg-3 col-md-4 right-panel">
    <div class="box box-solid box-primary">
        <div class="box-header">
            <h3 class="box-title box-title-sm"><i class="fa fa-cog"></i> Pilihan</h3>
        </div>
        <div class="box-body header-img-others">
            <?php
            $imgs = get_filenames('uploads/header-img/'); 
            foreach ($imgs as $img) { 
                $img_nm = pathinfo($img, PATHINFO_FILENAME); 
                $img_ext = pathinfo($img, PATHINFO_EXTENSION);
                $thumb = "{$img_nm}_thumb.{$img_ext}";
                if ( !in_array($img_ext, $allowed_exts) || $img == $curr_img || substr($img_nm, -6) == '_thumb' ) continue;
                $img_url = base_path('uploads/header-img/'.$img);
                $thumb_url = base_path("uploads/header-img/{$img_nm}_thumb.{$img_ext}");
                echo '<div class="item" data-img="'.$img.'" data-thumb="'.$thumb.'">
                    <a href="'.$img_url.'" class="colorbox"><img title="Klik untuk preview" src="'.$thumb_url.'"></a>
                    <button type="button" class="btn btn-xs btn-success btn-use-headerimg">
                        <i class="glyphicon glyphicon-ok"></i> Gunakan</button>
                    <button type="button" class="btn btn-xs btn-danger btn-del-headerimg">
                        <i class="glyphicon glyphicon-trash"></i> Hapus</button>
                </div>';
            }
            ?>
        </div>
        <div class="box-footer" id="headerimg-form">
            <div class="progress xs progress-striped active" style="margin:0 0 5px;display:none">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" 
                    aria-valuemin="0" aria-valuemax="100" style="width:0">
                    <span class="sr-only">0% Complete</span>
                </div>
            </div>
            <form method="post" action="<?php echo base_url('action/upload/headerimg') ?>" enctype="multipart/form-data">
                <div id="upload-headerimg-btn" class="btn btn-sm btn-primary upload-btn">
                    <span><i class="glyphicon glyphicon-upload"></i> Upload Gambar Baru</span>
                    <input type="file" accept="image/*" name="file"></div>
            </form>
        </div>
    </div>
</div>

<?php } ?>