<?php
page_header('<i class="fa fa-home"></i> Halaman Depan');
set_breadcumb('xx|Halaman Depan');
if (!has_akses('setting-frontpage')) {
    include_view('admin_noaccess');
} else {
    
include_assets('colorbox');
enqueue_css('icon-picker', NULL, 'bootstrap');
enqueue_js('nestable', NULL, 'bootstrap');
enqueue_js('icon-picker', NULL, 'jquery,bootstrap');
enqueue_js('jquery-form', NULL, 'jquery');
enqueue_js('frontpage-js', 'assets/custom/frontpage-adm.js', 'nestable,icon-picker,colorbox,jquery-form');   
add_style(".btn-block {text-align:left;padding-left:15px} #img-bann-fp-area { background:#ebebeb; margin-bottom:15px;
    text-align:center;height:150px; } #img-bann-fp-area > img { max-height:150px; max-width:100%; }");
add_script("$('.icon-picker').iconPicker();"); 
?>
<div class="col-md-8">
    <div class="box box-primary">
        <div class="box-body main-area" style="min-height:140px">
            <p class="text-muted">
                Drag &amp; drop untuk mengurutkan item.
            </p>
            <div id="frontpage-items" class="dd">
                <ol class="dd-list">
                <?php
                $frntpg = json_decode(config_item('front_page'), TRUE);
                echo generate_nestable_items($frntpg);
                ?>
                </ol>
            </div> 
            <textarea id="serialized-item" cols="90" rows="5" style="display:none"><?php echo config_item('front_page') ?></textarea>   
        </div>
        <div class="box-footer">
            <button type="button" id="btn-save-frontpage" class="btn btn-success">
                <i class="fa fa-save"></i> Simpan Pengaturan</button>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="box box-solid box-warning">
        <div class="box-header">
            <h3 class="box-title box-title-sm"><i class="fa fa-th-large"></i> Widget Tersedia</h3>
        </div>
        <div class="box-body" id="widget-items">
            <button class="btn btn-block btn-default" data-name="Content Slider" data-form="slider">
                <i class="fa fa-stack-exchange"></i> Content Slider</button>
            <button class="btn btn-block btn-default" data-name="Tulisan Terbaru (Model 1)" data-form="post">
                <i class="fa fa-thumb-tack"></i> Tulisan Terbaru (Model 1)</button>
            <button class="btn btn-block btn-default" data-name="Tulisan Terbaru (Model 2)" data-form="post">
                <i class="fa fa-thumb-tack"></i> Tulisan Terbaru (Model 2)</button>
            <button class="btn btn-block btn-default" data-name="Halaman" data-form="page">
                <i class="fa fa-file-o"></i> Halaman</button>     
            <!-- <button class="btn btn-block btn-default" data-name="Pengumuman" data-form="pengumuman">
                <i class="fa fa-bullhorn"></i> Pengumuman</button> -->
            <button class="btn btn-block btn-default" data-name="Banner" data-form="banner">
                <i class="fa fa-flag"></i> Banner</button>  
            <button class="btn btn-block btn-default" data-name="Galeri" data-form="gallery">
                <i class="fa fa-picture-o"></i> Galeri Foto</button>
            <button class="btn btn-block btn-default" data-name="HTML" data-form="html">
                <i class="fa fa-code"></i> Text HTML</button>     
        </div>
    </div>
</div>

<div style="display:none">
    <div id="frontpage-form-inline-1" class="colorbox-form" style="padding:20px">
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div>       
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="frontpage-form-inline-page" class="colorbox-form" style="padding:20px">
        <div class="form-group">
            <label>Pilih Halaman</label>
            <?php
            $this->form->select('grup', '', list_judul_post('page'), array('id'=>'optslist-grup'), FALSE);
            ?>
        </div>    
        <input type="hidden" name="judul" value="">   
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="frontpage-form-inline-slider" class="colorbox-form" style="padding:20px">  
        <div class="form-group">
            <label>Kategori</label>
            <?php
            echo categories_dropdown('post', '', array('id'=>'optslist-post','name'=>'grup',
                'first_label'=>'&mdash; Semua &mdash;','class'=>'form-control'));
            ?>
        </div>        
        <div class="form-group">
            <label>Jumlah Item <small>(Maksimal 8)</small></label>
            <input type="text" name="count" class="form-control" value="4">
        </div>      
        <input type="hidden" name="judul" value="">    
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="frontpage-form-inline-post" class="colorbox-form" style="padding:20px">  
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div>
        <div class="form-group">
            <label>Kategori</label>
            <?php
            echo categories_dropdown('post', '', array('id'=>'optslist-post','name'=>'grup',
                'first_label'=>'&mdash; Semua &mdash;','class'=>'form-control'));
            ?>
        </div>
        <div class="form-group">
            <label>Jumlah Item</label>
            <input type="text" name="count" class="form-control" value="4">
        </div>   
        <!--<div class="form-group">
            <label>Tampilan</label>
            <select name="icon" class="form-control">
                <option value="1">1 Kolom</option>
                <option value="2">2 Kolom</option>
            </select>
        </div>-->    
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="frontpage-form-inline-pengumuman" class="colorbox-form" style="padding:20px">  
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div>
        <div class="form-group">
            <label>Jumlah Item</label>
            <input type="text" name="count" class="form-control" value="4">
        </div>   
        <!--<div class="form-group">
            <label>Tampilan</label>
            <select name="icon" class="form-control">
                <option value="1">1 Kolom</option>
                <option value="2">2 Kolom</option>
            </select>
        </div>-->   
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="frontpage-form-inline-banner" class="colorbox-form" style="padding:20px">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-bann-new" data-toggle="tab">Upload Baru</a></li>
            <li><a href="#tab-bann-plh" data-toggle="tab">Pilih Banner Tersedia</a></li>
        </ul>
        <div class="tab-content" style="padding:15px 0">
            <div class="tab-pane active" id="tab-bann-new">                
                <form action="<?php echo base_url('action/upload/fpbanner') ?>" method="post" id="bann-fp-form" enctype="multipart/form-data">
                    <input type="text" name="judul" placeholder="Judul Banner" 
                        class="form-control" value="" required> 
                    <input type="text" name="link" placeholder="http://" 
                        class="form-control" value="" style="margin:7px 0"> 
                    <button type="button" class="btn btn-sm btn-info btn-uplfpban pull-right">Start Upload</button>
                    <input type="file" name="file" accept="image/*">                    
                </form>   
                <div class="progress progress-striped active" style="margin:10px 0;display:none">
                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" 
                        aria-valuemin="0" aria-valuemax="100" style="width:0">
                        <span class="sr-only">0% Complete</span>
                    </div>
                </div>  
            </div>
            <div class="tab-pane" id="tab-bann-plh">
                <div class="form-group" style="margin:0">
                    <select name="pilih" class="form-control"><?php echo banner_opts(); ?></select>
                </div>
            </div>
            <input type="hidden" name="grup" id="url-img-banner" value="">
        </div>
        <div id="img-bann-fp-area"></div>         
        <input type="hidden" name="judul" value="">    
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="frontpage-form-inline-gallery" class="colorbox-form" style="padding:20px">
        <div class="form-group">
            <label>Album Foto</label>
            <?php
            $this->form->select('grup', '', list_album(TRUE), array('id'=>'optslist-gallery'), FALSE);
            ?>
        </div>
        <div class="form-group">
            <label>Jumlah Item <small>(Minimal 6)</small></label>
            <input type="text" name="count" class="form-control" value="6">
        </div> 
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div>        
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
    
    <div id="frontpage-form-inline-html" class="colorbox-form" style="padding:20px">         
        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control">
        </div>    
        <div class="form-group">
            <label>HTML</label>
            <textarea name="grup" class="form-control" rows="9" style="resize:vertical"></textarea>
        </div>     
        <button type="submit" class="btn btn-success">
            <i class="glyphicon glyphicon-ok"></i> Update</button>
    </div>
</div>

<?php } ?>