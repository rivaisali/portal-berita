<div class="page-header">
    <h2>Edit Akun Anda</h2>
</div>

<?php
$this->form->set_help_text('password', '<em>( Kosongkan jika tidak ingin diganti )</em>');
$this->form->open('action/crud/update/user2/', 'class="mainfrm" id="opfrm"')
->ftext('name', $admin['nama'], 'Nama Lengkap', array('size'=>'50','maxlength'=>'50','class'=>'focus'))
->ftext('email', $admin['email'], 'Email', array('size'=>'50','maxlength'=>'150'))
->ftext('username', $admin['username'], 'Username', array('size'=>'30','maxlength'=>'50'))    
->fpass('password', '', 'Password', array('size'=>'30','maxlength'=>'50'))
->add_row('password2', '&nbsp;', $this->form->pass('password2', '', array('size'=>'30','maxlength'=>'50',
    'placeholder'=>'Konfirmasi password'), TRUE) );
echo '<div class="space-10"></div>';
//app_submit_button('Update Profil', 'opfrm');
$this->form->hidden('user_id', 0)
->hidden('level', $admin['level'], array('class'=>'no-clear'));
$this->form->close(); 
?>