<?php
$curr_type  = get_value('t', 'unknown');
$action     = get_value('a');
switch($action):
case 'add': case 'edit':    
    enqueue_js('dsAutocomplete', NULL, 'bootstrap');
    enqueue_css('dsAutocomplete', NULL, 'bootstrap');
    enqueue_js('dokumen', 'assets/custom/dokumen.js', 'dsAutocomplete');
    if ($action == 'add') {
        page_header('<i class="fa fa-files-o"></i> Upload Dokumen Baru');
        set_breadcumb('dokumen|Data Dokumen', 'add|Tambah');
        $submit_label = 'ok#Tambahkan';
        $submi_action = 'insert?n=download';
        $row = array('deskripsi'=>'', 'tanggal'=>ymdhis(),'nama'=>'','ID'=>'', 'kategori'=>'dokumen');
    } else {
        page_header('<i class="fa fa-thumb-tack"></i> Edit Data Dokumen 
            <a href="dokumen?a=add" class="btn-add">Tambah Baru</a>');
        set_breadcumb('dokumen|Data Dokumen', 'add|Edit');
        $submit_label = 'ok#Update Data';        
        if ($pers_id = get_value('id')) {
            $submi_action = 'update?n=download&id='.$pers_id;
            $qw = $this->db->get_where('download', array('ID'=>$pers_id), 1);
            if ($qw->num_rows() == 0) redirect($user_type.'/dokumen');
            $row = $qw->row_array();
        }
    }
    if ($post_data = get_flashdata('post_data'))         
        $row = array_merge($row, $post_data);
    echo '<div class="col-xs-12"><div class="box box-primary">';
    if ($result = get_flashdata('result')) {
        $res_type = $result['ok'] ? 'success' : 'danger';
        echo '<div class="alert alert-'.$res_type.' alert-dismissable" style="margin:15px 15px 0">
            <i class="fa fa-info-circle"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
            $result['msg'].'</div>';
    }
    add_style(".data-form label {font-weight:normal}");
    $this->form->upload = TRUE;
    $this->form->set_input_addon(array('tgl_lahir'=>'<i class="fa fa-calendar"></i>',
        'pangkat_gol'=>'<button type="button" class="btn lookup-btn btn-default"><i class="fa fa-chevron-down"></i></button>'), 'right');
    $this->form->open('action/crud/'.$submi_action,'class="form-horizontal data-form"')
    ->ftext('nama', $row['nama'], 'Nama Dokumen', array('size'=>'80','maxlength'=>'300','required'=>'required'))
    ->ftextarea('deskripsi', $row['deskripsi'], 'Deskripsi', array('cols'=>'80','maxlength'=>'300','rows'=>'3'))
    ->add_row('fileupl', 'File Dokumen', '<input type="file" name="file">')
    ->hidden('ID', $row['ID'])
    ->hidden('tanggal', $row['tanggal'])
    ->hidden('kategori', $row['kategori'])
    ->add_row('xx')
    ->fsubmit('simpan', $submit_label)    
    ->close();    
    echo '</div></div>';
break;

default:
    add_style('.dstable th { font-weight:600; } .dstable { font-size:13px; }');
    $headertxt = '<i class="fa fa-files-o"></i> Data Dokumen';
    $headertxt.= ' <a href="dokumen?a=add" class="btn-add">Tambah Baru</a>';
    page_header($headertxt);
    set_breadcumb('dokumen|Data Dokumen');
    include_assets('dsTable', 'colorbox');
    // enqueue_js('dokumen', 'assets/custom/dokumen.js', 'dsAutocomplete');
    $editaction = has_akses('personil-edit') ? "editAction: '?a=edit&id='," : '';
    $delaction = has_akses('personil-del') ? "deleteAction: site_root + 'action/crud/delete?n=download&id='," : '';
    $thactwi = $delaction && $editaction ? 70 : 35;
    add_script("if (jQuery().dsTable) {
        var mainTable = $('#data-table');
        mainTable.dsTable({
            dataSource: site_root+'ajax/dstable/download',
            columns: [
                { name:'rowNumber', label:'No', width:45, align:'center' },
                { name:'nama', label:'Nama Dokumen', minWidth:350, format:function(col){
                    var txt = col.nama;
                    if (col.deskripsi) txt += '<br/><small class=\"text-muted\">'+col.deskripsi+'</small>';
                    return txt;
                } },
                { name:'tanggal', label:'Tgl. Upload', width:150, align:'center', format:'datetime' },
                { name:'filename', label:'File', width:140, align:'center', format:function(col){
                    if ( ! col.filename) return '&mdash;';
                    return '<a href=\"'+site_root+'uploads/dokumen/'+col.filename+'\" class=\"btn btn-default btn-sm\"><i class=\"fa fa-download\"></i> Download</a>';
                } },
            ],
            {$editaction} {$delaction}
            thActionWidth: {$thactwi},
            filter: {
                kategori: 'kategori = dokumen'
            },
            orderBy: 'tanggal desc',
            perPage: 20
        });
    }");
?>
    <div class="col-xs-12">
        <div class="box box-primary">            
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8 post-table-filter"> 
                        <label for="filter-by-kategori">Tahun &nbsp;</label>
                        <?php  
                        $fkat = get_value('tahun', date('Y'));
                        $years = array('2014','2015','2017','2016','2017');
                        $years = array_combine($years, $years);
                        $this->form->select('tahun', $fkat, $years, array(
                            'id'=>'filter-by-kategori', 'class'=>'input-sm', 'autocomplete'=>'off'), FALSE) 
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-right">
                            <div class="input-group">                                                            
                                <input value="<?php echo get_value('q', '') ?>" type="text" class="form-control input-sm"
                                    name="q" id="filter-by-query" placeholder="Search" autocomplete="off">
                                <div class="input-group-btn">
                                    <button id="btn-reset-search-pers" type="buttton" class="btn btn-sm btn-danger"
                                        title="Reset pencarian" data-toggle="tooltip" style="display:none">
                                        <i class="fa fa-times"></i></button>
                                    <button id="btn-search-pers" type="buttton" class="btn btn-sm btn-primary">
                                        <i class="fa fa-search"></i></button>
                                </div>
                            </div>                                                     
                        </div>
                    </div>
                </div>
                <div class="table-responsive" style="margin:10px 0">
                    <table id="data-table" class="table table-bordered table-striped"></table>
                </div>
            </div>
        </div>
    </div>
<?php
endswitch;
