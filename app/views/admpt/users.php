<?php
$action = get_value('a');
switch($action):
case 'add': case 'edit': 
    if (!has_akses('user-'.$action)) {
        include_view('admin_noaccess');
        return;
    }
	$this->load->helper('form');
    add_script("
    $('.tbchkhak').on('click', '.tdfirst :checkbox', function(e){
       var ceked = $(this).prop('checked');
       $(this).closest('td').next('td').find(':checkbox').prop({
            'disabled': !ceked,
            'checked': false
       });
    }).find('.tdfirst :checkbox:not(:checked)').closest('td').next('td').find(':checkbox').prop('disabled', true);");
    if ($action == 'add') {
        page_header('<i class="fa fa-users"></i> Tambah User Baru');
        set_breadcumb('users|Data User', 'add|Tambah');
        $submit_label = 'ok#Tambahkan';
        $submit_action = 'insert?n=user';
        $row = array('nama'=>'','email'=>'','username'=>'','password'=>'','user_id'=>'0','level'=>'2','unitkrj_ID'=>'1',
            'hak'=>array('berita-view','berita-add','berita-edit','pengumuman-view','pengumuman-add','pengumuman-edit',
            'pengumuman-del','halaman-view','halaman-add','halaman-edit','halaman-del','personil-view','personil-add','personil-edit','personil-del','komoditi-view','komoditi-add','komoditi-edit','komoditi-del',
            'galeri-view','galeri-add','galeri-edit','galeri-del','banner-view','banner-add','banner-edit',
            'banner-del','polling-view','polling-add','polling-edit','polling-del'));    
    } else {
        page_header('<i class="fa fa-users"></i> Edit Data User');
        set_breadcumb('users|Data User', 'edit|Edit');
        $submit_label = 'ok#Update Data';
        if ($uid = get_value('id')) {
            $submit_action = 'update?n=user2&id='.$uid;
            $qw = $this->db->get_where('user', array('user_id'=>$uid), 1);
            if ($qw->num_rows() == 0) redirect($user_type.'/users');
            $row = $qw->row_array();
            if ($row['hak'] != 'all') $row['hak'] =  unserialize($row['hak']);
        }
    }
    if ($post_data = get_flashdata('post_data'))         
        $row = array_merge($row, $post_data);
    echo '<div class="col-xs-12"><div class="box box-primary">';
    if ($result = get_flashdata('result')) {
        $res_type = $result['ok'] ? 'success' : 'danger';
        echo '<div class="alert alert-'.$res_type.' alert-dismissable" style="margin:15px 15px 0">
            <i class="glyphicon glyphicon-info-sign"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
            $result['msg'].'</div>';
    }
    $this->form->open('action/crud/'.$submit_action,'class="form-horizontal data-form"');
	/*if ($action == 'add')
	    $this->form->fselect('nama', $row['nama'], 'Nama Personil', items_fromdb('personil', 'nama', 'nama', 'ORDER BY nama'), 
	    	array('required'=>'required')); else 
		$this->form->ftext('nama', $row['nama'], 'Nama Personil', array('size'=>'50','maxlength'=>'300','required'=>'required','readonly'=>'readonly'));*/
    // $this->form->fselect('unitkrj_ID', $row['unitkrj_ID'], 'Unit Kerja',list_unitkrj());
    $this->form->ftext('nama', $row['nama'], 'Nama Pengguna', array('size'=>'50','maxlength'=>'100','required'=>'required'));
    $this->form->ftext('email', $row['email'], 'Email', array('size'=>'40','maxlength'=>'100','required'=>'required')) 
    ->ftext('username', $row['username'], 'Username', array('size'=>'30','maxlength'=>'50',
        'required'=>'required')) 
    ->fpass('password', '', 'Password', array('size'=>'30','maxlength'=>'50')) 
    ->add_row('password2', '&nbsp;', $this->form->pass('password2', '', array('size'=>'30','maxlength'=>'50',
    'placeholder'=>'Konfirmasi password'), TRUE) )
    ->add_row('hak', 'Hak Akses', chk_hak_akses($row['hak']))
    ->hidden('user_id', $row['user_id'])
    ->hidden('level', $row['level'], array('class'=>'no-clear'))
    ->fsubmit('simpan', $submit_label)    
    ->close();    
    echo '</div></div>';
break;

default:
    if (!has_akses('user-view')) {
        include_view('admin_noaccess');
        return;
    }
    $headertxt = '<i class="fa fa-users"></i> Data User';
    if (has_akses('user-add'))
        $headertxt.= ' <a href="?a=add" class="btn-add add-banner">Tambah Baru</a>';
    page_header($headertxt);
    set_breadcumb('user|Data User');
    include_assets('dsTable');
    $editaction = has_akses('user-edit') ? "editAction: '?a=edit&id='," : '';
    $delaction = has_akses('user-del') ? "deleteAction: site_root + 'action/crud/delete?n=user&id='," : '';
    $thactwi = $delaction && $editaction ? 70 : 35;
    add_script("if (jQuery().dsTable) {
        var mainTable = $('#data-table');
        mainTable.dsTable({
            dataSource: site_root+'ajax/dstable/vuser',
            columns: [
                { name:'rowNumber', label:'No.', width:40, align:'center' },
                { name:'nama', label:'Nama', width:200, minWidth:200 },
                { name:'namaunit', label:'Unit Kerja', minWidth:180, minWidth:160 },
                { name:'username', label:'Username', width:140, minWidth:140 },
                { name:'email', label:'Email', minWidth:180 },
                { name:'last_login', label:'Terakhir Login', minWidth:160 }
            ],
            {$editaction} {$delaction}
            thActionWidth: {$thactwi},
            perPage: 30,
            filter: { 'level': 'level > 1' },
            showFooter: false
        });
    }");
?>
    <div class="col-xs-12">
        <div class="box box-primary">            
            <div class="box-body">
                <div class="table-responsive" style="margin:10px 0">
                    <table id="data-table" class="table table-bordered table-striped"></table>
                </div>
            </div>
        </div>
    </div>
<?php
endswitch;