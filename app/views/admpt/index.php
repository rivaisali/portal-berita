<?php
page_header('<i class="fa fa-dashboard"></i> Dashboard');
add_style('.small-box {margin-bottom:25px;border-radius:4px} .small-box h3 > .fa {opacity:0.7}');
?>
<div style="padding:0 15px">
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-aqua">
        <div class="inner">
            <h3><i class="fa fa-thumb-tack"></i></h3>
            <p>Tulisan</p>
        </div>
        <div class="icon">
            <i class="fa fa-thumb-tack"></i>
        </div>
        <a href="<?php echo $user_type.'/post?t=post' ?>" class="small-box-footer">
            Selengkapnya <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-yellow">
        <div class="inner">
            <h3><i class="fa fa-file"></i></h3>
            <p>Halaman</p>
        </div>
        <div class="icon">
            <i class="fa fa-file"></i>
        </div>
        <a href="<?php echo $user_type.'/perkara' ?>" class="small-box-footer">
            Selengkapnya <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-red">
        <div class="inner">
            <h3><i class="fa fa-picture-o"></i></h3>
            <p>Galeri Foto</p>
        </div>
        <div class="icon">
            <i class="fa fa-picture-o"></i>
        </div>
        <a href="<?php echo $user_type.'/gallery' ?>" class="small-box-footer">
            Selengkapnya <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>

<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-fuchsia">
        <div class="inner">
            <h3><i class="fa fa-flag"></i></h3>
            <p>Banner</p>
        </div>
        <div class="icon">
            <i class="fa fa-flag"></i>
        </div>
        <a href="<?php echo $user_type.'/banner' ?>" class="small-box-footer">
            Selengkapnya <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-purple">
        <div class="inner">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            <p>Polling</p>
        </div>
        <div class="icon">
            <i class="fa fa-bar-chart-o"></i>
        </div>
        <a href="<?php echo $user_type.'/polling' ?>" class="small-box-footer">
            Selengkapnya <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-blue">
        <div class="inner">
            <h3><i class="fa fa-users"></i></h3>
            <p>Data User</p>
        </div>
        <div class="icon">
            <i class="fa fa-users"></i>
        </div>
        <a href="<?php echo $user_type.'/users' ?>" class="small-box-footer">
            Selengkapnya <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div> 
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-green">
        <div class="inner">
            <h3><i class="fa fa-bars"></i></h3>
            <p>Manajemen Menu</p>
        </div>
        <div class="icon">
            <i class="fa fa-bars"></i>
        </div>
        <a href="<?php echo $user_type.'/menuman' ?>" class="small-box-footer">
            Selengkapnya <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div> 
<div class="col-lg-3 col-xs-6">
    <div class="small-box bg-teal">
        <div class="inner">
            <h3><i class="fa fa-cogs"></i></h3>
            <p>Pengaturan</p>
        </div>
        <div class="icon">
            <i class="fa fa-cogs"></i>
        </div>
        <a href="<?php echo $user_type.'/setting' ?>" class="small-box-footer">
            Selengkapnya <i class="fa fa-arrow-circle-right"></i>
        </a>
    </div>
</div>
</div>