<?php
page_header('<i class="fa fa-cogs"></i> Pengaturan');
set_breadcumb('pengaturan|Pengaturan');
if (!has_akses('setting-view')) {
    include_view('admin_noaccess');
} else {
    add_style('form label { font-weight:normal }');
?>
<div class="col-lg-12">
    <div class="box box-primary">
        <div class="box-body">
        <?php
        if ($result = get_flashdata('result')) {
            $res_type = $result['ok'] ? 'success' : 'danger';
            echo '<div class="alert alert-'.$res_type.' alert-dismissable" style="margin:15px 15px 0">
                <i class="glyphicon glyphicon-info-sign"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
                $result['msg'].'</div>';
        }
        $this->form->open('action/crud/updateconfig','class="form-horizontal data-form"')
        ->ftext('site_title', config_item('site_title'), 'Judul Website', array('size'=>'80'))
        // ->ftext('owner_name', config_item('owner_name'), 'Nama Desa', array('size'=>'80'))
        ->ftext('owner_addr', config_item('owner_addr'), 'Alamat', array('size'=>'80'))
        // ->ftext('owner_city', config_item('owner_city'), 'Kabupaten', array('size'=>'50'))
        ->ftext('owner_phone', config_item('owner_phone'), 'Telepon', array('size'=>'30'))
        ->ftext('owner_fax', config_item('owner_fax'), 'Fax', array('size'=>'30'))
        ->ftext('owner_email', config_item('owner_email'), 'Email', array('size'=>'40'))
        // ->add_row('xx')
        // ->ftextarea('text_scroller', config_item('text_scroller'), 'Teks Berjalan', array('cols'=>'83','rows'=>'3'))
        ->add_row('xx')
        ->fsubmit('simpan', 'ok#Update Setting')    
        ->close();
        ?>
        </div>
    </div>
</div>
<?php } ?>