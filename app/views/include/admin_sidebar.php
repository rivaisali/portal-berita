<aside class="left-side sidebar-offcanvas">
    <section class="sidebar">  
        <?php
        $sidebar_menus = array(
            array(
                'url'   => '',
                'label' => 'Kunjungi Website',
                'icon'  => 'globe',
                'a_attr'  => 'target=_blank&title=Kunjungi '.$base_url
            ),
            array(
                'url'   => $user_type,
                'label' => 'Dashboard',
                'attr'  => '',
                'icon'  => 'fa-dashboard') );
        if (has_akses('berita-view'))
            $sidebar_menus[] = generate_post_menu('post', $user_type, 'fa-angle-double-right', has_akses('berita-add'));
        if (has_akses('berita-view')) {
            
            $sidebar_menus[] = array(
                'url'   => "{$user_type}/berita_temp?t=post",
                'urls'  => array("{$user_type}/berita_temp?t=post"),
                'label' => 'Daftar Berita SKPD',
                'icon'  => 'fa-building');
        }
        if (has_akses('pengumuman-view'))
            $sidebar_menus[] = generate_post_menu('pengumuman', $user_type, 'fa-angle-double-right', has_akses('pengumuman-add'));
        if (has_akses('halaman-view'))
            $sidebar_menus[] = generate_post_menu('page', $user_type, 'fa-angle-double-right', has_akses('halaman-add'));
        /*if (has_akses('personil-view')) {
            $subs = array(array(
                'url'   => "{$user_type}/penduduk",
                'label' => 'Lihat Data',
                'icon'  => 'fa-angle-double-right'));
            if (has_akses('personil-add'))
                $subs[] = array(
                    'url'   => "{$user_type}/penduduk?a=add",
                    'label' => 'Tambah Data',
                    'icon'  => 'fa-angle-double-right');
            $sidebar_menus[] = array(
                'url'   => "{$user_type}/penduduk",
                'label' => 'Data Penduduk',
                'icon'  => 'fa-user',
                'sub'   => $subs);
        }*/
        if (has_akses('galeri-view'))
            $sidebar_menus[] = array(
                'url'   => "{$user_type}/gallery",
                'label' => 'Galeri Foto',
                'icon'  => 'fa-picture-o',
                'sub'   => array(
                    array(
                        'url'   => "{$user_type}/gallery?a=album",
                        'label' => 'Album',
                        'icon'  => 'fa-angle-double-right'
                    ),
                    array(
                        'url'   => "{$user_type}/gallery",
                        'label' => 'Kelola Foto',
                        'icon'  => 'fa-angle-double-right'
                    )                  
                ));
            
            /*$sidebar_menus[] = array(
                'url'   => "{$user_type}/dokumen",
                'label' => 'Dokumen',
                'icon'  => 'fa-files-o');*/

        if (has_akses('banner-view') || has_akses('polling-view') || has_akses('cntslider-view')) {
            $subs = array();
            /*if (has_akses('komoditi-view'))
                $subs[] = array(
                    'url'   => "{$user_type}/harga-komoditi",
                    'label' => 'Harga Komoditi',
                    'icon'  => 'fa-angle-double-right');*/
            if (has_akses('banner-view'))
                $subs[] = array(
                    'url'   => "{$user_type}/banner",
                    'label' => 'Banner',
                    'icon'  => 'fa-angle-double-right');
            if (has_akses('polling-view'))
                $subs[] = array(
                    'url'   => "{$user_type}/polling",
                    'label' => 'Polling',
                    'icon'  => 'fa-angle-double-right');
            if (has_akses('cntslider-view'))
                $subs[] = array(
                    'url'   => "{$user_type}/content-slider",
                    'label' => 'Slider Konten',
                    'icon'  => 'fa-angle-double-right');
            $sidebar_menus[] = array(
                'url'   => "{$user_type}/banner",
                'urls'  => array("{$user_type}/polling","{$user_type}/harga-komoditi"),
                'label' => 'Ekstra',
                'icon'  => 'fa-th',
                'sub'   => $subs);
        }
                
        if (has_akses('setting-view')) {
            $subs = array();
            if (has_akses('setting-view'))
                $subs[] = array(
                'url'   => "{$user_type}/setting",
                'label' => 'Umum',
                'icon'  => 'fa-angle-double-right');
            if (has_akses('setting-menu'))
                $subs[] = array(
                    'url'   => "{$user_type}/menuman",
                    'label' => 'Menu',
                    'icon'  => 'fa-angle-double-right');
            if (has_akses('setting-frontpage'))
                $subs[] = array(
                    'url'   => "{$user_type}/front-page",
                    'label' => 'Halaman Depan',
                    'icon'  => 'fa-angle-double-right');
            /*if (has_akses('setting-head'))
                $subs[] = array(
                    'url'   => "{$user_type}/header-img",
                    'label' => 'Gambar Header',
                    'icon'  => 'fa-angle-double-right');*/            
            if (has_akses('setting-sidebar'))
                $subs[] = array(
                    'url'   => "{$user_type}/sidebar-item",
                    'label' => 'Sidebar',
                    'icon'  => 'fa-angle-double-right');
            $sidebar_menus[] = array(
                'url'   => '#',
                'urls'  => array("{$user_type}/setting","{$user_type}/header-img","{$user_type}/menuman",
                    "{$user_type}/front-page","{$user_type}/sidebar-item","{$user_type}/content-slider"),
                'label' => 'Pengaturan',
                'icon'  => 'fa-cogs',
                'sub'   => $subs);
        }
        
        if (has_akses('user-view')) {
            $sub_add = has_akses('user-add') ? array(
                'url'   => "{$user_type}/users?a=add",
                'label' => 'Tambah User',
                'icon'  => 'fa-angle-double-right'
            ) : array();
            $sidebar_menus[] = array(
                'url'   => "{$user_type}/users",
                'urls'  => array("{$user_type}/profil"),
                'label' => 'Data User',
                'icon'  => 'fa-users',
                'sub'   => array(
                    array(
                        'url'   => "{$user_type}/users",
                        'label' => 'Kelola User',
                        'icon'  => 'fa-angle-double-right'
                    ), $sub_add,
                    array(
                        'url'   => "{$user_type}/profil",
                        'label' => 'Profil Anda',
                        'icon'  => 'fa-angle-double-right'
                    )
                ));
        }
		
		if (has_akses('user-view')) {
            $sub_add = has_akses('user-add') ? array(
                'url'   => "{$user_type}/event?a=add",
                'label' => 'Tambah Event',
                'icon'  => 'fa-angle-double-right'
            ) : array();
            $sidebar_menus[] = array(
                'url'   => "{$user_type}/event",
                'label' => 'Data Event',
                'icon'  => 'fa-list',
                'sub'   => array(
                    array(
                        'url'   => "{$user_type}/event",
                        'label' => 'Kelola Event',
                        'icon'  => 'fa-angle-double-right'
                    ), $sub_add
                ));
        }
        generate_menu($sidebar_menus, 'class=sidebar-menu', 
            array('li_dropdown_class' => 'treeview', 
                  'ul_dropdown_class' => 'treeview-menu',
                  'qstr_key'          => 't')); 
        ?>   
    </section>
    <!-- /.sidebar -->
</aside>
