<div class="navbar-right">
    <ul class="nav navbar-nav">
        <li class="messages-menu">
            <a href="<?php echo base_url('admpt/kontak') ?>" title="Pesan Masuk">
                <i class="fa fa-envelope"></i>
                <?php
                $unread_pesan = $this->db->where('dibaca', 0)->count_all_results('kontak');
                if ($unread_pesan > 0)
                    echo '<span class="label label-success">'.$unread_pesan.'</span>';
                ?>                
            </a>
        </li>
        
        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i>
                <span><?php echo $admin['nama'] ?> <i class="caret"></i></span>
            </a>
            <ul class="dropdown-menu">
                <li class="user-header bg-light-blue">
                    <img src="<?php echo empty($admin['foto']) ? 
                        base_url('assets/img/nophoto.jpg') : base_url('uploads/personil/thumbs/'.$admin['foto']) ?>" class="img-circle" alt="User Image" />
                    <p>
                        <?php echo $admin['nama'] ?>
                        <small>Email: <?php echo $admin['email'] ?></small>
                    </p>
                </li>
                <li class="user-footer">
                    <div class="pull-left">
                        <a href="<?php echo base_url($user_type.'/profil') ?>" class="btn btn-default btn-flat">
                            <i class="fa fa-user"></i> Profil</a>
                    </div>
                    <div class="pull-right">
                        <a href="<?php echo base_url("action/logout?redirect={$user_type}/login") ?>" class="btn btn-default btn-flat btn-logout">
                            <i class="fa fa-sign-out"></i> Keluar</a>
                    </div>
                </li>
            </ul>
        </li>
    </ul> 
</div> 