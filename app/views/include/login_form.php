<?php
$form_action = base_path('action/login');
if (isset($user_type)) $form_action .= "/{$user_type}";
?>
<form class="login-form" id="login-form" method="post" action="<?php echo $form_action; ?>" role="form">
    <p>
        <label for="username">Username</label>
        <input type="text" name="username" class="form-control" autocomplete="off" autofocus required />                            
    </p>
    <p>
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" required />                            
    </p>
    <p>
        <div id="post-result"></div>
        <div class="checkbox pull-right">
            <label>
              <input type="checkbox" name="remember" /> <strong>Remember me</strong> &nbsp; &nbsp;
            </label>
        </div>
        <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-log-in"></i> Login</button>                  
    </p>    
</form>