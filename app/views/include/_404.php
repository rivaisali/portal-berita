<div class="page-header">
    <h2>Halaman tidak ditemukan</h2>
</div>
<i class="glyphicon glyphicon-info-sign pull-left" style="font-size:40px;color:#D85353"></i>
<p style="padding-left:50px">
    URL yang anda tuju tidak ada dalam database kami atau kemungkinan telah terhapus.<br />
    Silahkan kunjungi halaman kami lainnya, atau <a href="<?php echo base_url() ?>">kembali ke halaman utama.</a>
</p>