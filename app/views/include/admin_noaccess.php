<?php
page_header('<i class="fa fa-ban"></i> Access Denied');
?>
<div class="error-page">
    <h3><i class="fa fa-warning text-yellow"></i> Anda tidak punya hak akses untuk halaman ini!</h3>
</div>