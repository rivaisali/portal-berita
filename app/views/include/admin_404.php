<?php
page_header('404 Error Page');
set_breadcumb('404|404 error');
?>
<div class="error-page">
    <h2 class="headline text-info"> 404</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
        <p>
            We could not find the page you were looking for.<br>
            Meanwhile, you may <a href='<?php echo base_url($user_type) ?>'>return to dashboard</a>.
        </p>
    </div>
</div>