<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$theme_path = config_item('theme_path');
$theme_path = "./{$theme_path}{$theme}" . EXT;
if ( file_exists($theme_path) ) {
    $this->load->file($theme_path); 
}

/* End of file theme_loader.php */
/* Location: ./app/views/theme_loader.php */