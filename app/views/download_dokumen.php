<?php 
page_title('Download Dokumen | '.config_item('site_title')); 
add_style('.input-group-btn .btn { border-width:1px; }
	.dstable th { font-weight:600;vertical-align:middle !important;text-align:center; } 
	.dstable { font-size:13px; }');
enqueue_js('dsfunction', 'assets/js/dsFunction.js');
enqueue_css('dstable', 'assets/css/dsTable.css');
enqueue_js('dstable', 'assets/js/dsTable.js');
add_script("if (jQuery().dsTable) {
    var mainTable = $('#data-table');
    mainTable.dsTable({
        dataSource: site_root+'ajax/dstable/download',
        columns: [
            { name:'rowNumber', label:'No', width:45, align:'center' },
            { name:'nama', label:'Nama Dokumen', minWidth:350, format:function(col){
                var txt = col.nama;
                if (col.deskripsi) txt += '<br/><small class=\"text-muted\">'+col.deskripsi+'</small>';
                return txt;
            } },
            { name:'tanggal', label:'Tgl. Upload', width:150, align:'center', format:'datetime' },
            { name:'filename', label:'File', width:140, align:'center', format:function(col){
                if ( ! col.filename) return '&mdash;';
                return '<a href=\"'+site_root+'uploads/dokumen/'+col.filename+'\" class=\"btn btn-warning btn-sm\"><i class=\"fa fa-download\"></i> Download</a>';
            } },
        ],
        filter: {
            kategori: 'kategori = dokumen'
        },
        orderBy: 'tanggal desc',
        perPage: 30
    });
}");
?>
<div class="page-header">
    <h2>Download Dokumen</h2>
</div>
<div class="table-responsive" style="margin:10px 0">
    <table id="data-table" class="table table-bordered table-striped"></table>
</div>