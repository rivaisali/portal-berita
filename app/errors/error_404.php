<!DOCTYPE html>
<html lang="en">
<head>
<title>404 Page Not Found</title>
<style type="text/css">
#container {
	margin: 50px auto;
	border: solid 1px #dcdcdc;
	padding: 15px 25px;
	width: 400px;
}
p {
	line-height: 2em
}
</style>
</head>
<body>
	<div id="container">
		<h1><?php echo $heading; ?></h1>
		<p>Halaman yang anda tuju tidak ditemukan atau mungkin telah dihapus. Silahkan kembali ke
			<a href="<?php echo base_url() ?>">Halaman utama</a></p>
	</div>
</body>
</html>