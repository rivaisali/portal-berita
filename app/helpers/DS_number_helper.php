<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function terbilang($angka) {
    $angka = (float)$angka;
    $bilangan = array('', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 
        'tujuh', 'delapan', 'sembilan', 'sepuluh', 'sebelas');
    if ($angka < 12) {
        return $bilangan[$angka];
    } else if ($angka < 20) {
        return $bilangan[$angka - 10] . ' belas';
    } else if ($angka < 100) {
        $hasil_bagi = (int)($angka / 10);
        $hasil_mod = $angka % 10;
        return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
    } else if ($angka < 200) {
        return sprintf('seratus %s', terbilang($angka - 100));
    } else if ($angka < 1000) {
        $hasil_bagi = (int)($angka / 100);
        $hasil_mod = $angka % 100;
        return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
    } else if ($angka < 2000) {
        return trim(sprintf('seribu %s', terbilang($angka - 1000)));
    } else if ($angka < 1000000) {
        $hasil_bagi = (int)($angka / 1000); 
        $hasil_mod = $angka % 1000;
        return sprintf('%s ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
    } else if ($angka < 1000000000) {
        $hasil_bagi = (int)($angka / 1000000);
        $hasil_mod = $angka % 1000000;
        return trim(sprintf('%s juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000) {
        $hasil_bagi = (int)($angka / 1000000000);
        $hasil_mod = fmod($angka, 1000000000);
        return trim(sprintf('%s milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000000) {
        $hasil_bagi = $angka / 1000000000000;
        $hasil_mod = fmod($angka, 1000000000000);
        return trim(sprintf('%s triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else {
        return 'Tak terdefinisi!';
    }
}

if (!function_exists('add_nol'))
{
    function add_nol($str, $jumnol=2)
    {
        if (strlen($str) > $jumnol)
            return $str;
        else
        {
            $res = '';
            $n = $jumnol - strlen($str);
            $res .= repeater('0', $n);
            return $res . $str;
        }
    }
}


if (!function_exists('ribuan'))
{
    function ribuan($num = 0, $dec = 'auto 2')
    {
        $auto = FALSE;
        if ( substr($dec, 0, 4) == 'auto') {
            list ( , $dec) = explode(' ', $dec);
            $auto = TRUE;
        }
        $num = number_format($num, $dec, ',', '.');
        if ($auto) $num = str_replace(',00', '', $num);
        return $num;
    }
}

/* End of file DD_number_helper.php */
/* Location: ./app/helpers/DD_number_helper.php */
