<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('result_to_array') ) 
{
    function result_to_array($result, $field = '') {
        $arr = array();
        foreach($result as $row) {
            $arr[] = $row->$field;
        }
        return $arr;
    }
}

if ( !function_exists('array_to_pre') ) 
{
    function array_to_pre($arr = '', $return = FALSE) {
        $ret = '<pre>'.print_r($arr, 1).'</pre>';
        if ($return) return $ret; else echo $ret;
    }
}

if ( !function_exists('qstr_to_array') )
{
    function qstr_to_array($qstr = NULL) {
        if ( is_array($qstr) ) return $qstr;
        if ($qstr == '' OR $qstr == NULL) return array();
        parse_str($qstr, $qstr);
        return $qstr;
    }
}

/**
 * Memasukkan nilai dari $_POST ke dalam array (jika parameter kosong akan diambil semua)
 * @return array
 */
if ( !function_exists('post_to_array') ) 
{
    function post_to_array() {
        $res = array();    
        $jum = func_num_args();
        $ci = &get_instance();
        if ($jum == 0) {
            global $_POST;
            foreach($_POST as $key=>$val) {
                $res[$key] = $ci->input->post($key);
            }
        } else {
            $post_index = func_get_args();
            foreach($post_index as $index) {
                $res[$index] = $ci->input->post($index);
            }
        }    
        return $res;
    }
}

/**
 * Memasukkan nilai dari $_GET ke dalam array (jika parameter kosong akan diambil semua)
 * @return array
 */
if ( !function_exists('get_to_array') ) 
{
    function get_to_array() {
        $res = array();    
        $jum = func_num_args();
        $ci = &get_instance();
        if($jum == 0) {
            global $_GET;
            foreach($_GET as $key=>$val) {
                $res[$key] = $ci->input->get($key);
            }
        } else {
            $get_index = func_get_args();
            foreach($get_index as $index) {
                $res[$index] = $ci->input->get($index);
            }
        }    
        return $res;
    }
}

if ( !function_exists('array_to_attr') )
{
    function array_to_attr($arr = array()) {
        $attr = array();
    	foreach ($arr as $key => $val) {
    	    if ($val == NULL && $key != 'value')
                $attr[] = $key; else
                $attr[] = "{$key}=\"{$val}\"";
    	}
        return implode(' ', $attr);
    }
}

if ( !function_exists('attr_to_array') )
{
    function attr_to_array($attr = '') {
        $arr = array();
    	$attr = trim($attr);
        if (preg_match_all('/([\w\-\_]+)\=[\"\']([\w\s\_\-]+)[\"\']/', $attr, $matches, PREG_SET_ORDER) != FALSE) {
            foreach ($matches as $match) {
                $arr[$match[1]] = $match[2];
            }
        }
        return $arr;
    }
}


if ( !function_exists('object_to_array') )
{
    function object_to_array($d) {
    	if (is_object($d)) {
    		$d = get_object_vars($d);
    	}
    	if (is_array($d)) {
    		return array_map(__FUNCTION__, $d);
    	} else {
    		return $d;
    	}
    }
}

if (!function_exists('array_to_object'))
{
    function array_to_object($d) {
    	if (is_array($d)) {
    		return (object) array_map(__FUNCTION__, $d);
    	} else {
    		return $d;
    	}
    }
}

/** 
 * merge array, kalo ada key yg sama, value beda, concat
 * contoh: array('a'=>'satu','b'=>'dua') + array('a'=>'tiga', 'b'=>'dua') = array('a'=>'satu tiga','b'=>'dua') 
 */
if ( !function_exists('array_merge_concat') )
{
    function array_merge_concat($arr1 = array(), $arr2 = array(), $separator = ' ') {
    	foreach ($arr1 as $key1 => $val1) {
    	   if ( array_key_exists($key1, $arr2) ) {
    	       if ($val1 != $arr2[$key1]) 
    	           $arr1[$key1].= $separator.$arr2[$key1];
               unset($arr2[$key1]);
    	   }
    	}
        if (count($arr2) > 0) 
            $arr1 = array_merge($arr1, $arr2);
        return $arr1;
    }
}

// In array multidimensional 
if ( !function_exists('in_array_r') ) 
{
    function in_array_r($needle, $haystack, $strict = FALSE) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) 
                || (is_array($item) 
                && in_array_r($needle, $item, $strict))) {
                return TRUE;
            }
        }    
        return FALSE;
    }
}

if ( !function_exists('insert_after_array_key') ) {
    function insert_after_array_key($key, $array, $new_items) {
        $new_arr = array();
        foreach ($array as $k => $v) {
            $new_arr[$k] = $v;
            if ($k == $key) {
                foreach ($new_items as $nk => $nv) {
                    $new_arr[$nk] = $nv;
                }
            }
        }
        return $new_arr;
    }   
}

/* End of file DD_array_helper.php */
/* Location: ./application/helpers/DD_array_helper.php */