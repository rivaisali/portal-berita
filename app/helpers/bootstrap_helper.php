<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* ------ Button --------------------------------------------------------- */
if (!function_exists('bs_button')) {
    function bs_button($color='', $label='', $size='', $icon='', $attr='', $type='button') {
        $format = '<%s %s%s>%s%s</%s>';  
        $tag = 'button';      
        if ($color == '') $color = 'default';
        if ($size != '') $size = " btn-{$size}";
        if ($icon != '') {
            if (substr($icon, 0, 3) == 'fa-')
                $icon = '<i class="fa '.$icon.'"></i> '; else
                $icon = bs_glyphicon($icon, 'span').' ';
        }
        $class = ' class="btn btn-'.$color.$size;
        if (is_array($attr)) {
            if (isset($attr['href'])) {
                $attr['href'] = proper_url($attr['href']);
                $tag = 'a';
            }
            if (isset($attr['class'])) {
                $class.= ' '.$attr['class'];
                unset($attr['class']);
            } 
            $ci = &get_instance();
            $ci->load->helper('array');
            $attr = array_to_attr($attr);
        }
        $attr.= $class.'"';
        $attr = trim($attr);
        if ($type != '') $type = ' type="'.$type.'"';
        return sprintf($format, $tag, $attr, $type, $icon, $label, $tag);
    }
}
if (!function_exists('bs_btn_default')) {
    function bs_btn_default($label='', $size='', $icon='', $attr='', $type='') {
        return bs_button('default', $label, $size, $icon, $attr, $type);
    }
}
if (!function_exists('bs_btn_success')) {
    function bs_btn_success($label='', $size='', $icon='', $attr='', $type='') {
        return bs_button('success', $label, $size, $icon, $attr, $type);
    }
}
if (!function_exists('bs_btn_primary')) {
    function bs_btn_primary($label='', $size='', $icon='', $attr='', $type='') {
        return bs_button('primary', $label, $size, $icon, $attr, $type);
    }
}
if (!function_exists('bs_btn_info')) {
    function bs_btn_info($label='', $size='', $icon='', $attr='', $type='') {
        return bs_button('info', $label, $size, $icon, $attr, $type);
    }
}
if (!function_exists('bs_btn_warning')) {
    function bs_btn_warning($label='', $size='', $icon='', $attr='', $type='') {
        return bs_button('warning', $label, $size, $icon, $attr, $type);
    }
}
if (!function_exists('bs_btn_danger')) {
    function bs_btn_danger($label='', $size='', $icon='', $attr='', $type='') {
        return bs_button('danger', $label, $size, $icon, $attr, $type);
    }
}

if (!function_exists('bs_btn_link')) {
    function bs_btn_link($label='', $size='', $icon='', $attr='', $type='') {
        return bs_button('link', $label, $size, $icon, $attr, $type);
    }
}

/* ------- Navbar ----------------------------------------------------- */
function bs_navbar($inverse=FALSE, $fixed=FALSE, $brand='', $in_container=TRUE, $content='', $attr='') {
    $res = '<nav role="navigation" class="navbar navbar-';
    if ($inverse) $res.= 'inverse'; else $res.= 'default';
    $res.= ' navbar-';
    if ($fixed) $res.= 'fixed'; else $res.= 'static';
    $res.= '-top"';
    if ($attr != '') $res.= " {$attr}";
    $res.= '>';
    if ($in_container) $res.= '<div class="container">';
    if ($brand != '') {
        $res.= '<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span>
            <span class="icon-bar"></span><span class="icon-bar"></span>
          </button><a class="navbar-brand" href="'.base_url().'">'.$brand.'</a></div>';
    }
    $res.= '<div class="navbar-collapse collapse">'.$content.'</div>';
    if ($in_container) $res.= '</div>';
    $res.= '</nav>';
    return $res;
}

function bs_navbar_menu($pull_right=FALSE) {
    $res = '<ul class="nav navbar-nav';
    if ($pull_right) $res.= ' navbar-right';
    $res.= '">';
    
    $res.= '</ul>';
    return $res;
}

/* ------- Typography ------------------------------------------------- */
function bs_page_header($title='', $right_items='') {  
    $ret = '<div class="page-header">';
    if ($right_items != '') {
        $ret.= '<div class="pull-right">'.$right_items.'</div>';
    }
    $ret.= '<h2>'.$title.'</h2></div>';
    return $ret;
}

/* ------- List ------------------------------------------------------ */
function bs_menu_list($items='') {
    $ret = '';
    foreach ($items as $item) {
        $uri = $item[0];
        $label = $item[1];
        $icon = isset($item[2]) ? '<span class="glyphicon glyphicon-'.$item[2].'"></span>' : '';
        $ret.= '<li'.is_current_uri($uri, 'active').'>';
        $ret.= anchor($uri, $icon.'<span class="menu-label">'.$label.'</span>');
        $ret.= '</li>';
    } 
    return $ret;
}

/* ------ Alert ---------------------------------------------------------- */
if (!function_exists('bs_alert')) {
    function bs_alert($text='', $color_type='', $icon='', $close_btn=FALSE) {
        if ($color_type == '') $color_type = 'success';
        $res = '<div class="alert alert-'.$color_type;
        if ($close_btn) {
            $res.= ' alert-dismissable">';
            $res.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        } else $res.= '">';
        if ($icon != '')
            $res.= bs_glyphicon($icon).' &nbsp; ';
        $res.= $text.'</div>';
        return $res;
    }
}

/* ------ Label ---------------------------------------------------------- */
if (!function_exists('bs_label')) {
    function bs_label($text='', $color_type='', $tag='span') {
        $format = '<%s class="label label-%s">%s</%s>';
        if ($color_type == '') $color_type = 'default';
        return sprintf($format, $tag, $color_type, $text, $tag);
    }
}

/* ------ Glyphicon ------------------------------------------------------ */
if (!function_exists('bs_glyphicon')) {
    function bs_glyphicon($icon='', $attr='', $tag='span') {
        $format = '<%s class="glyphicon glyphicon-%s"%s></%s>';
        return sprintf($format, $tag, $icon, $attr, $tag);
    }
}

/* ------- Badge --------------------------------------------------- */
if (!function_exists('bs_badge')) {
    function bs_badge($text='', $bgcolor='', $color='', $tag='span') {
        $style = '';
        if ($bgcolor != '') $style.= 'background-color:'.$bgcolor.';';
        if ($color != '') $style.= 'color:'.$color;
        if ($style != '') $style = ' style="'.$style.'"';
        $format = '<%s class="badge"%s>%s</%s>';
        return sprintf($format, $tag, $style, $text, $tag);
    }
}

/* ---------- Tools ----------------------------------------------- */
if (!function_exists('bs_datetime_picker')) {
    function bs_datetime_picker($name='', $value='', $options=array()) {
        $res = '';
        $ci = &get_instance();
        $ci->load->library('form');
        $default = array('year_range'=>'-3:+3', 'icon_type'=>'bs', 'width'=>'240', 'readonly'=>TRUE);  // [bs|fa] 
        $options = array_merge($default, $options); extract($options);
        //$year_range = isset($options['year_range']) ? $options['year_range'] : '-3:+3';
        $exp = explode(':', $year_range);
        list ($start_year, $end_year) = year_range($exp[0], $exp[1]);
        if ($value == 'now') $value = date('Y-m-d H:i:s');
        $picker_value = '';
        if (isset($options['date_only'])) {
            $opt_extra = 'startView: 2, minView: 2,';
            $date_format = 'dd MM yyyy';
            $link_format = 'yyyy-mm-dd';
            if ($value != '') {
                $value = date('Y-m-d', strtotime($value));
                $picker_value = tanggal($value);
            }
            if ($icon_type == 'fa')
                $icon = '<i class="fa fa-calendar"></i>'; else
                $icon = '<span class="glyphicon glyphicon-calendar"></span>';
            $ci->form->set_input_addon(array($name.'_picker'=>$icon), 'right');
        } elseif (isset($options['time_only'])) {
            $opt_extra = 'startView: 1,	minView: 0, maxView: 1,';
            $date_format = 'hh:ii';
            $link_format = 'hh:ii';
            if ($value != '') {
                $value = date('H:i', strtotime($value));
                $picker_value = date('H:i', strtotime($value));
            }
            if ($icon_type == 'fa')
                $icon = '<i class="fa fa-clock-o"></i>'; else
                $icon = '<span class="glyphicon glyphicon-time"></span>';
            $ci->form->set_input_addon(array($name.'_picker'=>$icon), 'right');
        } else {
            $opt_extra = '';
            $date_format = 'dd MM yyyy - hh:ii';
            $link_format = 'yyyy-mm-dd hh:ii';
            if ($value != '') $picker_value = tanggal_jam($value);
            if ($icon_type == 'fa')
                $icon = '<i class="fa fa-calendar"></i>'; else
                $icon = '<span class="glyphicon glyphicon-calendar"></span>';
            $ci->form->set_input_addon(array($name.'_picker'=>$icon), 'right');
        }   
        $res.= '<span style="display:block;width:'.$width.'px">';     
        $res.= $ci->form->text($name.'_picker', $picker_value, array('readonly'=>'readonly',
            'style'=>'cursor:pointer;width:100%'), TRUE);
        $res.= '</span>';
        $res.= $ci->form->hidden($name, $value, '', TRUE);
        add_script('$("#'.$name.'_picker").datetimepicker({ '.$opt_extra.'
            format: "'.$date_format.'", autoclose: 1, forceParse: 1,
            todayBtn: 1, showMeridian: 1, pickerPosition: "bottom-left", todayHighlight: 1,
            language: "id", startDate: "'.$start_year.'-01-01 00:00", endDate: "'.$end_year.'-12-31 23:59",
            minuteStep: 5, linkField: "'.$name.'", linkFormat: "'.$link_format.'"}).on("click", function(){
            $(this).val(""); $("#'.$name.'").val(""); });');
        return $res;
    }
}

if (!function_exists('bs_date_picker')) {
    function bs_date_picker($name='', $value='', $options=array()) {
        $default = array('date_only'=>TRUE, 'width'=>200);
        $options = array_merge($default, $options);
        $res = bs_datetime_picker($name, $value, $options);
        return $res;
    }
}

if (!function_exists('bs_time_picker')) {
    function bs_time_picker($name='', $value='', $options=array()) {
        $default = array('time_only'=>TRUE, 'width'=>200);
        $options = array_merge($default, $options);
        $res = bs_datetime_picker($name, $value, $options);
        return $res;
    }
}


/* End of file bootstrap_helper.php */
/* Location: ./app/helpers/bootstrap_helper.php */