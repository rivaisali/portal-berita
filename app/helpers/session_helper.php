<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Required : global_helper, session_lib

if ( !function_exists('get_userdata') )
{
    function get_userdata($name, $default = FALSE, $incpref = TRUE)
    {
        $ci = &get_instance();
        if ($incpref) $name = add_prefix($name);
        $udata = $ci->session->userdata($name);
        return $udata == FALSE ? $default : $udata;
    }
}

if (!function_exists('set_userdata'))
{
    function set_userdata($name, $value, $incpref=TRUE)
    {
        $ci = &get_instance();
        if(is_array($name)) {
            foreach($name as $key=>$val) {
                if($incpref) $key=add_prefix($key);
                $ci->session->set_userdata($key, $val);
            }
        } else {
            if($incpref) $name=add_prefix($name);
            $ci->session->set_userdata($name, $value);
        }
        
    }
}

if ( !function_exists('unset_userdata') )
{
    function unset_userdata($name, $incpref = TRUE)
    {
        $ci = &get_instance();
        if ($incpref) $name = add_prefix($name);
        $ci->session->unset_userdata($name);
    }
}

if ( !function_exists('isset_userdata') )
{
    function isset_userdata($name, $incpref=TRUE)
    {
        $ci = &get_instance();
        if($incpref) $name=add_prefix($name); 
        return $ci->session->userdata($name) != FALSE;
    }
}

if (!function_exists('push_userdata')) // tambahkan item di userdata array
{
    function push_userdata($name, $new_value, $incpref=TRUE)
    {
        if ($incpref) $name = add_prefix($name);        
        if ( isset_userdata($name, FALSE) ) {
            $cur_val = get_userdata($name, FALSE);
            if ( !is_array($cur_val) ) {
                return FALSE;
                exit;
            }
            if ( is_array($new_value) ) 
                $cur_val = array_merge($cur_val, $new_value); else
                $cur_val[] = $new_value;
            set_userdata($name, $cur_val, FALSE);   
            return TRUE;          
        } else {
            if ( !is_array($new_value) )
                $new_value = array($new_value); 
            set_userdata($name, $new_value, FALSE);    
            return TRUE;    
        }   
    }
}

if (!function_exists('get_flashdata'))
{
    function get_flashdata($name, $default=FALSE, $incpref=TRUE)
    {
        $CI = &get_instance();
        if ($incpref) $name = add_prefix($name);
        $fdata = $CI->session->flashdata($name);
        return $fdata == FALSE ? $default : $fdata;
    }
}

if (!function_exists('keep_flashdata'))
{
    function keep_flashdata($name, $incpref=TRUE)
    {
        $CI = &get_instance();
        if ($incpref) $name = add_prefix($name);
        $CI->session->keep_flashdata($name);
    }
}

if (!function_exists('set_flashdata'))
{
    function set_flashdata($name, $value='', $incpref=TRUE)
    {
        $ci = &get_instance();
        if (is_array($name)) {
            foreach ($name as $key=>$val) {
                if ($incpref) $key = add_prefix($key);
                $ci->session->set_flashdata($key, $val);
            }
        } else {
            if ($incpref) $name = add_prefix($name);
            $ci->session->set_flashdata($name, $value);
        }
        
    }
}

if (!function_exists('unset_flashdata'))
{
    function unset_flashdata($name, $incpref=TRUE)
    {
        $ci=&get_instance();
        if($incpref) $name=add_prefix($name);
        $ci->session->unset_flashdata($name);
    }
}

if (!function_exists('isset_flashdata'))
{
    function isset_flashdata($name, $incpref=TRUE)
    {
        $ci = &get_instance();
        if ($incpref) $name = add_prefix($name); 
        return $ci->session->flashdata($name) != FALSE;
    }
}


/* End of file session_helper.php */
/* Location: ./app/helpers/session_helper.php */
