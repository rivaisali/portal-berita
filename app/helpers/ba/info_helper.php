<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('get_cookie_size'))
{
    function get_cookie_size($name='') {
        if (isset($_COOKIE[$name])) {
            $data = $_COOKIE[$name];
            $serialized_data = serialize($data);
            $size = strlen($serialized_data);
            echo 'Length: '.strlen($data).' - Size: '.($size * 8/1024).' Kb';
        }
    }
}

/* End of file info_helper.php */
/* Location: ./app/helpers/info_helper.php */