<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('post_data'))
{
    function post_data($ind='', $ifempty='')
    {
        $arr = get_flashdata('post_data');
        if (!$arr || $arr==NULL || empty($arr[$ind])) return $ifempty; else        
        return $arr[$ind];
    }
}

if (!function_exists('list_tanggal'))
{
    function list_tanggal()
    {
        $day = array();
        for ($i = 1; $i <= 31; $i++)
        {
            $day[$i] = $i;
        }
        return $day;
    }
}

if (!function_exists('list_jk'))
{
    function list_jk()
    {
        $jk = array('L'=>'Laki-Laki','P'=>'Perempuan');
        return $jk;
    }
}

if (!function_exists('create_items')) 
{
    function create_items($arr=array()) {
        $items=array();
        foreach($arr as $val) {
            $items[$val]=ucfirst($val);
        }
        return $items;
    }    
}

if (!function_exists('create_combobox')) 
{
    function create_combobox($items=array(), $first_empty=TRUE) {
        $res = '';
        if ($first_empty)
            $res.= '<option></option>';
        foreach($items as $val=>$label) {
            $res.= '<option value="'.$val.'">'.$label.'</option>';
        }
        return $res;
    }    
}


if (!function_exists('items_fromdb'))
{
    function items_fromdb($table, $index_field, $value_field, $extra_query='', $first='', $last='')
    {
        $ci =& get_instance();
        $res = array();
        $query = $ci->db->query("SELECT $index_field, $value_field AS cbvalue FROM ".db_prefix($table)." " . $extra_query);
        if(is_array($first) && !empty($first)) {
            foreach($first as $key=>$val) {
                $res[$key]=$val;
            }
        }
        if ($query->num_rows() > 0)
        {
            foreach ($query->result_array() as $row)
            {
                $res[$row[$index_field]] = $row['cbvalue'];
            }
        }
        if(is_array($last) && !empty($last)) {
            foreach($last as $key=>$val) {
                $res[$key]=$val;
            }
        }
        return $res;
    }
}


if (!function_exists('next_id'))
{
    function next_id($table, $field, $prechar='', $digit=0)
    {
        $CI = &get_instance();
        $qw = $CI->db->query("SELECT $field FROM ".db_prefix($table)." ORDER BY $field DESC LIMIT 1");
        if($qw->num_rows()>0) {
            $row = $qw->row();
            $id = $row->$field;    
            if($prechar!='') {
                $id = substr($id, strlen($prechar), strlen($id));
            }  
            $newid = (int) $id;
            $newid++;
        } else {
            $newid = 1; 
        }    
        if($digit > 0) $prechar.add_nol($newid, $digit);
        return $newid;
    }
}

function longjk($jk) {
    if($jk=='L') return 'Laki-Laki';
    elseif($jk=='P') return 'Perempuan';
}

/* End of file DS_form_helper.php */
/* Location: ./app/helpers/DS_form_helper.php */
