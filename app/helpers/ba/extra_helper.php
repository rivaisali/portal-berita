<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function curl_post($url, $post=FALSE, $data=array(), $return_trans=TRUE) {
   if($post) {
       $fields = '';
       foreach($data as $key => $value) {        
            $fields .= $key . '=' . urlencode($value) . '&'; 
       }
       $fields = rtrim($fields, '&');
   }
   $chandle = curl_init();
   curl_setopt($chandle, CURLOPT_URL, $url);   
   if($post) {
        curl_setopt($chandle, CURLOPT_POST, TRUE);
        curl_setopt($chandle, CURLOPT_POSTFIELDS, $fields);
   }
   curl_setopt($chandle, CURLOPT_HEADER, 0);
   curl_setopt($chandle, CURLOPT_TIMEOUT,30);
   if($return_trans) {
        curl_setopt($chandle, CURLOPT_RETURNTRANSFER, TRUE);   
        $result = curl_exec($chandle);
        return $result;
   } else {
        curl_setopt($chandle, CURLOPT_RETURNTRANSFER, FALSE);
        curl_exec($chandle);
   }   
   curl_close($chandle);   
}

function google_shrink($url) {
   $curlHandle = curl_init();
   // melakukan request ke server Google API
   curl_setopt($curlHandle, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
   curl_setopt($curlHandle, CURLOPT_HEADER, 0);
   curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
   // menentukan tipe konten hasil request yg berupa JSON
   curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
   // parameter yang berisi URL yang akan disingkat
   curl_setopt($curlHandle, CURLOPT_POSTFIELDS, '{"longUrl":"'.$url.'"}');
   curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
   // lakukan request dengan POST method
   curl_setopt($curlHandle, CURLOPT_POST, 1);
   // baca data hasil request yg berupa JSON
   $content = curl_exec($curlHandle);
   curl_close($curlHandle);
   // ekstrak data JSON untuk mendapatkan hasil URL yg disingkat
   $data = json_decode($content);
   return $data->id;
}

function adfly_shrink($url) {
    $ci = &get_instance();
    $ci->config->load('account');
    $key = $ci->config->item('adfly_key');
    $uid = $ci->config->item('adfly_uid');
    $url = "http://api.adf.ly/api.php?key=$key&uid=$uid&advert_type=int&domain=adf.ly&url=".$url;
    return curl_post($url, FALSE, array(), TRUE);
}

/* End of file extra_helper.php */
/* Location: ./application/helpers/extra_helper.php */