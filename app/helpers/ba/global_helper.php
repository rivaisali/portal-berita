<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sama dengan $this->config->set_item 
 * @return void
 */
if ( !function_exists('set_config') ) {
    function set_config() {
        $args = func_get_args();
        $jum = func_num_args();
        $ci = &get_instance();
        if ($jum > 0) {
            $arg1 = func_get_arg(0);
            if (is_array($arg1)) {
                foreach($arg1 as $key=>$val) {
                    $ci->config->set_item($key, $val);
                }
            } else {
                if ($jum == 1) $arg2=''; else $arg2 = func_get_arg(1);
                $ci->config->set_item($arg1, $arg2);
            }
        }
    }
}

/**
 * Sama dengan $this->input->post 
 * @return string
 */
if ( !function_exists('post_value') ) {
    function post_value($index = '', $default = FALSE) {
        $ci = &get_instance();
        $res = $ci->input->post($index);
        return !$res ? $default : $res;
    }
}

/**
 * Sama dengan $this->input->get 
 * @return string
 */
if ( !function_exists('get_value') ) {
    function get_value($index = '', $default = FALSE) {
        $ci = &get_instance();
        $res = $ci->input->get($index);
        return !$res ? $default : $res;
    }
}

/**
 * Sama dengan $this->input->is_ajax_request()
 * @return boolean
 */ 
if(!function_exists('is_ajax')) {
    function is_ajax() {
        $ci=&get_instance();
        return $ci->input->is_ajax_request();
    }
}

/**
 * Mis: cari nama yang id=1   
 * @param string $table nama tabel
 * @param string $field nama field
 * @param string $where db library where format
 * @return string
 */
if (!function_exists('get_where_value'))
{
    function get_where_value($table='', $field='', $where, $default = FALSE) {
        $ci = &get_instance();
        $qw = $ci->db->select($field)->get_where($table, $where, 1);
        if ($qw->num_rows() > 0) {
            $row = $qw->row();
            return $row->$field;
        }
        return $default;
    }
}

/**
 * Mis: cari nama yang id=1   
 * @param string $table nama tabel
 * @param string $field nama field
 * @param string $where db library where format
 * @return string
 */
if (!function_exists('get_next_id'))
{
    function get_next_id($table='') {
        $ci = &get_instance();
        $query = $this->ci->db->query("SELECT Auto_increment FROM information_schema.tables 
            WHERE table_name='{$table}'");
        $row = $query->row();
        return $row->Auto_increment;
    }
}

/**
 * sama dengan lang() tapi pake fungsi sprintf  
 * @return string
 */
if (!function_exists('flang'))
{
    function flang($item='') {
        $ci = &get_instance();
        $format = $ci->lang->line($item);
        if(func_num_args() <= 1)
            return $format;
        else {
            $args = func_get_args();
            $args[0] = $format;
            return call_user_func_array('sprintf', $args);
        }        
    }
}

/**
 * sama dengan $this->encrypt->encode()
 * @return string
 */
if (!function_exists('encrypt'))
{
    function encrypt($str='')
    {
        $CI = &get_instance();
        $CI->load->library('encrypt');
        return $CI->encrypt->encode($str);
    }
}

/**
 * sama dengan $this->encrypt->decode()
 * @return string
 */
if (!function_exists('decrypt'))
{
    function decrypt($str='')
    {
        $CI = &get_instance();
        $CI->load->library('encrypt');
        return $CI->encrypt->decode($str);
    }
}

/**
 * sama dengan $this->encrypt->md5()
 * @return string
 */
if (!function_exists('md5encrypt'))
{
    function md5encrypt($str='')
    {
        $CI = &get_instance();
        $CI->load->library('encrypt');
        return $CI->encrypt->md5($str);
    }
}

/**
 * menambahkan prefix pada string sesuai $config['global_prefix']
 * @return string
 */
if ( !function_exists('add_prefix') )
{
    function add_prefix($str='') {
        $CI = &get_instance();
        $pref = $CI->config->item('global_prefix');
        return $pref.$str;
    }   
}

/**
 * menambahkan prefix pada string sesuai config db prefix
 * @return string
 */
if ( !function_exists('db_prefix') )
{
    function db_prefix($tbname = '') {
        $CI = &get_instance();
        return $CI->db->dbprefix($tbname);
    }   
}

/**
 * generate attribut, ex: id="myid" class="myclass"
 * @param string/array $params
 */
if ( !function_exists('build_html_attr') ) {
    function build_html_attr($params = NULL) {
        if ($params == NULL) return '';
        if ( !is_array($params) ) {
            parse_str($params, $params);
        }
        $attr_arr = array();
    	foreach ($params as $key => $val) {
    	    if ($val == NULL && $key != 'value')
                $attr_arr[] = $key; else
                $attr_arr[] = "{$key}=\"{$val}\"";
    	}
        return implode(' ', $attr_arr);
    }   
}

/* End of file global_helper.php */
/* Location: ./app/helpers/global_helper.php */