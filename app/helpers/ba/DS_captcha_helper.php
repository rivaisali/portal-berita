<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Menggunakan captcha  
 * @param int $digit jumlah digit
 * @param string $char tipe karakter [numeric | alnum] default numeric
 * @param string $sessname nama userdata menyimpan captcha. jika kosong tidak disimpan 
 * @return array
 */
if (!function_exists('mycaptcha'))
{
    function mycaptcha($digit=5, $char='', $sessname='CAPTCHA')
    {
        if($char == '') $char = 'numeric';
        $CI = &get_instance();
        $CI->load->helper('url','string');
        $vals = array(
            'word'          => strtoupper(random_string($char, 5)),
            'img_path'      => './assets/captcha/',
            'img_url'       => base_url('assets/captcha/'),
            'font_path'     => './sys/fonts/ariblk.ttf',
            'img_width'     => 120,
            'img_height'    => 35,
            'expiration'    => 60
        );
        $cap = create_captcha($vals);
        $res['image'] = $cap['image'];
        $res['word'] = $cap['word']; 
        if($sessname != '') {
            $CI->load->helper('session');
            set_userdata($sessname, $cap['word']);
        }        
        return $res;
    }     
}


/* End of file DS_captcha_helper.php */
/* Location: ./app/helpers/DS_captcha_helper.php */