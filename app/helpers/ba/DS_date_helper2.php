<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

function is_empty_date($datetime) {
    return empty($datetime) OR ($datetime == '0000-00-00') OR ($datetime == '0000-00-00 00:00:00');
}

function days_between($ymd1, $ymd2 = NULL) {
    if ( ! $ymd2) $ymd2 = date('Y-m-d');
    $datediff = abs(strtotime($ymd1) - strtotime($ymd2));
    return floor($datediff / (60 * 60 * 24));
}

function day_list($short = FALSE) {
    static $days = array();
    $lang_prefix = $short ? 'dayname_short_' : 'dayname_';
    if ( ! isset($days[$lang_prefix])) {
        get_instance()->lang->load('date');
        $days[$lang_prefix] = array();
        for ($i = 1; $i <= 12; $i++) {
            $days[$lang_prefix][$i] = lang($lang_prefix . $i);
        }
    }
    return $days[$lang_prefix];
}

/**
 * Get day name
 * require : custom date_lang
 * 
 * @param mixed $date [now|y-m-m|d-m-y|day_index(0 = sunday)|time]
 * @param bool $short short day name (3 digit)
 * @param  string $empty_val default value if error or empty
 * @return string
 */
function day_name($date = 'now', $short = FALSE, $empty_val = '') { 
    get_instance()->lang->load('date');
    $lang_prefix = $short ? 'dayname_short_' : 'dayname_';
    if ($date === '' OR $date === NULL OR $date === '0000-00-00') return $empty_val;
    if ($date == 'now') {
        return lang($lang_prefix . mdate('%w'));
    } elseif (is_string($date)) { 
        // in day index format (0 = sunday)
        if (strlen($date) == 1) {
            $date = (int) $date;
            return lang($lang_prefix . $date);
        }
        // in Y-m-d format
        if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}/', $date, $match)) {
            $date = date('w', strtotime($match[0]));
            return lang($lang_prefix . $date);
        }
        // in d/m/Y or d-m-Y format
        if (preg_match('/^[0-9]{2}([\-|\/]{1})[0-9]{2}[\-|\/]{1}[0-9]{4}/', $date, $match)) {
            $date = date('w', strtotime(dmy2ymd($match[0], $match[1]))); 
            return lang($lang_prefix . $date);
        }
    } elseif (is_int($date) && ($date >= 0 && $date <= 6)) { 
        return lang($lang_prefix . $date);
    } else { // in time format
        return lang($lang_prefix . date('w', $date));
    }
    return $empty_val;
}

function month_list($short = FALSE) {
    static $months = array();
    $lang_prefix = $short ? 'monthname_short_' : 'monthname_';
    if ( ! isset($months[$lang_prefix])) {
        get_instance()->lang->load('date');
        $months[$lang_prefix] = array();
        for ($i = 1; $i <= 12; $i++) {
            $months[$lang_prefix][$i] = lang($lang_prefix . $i);
        }
    }
    return $months[$lang_prefix];
}

/**
 * Get month name
 * require : custom date_lang
 * 
 * @param mixed $date [now|y-m-m|d-m-y|month_index(1 = jan)|time]
 * @param bool $short short month (3 digit)
 * @param  string $default default value if error or empty
 * @return string
 */
function month_name($date = 'now', $short = FALSE, $default = '') { 
    get_instance()->lang->load('date');
    $lang_prefix = $short ? 'monthname_short_' : 'monthname_';
    if (empty($date) OR $date == '0000-00-00') return $default;
    if ($date == 'now') {
        return lang($lang_prefix . mdate('%n'));
    } elseif (is_string($date)) { 
        // check if date in Y-m-d format
        list(, $m, ) = array_pad(explode('-', $date), 3, '0');
        if ($m = (int) $m) return lang($lang_prefix . $m);
        // check if date in d/m/Y format
        list(, $m, ) = array_pad(explode('/', $date), 3, '0');
        if ($m = (int) $m) return lang($lang_prefix . $m);
        // check if string month index
        $date = (int) $date;
        if ($date > 0 && $date < 13) 
            return lang($lang_prefix . $date);
    } elseif (is_int($date) && ($date > 0 && $date < 13)) {
        return lang($lang_prefix . $date);
    } else { // in time format
        return lang($lang_prefix . date('n', $date));
    }
    return $default;
}

function month_index($month_name, $short = FALSE) {
    $month_list = month_list($short);
    return array_search($month_name, $month_list);
}

function fdate_date($date = 'now', $short_month = FALSE, $default = NULL) { 
    $format = $short_month ? '%j %M %Y' : '%j %F %Y'; 
    return format_date($date, $format, $default);
}

function fdate_datetime($datetime = '', $time_sep = ' - ', $default = NULL) {
    if (empty($time_sep)) $time_sep = ' ';
    return format_date($datetime, "%j %F %Y{$time_sep}%H:%i", $default);
}

function fdate_daydate($date = 'now', $default = NULL) {
    return format_date($date, "%l, %j %F %Y", $default); 
}

function fdate_daydatetime($date = 'now', $time_sep = ' Pukul ', $default = NULL) {
    if (empty($time_sep)) $time_sep = ' ';
    return format_date($date, "%l, %j %F %Y{$time_sep}%H:%i", $default);
}

function date_dmy($date = 'now', $short_year = FALSE, $sep = '/', $default = NULL) {
    $format = $short_year ? "%d{$sep}%m{$sep}%y" : "%d{$sep}%m{$sep}%Y";
    return format_date($date, $format, $default);
}

function date_ymd($date = 'now', $default = NULL) {
    return format_date($date, '%Y-%m-%d', $default);
}

function date_ymdhis($date = 'now', $default = NULL) {
    return format_date($date, '%Y-%m-%d %H:%i:%s', $default);
}

function date_hi($date = 'now', $default = NULL) {
    return format_date($date, '%H:%i', $default);
}

function dmy2ymd($dmy, $default = NULL) {
    if (preg_match('/^([0-9]{2})[\/\-\.]{1}([0-9]{2})[\/\-\.]{1}([0-9]{2,4})$/', $dmy, $match)) { 
        return $match[3] . '-' . $match[2] . '-' . $match[1];
    }
    return $default;
}

function format_date($date = 'now', $format = '%d/%m/%Y', $default = NULL) {
    if (empty($date) OR $date == '0000-00-00' OR $date == '0000-00-00 00:00:00') return $default;
    if ($date === 'now') $date_int = now();
    elseif (is_int($date)) $date_int = $date;
    elseif (preg_match('/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4})([0-9\ \:]*)/', $date, $match)) { 
        $date_int = strtotime( $match[3] . '-' . $match[2] . '-' . $match[1] . (isset($match[4]) ? ' '.$match[4] : '') );
    } else {
        if ( ! $date_int = strtotime($date)) {
            return $default;
        }
    }
    $format = str_replace(array('%F','%M','%l','%D'), 
        array(
            month_name($date_int), 
            month_name($date_int, TRUE),
            day_name($date_int),
            day_name($date_int, TRUE)
        ), $format
    );
    return mdate($format, $date_int);
}

function year_range($start='', $end='') {
    if (strlen($start) < 4) {
        if (substr($start, 0, 1) == '+')
            $year1 = date("Y") + substr($start, 1, strlen($start));
        elseif (substr($start, 0, 1) == '-')
            $year1 = date("Y") - substr($start, 1, strlen($start));
        elseif ($start == '0')
            $year1 = date("Y");
    } else $year1 = $start;
    if (strlen($end) < 4) {
        if (substr($end, 0, 1) == '+')
            $year2 = date("Y") + substr($end, 1, strlen($end));
        elseif (substr($end, 0, 1) == '-')
            $year2 = date("Y") - substr($end, 1, strlen($end));
        elseif ($end == '0')
            $year2 = date("Y");
    } else $year2 = $end;
    $arr = range($year1, $year2);
    return array_combine($arr, $arr);
}

/**
 * Menghitung umur berdasar tanggal lahir
 * @param date $date_of_birth Y-m-d
 * @param int [0-4]
 * 0 = (int) tahun
 * 1 = 25 Tahun
 * 2 = 25 Tahun 3 Bulan
 * 3 = 25 Tahun 3 Bulan 12 hari
 * 4 = obj(y, m, d, h, i ,s)
 * @return int/string/obj age
 */
function get_age($date_of_birth, $unit = 0) { 
    $from = new DateTime($date_of_birth);
    $to = new DateTime('today');
    $obj = $from->diff($to);
    $x_tahun = "{$obj->y} Tahun";
    $x_bulan = $obj->m > 0 ? " {$obj->m} Bulan" : '';
    $x_hari = $obj->d > 0 ? " {$obj->d} Hari" : '';
    switch(strtolower($return_type)) {
        case 1: 
            return $x_tahun; break;
        case 2:                 
            return $x_tahun.$x_bulan; break;
        case 3: 
            return $x_tahun . $x_bulan . $x_hari; break;
        case 4: 
            return $obj; break;
        default:
            return $obj->y;
    }
}  

function xtime($ymdhis='') { 
    if (!$ymdhis OR $ymdhis == '0000-00-00 00:00:00') return ''; 
    $ago = strtotime($ymdhis); 
    $now = now();
    $tgl = date('j', $ago);
    $nama_hari = day_name($ymdhis);
    $nama_bulan = month_name($ymdhis);
    $pukul = date('H:i', $ago);
    $seldetik = abs(floor($now - $ago));
    $selmenit = abs(round($seldetik/60));
    $seljam = abs(round($seldetik/3600)); 
    if ($seldetik < 50) return $seldetik.' detik yang lalu';
    elseif ($selmenit < 50) return $selmenit.' menit yang lalu';
    elseif ($seljam < 4) return $seljam.' jam yang lalu';
    elseif ($seljam < 48) {
        if (date('Y-m-d', $now) == date('Y-m-d', $ago)) {
            return 'Hari ini pukul '.$pukul;
        } else {
            return 'Kemarin pukul '.$pukul;
        }
    }
    // elseif ($seljam < 48) return 'Kemarin pukul '.$pukul;
    elseif (date('W', $ago) == date('W', $now)) return $nama_hari.' '.$pukul;
    elseif (date('Y', $ago) == date('Y', $now)) return $tgl.' '.$nama_bulan.' '.$pukul; 
    else return fdate_datetime($ymdhis);
} 