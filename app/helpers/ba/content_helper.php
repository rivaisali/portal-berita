<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Menambahkan area pada template
 * @param string $name nama area, harus unik. header, footer sudah terpakai
 * @param bool $return echo atau return. default FALSE
 */
if ( !function_exists('content_area') )
{
    function content_area($name = '', $return = FALSE) {
        $ci = &get_instance();
        $ci->content->area($name, $return); 
    }   
}

/**
 * Menambahkan konten pada akhir content area  
 * @param string $area nama area dari fungsi content_area()
 * @param string $content content yang ingin ditambahkan
 */ 
if ( !function_exists('add_content') )
{
    function add_content($area_name = '', $content = '') {
        $ci = &get_instance();
        $ci->content->add($area_name, $content); 
    }   
}

/**
 * Menyisipkan konten pada awal content area  
 * @param string $area nama area dari fungsi content_area()
 * @param string $content content yang ingin disisipkan
 */
if ( !function_exists('insert_content') )
{
    function insert_content($area_name = '', $content = '') {
        $ci = &get_instance();
        $ci->content->insert($area_name, $content); 
    }   
}

/**
 * Menetapkan konten pada content area. Yang ditembahkan sebelumnya ditimpa
 * @param string $area nama area dari fungsi content_area()
 * @param string $content content yang ingin disisipkan
 */
if ( !function_exists('set_content') )
{
    function set_content($area_name = '', $content = '') {
        $ci = &get_instance();
        $ci->content->set($area_name, $content); 
    }   
}

/**
 * Sama dengan menambahkan content_area('head')
 */
if ( !function_exists('head_content') )
{
    function head_content() {
        $ci = &get_instance();  
        $ci->content->head_area();
    }   
}

/**
 * Sama dengan menambahkan content_area('foot')
 */
if ( !function_exists('foot_content') )
{
    function foot_content() {
        $ci = &get_instance();  
        $ci->content->foot_area();   
    }   
}

if ( !function_exists('add_style') )
{
    function add_style($style, $media = 'screen') {
        $ci = &get_instance();
        $ci->content->add_style($style, $media);
    }   
}

if ( !function_exists('add_script') )
{
    function add_script($script, $on_ready = FALSE) { 
        $ci = &get_instance();
        $ci->content->add_script($script, $on_ready);
    }   
}

/**
 * Set or Get page title
 * Kosongkan parameter untuk Get, sebaliknya untuk set
 */
if ( !function_exists('page_title') )
{
    function page_title($title='') {
        $ci = &get_instance();
        return $ci->content->page_title($title);
    }   
}

if ( !function_exists('page_description') )
{
    function page_description($description='') {
        $ci = &get_instance();
        return $ci->content->page_description($description);
    }   
}

if ( !function_exists('page_imageurl') )
{
    function page_imageurl($imageurl='') {
        $ci = &get_instance();
        return $ci->content->page_imageurl($imageurl);
    }   
}

if ( !function_exists('page_header') ) {
    function page_header($title = '') {
        $ci = &get_instance();
        return $ci->content->page_header($title);
    }   
}

if ( !function_exists('set_breadcumb') ) {
    function set_breadcumb() {
        $ci = &get_instance();
        call_user_func_array(array($ci->content, 'set_breadcumb'), func_get_args());
    }   
}

if ( !function_exists('get_breadcumb') ) {
    function get_breadcumb($base_uri = '', $home_label = 'Home', $attr = '') {
        $ci = &get_instance();
        return $ci->content->get_breadcumb($base_uri, $home_label, $attr);
    }   
}

if ( !function_exists('include_view') ) {
    function include_view($view_name = '', $return = FALSE) {
        $ci = &get_instance();
        return $ci->load->view('include/'.$view_name, array(), $return);
    }   
}

/**
 * Generate bootstrap nav menu
 * @param array $items [url label icon attr a_attr urls]
 * urls = jika curr url ada di array urls maka aktif
 **/
if ( !function_exists('generate_menu') ) 
{
    function generate_menu($items = array(), $attr = '', $params = NULL, $return = FALSE) { 
        $options = array('li_active_class'   => 'active',
                         'li_dropdown_class' => 'dropdown',
                         'qstr_key'          => NULL,
                         'default_icon'      => '',
                         'submenu_key'       => 'sub',
                         'ul_dropdown_class' => 'dropdown-menu');
        if (is_array($params)) 
            $options = array_merge($options, $params);
        $attr = generate_attr($attr);
        $ret = "<ul{$attr}>";
        foreach ($items as $item) {
            if (empty($item)) continue;
            $has_sub = isset($item[$options['submenu_key']]) && !empty($item[$options['submenu_key']]);
            if ( !is_absolut_url($item['url']) && $item['url'] != '#' )
                $item['url'] = base_url($item['url']);
            if ( !isset($item['icon']) || empty($item['icon']) ) {
                $item['icon'] = $options['default_icon'];
            }                    
            if ( !empty($item['icon']) ) {
                if (substr($item['icon'], 0, 3) == 'fa-')
                    $item['icon'] = '<i class="fa '.$item['icon'].'"></i> '; else
                    $item['icon'] = '<i class="fa fa-'.$item['icon'].'"></i> ';     
            } else $item['icon'] = '';
            $item['attr'] = isset($item['attr']) ? generate_attr( $item['attr'] ) : '';   
            $item['urls'] = isset($item['urls']) ? $item['urls'] : array();
            $item['urls'][] = $item['url'];
            if (url_is_current_uri($item['urls'], $options['qstr_key']))
                $item['attr'] = add_class_attr($item['attr'], $options['li_active_class']);
            if ( $has_sub ) {
                $item['attr'] = add_class_attr($item['attr'], $options['li_dropdown_class']); 
                $item['label'].= ' <span class="caret"></span>';
                // $item['a_attr'] = array('class'=>'dropdown-toggle','data-toggle'=>'dropdown');
            }
            $item['attr'] = $item['attr'] == '' ? '' : ' ' . $item['attr']; 
            $item['a_attr'] = isset($item['a_attr']) ? generate_attr( $item['a_attr'] ) : '';  
            $ret .= "<li{$item['attr']}><a href=\"{$item['url']}\"{$item['a_attr']}>{$item['icon']}{$item['label']}</a>";  
            if ( $has_sub ) 
                $ret .= generate_menu($item[$options['submenu_key']], 'class='.$options['ul_dropdown_class'], 
                    array('submenu_key'=>'children'), TRUE);
            $ret .= '</li>';
        }
        $ret.= '</ul>';
        if ($return) return $ret; else echo $ret;
    }   
}

if ( !function_exists('generate_menu_user') ) 
{
    function generate_menu_user($items = array(), $attr = '', $params = NULL, $return = FALSE) { 
        $options = array('li_active_class'   => 'current',
                         'li_dropdown_class' => 'dropdown',
                         'qstr_key'          => NULL,
                         'default_icon'      => '',
                         'submenu_key'       => '',
                         'ul_dropdown_class' => '');
        if (is_array($params)) 
            $options = array_merge($options, $params);
        $attr = generate_attr($attr);
        $ret = "<ul{$attr}>";
        foreach ($items as $item) {
            if (empty($item)) continue;
            $has_sub = isset($item[$options['submenu_key']]) && !empty($item[$options['submenu_key']]);
            if ( !is_absolut_url($item['url']) && $item['url'] != '#' )
                $item['url'] = base_url($item['url']);
            if ( !isset($item['icon']) || empty($item['icon']) ) {
                $item['icon'] = $options['default_icon'];
            }                    
            if ( !empty($item['icon']) ) {
                if (substr($item['icon'], 0, 3) == 'fa-')
                    $item['icon'] = '<i class="fa '.$item['icon'].'"></i> '; else
                    $item['icon'] = '<i class="fa fa-'.$item['icon'].'"></i> ';     
            } else $item['icon'] = '';
            $item['attr'] = isset($item['attr']) ? generate_attr( $item['attr'] ) : '';   
            $item['urls'] = isset($item['urls']) ? $item['urls'] : array();
            $item['urls'][] = $item['url'];
            if (url_is_current_uri($item['urls'], $options['qstr_key']))
                $item['attr'] = add_class_attr($item['attr'], $options['li_active_class']);
            if ( $has_sub ) {
                $item['attr'] = add_class_attr($item['attr'], $options['li_dropdown_class']); 
                // $item['label'].= ' <span class="caret"></span>';
                // $item['a_attr'] = array('class'=>'dropdown-toggle','data-toggle'=>'dropdown');
            }
            $item['attr'] = $item['attr'] == '' ? '' : ' ' . $item['attr']; 
            $item['a_attr'] = isset($item['a_attr']) ? generate_attr( $item['a_attr'] ) : '';  
            $ret .= "<li{$item['attr']}><a href=\"{$item['url']}\"{$item['a_attr']}>{$item['icon']}{$item['label']}</a>";  
            if ( $has_sub ) 
                $ret .= generate_menu($item[$options['submenu_key']], 'class='.$options['ul_dropdown_class'], 
                    array('submenu_key'=>'children'), TRUE);
            $ret .= '</li>';
        }
        $ret.= '</ul>';
        if ($return) return $ret; else echo $ret;
    }   
}

if ( !function_exists('add_class_attr') )
{
    /**
     * Menambahkan attribut kelas
     * @param string $attr attribut semula ex: id="name" rel="galeri"
     * @param string $class kelas baru
     */
    function add_class_attr($attr = '', $class = '') {
        if ( !is_array($attr) ) $attr = attr_to_array($attr);
        if ( isset($attr['class']) ) 
            $attr['class'].= ' '.$class; else
            $attr['class'] = $class;
        return array_to_attr($attr);
    }
}

/**
 * Generate html tag attribut from array or query string
 * @param string/array $attr
 * @param bool $add_space add space before if attr not empty
 * @return string 
 */
if ( !function_exists('generate_attr') ) 
{
    function generate_attr($attr = '', $add_space = TRUE) {
        if ($attr == '' || $attr == NULL) return '';
        $ci = &get_instance();
        $ci->load->helper('array');
        if ( !is_array($attr) ) parse_str($attr, $attr);
        $attr = array_to_attr($attr);
        if ($add_space) {
            if ($attr != '') $attr = " {$attr}";
        }        
        return $attr;
    }  
}

/**
 * Menambahkan eksternal javascript. Diletakkan di footer 
 * @param string $path path to file. required
 * @return void
 */
if ( !function_exists('enqueue_js') )
{
    function enqueue_js($id = '', $path = '', $dependency = array()) {
        $ci = &get_instance();
        $ci->content->enqueue_js($id, $path, $dependency);
    }   
}

/**
 * Menambahkan eksternal css. Diletakkan di head 
 * @param string $path path to file. required
 * @param string $media media (screen|all|print|...)
 * @return void
 */
if ( !function_exists('enqueue_css') )
{
    function enqueue_css($id = '', $path = '', $dependency = array()) {
        $ci = &get_instance();
        $ci->content->enqueue_css($id, $path, $dependency);
    }   
}

/**
 * Include predefined script & style. required script/style otomatis diatambahkan
 * misalnya bootstrap butuh jquery, saat inlude bootstrap tidak perlu lagi jquery
 * @param string dynamic args: jquery|jquery-form|bootstrap|bootstrap-theme|font-awesome|jqueryui|tinymce|dsplugin|datepicker
 * @return void
 */
if ( !function_exists('include_assets') )
{
    function include_assets() {
        $args = func_get_args();
        if (count($args) == 0) return FALSE;
        $use_cdn = config_item('assets_use_cdn');
        $assets_path = config_item('assets_path');
        $cdn = array('jquery'          => '//code.jquery.com/jquery-1.11.0.min.js',
                     'jquery-migrate'  => '//code.jquery.com/jquery-migrate-1.2.1.min.js',
                     'bootstrap_js'    => '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js',
                     'bootstrap_css'   => 'https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css',
                     'bootstrap-theme' => '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css');
        foreach($args as $arg) {
            $params = array();
            @list ($name, $param) = explode('?', $arg);
            if ($param) parse_str($param, $params);            
            switch ($name) {
                case 'jquery': 
                    if ($use_cdn)
                        enqueue_js($name, $cdn['jquery']); else
                        enqueue_js($name); 
                    if ( !empty($params['migrate']) ) {
                        if ($use_cdn)
                            enqueue_js('jquery-migrate', $cdn['jquery-migrate'], 'jquery'); else
                            enqueue_js('jquery-migrate', NULL, 'jquery'); 
                    }                
                break;
                case 'bootstrap':
                    include_assets('jquery');
                    if ($use_cdn) {
                        enqueue_js($name, $cdn['bootstrap_js'], 'jquery');
                        enqueue_css($name, $cdn['bootstrap_css']);
                    } else {
                        enqueue_js($name, NULL, 'jquery');
                        enqueue_css($name);
                    }
                    if ( !empty($params['theme']) )  {
                        if ($use_cdn)
                            enqueue_css('bootstrap-theme', $cdn['bootstrap-theme'], $name); else
                            enqueue_css('bootstrap-theme', NULL, $name);  
                    }            
                    $ex_path = "{$assets_path}js/html5shiv-respond.js";   
                    $ex = "<script src=\"{$ex_path}\"></script>"; 
                    add_content('head', "<!--[if lt IE 9]>\n{$ex}\n<![endif]-->\n");
                break;
                case 'tinymce':
                    enqueue_js($name, "{$assets_path}tinymce/tinymce.min.js");
                    if ( !empty($params['mode']) ) {
                       $mce_mode = $params['mode'];
                       unset($params['mode']);
                       $options = array('selector'           => 'textarea', 
                                        'menubar'            => FALSE, 
                                        'relative_urls'      => FALSE,
                                        'remove_script_host' => FALSE);                       
                       switch ($mce_mode) {
                            case 'simple':
                                $options['plugins'] = 'link';
                                $options['toolbar'] = 'bold italic underline | bullist numlist | link unlink';      
                            break;
                            case 'full':
                                $options['image_advtab'] = TRUE;
                                $options['plugins'] = 'autosave paste advlist charmap textcolor hr print image media searchreplace table contextmenu fullscreen preview code link pagebreak wordcount emoticons';
                                $options['toolbar'] = 'styleselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | link unlink | bullist numlist | image media | pagebreak table hr | charmap emoticons | searchreplace  print | undo redo | fullscreen preview code'; 
                                
                       } 
                       $options = array_merge($options, $params);
                       $script = "tinymce.init({ \n";
                       foreach ($options as $key => $val) {
                            if ($val === FALSE) $val = 'false';
                            elseif ($val === TRUE) $val = 'true';
                            else $val = "'{$val}'";
                            $script.= "\t{$key}: {$val},\n"; 
                       }
                       $script.= '});';
                       add_script($script);
                    }
                break;
                case 'datetimepicker':
                    include_assets('jquery');
                    enqueue_js('datetimepicker', NULL, 'jquery');
                    enqueue_css('datetimepicker');
                break;
                case 'dataTables':
                    include_assets('jquery');
                    enqueue_js($name, NULL, 'jquery,bootstrap');
                    if ( !empty($params['bootstrap']) ) {
                        enqueue_js('dataTables-bootstrap', NULL, $name);   
                        enqueue_css('dataTables-bootstrap', NULL, 'bootstrap');   
                    } 
                break;
                case 'cropit':
                    include_assets('jquery');
                    enqueue_js($name, NULL, 'jquery,bootstrap');
                break;
                case 'dsTable':
                    include_assets('bootstrap');
                    enqueue_js($name, NULL, 'jquery,bootstrap');
                    enqueue_css($name, NULL, 'bootstrap');
                break;
                case 'colorbox':
                    include_assets('jquery');
                    enqueue_js($name, NULL, 'jquery');
                    enqueue_css($name);
                break;
                case 'dropzone':
                    include_assets('jquery');
                    enqueue_js($name, NULL, 'jquery');
                    enqueue_css($name);
                break;
                case 'icheck':
                    include_assets('jquery');
                    enqueue_js($name, NULL, 'jquery,bootstrap');
                    enqueue_css($name, NULL, 'bootstrap');
                break;
                case 'owl-carousel':
                    include_assets('jquery');
                    enqueue_js($name, NULL, 'jquery');
                    enqueue_css($name);
                break;
            }
        }
    }   
}


/* End of file content_helper.php */
/* Location: ./app/helpers/content_helper.php */