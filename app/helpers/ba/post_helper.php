<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('get_categories') ) {
    function get_categories($post_type = 'news', $order_by = 'term_name') {
        $ci = &get_instance();
        return $ci->post->get_terms('category', $post_type, $order_by);
    }   
}

if ( !function_exists('categories_checkbox') ) {
    function categories_checkbox($post_type = 'news', $checkeds = array(), $params = array()) {
        $ci = &get_instance();
        $params = array_merge(array(
            'tax_name'  => 'category', 
            'post_type' => $post_type,
            'checkeds'  => $checkeds), $params);
        return $ci->post->terms_checkbox($params);
    }   
}

if ( !function_exists('categories_dropdown') ) {
    function categories_dropdown($post_type = 'news', $value = '', $params = array()) {
        $ci = &get_instance();
        $params = array_merge(array(
            'tax_name'  => 'category', 
            'post_type' => $post_type,
            'selected'  => $value), $params);
        return $ci->post->terms_dropdown($params);
    }   
}

if ( !function_exists('get_tags') ) {
    function get_tags($post_type = 'news', $order_by = 'term_name') {
        $ci = &get_instance();
        return $ci->post->get_terms('tag', $post_type, $order_by);
    }   
}

if ( !function_exists('generate_post_menu') ) {
    function generate_post_menu($post_type, $base_uri = 'admin', $submenu_icon = 'fa-angle-double-right', $add_new_menu = TRUE) {
        $ci = &get_instance();
        return $ci->post->generate_post_menu($post_type, $base_uri, $submenu_icon, $add_new_menu);
    }   
}

if ( !function_exists('register_post_type') ) {
    function register_post_type($post_type = 'news', $label = 'Berita', $params = NULL) {
        $ci = &get_instance();
        return $ci->post->register_type($post_type, $label, $params);
    }   
}

if ( !function_exists('get_post_types') ) {
    function get_post_types($type = NULL) {
        $ci = &get_instance();
        return $ci->post->get_registered_types($type);
    }   
}

/* End of file post_helper.php */
/* Location: ./application/helpers/post_helper.php */