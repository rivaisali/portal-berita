<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function has_akses($name = '') {
    $ci = &get_instance();
    if ($udata = $ci->user->get_user_login('admpt')) { 
        if ($udata['hak'] == 'all') return TRUE;
        else {
            $haks = unserialize($udata['hak']);
            return in_array($name, $haks);
        }
    }    
    return FALSE;
}

function header_no_cache() {
    header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", FALSE);
    header("Pragma: no-cache");
}

function list_content_slider($limit = 0, $where = NULL) {
    $ci = &get_instance();
    if ($limit > 0) $ci->db->limit($limit);
    if ($where != NULL) $ci->db->where($where);
    $qw = $ci->db->order_by('urutan ASC, ID DESC')->get('content_slider');
    return $qw->result_array();
}


function foto_thumb_personil_url($file = '') {
    $path = base_url('uploads/personil/thumbs');
    if (empty($file)) $file = 'nophoto.jpg';
    return $path.'/'.$file;
}

function generate_frontpage_content($arrs = array()) {
	if (empty($arrs)) return '';
    $ci = &get_instance();
    $ret = '';
    $baseurl = base_url();
    foreach ($arrs as $arr) {
        // $ret.= '<div class="row"><div class="col-xs-12">';
        switch (strtolower($arr['type'])) {
            case 'content slider':
                $post_items = $ci->post->get_result('post', "perpage={$arr['count']}&has_thumb=1&cat_id={$arr['grup']}");
				$ret.='<section class="team-section">
							<div class="auto-container">';
				$ret.=	'<div class="sec-title-two text-center">
							<h6>Trending</h6>
							<h2><span>Berita</span> Utama</h2>
						</div>';
                $ret.= '<div class="content-item single-item-slider img-rounded2 animation-item">';
                foreach ($post_items['rows'] as $post) {
                    $ret.= '<div class="item" style="display:none">
                        <div class="img-container img-stretch img-rounded2">
                            <img class="lazyOwl_" alt="" src="'.$post['post_thumb'].'">
                            <div class="img-caption">
                                <span class="post-date-ex"><i class="fa fa-calendar"></i> '.tanggal($post['post_date'], FALSE, 'auto').'</span>
                                <h3><a href="'.$baseurl.$post['post_type'].'/'.$post['post_slug'].'" title="Baca selengkapnya">'.$post['post_title'].'</a></h3>
                                <p>'.word_limiter(strip_tags($post['post_content']), 20).'</p>
                            </div>
                        </div>
                    </div>';
                }
                $ret.= '</div><div class="space-25"></div>';
				$ret.= '</div></section>';
            break;
            case 'tulisan terbaru (model 1)':
				$ret.='<div class="row">
						<div class="featured-blocks clearfix">';
                $post_limit = $arr['count'];
                // $half_col = ceil(($post_limit-4)/2) + 1;
                $fcatid = empty($arr['grup']) ? '' : $arr['grup'];
                $post_items = $ci->post->get_result('post', "perpage={$post_limit}&has_thumb=1&cat_id={$fcatid}");
                $no = 1;
                foreach ($post_items['rows'] as $post) {
                    $post_url = "{$baseurl}{$post['post_type']}/{$post['post_slug']}"; 
					$ret.='<div class="col-md-4 col-sm-4 featured-block">';
					$ret.='<a href="'.$post_url.'" class="img-thumbnail"> 
							<img src="'.$post['post_thumb'].'"> 
							<strong>'.character_limiter($post['post_title'], 50).'</strong> <span class="more">read more</span> 
						   </a>';
					$ret.='</div>';
                    $no++;
                }
                $ret.= '</div>';
            break;
            case 'tulisan terbaru (model 2)':
                $ret.= '<div class="content-item">
                    <div class="page-header diagonal">
                    <a href="post" class="more-link"><i class="fa fa-chevron-right"></i> Selengkapnya</a>
                    <h3><a href="post" title="Lihat berita lainnya">'.
                    $arr['judul'].'</a></h3></div>
                <div class="row">';
                $post_limit = $arr['count'];
                // $half_col = ceil(($post_limit-4)/2) + 1;
                $fcatid = empty($arr['grup']) ? '' : $arr['grup'];
                $post_items = $ci->post->get_result('post', "perpage={$post_limit}&has_thumb=1&cat_id={$fcatid}");
                $no = 1;
                foreach ($post_items['rows'] as $post) {
                    $post_url = "{$baseurl}{$post['post_type']}/{$post['post_slug']}"; 
                    $ret.= '<div class="col-sm-6 col-md-4">';
                    $ret.= '<div class="post-item animation-item" data-animation-type="zoomIn">
                            <a href="'.$post_url.'" class="img-container img-rounded2 img-stretch">
                                <img class="img-responsive img-rounded2" src="'.$post['post_thumb'].'">
                            </a>
                            <h3 class="post-title post-title-small">
                                <a href="'.$post_url.'">'.character_limiter($post['post_title'], 50).'</a><br/>
                                <small><i class="fa fa-clock-o"></i> '.xtime($post['post_date']).'</small></h3>
                        </div>
                        <div class="space-15"></div>';
                    $ret.= '</div>';
                    $no++;
                }
                $ret.= '</div></div>';
            break;
            case 'pengumuman':
                $ret.= '<div class="content-item"><div class="page-header diagonal">
                    <h3><a href="pengumuman">'.$arr['judul'].'</a></h3></div><ul class="media-list">';
                $post_items = $ci->post->get_result('pengumuman', "perpage={$arr['count']}");
                $no = 1;
                foreach ($post_items['rows'] as $post) {
                    $post_url = "{$baseurl}{$post['post_type']}/{$post['post_slug']}";
                    $post_summary = character_limiter(strip_tags($post['post_content']), 150);
                    $ret.= '<li class="media post-item"><div class="media-body">
                        <h4 class="media-heading post-title">
                            <a href="'.$post_url.'" title="Baca selengkapnya">'.$post['post_title'].'</a>
                        </h4>
                        <p class="post-excerpt" style="margin-bottom:5px">'.$post_summary.'</p>
                        <small class="post-meta text-muted">
                            <i class="fa fa-clock-o"></i> '.xtime($post['post_date']).' &nbsp;|&nbsp;
                            <i class="fa fa-user"></i> '.$post['namauser'].'
                        </small></div></li>';
                }
                $ret.= '</ul></div>';
            break;
            case 'halaman':
                if ($post = $ci->post->get_row($arr['grup'])) {
                    $ret.= '<div class="page-header"><h3><a href="page/'.$post['post_slug'].'">'.$post['post_title'].'</a></h3></div>';
                    $ret.= $post['post_content'];
                }
            break;            
            case 'banner':
				$ret.= '<section class="testimonials-section-two">
							<div class="auto-container">
								<div class="sec-title-two text-center">
										<h2>Link Terkait</h2>
								</div>
							';
				$ret.= '<div class="testimonial-area-two">
						<div class="three-item-carousel owl-carousel owl-theme owl-dots-none owl-nav-style-two">';
                if (!empty($arr['grup'])) {
                    $qwban = $ci->db->get_where('banner', array('ban_id'=>$arr['grup']), 1);
                    if ( $qwban->num_rows() > 0 ) {
                        $rban = $qwban->row();
						$qdata	=	$ci->db->get_where('banner', array('grup' => $rban->grup));
						$data	=	$qdata->result();
						foreach($data as $d){
							if (file_exists('uploads/banner/'.$d->url)) {
								// $ret.= $d->judul;
								$ret.= '
									<div class="testimonial-item-one text-center">
										<div class="image-box">
											<div class="image">
												<figure>
													<a href="'.$d->link.'">
														<img src="'.$baseurl.'uploads/banner/'.$d->url.'">
													</a>
												</figure>
											</div>
											<a href="'.$d->link.'" title="'.$d->judul.'"><h6>'.$d->judul.'</h6></a>
										</div>
									</div>';
							}
						}
                        // if (file_exists('uploads/banner/'.$rban->url)) {
							// $ret.=$rban->grup;
                        	// $ret.= '<div class="single-banner">';
                            // if (!empty($rban->link)) 
                                // $ret.= '<a href="'.$rban->link.'" title="'.$rban->judul.'">';
                            // $ret.= '<img src="'.$baseurl.'uploads/banner/'.$rban->url.'" style="width:100%">';
                            // if (!empty($rban->link)) $ret.= '</a>';
							// $ret.= '</div>';
                        // }
                    }
                }
                $ret.= '</div></div>';
                $ret.= '</div></section>';
            break;
            case 'galeri':
				$ret.= '<section class="testimonials-section-two">
							<div class="auto-container">
								<div class="sec-title-two text-center">
									<a href="gallery">
										<h6>Foto</h6>
										<h2>'.$arr['judul'].'</h2>
									</a>
								</div>
							';
				$ret.= '<div class="testimonial-area-two">
						<div class="three-item-carousel owl-carousel owl-theme owl-dots-none owl-nav-style-two">';
                $ret.= '';
                    if (!empty($arr['grup']))
                        $ci->db->where('album_id', $arr['grup']);
                    $qw = $ci->db->order_by('foto_id','desc')->get('foto', $arr['count']);
                    foreach ($qw->result() as $row) {
                        $imglink = $baseurl.'gallery?album='.$row->album_id.'&photo='.$row->foto_id;
                        $imgurl = $baseurl.'uploads/gallery/thumbs/'.$row->url;
                        $ret.= '
							<div class="testimonial-item-one text-center">
								<div class="image-box">
									<div class="image">
										<figure>
											<a href="'.$imglink.'">
												<img src="'.$imgurl.'">
											</a>
										</figure>
									</div>
								</div>
								<div class="text-area"">
									<span>“</span>
									<p>'.$row->judul.'</p>
								</div>
							</div>';
                    }
                $ret.= '</div></div>';
                $ret.= '</div></div>';
                $ret.= '</div></section>';
            break;
            case 'html':
                // $ret.= 'Ini html';
                // $ret.= '';
				$ret.= urldecode($arr['grup']);
            break;
            case 'pencarian perkara':
                $ret.= '<div class="cari-perkara">
                    <h3>Pencarian Perkara</h3>
                    <form method="get" role="search" action="perkara" class="search-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Nomor perkara atau nama pelaku" autocomplete="off">    
                            <div class="input-group-btn">       
                                <button type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
                            </div>             
                        </div>
                        <p style="margin:10px 0 0">
                            <label class="radio-inline"><input type="radio" name="j" value="Pidana" checked> Pidana</label>
                            <label class="radio-inline"><input type="radio" name="j" value="Perdata"> Perdata</label>
                            <label class="radio-inline"><input type="radio" name="j" value="Tipikor"> Tipikor</label>
                        </p>
                    </form>
                </div>';
            break;
        }
        // $ret.= '</div></div>';
    }
    return $ret;
}

function get_post_thumb_url($large_url = '') {
    return str_replace('uploads/file/', 'uploads/thumbs/', $large_url);
}

function get_post_thumb_url_user($large_url = '') {
    // return str_replace('uploads/file/', 'uploads/thumbs/', $large_url);
    return str_replace('uploads/file/', 'uploads/file/', $large_url);
}

function list_judul_post($type='page') {
    $ci = &get_instance();
    $posts = $ci->post->get_result($type);
    $arr = array();
    foreach ($posts['rows'] as $post) {
        $arr[$post['post_id']] = $post['post_title'];
    }
    return $arr;
}

function generate_sideitems_content($arrs = array()) {
	if (empty($arrs)) return '';
    $ci = &get_instance();
    $ret = '';
    $baseurl = base_url();
    foreach ($arrs as $arr) {
        if ( ! empty($arr['judul'])) {
            $ret.= '<div class="widget sidebar-widget">
              <div class="sidebar-widget-title">
                <h3>'.$arr['judul'].'</h3>
              </div>
              <div class="">';
			  //$ret.= '<div class="resent-post-area">
              // <div class="sec-title-four">
                // <h6>'.$arr['judul'].'</h6>
              // </div>
              // <div class="">';
        }
        switch (strtolower($arr['type'])) {
            case 'menu':
                $ret.= generate_menu(get_menu_items($arr['grup']), 'class=nav nav-pills nav-stacked', 
                    array('default_icon'=>'th','submenu_key'=>'children'), TRUE);
            break;
            case 'personil':
                $lists = list_kategori_pesonil();
                $ret.= '<ul class="nav nav-pills nav-stacked animation-item">';
                foreach($lists as $key=>$val) {
                    $ret.= '<li><a href="'.$baseurl.'profil-personil?k='.$key.'"><i class="fa fa-th"></i> '.$val.'</a></li>';
                }
                $ret.= '</ul>';
            break;
            case 'pengunjung':
                $ret.= '<div class="visitor-panel animation-item"><p class="hariini"><span class="pull-right">'.$ci->visitor_counter->get_count('d').'</span> Hari ini</p>
                  <p class="kemarin"><span class="pull-right">'.$ci->visitor_counter->get_count('y').'</span> Kemarin</p>
                  <p class="mingguini"><span class="pull-right">'.$ci->visitor_counter->get_count('w').'</span> Minggu ini</p>
                  <p class="bulanini"><span class="pull-right">'.$ci->visitor_counter->get_count('m').'</span> Bulan ini</p>
                  <p class="total"><span class="pull-right">'.$ci->visitor_counter->get_count('a').'</span> Total</p></div>';
            break;
            case 'html':
                $ret.= urldecode($arr['grup']);
            break;
            case 'pengumuman':
                $ret.= '<div class="item-content animation-item" style="margin-bottom:20px"><div class="news-scroll"><div class="slider">';
                if (empty($arr['count'])) $arr['count'] = 5;
                $posts = $ci->post->get_result('pengumuman', array('perpage'=>$arr['count']));
                foreach($posts['rows'] as $post) {
                    $titl = character_limiter($post['post_title'], 60);
                    $summ = character_limiter(strip_tags($post['post_content']), 100);
                    $href = $baseurl.'pengumuman/'.$post['post_slug'];
                    $ret.= '<div class="item">
                        <h4><a href="'.$href.'" title="'.$post['post_title'].'">'.$titl.'</a></h4>
                        <p class="summary">'.$summ.'</p>
                        <p class="post-meta"><i class="fa fa-clock-o"></i> '.xtime($post['post_date']).'</p>
                        <p class="text-right"><a href="'.$href.'" class="btn btn-xs btn-default">Selengkapnya &raquo;</a></p>
                        </div>';
                }
                $ret.= '</div></div></div>';
            break;
            case 'profil kepala daerah': 
                $ret.= '<div class="item-content animation-item" style="margin-bottom:25px"><div class="news-scroll"><div class="slider sidebar-slider">';
                $ret.= '<div class="item"><img src="https://gorontalokota.go.id/uploads/file/walikota3-300x188.jpg" alt="Walikota" class="img-responsive" style="width:100%">
                    <h5 style="margin:15px 0 10px 0;padding:0">WALIKOTA</h5>
                    <table style="margin-bottom:5px">
                    <tr>
                        <td>Nama</td><td>&nbsp; : &nbsp;</td><td>H. Marten Taha, SE., M.Ec.Dev</td>
                    </tr><tr>
                        <td>Tempat Tgl/lahir</td><td>&nbsp; : &nbsp;</td><td>Gorontalo, 29 Agustus 1959</td>
                    </tr><tr>
                        <td>Agama</td><td>&nbsp; : &nbsp;</td><td>Islam</td>
                    </tr>
                    </table>
                    <p class="text-right"><a href="'.base_url('page/profil-walikota').'" class="btn btn-xs btn-primary">Selengkapnya &raquo;</a></p>
                    </div>';
                // $ret.= '<div class="item"><img src="https://gorontalokota.go.id/uploads/file/Budi-Doku-Dr..jpg" alt="Walikota" class="img-responsive" style="width:100%">
                    // <h5 style="margin:15px 0 10px 0;padding:0">WAKIL WALIKOTA</h5>
                    // <table style="margin-bottom:5px">
                    // <tr>
                        // <td>Nama</td><td>&nbsp; : &nbsp;</td><td>dr. Budi Doku</td>
                    // </tr><tr>
                        // <td>Tempat Tgl/lahir</td><td>&nbsp; : &nbsp;</td><td>Gorontalo, 6 Mei 1971</td>
                    // </tr><tr>
                        // <td>Agama</td><td>&nbsp; : &nbsp;</td><td>Islam</td>
                    // </tr>
                    // </table>
                    // <p class="text-right"><a href="'.base_url('page/profil-wakil-walikota').'" class="btn btn-xs btn-warning">Selengkapnya &raquo;</a></p>
                    // </div>';
                $ret.= '</div></div></div>';
            break;
            case 'polling': 
                $qw = $ci->db->get_where('polling', array('poll_id'=>$arr['grup']), 1);
                $poll_ques = $qw->row()->pertanyaan;
                $ret.= '<p>'.$poll_ques.'</p>';
                $qw = $ci->db->get_where('polling_opsi', array('poll_id'=>$arr['grup']));
                $totsuara = 0;
                foreach ($qw->result() as $row) {
                    $totsuara+= $row->jumlah;
                }
                $pollreshide = $ci->input->cookie('pllg') ? '' : ' style="display:none"';
                $pollfrmhide = $pollreshide != '' ? '' : ' style="display:none"';
                $ret.= '<div class="polling-widget animation-item">';
                $ret.= '<div class="polling-result"'.$pollreshide.'><div class="space-15"></div>';
                $userpil = '';
                foreach ($qw->result() as $row) {
                    if ($ci->input->cookie('pllg') == $row->opsi_id)
                        $userpil = $row->opsi;
                    $persen = $totsuara == 0 ? 0 : ($row->jumlah/$totsuara) * 100; 
                    $persen = round($persen);
                    $ret.= $row->opsi.'<div class="progress" style="margin:2px 0 10px 0">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="'.$persen.'" 
                        aria-valuemin="0" aria-valuemax="100" style="width:'.$persen.'%">'.$persen.'%</div></div>';
                }
                $ret.= '<div class="alert alert-success" style="margin:15px 0 0;padding:7px 12px;font-size:12px">
                    Total suara : <strong>'.$totsuara.'</strong>';
                if ($userpil != '')
                    $ret.= '<br>Pilihan anda : <strong>'.$userpil.'</strong>';                
                $ret.= '</div>';
                if ($pollreshide != '')
                    $ret.= '<button type="button" class="btn-view-pollop btn btn-sm btn-danger" style="margin-top:10px">
                        <i class="fa fa-check"></i> Beri Suara</button>';   
                $ret.= '</div>';
                $ret.= '<form method="post" action="'.base_path('ajax/polling/submit').'"'.$pollfrmhide.'>';
                foreach ($qw->result() as $row) {
                    $ret.= '<div class="radio"><label><input type="radio" name="pilihan" value="'.$row->opsi_id.'"> '.$row->opsi.'</label></div>';
                }
                $ret.= '<input type="hidden" name="poll_id" value="'.$arr['grup'].'">';
                $ret.= '<button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-check"></i> Vote</button> ';
                $ret.= '<button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-list"></i> Lihat Hasil</button>';
                $ret.= '</form>';
                $ret.= '</div>';
            break;
            case 'banner':
                // $ci->db->order_by('RAND()');
                if (!empty($arr['count']))
                    $ci->db->limit($arr['count']);
                if (!empty($arr['grup']))
                    $ci->db->where('grup', urldecode($arr['grup']));
                $qw = $ci->db->get('banner');
                $ret.= '<div class="item-content text-center">';
                foreach ($qw->result() as $row) {
                    $bann_url = $baseurl.'uploads/banner/'.$row->url;
                    if ($row->link != '')
                        $ret.=  '<a data-animation-type="zoomIn" class="banner-item animation-item" href="'.$row->link.'" title="'.$row->judul.'"><img src="'.$bann_url.'" class="img-rounded2"></a>'; else
                        $ret.=  '<div data-animation-type="zoomIn" class="banner-item animation-item"><img src="'.$bann_url.'" title="'.$row->judul.'" class="img-rounded2"></div>';
                }
                $ret.= '</div>';
            break;
            case 'berita':
                $ret.= '<ul class="text-list animation-item">';
                if (empty($arr['count'])) $arr['count'] = 5;
                $posts = $ci->post->get_result('post', array('perpage'=>$arr['count'],'cat_id'=>$arr['grup']));
                foreach($posts['rows'] as $post) {
                    $ret.= '<li><a href="'.$baseurl.'post/'.$post['post_slug'].'">
                        <i class="fa fa-stop"></i>'.$post['post_title'].'</a></li>';
                }
                $ret.= '</ul>';
            break;
            case 'halaman':
                $ret.= '<ul class="text-list animation-item">';
                if (empty($arr['count'])) $arr['count'] = 5;
                $posts = $ci->post->get_result('page', array('perpage'=>$arr['count'],'cat_id'=>$arr['grup']));
                foreach($posts['rows'] as $post) {
                    $ret.= '<li><a href="'.$baseurl.'page/'.$post['post_slug'].'">
                        <i class="fa fa-stop"></i>'.$post['post_title'].'</a></li>';
                }
                $ret.= '</ul>';
            break;
        }
        if ( ! empty($arr['judul'])) {
            $ret.= '</div></div>';
        } else {
            $ret.= '<div class="space-20"></div>';
        }
    }
    return $ret;
}

function generate_nestable_items($arrs = array()) {
    $ret = '';
    if (!is_array($arrs)) return FALSE;
    foreach ($arrs as $arr) {
        if (!is_array($arr)) continue;
        if (!isset($arr['form'])) $arr['form'] = '1';
        $ret.= '<li class="dd-item" data-judul="'.$arr['judul'].'" data-type="'.$arr['type'].'" data-icon="'.$arr['icon'].'" 
            data-count="'.$arr['count'].'" data-grup="'.$arr['grup'].'" data-form="'.$arr['form'].'"><div class="dd-handle">';
        if (isset($arr['icon']))
            $ret.= '<i class="fa fa-'.$arr['icon'].'"></i> ';
        $ret.= $arr['type'];
        if (isset($arr['judul']))
            $ret.= ': <small>'.$arr['judul'].'</small> ';
        $ret.= '</div><div class="menuman-item-btns">
            <a href="#" class="menu-edit" title="Edit" data-toggle="tooltip"><i class="fa fa-edit"></i></a>
            <a href="#" class="menu-delete" title="Hapus" data-toggle="tooltip"><i class="fa fa-trash-o"></i></a></div>';
        if ( isset($arr['children']) && is_array($arr['children']) ) {
            $ret.= '<ol class="dd-list">';
            foreach ($arr['children'] as $child) {
                $ret.= generate_nestable_items($child);
            }
            $ret.= '</ol>';
        }
    }
    return $ret;
}

function prog_keg_opts($selected = '', $empty_label = '&mdash; Pilih &mdash;') {
    $ci = &get_instance();
    $qw = $ci->db->order_by('ID_pro, ID_keg')->get('vapbdes_kegiatan');
    $ret = '<option value="">'.$empty_label.'</option>'; 
    $prev_grup = NULL;
    foreach($qw->result() as $row) {
        if ($row->nama_pro === NULL OR $row->nama_pro != $prev_grup) {
            if ($prev_grup !== NULL)
                $ret.= '</optgroup>';
            $ret.= '<optgroup label="'.$row->nama_pro.'">';
            $prev_grup = $row->nama_pro;
        }
        $ret.= '<option value="'.$row->ID_keg.'"';
        if ($selected == $row->ID_keg) $ret.= ' selected';
        $ret.= '>'.$row->nama.'</option>';
    }
    return $ret;
}

function banner_opts($empty_label = '&mdash; Pilih Banner &mdash;') {
    $ci = &get_instance();
    $qw = $ci->db->order_by('grup ASC, ban_id DESC')->get('banner');
    $ret = '<option value="">'.$empty_label.'</option>'; 
    $prev_grup = NULL;
    foreach($qw->result() as $row) {
        if ($row->grup === NULL OR $row->grup != $prev_grup) {
            if ($prev_grup !== NULL)
                $ret.= '</optgroup>';
            $ret.= '<optgroup label="'.$row->grup.'">';
            $prev_grup = $row->grup;
        }
        $ret.= '<option data-url="'.$row->url.'" data-link="'.$row->link.'" 
            data-grup="'.$row->grup.'" value="'.$row->ban_id.'">'.$row->judul.'</option>';
    }
    return $ret;
}

function list_pangkatgol() {
    $ci = &get_instance();
    $ci->load->helper('form');
    $arr = array('Pembina Utama (IV/e)','Pembina Utama Madya (IV/d)','Pembina Utama Muda (IV/c)',
    'Pembina Tingkat I (IV/b)', 'Pembina (IV/a)', 'Penata Tingkat I (III/d)', 'Penata (III/c)',
    'Penata Muda Tingkat I (III/b)', 'Penata Muda (III/a)', 'Pengatur Tingkat I (II/d)',
    'Pengatur (II/c)', 'Pengatur  Muda Tingkat I (II/b)', 'Pengatur  Muda (II/a)',
    'Juru Tingkat I (I/d)', 'Juru (I/c)', 'Juru   Muda Tingkat I (I/b)', 'Juru Muda (I/a)');
    return create_items($arr);
}

function list_jenjang() {
    $arr = array('SD'=>'SD','SMP'=>'SMP','SMA'=>'SMA','D2'=>'D2','D3'=>'D3',
        'D4'=>'D4','S1'=>'S1','S2'=>'S2','S3'=>'S3');
    return $arr;
}

function list_agama() {
    $arr = array('Islam','Kristen','Hindu','Budha');
    $arr = array_combine($arr, $arr);
    return $arr;
}

function list_goldarah() {
    $arr = array('A','B','AB','O');
    $arr = array_combine($arr, $arr);
    return $arr;
}

function list_sumber_apbdes() {
    $arr = array('ADD'=>'Anggaran Dana Desa','DD'=>'Dana Desa');
    return $arr;
}

function list_kategori_pesonil() {
    $ci = &get_instance();
    $ci->load->helper('form');
    $arr = items_fromdb('kategori','kat_id','nmkat','ORDER BY urutan ASC');
    return $arr;
}

function get_post_count($type = 'post', $status = 'all') {
    $ci = &get_instance();
    $ci->db->where('post_type', $type);
    switch($status) {
        case 'all':
            $ci->db->where('trash', 0);
        break;
        case 'trash':
            $ci->db->where('trash', 1);
        break;
        default:
            $ci->db->where('trash', 0);
            $ci->db->where('post_status', $status);
    }
    return $ci->db->count_all_results('post');
}

function list_postfilter_byrange() {
    $arr = array('all'=>'Semua waktu', 'd'=>'Hari ini', 'w'=>'Minggu ini', 'm'=>'Bulan ini');
    return $arr;
}

function get_menu_items($id = 1) {
    $ci = &get_instance();
    $qw = $ci->db->select('items')->get_where('menu', array('menu_id'=>$id), 1);
    if ($qw->num_rows() > 0) {
        return json_decode($qw->row()->items, TRUE); 
    } else
        return array();    
}

function list_album($inc_all = FALSE) {
    $ci = &get_instance();
    $ci->load->helper('form');
    $arr = items_fromdb('album', 'album_id', 'nm_album', "ORDER BY nm_album");
    if ($inc_all) 
        $arr = array(''=>'&mdash; Semua &mdash;') + $arr;
    return $arr;
}

function list_unitkrj($inc_all = FALSE) {
    $ci = &get_instance();
    $ci->load->helper('form');
    $arr = items_fromdb('unitkrj', 'ID', 'nama');
    if ($inc_all) 
        $arr = array(''=>'&mdash; Semua &mdash;') + $arr;
    return $arr;
}

function list_polling() {
    $ci = &get_instance();
    $ci->load->helper('text');
    $qw = $ci->db->get('polling');
    $arr = array();
    foreach($qw->result() as $row) {
        $arr[$row->poll_id] = character_limiter($row->pertanyaan, 50);
    }
    return $arr;
}

function list_personil($kat = '') {
    $ci = &get_instance();
    $ci->load->helper('form');
    $where = $kat == '' ? '' : "WHERE kategori='{$kat}'";
    return items_fromdb('personil', 'pers_id', 'nama', $where);
}

function list_grup_banner($inc_all = FALSE) {
    $ci = &get_instance();
    $tb = $ci->db->dbprefix('banner');
    $qw = $ci->db->query("SELECT DISTINCT grup FROM {$tb}");
    $arr = array();
    foreach($qw->result() as $row) {
        if (trim($row->grup) == '') continue;
        $arr[$row->grup] = $row->grup;
    }
    if ($inc_all) 
        $arr = array(''=>'&mdash; Semua &mdash;') + $arr;
    return $arr;
}

function in_array_set_checked($arr, $val) {
    if ($arr == 'all') return ' checked';
    if (!is_array($arr)) return '';
    if (in_array($val, $arr))
        return ' checked'; else return '';
}

function chk_hak_akses($checkeds = array()) {
    $ci = &get_instance();
    $ci->load->helper('form');
    $arr = array('berita','halaman','galeri','banner','polling','user','cntslider');
    $ret = '<table class="tbchkhak">';
    foreach ($arr as $item) {
        $itemlabel = $item == 'cntslider' ? 'Slider Konten' : ucfirst($item);
        $ret.= '<tr><td valign="top" class="tdfirst">
            <label class="checkbox-inline">
                <input type="checkbox" name="hak[]" value="'.$item.'-view"'.in_array_set_checked($checkeds, "{$item}-view").'> '.$itemlabel.'</label>
            </td><td>
            <label class="checkbox-inline">
                <input type="checkbox" name="hak[]" value="'.$item.'-add"'.in_array_set_checked($checkeds, "{$item}-add").'> <span>Buat baru</span></label>
            <label class="checkbox-inline">
                <input type="checkbox" name="hak[]" value="'.$item.'-edit"'.in_array_set_checked($checkeds, "{$item}-edit").'> <span>Ubah</span></label>
            <label class="checkbox-inline">
                <input type="checkbox" name="hak[]" value="'.$item.'-del"'.in_array_set_checked($checkeds, "{$item}-del").'> <span>Hapus</span></label>
            </td></tr>';
    }
    $ret.= '<tr><td valign="top" class="tdfirst">
            <label class="checkbox-inline">
                <input type="checkbox" name="hak[]" value="setting-view"'.in_array_set_checked($checkeds, 'setting-view').'> Pengaturan</label>
            </td><td>
            <label class="checkbox-inline">
                <input type="checkbox" name="hak[]" value="setting-menu"'.in_array_set_checked($checkeds, 'setting-menu').'> <span>Menu</span></label>
            <label class="checkbox-inline">
                <input type="checkbox" name="hak[]" value="setting-head"'.in_array_set_checked($checkeds, 'setting-frontpage').'> <span>Halaman Depan</span></label>
            <label class="checkbox-inline">
                <input type="checkbox" name="hak[]" value="setting-sidebar"'.in_array_set_checked($checkeds, 'setting-sidebar').'> <span>Sidebar</span></label>
            </td></tr>';
    return $ret.'</table>';
}

function list_menu() {
    $ci = &get_instance();
    $ci->load->helper('form');
    return items_fromdb('menu', 'menu_id', 'nm_menu');
}

function include_assets() {
    foreach(func_get_args() as $asset) {
        switch($asset) {
            case 'dsselect':
                enqueue_js('dsSelect', 'assets/js/dsSelect.js');
                enqueue_css('dsSelect', 'assets/css/dsSelect.css');
            break;
            case 'dsbrowse':
                enqueue_js('dsBrowse', 'assets/js/dsBrowse.min.js');
                enqueue_css('dsBrowse', 'assets/css/dsBrowse.css');
            break;
            case 'datepicker':
                enqueue_js('bs-datepicker', 'assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
                enqueue_js('bs-datepicker-id', 'assets/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js');
                enqueue_css('bs-datepicker', 'assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css');
                enqueue_js('inputmask-date', 'assets/js/inputmask-date.min.js');
            break;
            case 'form':
                enqueue_js('jquery.form', 'assets/js/jquery.form.min.js');
            break;
            case 'dstable':
                enqueue_css('dsTable', 'assets/css/dsTable.min.css');
                enqueue_js('dsTable', 'assets/js/dsTable.min.js');
            break;
            case 'toastr':
                enqueue_css('toastr', 'assets/toastr/toastr.min.css');
                enqueue_js('toastr', 'assets/toastr/toastr.min.js');
            break;
            case 'colorbox':
                enqueue_css('colorbox', 'assets/colorbox/colorbox.min.css');
                enqueue_js('colorbox', 'assets/colorbox/colorbox.min.js');
            break;
            case 'dsautocomplete':
                enqueue_css('dsautocomplete', 'assets/css/dsAutocomplete.css');
                enqueue_js('dsutocomplete', 'assets/js/dsAutocomplete.js');
            break;
            case 'owl-carousel':
                enqueue_css('owl-carousel', 'assets/owl-carousel/owl.carousel.all.min.css');
                enqueue_js('owl-carousel', 'assets/owl-carousel/owl.carousel.min.js');
            break;
            case 'limitchar':
                enqueue_js('limitchar', 'assets/js/limitchar.js');
            break;
        }
    }    
}

/* End of file custom_helper.php */
/* Location: ./app/helpers/custom_helper.php */
