<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('assets_path') )
{
    function assets_path($extra = '', $relative_path = TRUE) {
        $path = config_item('assets_path') . $extra;
        if ($relative_path) 
            return base_path($path); else
            return base_url($path);
    }   
}

if ( !function_exists('img_path') )
{
    function img_path($extra = '', $relative_path = TRUE) {
        $path = config_item('assets_path').'img/'. $extra;
        if ($relative_path) 
            return base_path($path); else
            return base_url($path);
    }   
}

if ( !function_exists('theme_path') ) 
{
    function theme_path($extra = '', $relative_path = TRUE) {
        $path = config_item('theme_path') . $extra;
        if ($relative_path) 
            return base_path($path); else
            return base_url($path);
    }
}

if (!function_exists('admin_path')) {
    function admin_path($uri='') {
        $adm_uri = config_item('admin_uri');
        return base_path("{$adm_uri}/{$uri}");
    }
}

if (!function_exists('admin_url')) {
    function admin_url($uri = '') {
        $adm_uri = config_item('admin_uri');
        return base_url("{$adm_uri}/{$uri}");
    }
}

if (!function_exists('add_ending_slash')) {
    function add_ending_slash($path){ 
        $slash_type = (strpos ($path, '\\')===0) ? 'win' : 'unix';  
        $last_char = substr ($path, strlen ($path)-1, 1); 
        if ($last_char != '/' and $last_char != '\\') {
            $path .= ($slash_type == 'win') ? '\\' : '/';
        } 
        return $path;
    }
}

if ( !function_exists('add_qstring') ) {
    function add_qstring( $url = '', $qstring = array() ) {
        @list ($url, $query) = explode('?', $url);
        parse_str($query, $query_arr);
        if ( !is_array($qstring) ) parse_str($qstring, $qstring);
        $qstring = array_merge($query_arr, $qstring);
        $qstring = http_build_query($qstring);
        if ($qstring != '') $url.= "?{$qstring}";
        return $url;
    }
}

if ( !function_exists('remove_qstring') ) {
    function remove_qstring( $url = '', $qstring = '' ) {
        @list ($url, $query) = explode('?', $url);
        parse_str($query, $query_arr);
        if ( !is_array($qstring) ) $qstring = explode(',', $qstring);
        foreach ($qstring as $key) {
            $key = trim($key);
            unset( $query_arr[$key] );
        }
        $qstr = http_build_query($query_arr);
        if ($qstr != '') $url.= "?{$qstr}";
        return $url;
    }
}

if (!function_exists('remove_http')) {
    function remove_http($url){ 
        $url = str_replace('http://', '', $url);
        $url = str_replace('https://', '', $url);
        return $url;
    }
}

if (!function_exists('current_uri')) {
    function current_uri($inc_qstring = FALSE) {
        $CI = &get_instance();
        $uri = $CI->uri->uri_string();
        if ($inc_qstring)
            return $_SERVER['QUERY_STRING'] ? $uri.'?'.$_SERVER['QUERY_STRING'] : $uri; else
            return $uri;
    }
}

if (!function_exists('full_url')) {
    function full_url() { 
        $pageURL = 'http';
        if (!empty($_SERVER['HTTPS'])) { $pageURL .= "s"; }  
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
}

if ( !function_exists('proper_url') ) {
    function proper_url($url = '', $extra_uri = '') { 
        $url = trim($url);
        if ($url == '') {
            $url = base_url($extra_uri);
        } elseif ($url == '#') {
            $url = '#';
        } else {
            $pos = strpos($url, '://');
            if ($pos === FALSE && substr($url, 0, 1) != '?' && substr($url, 0, 2) != '//') {
                if ($extra_uri != '') 
					$url = "{$extra_uri}/{$url}";
                $url = base_path($url);
            } 
            
        }
        return $url;
    }
}

if ( !function_exists('base_path') ) {
    function base_path($extra='') {
        $ci=&get_instance();
        $seg = $ci->uri->total_segments();        
        if ($seg == 0) return $extra; else {            
            $res = '';
            $qs = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
            $fullurl = str_replace($qs, '', full_url());
            if (substr($fullurl, -1) != '/') $seg = $seg-1;
            for ($i=1; $i<=$seg; $i++) {
                $res .= '../';
            }
            return $res . $extra;
        }
    }
}

if ( !function_exists('is_absolut_url') ) {
    function is_absolut_url($url = '') {
        return substr($url, 0, 2) == '//' 
            || substr($url, 0, 5) == 'http:'
            || substr($url, 0, 6) == 'https:';
    }   
}

/**
 * Mencocokkan URI aktif dengan URL/array URL tertentu
 * jika $qstr_key diisi (ex: t), maka 'admin/page?t=1' == 'admin/page?t=1'
 * @param string/array $urls url/array url yg akan dicocokkan
 * @param string $qstr_key dari query string yg harus sama
 * @return bool
 */
if ( !function_exists('url_is_current_uri') ) {
    function url_is_current_uri($urls = '', $qstr_key = NULL) {
        $curr_uri = uri_string();
        $base_url = base_url();
        if (!is_array($urls)) $urls = array($urls);
        foreach ($urls as $url) {
            @list ($url, $qstr) = explode('?', $url, 2);
            $uri = str_replace($base_url, '', $url);
            $is_true = $curr_uri == $uri;
            if ($qstr_key != NULL) {
                parse_str($_SERVER['QUERY_STRING'], $curr_qstr);
                $curr_val = isset($curr_qstr[$qstr_key]) ? $curr_qstr[$qstr_key] : '';
                parse_str($qstr, $qstr);
                $url_val = isset($qstr[$qstr_key]) ? $qstr[$qstr_key] : '';
                $is_true = $is_true && ($curr_val == $url_val);
            }
            if ($is_true) break;
        }
        return $is_true;
    }   
}


/* End of file DD_url_helper.php */
/* Location: ./app/helpers/DD_url_helper.php */