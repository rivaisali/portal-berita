<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function get_all_tables() {
    $tables = array();
    $ci = &get_instance();
    $dbname = $ci->db->database; 
    $qw = $ci->db->query("show full tables where Table_Type = 'BASE TABLE'");
    if ($qw->num_rows() > 0) {
        $fname = 'Tables_in_'.$dbname;
        foreach ($qw->result() as $row) {
            $tables[] = $row->$fname;
        }
    }
    return $tables;
}

function get_all_views() {
    $views = array();
    $ci = &get_instance();
    $dbname = $ci->db->database; 
    $qw = $ci->db->query("show full tables where Table_Type = 'VIEW'");
    if ($qw->num_rows() > 0) {
        $fname = 'Tables_in_'.$dbname;
        foreach ($qw->result() as $row) {
            $views[] = $row->$fname;
        }
    }
    return $views;
}

function get_all_functions() {
    $funcs = array();
    $ci = &get_instance();
    $dbname = $ci->db->database; 
    $qw = $ci->db->query("SHOW FUNCTION STATUS WHERE Db='{$dbname}'");
    if ($qw->num_rows() > 0) {
        $fname = 'Name';
        foreach ($qw->result() as $row) {
            $funcs[] = $row->$fname;
        }
    }
    return $funcs;
}

function get_all_procedures() {
    $procs = array();
    $ci = &get_instance();
    $dbname = $ci->db->database; 
    $qw = $ci->db->query("SHOW PROCEDURE STATUS WHERE Db='{$dbname}'");
    if ($qw->num_rows() > 0) {
        $fname = 'Name';
        foreach ($qw->result() as $row) {
            $procs[] = $row->$fname;
        }
    }
    return $procs;
}

if ( !function_exists('get_primary_key') ) {
    function get_primary_key($table = '') {
        $ci = &get_instance();
        $table = $ci->db->dbprefix($table);
        $qw = $ci->db->query("SHOW KEYS FROM {$table} WHERE Key_name = 'PRIMARY'");
        return $qw->row()->Column_name;
    }   
}

/* End of file db_helper.php */
/* Location: ./app/helpers/db_helper.php */