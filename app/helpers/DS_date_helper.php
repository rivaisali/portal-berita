<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('nama_hari'))
{
    function nama_hari($tanggal = '')
    {
        if ($tanggal == '') {
            $tanggal = date("Y-m-d H:i:s");
            $ind = date('w', strtotime($tanggal));
        } elseif (strlen($tanggal) < 2)
            $ind = $tanggal - 1;
        else
            $ind = date('w', strtotime($tanggal));
        $arr_hari = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
        return $arr_hari[$ind];
    }
}

if (!function_exists('list_bulan'))
{
    function list_bulan($short=FALSE)
    {
        if ($short)
            $bln = array('1'=>'Jan', '2'=>'Feb', '3'=>'Mar', '4'=>'Apr', '5'=>'Mei', '6'=>'Jun', 
                '7'=>'Jul', '8'=>'Agu', '9'=>'Sep', '10'=>'Okt', '11'=>'Nov', '12'=>'Des'); else            
            $bln = array('1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' =>
            'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
        return $bln;
    }
}

if (!function_exists('nama_bulan'))
{
    function nama_bulan($tanggal='', $short=FALSE)
    {
        if ($tanggal == '' || $tanggal == 'now') {
            $tanggal = date("Y-m-d H:i:s");
            $ind = date('m', strtotime($tanggal));
        } elseif (strlen($tanggal) < 3)
            $ind = $tanggal;
        else
            $ind = date('m', strtotime($tanggal));
        $ind = $ind - 1;
        if ($short)
            $arr_bulan = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'); else
            $arr_bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        return $arr_bulan[$ind];
    }
}

if ( !function_exists('index_nama_bulan') ) 
{
    function index_nama_bulan($nama_bulan='', $short=FALSE) {
        $list_bulan = list_bulan($short);
        return array_search($nama_bulan, $list_bulan);
    }
}

if (!function_exists('tanggal'))
{
    function tanggal($tanggal='now', $short_month=FALSE, $hide_year = FALSE, $empty_val='')
    {
        $null = array('', '0000-00-00', '0000-00-00 00:00:00', NULL);
        if ( in_array($tanggal, $null) ) return $empty_val;
        if ($tanggal == 'now') $tanggal = date("Y-m-d H:i:s");
        $tgl = date('j', strtotime($tanggal));
        $thn = date('Y', strtotime($tanggal));
        $bln = nama_bulan($tanggal, $short_month);
        if ($hide_year === TRUE) return $tgl.' '.$bln;
        if ($hide_year === 'auto') {
            $curthn = date('Y');
            if ($thn == $curthn) return $tgl.' '.$bln;
        }
        return $tgl . ' ' . $bln . ' ' . $thn;
    }
}

if (!function_exists('tanggal_jam'))
{
    function tanggal_jam($tanggal='', $sep=' - ')
    {
        if ($tanggal == '')
            $tanggal = date("Y-m-d H:i:s");
        return tanggal($tanggal).$sep.date("H:i", strtotime($tanggal));
    }
}

if (!function_exists('hari_tanggal'))
{
    function hari_tanggal($tanggal = '')
    {
        if ($tanggal == '')
            $tanggal = date("Y-m-d H:i:s");
        $tgl = date('d', strtotime($tanggal));
        $thn = date('Y', strtotime($tanggal));
        $hari = nama_hari($tanggal);
        $tgl = (int)$tgl;
        $bln = nama_bulan($tanggal);
        return $hari . ', ' . $tgl . ' ' . $bln . ' ' . $thn;
    }
}

if (!function_exists('hari_tanggal_jam'))
{
    function hari_tanggal_jam($tanggal='', $sep=' pukul ')
    {
        if ($tanggal == '')
            $tanggal = date("Y-m-d H:i:s");
        return hari_tanggal($tanggal).$sep.date("H:i", strtotime($tanggal));
    }
}

if (!function_exists('jam_saja'))
{
    function jam_saja($tanggal='')
    {
        if ($tanggal == '')
            $tanggal = date("Y-m-d H:i:s");
        return date("H:i", strtotime($tanggal));
    }
}
   

if (!function_exists('ddmmy'))
{
    function ddmmy($tanggal='', $sep='/')
    {
        if ($tanggal == NULL) return '';
        if ($tanggal == '')
            $tanggal = date("Y-m-d H:i:s");
        $tanggal = strtotime($tanggal);
        return date('d'.$sep.'m'.$sep.'Y', $tanggal);
    }
}

if (!function_exists('ymdhis'))
{
    function ymdhis($tanggal='', $sep='/')
    {
        if ($tanggal=='') {
            return date('Y-m-d H:i:s');
        } else {
            $pecah = explode($sep, $tanggal);
            $d = add_nol($pecah[0], 2);
            $m = add_nol($pecah[1], 2);
            $y = $pecah[2];
            return $y.'-'.$m.'-'.$d.' '.date('H:i:s');
        }
    }
}

if (!function_exists('year_range')) {
    function year_range($start='', $end='') {
        if (strlen($start) < 4) {
            if (substr($start, 0, 1) == '+')
                $year1 = date("Y") + substr($start, 1, strlen($start));
            elseif (substr($start, 0, 1) == '-')
                $year1 = date("Y") - substr($start, 1, strlen($start));
            elseif ($start == '0')
                $year1 = date("Y");
        } else $year1 = $start;
        if (strlen($end) < 4) {
            if (substr($end, 0, 1) == '+')
                $year2 = date("Y") + substr($end, 1, strlen($end));
            elseif (substr($end, 0, 1) == '-')
                $year2 = date("Y") - substr($end, 1, strlen($end));
            elseif ($end == '0')
                $year2 = date("Y");
        } else $year2 = $end;
        return array($year1, $year2);
    }
}

if (!function_exists('list_tahun'))
{
    function list_tahun($start, $end)
    {
        if (strlen($start) < 4)
        {
            if (substr($start, 0, 1) == '+')
                $year1 = date("Y") + substr($start, 1, strlen($start));
            elseif (substr($start, 0, 1) == '-')
                $year1 = date("Y") - substr($start, 1, strlen($start));
            elseif ($start == '0')
                $year1 = date("Y");
        } else
            $year1 = $start;
        if (strlen($end) < 4)
        {
            if (substr($end, 0, 1) == '+')
                $year2 = date("Y") + substr($end, 1, strlen($end));
            elseif (substr($end, 0, 1) == '-')
                $year2 = date("Y") - substr($end, 1, strlen($end));
            elseif ($end == '0')
                $year2 = date("Y");
        } else
            $year2 = $end;
        $arr = array();
        if ($year1 > $year2)
            return $arr;
        else
        {
            for ($i = $year1; $i <= $year2; $i++)
            {
                $arr[$i] = $i;
            }
            return $arr;
        }
    }
}

if (!function_exists('xtime'))
{
    function xtime($ymdhis='') { 
        $ago = strtotime($ymdhis);
        $now = time();
        $tgl = date('j', $ago);
        $nama_hari = nama_hari($ymdhis);
        $nama_bulan = nama_bulan($ymdhis);
        $pukul = date('H:i', $ago);
        $seldetik = abs(floor($now - $ago));
        $selmenit = abs(round($seldetik/60));
        $seljam = abs(round($seldetik/3600));
        if ($seldetik < 50) return $seldetik.' detik yang lalu';
        elseif ($selmenit < 50) return $selmenit.' menit yang lalu';
        elseif ($seljam < 4) return $seljam.' jam yang lalu';
        elseif ($seljam < 24) return 'Hari ini pukul '.$pukul;
        elseif ($seljam < 48) return 'Kemarin pukul '.$pukul;
        elseif (date('W', $ago) == date('W', $now)) return $nama_hari.' '.$pukul;
        elseif (date('Y', $ago) == date('Y', $now)) return $tgl.' '.$nama_bulan.' '.$pukul; 
        else return tanggal_jam($ymdhis);
    }   
}

/* End of file DS_date_helper.php */
/* Location: ./application/helpers/DS_date_helper.php */