<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('pagination_link') ) {
    function pagination_link($page = FALSE, $label = NULL, $active = FALSE) {
        if ($label == NULL) $label = $page;
        if ($page == FALSE)
            return '<li class="disabled"><a>...</a></li>'; 
        else {
            $act_class = $active ? ' class="active"' : '';
            $url = add_qstring(full_url(), "p={$page}");
            return '<li'.$act_class.'><a data-page="'.$page.'" href="'.$url.'">'.$label.'</a></li>';
        }
    }   
}

if ( !function_exists('generate_paginiation_link') ) 
{    
    function generate_paginiation_link($cur_page, $total_page = 0, $max_num_link = 7) { 
        if ($total_page <= 1) return '';
        $ret = '<ul class="pagination pagination-sm">';
        if ($cur_page > 1) {
            $page = $cur_page - 1;
            $ret.= pagination_link($page, '<i class="glyphicon glyphicon-arrow-left"></i>');
        }    
        if ($total_page <= $max_num_link) {
            for ($page=1; $page<=$total_page; $page++)
                $ret.= pagination_link($page, $page, $page == $cur_page);
        } else {  
            for ($page=1; $page<=2; $page++)
                $ret.= pagination_link($page, $page, $page == $cur_page);
            $half_1 = $cur_page - ( floor(($max_num_link-4)/2) );
            $half_2 = $half_1 + ($max_num_link-5);        
            if ($half_1 > 3)
                $ret.= pagination_link(FALSE);
            else {
                $half_2+= (3 - $half_1); 
                $half_1 = 3;
            }
            if ($half_2 >= ($total_page-1)) {
                $half_1 =  $half_1 - ($half_2 - ($total_page-2));
                $half_2 = $total_page-2;            
            }
            for ($page=max($half_1, 3); $page<=$half_2; $page++) 
                $ret.= pagination_link($page, $page, $page == $cur_page);        
            if ($total_page >  2) {
                if ($half_2 < ($total_page-2))
                    $ret.= pagination_link(FALSE);
                for ($page=($total_page-1); $page<=$total_page; $page++)
                    $ret.= pagination_link($page, $page, $page == $cur_page);
            }
        }
        if ($cur_page < $total_page) {
            $page = $cur_page + 1;
            $ret.= pagination_link($page, '<i class="glyphicon glyphicon-arrow-right"></i>');
        }
        $ret.= '</ul>';
        return $ret;
    }   
}

if ( !function_exists('generate_paginiation_link_user') ) 
{    
    function generate_paginiation_link_user($cur_page, $total_page = 0, $max_num_link = 7) { 
        if ($total_page <= 1) return '';
        $ret = '<ul class="pagination">';
        if ($cur_page > 1) {
            $page = $cur_page - 1;
            $ret.= pagination_link($page, '<i class="fa fa-chevron-left"></i>');
        }    
        if ($total_page <= $max_num_link) {
            for ($page=1; $page<=$total_page; $page++)
                $ret.= pagination_link($page, $page, $page == $cur_page);
        } else {  
            for ($page=1; $page<=2; $page++)
                $ret.= pagination_link($page, $page, $page == $cur_page);
            $half_1 = $cur_page - ( floor(($max_num_link-4)/2) );
            $half_2 = $half_1 + ($max_num_link-5);        
            if ($half_1 > 3)
                $ret.= pagination_link(FALSE);
            else {
                $half_2+= (3 - $half_1); 
                $half_1 = 3;
            }
            if ($half_2 >= ($total_page-1)) {
                $half_1 =  $half_1 - ($half_2 - ($total_page-2));
                $half_2 = $total_page-2;            
            }
            for ($page=max($half_1, 3); $page<=$half_2; $page++) 
                $ret.= pagination_link($page, $page, $page == $cur_page);        
            if ($total_page >  2) {
                if ($half_2 < ($total_page-2))
                    $ret.= pagination_link(FALSE);
                for ($page=($total_page-1); $page<=$total_page; $page++)
                    $ret.= pagination_link($page, $page, $page == $cur_page);
            }
        }
        if ($cur_page < $total_page) {
            $page = $cur_page + 1;
            $ret.= pagination_link($page, '<i class="fa fa-chevron-right"></i>');
        }
        $ret.= '</ul>';
        return $ret;
    }   
}

/* End of file DD_html_helper.php */
/* Location: ./app/helpers/DD_html_helper.php */