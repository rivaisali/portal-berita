<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Get either a Gravatar URL or complete image tag for a specified email address.
*
* @param string $email The email address
* @param string $s Size in pixels, defaults to 80px [ 1 - 512 ]
* @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
* @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
* @param boole $img True to return a complete IMG tag False for just the URL
* @param array $atts Optional, additional key/value attributes to include in the IMG tag
* @return String containing either just a URL or a complete image tag
* @source http://gravatar.com/site/implement/images/php/
*/
if ( ! function_exists('get_gravatar')) {
    function get_gravatar( $email, $s = 50, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
	    $url = 'http://www.gravatar.com/avatar/';
	    $url .= md5( strtolower( trim( $email ) ) );
	    $url .= "?s=$s&d=$d&r=$r";
	    if ( $img ) {
	        $url = '<img src="' . $url . '"';
	        foreach ( $atts as $key => $val )
	            $url .= ' ' . $key . '="' . $val . '"';
	        $url .= ' />';
	    }
	    return $url;
	}
}

if ( ! function_exists('ym_status'))
{
    function ym_status($id='',$imgon='',$imgoff='',$linktitle='Chatting dengan #id via YM') 
    {
        $linktitle = str_replace('#id', $id, $linktitle);
        $res = '<a title="'.$linktitle.'" href="ymsgr:sendim?'.$id.'">';
	    $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, "http://opi.yahoo.com/online?u=$id&m=s");
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);
        $strPage = curl_exec($cURL);
        curl_close($cURL);
        if ($strPage=="$id is NOT ONLINE"){
            $res.='<img border="0" src="'.$imgoff.'" />';
        } else {
            $res.='<img border="0" src="'.$imgon.'" />';
        }
        $res.='</a>';
        return $res;
	}
}

/**
* Get Twitter profile picture
*
* @param string $provider avatar provider [ gravatar | twitter | facebook | google | yahoo | tumblr ]
* @param string $userid userid yang terdaftar
* @param integer $size ukuran gambar dalam angka
* @param string $default gambar default jika error
*/ 
if (!function_exists('get_avatar')) {
    function get_avatar($provider, $userid, $size=50, $default='') {
        $sizeparam = '';
        switch(strtolower($provider)) {
            case 'gravatar':
                $userid = md5(strtolower(trim($userid)));
                $default = 'http://www.gravatar.com/avatar/'.$userid.'?s='.$size.'&d=mm&r=g';
            break;
            case 'twitter':
                if(is_numeric($size)) {
                    if ($size >= 73) $sizeparam = 'bigger';
                    if ($size >= 48 && $size < 73) $sizeparam = 'normal';
                    if ($size < 48) $sizeparam = 'mini';
                } else {
                    $sizeparam = $size;
                } 
                $default = 'http://api.twitter.com/1/users/profile_image?screen_name='.$userid.'&size='.$sizeparam;
            break;
            case 'facebook':
                if(is_numeric($size)) {
                    if ($size >= 200) $sizeparam = 'large';
                    if ($size >= 100 && $size < 200) $sizeparam = 'normal';
                    if ($size >= 50 && $size < 100) $sizeparam = 'small';
                    if ($size < 50) $sizeparam = 'square';
                } else {
                    $sizeparam = $size;
                } 
                $default = 'https://graph.facebook.com/'.$userid.'/picture?type='.$sizeparam;
            break;
            case 'google':
                $default = 'http://profiles.google.com/s2/photos/profile/'.$userid.'?sz='.$size;
            break;
            case 'tumblr':
                if ($size >= 512) $sizeparam = '512';
                if ($size >= 128 && $size < 512) $sizeparam = '128';
                if ($size >= 96 && $size < 128) $sizeparam = '96';
                if ($size >= 64 && $size < 96) $sizeparam = '64';
                if ($size >= 48 && $size < 64) $sizeparam = '48';
                if ($size >= 40 && $size < 48) $sizeparam = '40';
                if ($size >= 30 && $size < 40) $sizeparam = '30';
                if ($size >= 24 && $size < 30) $sizeparam = '24';
                if ($size < 24) $sizeparam = '16';
                $default = 'http://api.tumblr.com/v2/blog/'.$userid.'/avatar/'.$sizeparam;
            break;
        }        
        return $default;
    }
}

/**
* Get Twitter profile picture
*
* @param string $size picture size [ square (50x50) | small (50xH) | normal (100xH) | large (200xH) ]
*/ 
if (!function_exists('get_facebook_pic')) {
    function get_facebook_pic($user_id, $size='square') {
        return 'https://graph.facebook.com/'.$user_id.'/picture?type='.$size;
    }
}

function getYoutubeDuration($url=''){
    $url = 'http://www.youtube.com/watch?v='.$url;
    parse_str(parse_url($url,PHP_URL_QUERY),$arr);
    $video_id=$arr['v']; 
    $data=@file_get_contents('http://gdata.youtube.com/feeds/api/videos/'.$video_id.'?v=2&alt=jsonc');
    if (false===$data) return false;
    $obj=json_decode($data);
    return $obj->data->duration;
}

/* End of file gravatar_helper.php */
/* Location: ./app/helpers/gravatar_helper.php */