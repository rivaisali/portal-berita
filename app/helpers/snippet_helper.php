<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function cb_bulan_tahun($params = array(), $return = FALSE) {
    $defaults = array(
        'bulan_name'  => 'bulan',
        'tahun_name'  => 'tahun',
        'bulan_val'   => date('m'),
        'tahun_val'   => date('Y'),
        'bulan_label' => 'Bulan : ',
        'tahun_label' => 'Tahun : ',
        'tahun_range' => '-10:0',
        'show_label'       => 1  );
    if ( !is_array($params) ) parse_str($params, $params);
    $params = array_merge($defaults, $params);
    extract($params); 
    list ($tahun1, $tahun2) = explode(':', $tahun_range);
    $ci = &get_instance();
    $ci->form->layout = 'simple';
    if ($show_label) {
        $ret = $ci->form->fselect($bulan_name, $bulan_val, $bulan_label, list_bulan(), '', '', '', FALSE, TRUE);
        $ret.= $ci->form->fselect($tahun_name, $tahun_val, $tahun_label, list_tahun($tahun1, $tahun2), '', '', '', FALSE, TRUE);
    } else {
        $ret = $ci->form->select($bulan_name, $bulan_val, list_bulan(), '', FALSE, TRUE);
        $ret.= $ci->form->select($tahun_name, $tahun_val, list_tahun($tahun1, $tahun2), '', FALSE, TRUE);
    }
    if ($return) return $ret; else echo $ret;
}

/* End of file snippet_helper.php */
/* Location: ./application/helpers/snippet_helper.php */