<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('get_file_ext'))
{
    function get_file_ext($filename)
    {
        $filename = strtolower($filename);
        $exp = explode('.', $filename);
        $n = count($exp) - 1;
        $exts = $exp[$n];
        return $exts;
    }
}

function add_filename_prefix($filename, $prefix) {
    $name = pathinfo($filename, PATHINFO_FILENAME);
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    return $name.$prefix.'.'.$ext;
}  

if (!function_exists('str_size'))
{
    function str_size($bytes = 0)
    {
        $res = $bytes / 1024;
        if ($res > 1024)
        {
            $res = ceil($res / 1024);
            $res .= ' Mb';
        } else
        {
            $res = ceil($res);
            $res .= ' Kb';
        }
        return $res;
    }
}

function get_mime_from_header($headers) { 
    if (array_key_exists('Content-Type', $headers)) {
        return $headers['Content-Type'];
    } else {
        foreach ($headers as $row) {
            if (preg_match("/Content-Type\s*:\s*(\w+\/\w+)/", $row, $match))
                return $match[1];
        }
    }
    return FALSE;
}

function get_size_from_header($headers, $unit=FALSE) {
	$bytes = -1;
    if (array_key_exists('Content-Length', $headers)) {
        $bytes = $headers['Content-Length'];
    } else {
        foreach ($headers as $row) {
            if (preg_match("/Content-Length\s*:\s*(\d*)/", $row, $match))
                $bytes = $match[1];
        }
    }	
	if ($unit == TRUE) {
		if ($bytes >= 1048576) {
			$mb = $bytes/1048576;
			$bytes = number_format($mb, 2, ',', '.');
			$bytes.= ' Kb';
		} elseif ($bytes > 1024) {
			$kb = $bytes/1024;
			$bytes = number_format($kb, 2, ',', '.');
			$bytes.= ' Mb';
		}
	}
	return $bytes;
}

/**
*	$max_size : [2 mb|123 kb] 
*   $dest_path : full path with filename without ext
*/
function save_file_from_url($url='', $dest_path='', $max_size='2 mb', $allow_types='jpg,gif,png'){
    $return = array('ok'=>FALSE, 'msg'=>'', 'size'=>'', 'type'=>'', 'ext'=>'', 'path'=>'');
    if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
        $return['msg'] = 'URL tidak valid!';
        return $return;
        exit;
    }
    $arr_ext = array(
        'image/jpeg' => 'jpg', 
        'image/pjpeg' => 'jpg', 
        'image/gif' => 'gif', 
        'image/png' => 'png',
        'image/x-png' => 'png', 
        'text/html' => 'html',
        'text/plain' => 'txt',
        'text/xml' => 'xml',
        'application/json' => 'json',
        'text/json' => 'json',
        'application/zip' => 'zip',
        'application/x-zip' => 'zip',
        'application/x-zip-compressed' => 'zip',
        'application/msword' => 'doc',
        'application/excel' => 'xls',
        'application/pdf'=>'pdf', 
        'application/x-download'=>'pdf');
    $alt_exts = array('jpeg','htm','shtml','docx','xlsx');
    $file_headers = get_headers($url); 
    $file_type = get_mime_from_header($file_headers); 
	$file_size = get_size_from_header($file_headers); 
	if (!array_key_exists($file_type, $arr_ext)) {
		$return['msg'] = 'File jenis ini tidak diperbolehkan!';
		return $return;
        exit;
	}	
	$file_ext = $arr_ext[$file_type];
	if (!is_array($allow_types))
		$allow_types = explode(',', $allow_types);
	if (!in_array($file_ext, $allow_types)) {		
		$return['msg'] = "Hanya file ".implode(', ', $allow_types)." yang dibolehkan!";
		return $return;
        exit;
	}
	$exp_max = explode(' ', $max_size);
	if (isset($exp_max[1])) {
		if (strtolower($exp_max[1]) == 'kb') $max_file_size = $exp_max[0] * 1024;
		elseif (strtolower($exp_max[1]) == 'mb') $max_file_size = $exp_max[0] * 1048576;
	}
	if ($file_size < 0) {
		$return['msg'] = 'Ukuran file tidak diketahui!';
		return $return;
        exit;
	}
	if ($file_size > $max_file_size) {
		$return['msg'] = 'Ukuran file maksimal hanya '.$max_size.'!';
		return $return;
        exit;
	}
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
    $rawdata = curl_exec($ch);
    curl_close($ch);  
	$ext_url = pathinfo(basename($url), PATHINFO_EXTENSION);
	if ($ext_url != '' && in_array($ext_url, $alt_exts))
		$file_ext = $ext_url; 
    $dest_path.= '.'.$file_ext;
	if (file_exists($dest_path)) {
		unlink($dest_path);
	}
	$fp = fopen($dest_path, 'x');
	$fw = fwrite($fp, $rawdata);
	fclose($fp);
	if ($fw !== FALSE) {
		$return['path'] = substr($dest_path, 0, 1) == './' ? substr($dest_path, 2, strlen($dest_path)-2) : $dest_path;
		$return['ok'] = TRUE;
		$return['msg'] = "File berhasil disimpan..";
	}
	
    $return['size'] = $file_size;
    $return['type'] = $file_type;
    $return['ext'] = isset($file_ext) ? $file_ext : '';
    return $return;
}

/**
 * mendaftar gambar yang ada dalam direktori
 * @param string $dir nama direktori
 * @param int $limit batas file yang akan ditampilkan. 0=semua
 * @param string $imagetypes filter extensi, batasi dengan ",". (jpg,png,gif)  
 * @return array [name|path|url|width|height|attr|ext|mode]
 */
if (!function_exists('list_images'))
{ 
    function list_images($dir, $limit=0, $imagetypes="jpg,jpeg,gif,png") {    
        $ci = &get_instance();
        $ci->load->helper('url');
        $res = array();
        $imagetypes=str_replace(' ', '', $imagetypes);
        $imgs = explode(",", strtolower($imagetypes));
        if(substr($dir, -1) != "/") $dir .= "/";
        if(!file_exists($dir)) {
            return $res;
            exit;
        }
        $ind = 1;
        $myDirectory = opendir($dir);
        while($fname = readdir($myDirectory)) {
            $fext = get_file_ext($fname);
            if(substr($fname, 0, 1) != "." && in_array($fext, $imgs)) { 
                if($limit!=0 && $ind>$limit) break;
                $imginfo = getimagesize($dir.$fname);
                if($imginfo[0] / $imginfo[1] > 1)
                    $orient = 'landscape'; else $orient = 'portrait';
                $res[] = array(
                    'name' => $fname,
                    'path' => $dir.$fname,
                    'url' => base_url().$dir.$fname,
                    'width' => $imginfo[0],
                    'height' => $imginfo[1],
                    'attr' => $imginfo[3],
                    'ext' => $fext,
                    'mode' => $orient
                );
                $ind++;
            }
        }
        closedir($myDirectory);
        return $res;
    }
}

/* End of file DD_file_helper.php */
/* Location: ./app/helpers/DD_file_helper.php */
