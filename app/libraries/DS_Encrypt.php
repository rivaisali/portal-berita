<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DS_Encrypt extends CI_Encrypt
{
    var $CI = NULL;
    
    public function __construct()
    {
        parent::__construct();
    } 
    
    public function md5($str)
    {
        if (is_null($this->CI)) $this->CI = get_instance();
        $key = $this->CI->config->item('encryption_key');
        return md5(md5($key).md5($str));
    }

}

/* End of file DS_encrypt.php */
/* Location: ./application/libraries/DS_encrypt.php */
