<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Image {
    
    private $_mime = '';
    private $_sourceWidth = 0;
    private $_sourceHeight = 0;
    private $_sourceType = NULL;    
    private $_source_gd_image = NULL;
    
    var $source_img = '';
    var $transp_png = FALSE;
    var $transp_gif = FALSE;  
    var $quality = 60;  // 10 - 100
    var $transp_color = 'white';  // [white|black|array(rgb)]
    
    function __construct() { }
    
    public function initialize() {
        @list($this->_sourceWidth, $this->_sourceHeight, $this->_sourceType) = getimagesize($this->source_img); 
        switch ($this->_sourceType) { 
            case IMAGETYPE_GIF:
                $this->_source_gd_image = imagecreatefromgif($this->source_img); 
                $this->_mime = 'image/gif';
                break;
            case IMAGETYPE_JPEG:
                $this->_source_gd_image = imagecreatefromjpeg($this->source_img); 
                $this->_mime = 'image/jpg';
                break;
            case IMAGETYPE_PNG:
                $this->_source_gd_image = imagecreatefrompng($this->source_img); 
                $this->_mime = 'image/png';
                break;
        }
    }
    
    /**
     *  @param $mode = [crop|stretch|keep/(empty)]
     * */
    public function resize($width=640, $height=480, $dest_file=NULL, $mode='crop') {        
        if ($this->_source_gd_image === FALSE) { return FALSE; }
        $this->initialize();
        $src_x = 0;
        switch ($mode) {
            case 'crop':
                $ratio = max($width/$this->_sourceWidth, $height/$this->_sourceHeight);
                $this->_sourceHeight = ceil($height / $ratio);
                $src_x = ($this->_sourceWidth - $width / $ratio) / 2;
                $this->_sourceWidth = ceil($width / $ratio);
                $thumb_width = $width;
                $thumb_height = $height;                               
            break;
            case 'stretch':
                $thumb_width = $width;
                $thumb_height = $height;
            break;
            default:
                $source_ratio = $this->_sourceWidth / $this->_sourceHeight;
                $thumb_ratio = $width / $height;
                if ($this->_sourceWidth <= $width && $this->_sourceHeight <= $height) {
                    $thumb_width = $this->_sourceWidth;
                    $thumb_height = $this->_sourceHeight;
                } elseif ($thumb_ratio > $source_ratio) {
                    $thumb_width = (int) ($height * $source_ratio);
                    $thumb_height = $height;
                } else {
                    $thumb_width = $width;
                    $thumb_height = (int) ($width / $source_ratio);
                }                
            break;
        }
        $thumb_gd_image = imagecreatetruecolor($thumb_width, $thumb_height);
        $transparent = ($this->_sourceType == IMAGETYPE_PNG && $this->transp_png) || ($this->_sourceType == IMAGETYPE_GIF && $this->transp_gif);
        if ($transparent) {
            if ($this->_sourceType == IMAGETYPE_GIF) {
                $cur_transp = imagecolortransparent($this->_source_gd_image); 
                $palletsize = imagecolorstotal($this->_source_gd_image);
                if ($cur_transp != -1 && $cur_transp < $palletsize) {
                    $transp_color = imagecolorsforindex($this->_source_gd_image, $cur_transp); 
                    $cur_transp = imagecolorallocate($thumb_gd_image, $transp_color['red'], $transp_color['green'], $transp_color['blue']); 
                    imagefill($thumb_gd_image, 0, 0, $cur_transp); 
                    imagecolortransparent($thumb_gd_image, $cur_transp);   
                } 
            } else {
                imagealphablending($thumb_gd_image, FALSE);
                imagesavealpha($thumb_gd_image, TRUE);
                $img_transp = imagecolorallocatealpha($thumb_gd_image, 255, 255, 255, 127);
                imagefilledrectangle($thumb_gd_image, 0, 0, $width, $height, $img_transp);
            }     
        } else {
            if (is_array($this->transp_color) && !empty($this->transp_color[2])) {
                $filled_img = imagecolorallocate($thumb_gd_image, $this->transp_color[0], $this->transp_color[1], $this->transp_color[2]);
            } elseif ($this->transp_color == 'black') {
                $filled_img = imagecolorallocate($thumb_gd_image, 0, 0, 0);
            } else {  // default white
                $filled_img = imagecolorallocate($thumb_gd_image, 255, 255, 255); 
            }          
            imagefill($thumb_gd_image, 0, 0, $filled_img);
        }
        imagecopyresampled($thumb_gd_image, $this->_source_gd_image, 0, 0, $src_x, 0, $thumb_width, $thumb_height, $this->_sourceWidth, $this->_sourceHeight);
        if ($dest_file == '') $dest_file = NULL;
        if ($dest_file == NULL) header("Content-type: {$this->_mime}");
        switch ($this->_sourceType) { 
            case IMAGETYPE_GIF:
                if ($this->transp_gif)
                    imagegif($thumb_gd_image, $dest_file); else
                    imagejpeg($thumb_gd_image, $dest_file, $this->quality);
                break;
            case IMAGETYPE_JPEG:
                imagejpeg($thumb_gd_image, $dest_file, $this->quality); break;
            case IMAGETYPE_PNG:
                if ($this->transp_png) {
                    $this->quality = 10 - ($this->quality / 10);
                    $this->quality = ceil($this->quality);
                    imagepng($thumb_gd_image, $dest_file, $this->quality);
                } else
                    imagejpeg($thumb_gd_image, $dest_file, $this->quality);
                break;
        }
        imagedestroy($thumb_gd_image);
        return TRUE;
    }
    
    public function clear() {
        imagedestroy($this->_source_gd_image);        
    }
}