<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Form
{    
    var $return = FALSE;
    var $upload = FALSE;    
    var $layout = 'bootstrap'; // [simple/{empty}|bootstrap|table]
    var $input_size = ''; // bootstrap only [lg|sm|{empty}]
    var $inline = FALSE;  
    var $label_width = 0; // for simple and table layout only
    
    private $charset = '';
    private $temp_array = array(0 => '', 1 => '');
    private $validation_states = array();  // bootstrap only
    private $input_addon = array();  // bootstrap only
    private $help_text = array();  // bootstrap only
    
    function __construct()
    {
        $CI = &get_instance();
        $CI->load->helper(array('inflector','array'));
        $this->charset = $CI->config->item('charset');
    }

    public function open($action, $attr='', $method='post', $return='')
    {       
        $res = '';
        if ($this->label_width > 0) $res.='<style>form label {width:'.$this->label_width.'px}</style>';
        if ($return == '') $return = $this->return;
        $action = (strpos($action, '://') === false) ? base_url($action) : $action; 
        $res.= '<form role="form" method="'.$method.'" accept-charset="'.$this->charset.'" ';  
        $res.= 'action="' . $action . '" ';      
        if ($this->upload == TRUE) $res.= 'enctype="multipart/form-data" ';
        $data = array();
        if ($this->layout == 'bootstrap') {
            if ($this->inline == TRUE)
                $data = array('class'=>'form-inline'); else
                $data = array('class'=>'form-horizontal');
        }
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        $res.= array_to_attr($data).'>';
        if ($this->layout == 'table') $res.= "<table border='0'>";
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function hidden($name, $val='', $attr='', $return='')
    {  
        if ($return == '') $return = $this->return;
        $data = array(
            'name' => $name,
            'id' => $name,
            'value' => $val            
        );
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        $res = '<input type="hidden" '.array_to_attr($data).' />';
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function text($name, $val='', $attr='', $return='')
    {  
        if ($return == '') $return = $this->return;
        $data = array(
            'name' => $name,
            'id' => $name,
            'value' => $val);
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        $data = $this->add_bootstrap_extra($data); 
        $attr = array_to_attr($data);
        $res = sprintf('%s<input type="text" %s />%s', $this->temp_array[0], $attr, $this->temp_array[1]);
        $this->temp_array = array(0 => '', 1 => '');
        if ($return) return $res; else { echo $res; return $this; }
    }
       
    public function ftext($name, $val='', $label='', $attr='', $before='', $after='', $return='')
    {
        $res = '';
        if ($return == '') $return = $this->return;
        if ($label == '=') $label = humanize($name);
        if ($this->layout == 'table') { 
            $res.= '<tr>'; 
            $res.= '<td>'.$label.'</td>';
            $res.= '<td>'.$before.self::text($name, $val, $attr, TRUE).$after.'</td>';
            $res.= '</tr>'; 
        } elseif ($this->layout == 'bootstrap') {
            $res.= '<div class="form-group clearfix';
            if (isset($this->validation_states[$name]))
                $res.= ' '.$this->validation_states[$name];
            $res.= '"><label for="'.$name.'" class="col-lg-2 col-md-3 control-label">'.$label.'</label>';
            $res.= '<div class="col-lg-10 col-md-9">';
            $res.= $before.self::text($name, $val, $attr, TRUE).$after;
            $res.= '</div></div>';
        } else {
            $res.= '<p>';
            $res.= '<label for="'.$name.'">'.$label.'</label>';
            $res.= $before.self::text($name, $val, $attr, TRUE).$after;
            $res.= '</p>';
        }    
        if ($return) return $res; else { echo $res; return $this; }
    }    
    
    public function pass($name, $val='', $attr='', $return='')
    {  
        if ($return == '') $return = $this->return;
        $data = array(
            'name' => $name,
            'id' => $name,
            'value' => $val);
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        $data = $this->add_bootstrap_extra($data); 
        $attr = array_to_attr($data);
        $res = sprintf('%s<input type="password" %s />%s', $this->temp_array[0], $attr, $this->temp_array[1]);
        $this->temp_array = array(0 => '', 1 => '');
        if ($return) return $res; else { echo $res; return $this; }
    }    
    
    public function fpass($name, $val='', $label='', $attr='', $before='', $after='', $return='')
    {
        $res = '';
        if ($return == '') $return = $this->return;
        if ($label == '=') $label = humanize($name);
        if ($this->layout == 'table') { 
            $res.= '<tr>'; 
            $res.= '<td>'.$label.'</td>';
            $res.= '<td>'.$before.self::pass($name, $val, $attr, TRUE).$after.'</td>';
            $res.= '</tr>'; 
        } elseif ($this->layout == 'bootstrap') {
            $res.= '<div class="form-group clearfix';
            if (isset($this->validation_states[$name]))
                $res.= ' '.$this->validation_states[$name];
            $res.= '"><label for="'.$name.'" class="col-lg-2 col-md-3 control-label">'.$label.'</label>';
            $res.= '<div class="col-lg-10 col-md-9">';
            $res.= $before.self::pass($name, $val, $attr, TRUE).$after;
            $res.= '</div></div>';
        } else {
            $res.= '<p>';
            $res.= '<label for="'.$name.'">'.$label.'</label>';
            $res.= $before.self::pass($name, $val, $attr, TRUE).$after;
            $res.= '</p>';
        }    
        if ($return) return $res; else { echo $res; return $this; }
    } 
    
    public function select($name, $val='', $items=array(), $attr='', $first_empty=TRUE, $return='')
    {  
        if ($return == '') $return = $this->return;
        $data = array(
            'name' => $name,
            'id' => $name);
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        $empty_label = isset($data['placeholder']) ? $data['placeholder'] : '';
        unset($data['placeholder']);
        $data = $this->add_bootstrap_extra($data); 
        $attr = array_to_attr($data);
        if ($first_empty) $options = '<option value="">'.$empty_label.'</option>'; else $options = '';
        foreach ($items as $item_val=>$item_label) {
            $options.= '<option value="'.$item_val.'"';
            if ($item_val == $val) $options.= ' selected';
            $options.= '>'.$item_label.'</option>';
        }
        $res = sprintf('%s<select %s>%s</select>%s', $this->temp_array[0], $attr, $options, $this->temp_array[1]);
        $this->temp_array = array(0 => '', 1 => '');
        if ($return) return $res; else { echo $res; return $this; }
    }  
    
    public function fselect($name, $val='', $label='', $items=array(), $attr='', $before='', $after='', $first_empty=TRUE, $return='')
    {
        $res = '';
        if ($return == '') $return = $this->return;
        if ($label == '=') $label = humanize($name);
        if ($this->layout == 'table') { 
            $res.= '<tr>'; 
            $res.= '<td>'.$label.'</td>';
            $res.= '<td>'.$before.self::select($name, $val, $items, $attr, $first_empty, TRUE).$after.'</td>';
            $res.= '</tr>'; 
        } elseif ($this->layout == 'bootstrap') {
            $res.= '<div class="form-group clearfix';
            if (isset($this->validation_states[$name]))
                $res.= ' '.$this->validation_states[$name];
            $res.= '"><label for="'.$name.'" class="col-lg-2 col-md-3 control-label">'.$label.'</label>';
            $res.= '<div class="col-lg-10 col-md-9">';
            $res.= $before.self::select($name, $val, $items, $attr, $first_empty, TRUE).$after;
            $res.= '</div></div>';
        } else {
            $res.= '<p>';
            $res.= '<label for="'.$name.'">'.$label.'</label>';
            $res.= $before.self::select($name, $val, $items, $attr, $first_empty, TRUE).$after;
            $res.= '</p>';
        }    
        if ($return) return $res; else { echo $res; return $this; }
    } 
    
    public function textarea($name, $val='', $attr='', $return='')
    {  
        if ($return == '') $return = $this->return;
        $data = array(
            'name' => $name,
            'id' => $name);
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        $data = $this->add_bootstrap_extra($data); 
        $attr = array_to_attr($data);
        $res = sprintf('%s<textarea %s>%s</textarea>%s', $this->temp_array[0], $attr, $val, $this->temp_array[1]);
        $this->temp_array = array(0 => '', 1 => '');
        if ($return) return $res; else { echo $res; return $this; }
    }
       
    public function ftextarea($name, $val='', $label='', $attr='', $before='', $after='', $return='')
    {
        $res = '';
        if ($return == '') $return = $this->return;
        if ($label == '=') $label = humanize($name);
        if ($this->layout == 'table') { 
            $res.= '<tr>'; 
            $res.= '<td>'.$label.'</td>';
            $res.= '<td>'.$before.self::textarea($name, $val, $attr, TRUE).$after.'</td>';
            $res.= '</tr>'; 
        } elseif ($this->layout == 'bootstrap') {
            $res.= '<div class="form-group clearfix';
            if (isset($this->validation_states[$name]))
                $res.= ' '.$this->validation_states[$name];
            $res.= '"><label for="'.$name.'" class="col-lg-2 col-md-3 control-label">'.$label.'</label>';
            $res.= '<div class="col-lg-10 col-md-9">';
            $res.= $before.self::textarea($name, $val, $attr, TRUE).$after;
            $res.= '</div></div>';
        } else {
            $res.= '<p>';
            $res.= '<label for="'.$name.'">'.$label.'</label>';
            $res.= $before.self::textarea($name, $val, $attr, TRUE).$after;
            $res.= '</p>';
        }    
        if ($return) return $res; else { echo $res; return $this; }
    } 
    
    public function radio($name, $val='', $items=array(), $wrapper='', $inline=TRUE, $attr='', $return='')
    {
        $res = '';
        if ($return == '') $return = $this->return;
        $data = array('name' => $name);
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        $data = array_merge_concat($data, array('class'=>$name));       
        unset($data['id']);
        $attr = array_to_attr($data);
        $index = 1;
        foreach ($items as $item_val => $item_label) {   
            if (is_array($wrapper) && !empty($wrapper[0])) $res.= $wrapper[0];
            if ($inline == TRUE) {
                $res.= '<label class="radio-inline"><input ';
                if ($val == $item_val) $res.= 'checked ';
                $res.= 'type="radio" value="'.$item_val.'" '.$attr.' /> '.$item_label.'</label>';
            } else {
                $res.= '<div class="radio"><label><input ';
                if ($val == $item_val) $res.= 'checked ';
                $res.= 'type="radio" value="'.$item_val.'" '.$attr.' />'.$item_label.'</label></div>';
            }          
            if (is_array($wrapper) && !empty($wrapper[1])) $res.= $wrapper[1];       
            $index++;   
        }  
        if (isset($this->help_text[$name])) 
            $res.= '<span class="help-block">'.$this->help_text[$name].'</span>';                    
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function fradio($name, $val='', $label='', $items=array(), $wrapper='', $inline=TRUE, $attr='', $before='', $after='', $return='')
    {
        $res = '';
        if ($return == '') $return = $this->return;
        if ($label == '=') $label = humanize($name);        
        if ($this->layout == 'table') { 
            $res.= '<tr>'; 
            $res.= '<td>'.$label.'</td>';
            $res.= '<td>'.$before.self::radio($name, $val, $items, $wrapper, $inline, $attr, TRUE).$after.'</td>';
            $res.= '</tr>'; 
        } elseif ($this->layout == 'bootstrap') {
            $res.= '<div class="form-group clearfix';
            if (isset($this->validation_states[$name]))
                $res.= ' '.$this->validation_states[$name];
            $res.= '"><label for="'.$name.'" class="col-lg-2 col-md-3 control-label">'.$label.'</label>';
            $res.= '<div class="col-lg-10 col-md-9">';
            $res.= $before.self::radio($name, $val, $items, $wrapper, $inline, $attr, TRUE).$after;
            $res.= '</div></div>';
        } else {
            $res.= '<p>';
            $res.= '<label for="'.$name.'">'.$label.'</label>';
            $res.= $before.self::radio($name, $val, $items, $wrapper, $inline, $attr, TRUE).$after;
            $res.= '</p>';
        }    
        if ($return) return $res; else { echo $res; return $this; }
    } 
    
    public function checkbox($name, $val='', $items=array(), $wrapper='', $inline=TRUE, $attr='', $return='')
    {
        $res = '';
        if ($return == '') $return = $this->return;
        $data = array('name' => $name);
        if (count($items) > 1) $data['name'].= '[]';
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        $data = array_merge_concat($data, array('class'=>$name));     
        unset($data['id']);
        $attr = array_to_attr($data);
        if ( !is_array($val) ) $val = array($val);
        foreach ($items as $item_val => $item_label) {   
            if (is_array($wrapper) && !empty($wrapper[0])) $res.= $wrapper[0];
            if ($inline == TRUE) {
                $res.= '<label class="checkbox-inline" title="'.$item_label.'"><input ';
                if ( in_array($item_val, $val) ) $res.= 'checked ';
                $res.= 'type="checkbox" value="'.$item_val.'" '.$attr.' /> '.$item_label.'</label>';
            } else {
                $res.= '<div class="checkbox" title="'.$item_label.'"><label><input ';
                if ( in_array($item_val, $val) ) $res.= 'checked ';
                $res.= 'type="checkbox" value="'.$item_val.'" '.$attr.' />'.$item_label.'</label></div>';
            }          
            if (is_array($wrapper) && !empty($wrapper[1])) $res.= $wrapper[1];   
        }  
        if (isset($this->help_text[$name])) 
            $res.= '<span class="help-block">'.$this->help_text[$name].'</span>';                    
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function fcheckbox($name, $val='', $label='', $items=array(), $wrapper='', $inline=TRUE, $attr='', $before='', $after='', $return='')
    {
        $res = '';
        if ($return == '') $return = $this->return;
        if ($label == '=') $label = humanize($name);        
        if ($this->layout == 'table') { 
            $res.= '<tr>'; 
            $res.= '<td>'.$label.'</td>';
            $res.= '<td>'.$before.self::checkbox($name, $val, $items, $wrapper, $inline, $attr, TRUE).$after.'</td>';
            $res.= '</tr>'; 
        } elseif ($this->layout == 'bootstrap') {
            $res.= '<div class="form-group clearfix';
            if (isset($this->validation_states[$name]))
                $res.= ' '.$this->validation_states[$name];
            $res.= '"><label for="'.$name.'" class="col-lg-2 col-md-3 control-label">'.$label.'</label>';
            $res.= '<div class="col-lg-10 col-md-9">';
            $res.= $before.self::checkbox($name, $val, $items, $wrapper, $inline, $attr, TRUE).$after;
            $res.= '</div></div>';
        } else {
            $res.= '<p>';
            $res.= '<label for="'.$name.'">'.$label.'</label>';
            $res.= $before.self::checkbox($name, $val, $items, $wrapper, $inline, $attr, TRUE).$after;
            $res.= '</p>';
        }    
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function submit($name, $label='', $attr='', $return='')
    {  
        if ($return == '') $return = $this->return;
        $data = array(
            'name' => $name,
            'id' => $name);
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        if ($this->layout == 'bootstrap') {
            if (!isset($data['class'])) {
                $data['class'] = 'btn btn-success';
                if ($this->input_size != '') $data['class'].= ' btn-'.$this->input_size;
            }
            $exp = explode('#', $label);
            if ($exp > 1) {
                if (substr($exp[0], 0, 3) == 'fa-')
                    $label = '<i class="fa '.$exp[0].'"></i> '; else
                    $label = '<span class="glyphicon glyphicon-'.$exp[0].'"></span> ';
                $label.= $exp[1]; 
            }
        }
        $attr = array_to_attr($data);
        $res = '<button type="submit" '.$attr.'>'.$label.'</button>';
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function fsubmit($name, $label='', $attr='', $return='')
    {
        if ($return == '') $return = $this->return;
        $res = self::no_label($name, self::submit($name, $label, $attr, TRUE), TRUE);
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function reset($name, $label='', $attr='', $return='')
    {  
        if ($return == '') $return = $this->return;
        $data = array(
            'name' => $name,
            'id' => $name);
        if ($attr != '') {
            if (!is_array($attr)) $attr = attr_to_array($attr);            
            $data = array_merge($data, $attr);
        }
        if ($this->layout == 'bootstrap') {
            if (!isset($data['class'])) {
                $data['class'] = 'btn btn-danger';
                if ($this->input_size != '') $data['class'].= ' btn-'.$this->input_size;
            }
            $exp = explode('#', $label);
            if ($exp > 1) {
                if (substr($exp[0], 0, 3) == 'fa-')
                    $label = '<i class="fa '.$exp[0].'"></i> '; else
                    $label = '<span class="glyphicon glyphicon-'.$exp[0].'"></span> ';
                $label.= $exp[1]; 
            }
        }
        $attr = array_to_attr($data);
        $res = '<button type="reset" '.$attr.'>'.$label.'</button>';
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function freset($name, $label='', $attr='', $return='')
    {
        if ($return == '') $return = $this->return;
        $res = self::no_label($name, self::reset($name, $label, $attr, TRUE), TRUE);
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function fsubmit_reset($submit_args=array(), $reset_args=array(), $return='')
    {
        if ($return == '') $return = $this->return;
        $submit_args[2] = isset($submit_args[2]) ? $submit_args[2] : '';
        $reset_args[2] = isset($reset_args[2]) ? $reset_args[2] : '';
        $res = self::no_label('submit_reset', self::submit($submit_args[0], $submit_args[1], $submit_args[2], TRUE).'&nbsp;&nbsp;'.
            self::reset($reset_args[0], $reset_args[1], $reset_args[2], TRUE), TRUE);
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function close($return='')
    {
        if ($this->layout == 'table') 
            $res = '</table></form>'; else
            $res = '</form>';
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function no_label($name='', $field='', $return='')
    {
        $res = '';
        if ($return == '') $return = $this->return;
        if ($this->layout == 'table') { 
            $res.= '<tr>'; 
            $res.= '<td>&nbsp;</td>';
            $res.= '<td>'.$field.'</td>';
            $res.= '</tr>'; 
        } elseif ($this->layout == 'bootstrap') {
            $res.= '<div class="form-group clearfix';
            if (isset($this->validation_states[$name]))
                $res.= ' '.$this->validation_states[$name];
            $res.= '">';
            $res.= '<div class="col-lg-offset-2 col-md-offset-3 col-lg-10 col-md-9">';
            $res.= $field;
            $res.= '</div></div>';
        } else {
            $res.= '<p>';
            $res.= '<label>&nbsp;</label>'.$field.'</p>';
        }    
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    public function add_row($name='', $label='', $field='', $static_control=FALSE, $return='')
    {        
        $res = '';
        if ($return == '') $return = $this->return;
        if ($label == '=') $label = humanize($name);
        if ($this->layout == 'table') { 
            $res.= '<tr>'; 
            $res.= '<td>'.$label.'</td>';
            $res.= '<td>'.$field.'</td>';
            $res.= '</tr>'; 
        } elseif ($this->layout == 'bootstrap') {
            $res.= '<div class="form-group clearfix';
            if (isset($this->validation_states[$name]))
                $res.= ' '.$this->validation_states[$name];
            $res.= '"><label for="'.$name.'" class="col-lg-2 col-md-3 control-label">'.$label.'</label>';
            $res.= '<div class="col-lg-10 col-md-9">';
            if ($static_control == TRUE)
                $res.= '<p class="form-control-static">'.$field.'</p>'; else $res.= $field;
            $res.= '</div></div>';
        } else {
            $res.= '<p>';
            $res.= '<label for="'.$name.'">'.$label.'</label>'.$field.'</p>';
        }    
        if ($return) return $res; else { echo $res; return $this; }
    }
    
    
    public function set_validation_states($fields='', $state='error') // bootstrap only
    {
        if (!is_array($fields)) {
            $this->validation_states[$fields] = 'has-'.$state;
        } else {
            foreach ($fields as $field) {
                $this->validation_states[$field] = 'has-'.$state;
            }
        }
        
    }
    
    public function set_input_addon($fields='', $position='left') // bootstrap only
    {
        if (!is_array($fields)) {
            list ($name, $addon) = explode('=', $fields);
            $this->input_addon[$name][$position] = $addon;
        } else {
            foreach ($fields as $name => $addon) {
                $this->input_addon[$name][$position] = $addon;
            }
        }
        
    }
    
    public function set_help_text($fields='', $text='') // bootstrap only
    {
        if (!is_array($fields)) {
            $this->help_text[$fields] = $text;
        } else {
            foreach ($fields as $name => $text) {
                $this->help_text[$name] = $text;
            }
        }
        
    }
    
    private function add_bootstrap_extra($data=array()) {
        if ($this->layout == 'bootstrap') {
            $name = $data['name'];
            $base_class = 'form-control';
            if ($this->input_size != '') {
                if (!isset($this->input_addon[$name]))
                    $base_class.= ' input-'.$this->input_size;
            }
            if (isset($data['width'])) {
                $input_width = ' style="width:'.$data['width'].'px"';
                unset($data['width']);
            } else $input_width = '';
            $data = array_merge_concat($data, array('class'=>$base_class));
            if (isset($this->input_addon[$name])) {
                $addon_left = ''; $addon_right = '';
                if ( isset($this->input_addon[$name]['left']) ) {
                    $addon_left = $this->input_addon[$name]['left'];
                    $isbtn = strpos($addon_left, '<button');
                    $addon_left = $isbtn === FALSE ? '<span class="input-group-addon">'.$addon_left.'</span>' :
                        '<div class="input-group-btn">'.$addon_left.'</div>';
                } else {
                    $addon_right = $this->input_addon[$name]['right'];
                    $isbtn = strpos($addon_right, '<button');
                    $addon_right = $isbtn === FALSE ? '<span class="input-group-addon">'.$addon_right.'</span>' :
                        '<div class="input-group-btn">'.$addon_right.'</div>';
                }
                $this->temp_array[0] = '<div'.$input_width.' class="input-group';
                if ($this->input_size != '') $this->temp_array[0].= ' input-group-'.$this->input_size;
                $this->temp_array[0].= '">'.$addon_left;                
                $this->temp_array[1] = $addon_right.'</div>';
            }
            if (isset($this->help_text[$name])) {
                $this->temp_array[1].= '<span class="help-block">'.$this->help_text[$name].'</span>';
            }
        }     
        unset($data['addon_left'], $data['addon_right'], $data['help_text']); 
        return $data;
    }
    
}

/* End of file Form.php */
/* Location: ./app/libraries/form.php */