<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content 
{    
    private $contents = array();
    private $enqueued_js = array();
    private $enqueued_css = array();    
    private $extra_styles = '';
    
    private $_registered_js = array();
    private $_registered_css = array();
    
    private $ci = NULL;
    
    public function __construct()
    {
        $this->ci = &get_instance();
        $this->ci->config->load('assets'); 
        $this->ci->load->helper( array('url') );
        $this->_registered_js = $this->ci->config->item('assets_js'); 
        $this->_registered_css = $this->ci->config->item('assets_css'); 
    }
    
    /**
     * Menambahkan area pada template
     * @param string $name nama area, harus unik. 'head', 'foot' sudah terpakai
     * @param bool $return echo atau return. default FALSE
     */
    public function area($name = '', $return = FALSE) 
    {
        $content = isset($this->contents[$name]) ? $this->contents[$name] : '';         
        if ($return) return $content; else echo $content;
    }   
    
    /**
     * Menambahkan konten pada akhir content area  
     * @param string $area nama area dari fungsi area()
     * @param string $content content yang ingin ditambahkan
     */    
    public function add($area_name = '', $content = '') 
    {
        if ( isset($this->contents[$area_name]) ) 
            $content = $this->contents[$area_name] . "\n" . $content;    
        $this->contents[$area_name] = $content;
    } 
    
    /**
     * Menyisipkan konten pada awal content area  
     * @param string $area nama area dari fungsi area()
     * @param string $content content yang ingin disisipkan
     */
    public function insert($area_name = '', $content = '') 
    {
        if ( isset($this->contents[$area_name]) ) 
            $content .= "\n" . $this->contents[$area_name];    
        $this->contents[$area_name] = $content;
    } 
    
    /**
     * Menetapkan konten pada content area. Yang ditembahkan sebelumnya ditimpa
     * @param string $area nama area dari fungsi area()
     * @param string $content content yang ingin disisipkan
     */
    public function set($area_name = '', $content = '') 
    {   
        $this->contents[$area_name] = $content;
    } 
    
    /**
     * Sama dengan menambahkan area('head')
     */
    public function head_area() 
    {        
        $this->enqueued_css = $this->_sortby_dependency($this->enqueued_css);
        $new_queued = array();
        $i = 1;
        foreach ($this->enqueued_css as $id => $path) {
            $can_merged = isset($this->_registered_css[$id]) && !is_absolut_url($path);
            if ($can_merged) {
                $new_queued[$i][] = $id;
            } else {
                $i++;
                $new_queued[$id] = $path;
            }          
        }  
        foreach ($new_queued as $path) {
            $media = '';
            if ( is_array($path) ) {
                $scripts_str = urlencode( implode(' ', $path) );
                $path = "loadcss?ids={$scripts_str}";
            } else {
                @list ($path, $media) = explode('|', $path);
                if ($media)
                    $media = ' media="'.$media.'"';
            }
            $path = proper_url($path);
            //$media = isset($media) ? '' : ' media="'.$media.'"';
            echo "<link rel=\"stylesheet\" href=\"{$path}\"{$media}/>\n";
        }  
        if ( !empty($this->contents['_extra_styles_']) ) {
            $styles = $this->contents['_extra_styles_'];
            foreach ($styles as $media => $style) {
                echo "<style type=\"text/css\" media=\"{$media}\">\n<!--\n{$style}\n-->\n</style>\n";
            }            
        }
        $this->area('head');
    }
    
    /**
     * Sama dengan menambahkan area('foot')
     */
    public function foot_area() 
    {     
        $this->enqueued_js = $this->_sortby_dependency($this->enqueued_js);
        $new_queued = array();
        $i = 1;
        foreach ($this->enqueued_js as $id => $path) {
            $can_merged = isset($this->_registered_js[$id]) && !is_absolut_url($path);
            if ($can_merged) {
                $new_queued[$i][] = $id;
            } else {
                $i++;
                $new_queued[$id] = $path;
            }          
        }  
        foreach ($new_queued as $path) {
            if ( is_array($path) ) {
                $scripts_str = urlencode( implode(' ', $path) );
                $path = "loadjs?ids={$scripts_str}";
            }
            $path = proper_url($path);
            echo "<script src=\"{$path}\"></script>\n";
        }      
        $scripts = !empty($this->contents['_extra_scripts_']) ? $this->contents['_extra_scripts_'] : '';         
        $on_ready = !empty($this->contents['_extra_scripts_ready_']) ? $this->contents['_extra_scripts_ready_'] : ''; 
        if ($on_ready != '') 
            $scripts.= " jQuery(document).ready(function($){ {$on_ready} });";
        if ($scripts != '')
            echo "<script>\n<!--\n{$scripts}\n-->\n</script>\n";    
        $this->area('foot');    
    } 
    
    /**
     * Menambahkan eksternal javascript. Diletakkan di footer 
     * @param string $path path to file. required
     * @return void
     */
    public function enqueue_js( $id = '', $path = '', $dependency = array() )
    {
        if ( !is_array($dependency) && !is_null($dependency) ) {
            $dependency = str_replace(' ', '', $dependency);
            $dependency = explode(',', $dependency);
        }      
        if ($path == '' || $path == NULL) {    
            $path = isset($this->_registered_js[$id]) ? $this->_registered_js[$id] : ''; 
        }
        if ( !array_key_exists($id, $this->enqueued_js) ) {
            $this->enqueued_js[$id] = array('path' => $path, 'dependency' => $dependency); 
        }
    }
    
    /**
     * 
     * Menambahkan eksternal css. Diletakkan di head 
     * 
     * @param string $path path to file. required |all -> media
     * @return void
     * 
     */
    public function enqueue_css( $id = '', $path = '', $dependency = array() )
    {
        if ( !is_array($dependency) && !is_null($dependency) ) {
            $dependency = str_replace(' ', '', $dependency);
            $dependency = explode(',', $dependency);
        }
        if ($path == '' || $path == NULL) {    
            $path = isset($this->_registered_js[$id]) ? $this->_registered_js[$id] : ''; 
        }
        if ( !array_key_exists($id, $this->enqueued_css) ) {
            $this->enqueued_css[$id] = array('path' => $path, 'dependency' => $dependency); 
        }
    }  
  
    public function add_script($script, $on_ready = FALSE) 
    { 
        $key = '_extra_scripts_';
        if ($on_ready) $key .= 'ready_';
        if ( isset($this->contents[$key]) ) 
            $script = $this->contents[$key] . $script;    
        $this->contents[$key] = $script;
    }   
    
    public function add_style($style, $media = 'screen') 
    { 
        $key = '_extra_styles_';
        if ( !isset($this->contents[$key]) )
            $this->contents[$key] = array();
        if ( isset($this->contents[$key][$media]) ) 
            $style = $this->contents[$key][$media] . $style;    
        $this->contents[$key][$media] = $style;
    } 
    
    public function page_title($title = '') 
    {
        if ($title == '') {
            return isset( $this->contents['_page_title_'] ) ? html_escape($this->contents['_page_title_']) : '';
        } else {
            $this->contents['_page_title_'] = $title;
            return TRUE;
        }
    } 
    
    public function page_description($description = '') 
    {
        if ($description == '') {
            return isset( $this->contents['_page_description_'] ) ? html_escape($this->contents['_page_description_']) : '';
        } else {
            $this->contents['_page_description_'] = $description;
            return TRUE;
        }
    } 
    
    public function page_imageurl($imageurl = '') 
    {
        if ($imageurl == '') {
            return isset( $this->contents['_page_imageurl_'] ) ? $this->contents['_page_imageurl_'] : '';
        } else {
            $this->contents['_page_imageurl_'] = $imageurl;
            return TRUE;
        }
    } 
    
    public function page_header($title = '') 
    {
        if ($title == '') {
            return isset( $this->contents['_page_header_'] ) ? $this->contents['_page_header_'] : '';
        } else {
            $this->contents['_page_header_'] = $title;
            return TRUE;
        }
    }
    
    // params: url|label
    public function set_breadcumb()
    {
        $items = array();
        foreach (func_get_args() as $url_label) {
            list($key, $val) = explode('|', $url_label);
            $items[$key] = $val;
        }
        $this->contents['_breadcumb_items_'] = $items;
    }  
    
    public function get_breadcumb($base_uri = '', $home_label = 'Home', $attr = '')
    {
        $base_url = base_url($base_uri);
        $attr = generate_attr($attr);
        $attr = add_class_attr($attr, 'breadcrumb');
        $items = isset($this->contents['_breadcumb_items_']) ? $this->contents['_breadcumb_items_'] : array(); 
        $ret = "<ol {$attr}>";
        $ret.= '<li><a href="'.$base_url.'">'.$home_label.'</a></li>';
        $i = 1; $count = count($items);
        foreach ($items as $url => $label) {
            $url = "{$base_url}/{$url}";
            if ($i == $count)
                $ret.= '<li class="active">'.$label.'</li>'; else
                $ret.= '<li><a href="'.$url.'">'.$label.'</a></li>';
            $i++;
        }
        $ret.= '</ol>';
        return $ret;
    } 
    
    /**
     * Menggabungkan js/css dalam 1 file
     * path file sesuai yg terdaftar di app/config/assets.php, 
     * jika tidak ada, maka dicari di js_path/css_path
     * @link http://rakaz.nl
     * @param string $type [css|js]
     * @param $files string dibatasi spasi atau array
     * @param string/array $search_path jika tidak ditemukan di css_path/js_path
     */
    public function combine_assets($type='', $files='')
    {   
        $content_type = $type == 'js' ? 'application/javascript' : 'text/css';
        $path = $this->ci->config->item("assets_{$type}"); 
        $cache_on = TRUE; 
        $cachedir = APPPATH . 'cache/combined_assets';
        $elements = is_array($files) ? $files : explode(' ', $files); 
        $str_files = implode(' ', $elements);
        $lastmodified = 0;  
        $files = array();  
        foreach ($elements as $element) { 
            $tmp_file = $path[$element]; 
            //$tmp_file = "{$element}.{$type}";           
            if (file_exists($tmp_file)) {
                $files[] = $tmp_file;
                $lastmodified = max($lastmodified, filemtime($tmp_file));    
            } 
        } 
        if (empty($files)) { show_404(); exit; }        
        $hash = $lastmodified . '-' . md5($str_files); 
        header("Etag: \"{$hash}\"");
        $none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? stripslashes($_SERVER['HTTP_IF_NONE_MATCH']) : FALSE;
        if ($none_match == "\"{$hash}\"") { 
            // jika sudah pernah diakses dan belum dimodifikasi, tidak diproses server
            header("HTTP/1.0 304 Not Modified"); 
            header('Content-Length: 0'); 
        } else {
            if ($cache_on) {
                // Determine supported compression method
                $encoding = 'none'; 
                if (isset($_SERVER['HTTP_ACCEPT_ENCODING'])) {
                    $gzip = strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip');
                    $deflate = strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'deflate');
                    $encoding = $gzip ? 'gzip' : ($deflate ? 'deflate' : 'none');
                }
                // Check for buggy versions of Internet Explorer
                if (!strstr($_SERVER['HTTP_USER_AGENT'], 'Opera') && 
                    preg_match('/^Mozilla\/4\.0 \(compatible; MSIE ([0-9]\.[0-9])/i', $_SERVER['HTTP_USER_AGENT'], $matches)) {
                    $version = floatval($matches[1]);                    
                    if ($version < 6) $encoding = 'none';                        
                    if ($version == 6 && !strstr($_SERVER['HTTP_USER_AGENT'], 'EV1')) $encoding = 'none';
                }                
                // Try the cache first to see if the combined files were already generated 
                $cachefile = $cachedir.'/cache-'.$hash.'.' .$type. ($encoding != 'none' ? '.' . $encoding : '');                
                if (file_exists($cachefile)) { 
                    if ($fp = fopen($cachefile, 'rb')) {     
                        if ($encoding != 'none') 
                            header("Content-Encoding: " . $encoding);                                             
                        header("Content-Type: {$content_type}"); 
                        header("Content-Length: ".filesize($cachefile));             
                        $out = fread($fp, filesize($cachefile));
                        echo $out; 
                        fclose($fp); 
                        exit; 
                    } 
                }
            }
            // Get contents of the files 
            $contents = ''; 
            foreach($files as $file) { 
                $contents .= file_get_contents($file) . "\n\n"; 
            } 
            // Send Content-Type 
            header ("Content-Type: {$content_type}");              
            if (isset($encoding) && $encoding != 'none') { 
                // Send compressed contents 
                $contents = gzencode($contents, 9, $gzip ? FORCE_GZIP : FORCE_DEFLATE); 
                header ("Content-Encoding: " . $encoding); 
                header ('Content-Length: '.strlen($contents)); 
                echo $contents; 
            } else   { 
                // Send regular contents 
                header ('Content-Length: '.strlen($contents)); 
                echo $contents; 
            }     
            // Store cache 
            if ($cache_on) { 
                if ($fp = fopen($cachefile, 'wb')) { 
                    fwrite($fp, $contents); 
                    fclose($fp); 
                } 
            }
        }
    }
    
    private function _sortby_dependency( $arr = array() )
    {
        if ( empty($arr) ) return $arr;         
        $sorted = array();
        foreach ($arr as $name => $void) { 
            do { 
                $depends = $this->_depends($arr, $sorted, $name);         
                $sorted[$depends] = $arr[$depends]['path']; 
            } while ($depends != $name ); 
        }
        return $sorted;
    }
    
    private function _depends($arr, $sorted, $name) 
    { 
        foreach ($arr[$name]['dependency'] as $item) { 
            $item = trim($item);
            if ( !array_key_exists($item, $arr) ) continue;
            if ( !isset($sorted[$item]) ) 
                return $this->_depends($arr, $sorted, $item); 
        } 
        return $name; 
    }
}

/* End of file Content.php */
/* Location: ./app/libraries/Content.php */
