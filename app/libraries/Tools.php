<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tools
{    
    private $ci = NULL;
    
    // generate_assets_config
    public $js_libs = array();
    public $css_libs = array();
    
    public function __construct() 
    { 
        $this->ci = &get_instance(); 
    } 
    
    // generate file app/config/assets.php
    public function generate_assets_config($dirs = array(), $write_file = TRUE)
    {        
        $this->ci->load->helper('file');   
        if ( !is_array($dirs) ) $dirs = array($dirs);
        foreach ($dirs as $dir) {     //if ( !file_exists($dir) ) exit;
            $dir_info = get_dir_file_info($dir);
            foreach ($dir_info as $item) {
                if ( is_dir($item['server_path']) ) {  
                    $this->generate_assets_config($dir . $item['name'] . '/', FALSE);    
                } else {
                    $name = str_replace(array('.min.js', '.min.css', '.js', '.css'), '', $item['name']);
                    $name = str_replace('.', '-', $name);                
                    if ( substr($item['server_path'], -3) == '.js' ) { 
                        $this->js_libs[$name] = $item['relative_path'] . $item['name'];
                    } elseif ( substr($item['server_path'], -4) == '.css' ) { 
                        $this->css_libs[$name] = $item['relative_path'] . $item['name'];
                    } 
                }
            }
        }
        ksort($this->js_libs);
        ksort($this->css_libs);
        $libs = array(
            'js'  => $this->js_libs,
            'css' => $this->css_libs
        );
        if ($write_file) { 
            $text = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');\n\n";
            $text.= "\$config['assets_js'] = array(\n";
            foreach ($this->js_libs as $id => $path) {
                $text.= "\t'{$id}' => '{$path}',\n";   
            }
            $text.= ");\n\n\$config['assets_css'] = array(\n";
            foreach ($this->css_libs as $id => $path) {
                $text.= "\t'{$id}' => '{$path}',\n";   
            }
            $text.= ");\n\n/* End of file assets.php */\n/* Location: ./app/config/assets.php */";
            write_file(APPPATH . 'config/assets.php', $text);
        }
        return $libs;
    }
    
    // Untuk file form_validation.php di config
    public function generate_rules($table)
    {
        $ci = &get_instance();         
        $ci->load->library('table');
        $ci->load->helper(array('form','inflector'));
        $tmpl = array('table_open'=>'<table border="1" cellpadding="5" cellspacing="1">' );
        $ci->table->set_template($tmpl);
        $ci->table->set_heading('Field', 'Label', 'Trim', 'Required', 'Min Length', 
            'Max Length', 'Valid Email', 'Matches', 'Unique', 'Encrypt', 'MD5', 'Strip Tags');
        echo "<h3>{$table}</h3>";
        $fields = $ci->db->list_fields($table);   
        $no = 1;  $pkey = '';   
        foreach ($fields as $field) {
            if ($no == 1) $pkey = $field;
            $label = form_input("label[$field]", humanize($field));
            $trim = form_checkbox("trim[$field]", $field);
            $req = form_checkbox("required[$field]", $field, TRUE);
            $min = form_input("min_length[$field]", '', 'size="3"');
            $max = form_input("max_length[$field]", '', 'size="3"');
            $email = form_checkbox("valid_email[$field]", $field);
            $match = form_input("match[$field]");
            $uniq = form_checkbox("unique[$field]", $field);
            $enc = form_checkbox("encrypt[$field]", $field);
            $md5 = form_checkbox("md5[$field]", $field);
            $strip = form_checkbox("strip_tags[$field]", $field);
            $ci->table->add_row($field, $label, $trim, $req, $min, $max, $email, $match, 
                $uniq, $enc, $md5, $strip);
            $no++;
        }
        echo form_open();
        echo $ci->table->generate();
        echo '<br />', form_submit('submit', 'Generate Rules');
        echo form_close();
        if ($ci->input->post('submit')) {            
            echo '<pre style="padding:10px 0;overflow:auto;border:solid 1px #333;background:#F9F9F9">';
            echo "  '{$table}' => array(";
            foreach ($fields as $field) {                
                $rules = '';
                $labels = $ci->input->post('label');
                if ($labels[$field] == '') continue;
                $trim = $ci->input->post('trim');
                $req = $ci->input->post('required');
                $min = $ci->input->post('min_length');
                $max = $ci->input->post('max_length');
                $email = $ci->input->post('valid_email');
                $match = $ci->input->post('match');
                $uniq = $ci->input->post('unique');
                $enc = $ci->input->post('encrypt');
                $md5 = $ci->input->post('md5');
                $strip = $ci->input->post('strip_tags');
                echo "\n        array("; 
                echo "\n            'field' => '{$field}',";
                echo "\n            'label' => '{$labels[$field]}',";
                echo "\n            'rules' => '";
                if (isset($trim[$field])) $rules.= 'trim|';
                if (isset($req[$field])) $rules.= 'required|';
                if ($min[$field]!='') $rules.= "min_length[$min[$field]]|";
                if ($max[$field]!='') $rules.= "max_length[$max[$field]]|";
                if (isset($email[$field])) $rules.= 'valid_email|';
                if ($match[$field]!='') $rules.= "matches[$match[$field]]|";
                if (isset($uniq[$field])) $rules.= "unique[{$table}.{$field},{$pkey}]|";
                if (isset($enc[$field])) $rules.= 'encrypt|';
                if (isset($md5[$field])) $rules.= 'md5encrypt|';
                if (isset($strip[$field])) $rules.= 'strip_tags|';
                echo rtrim($rules, '|');
                echo "'),";
            } 
            echo "\n    ),";   
            echo '</pre>';
        }
    }

}

/* End of file tools.php */
/* Location: ./application/libraries/tools.php */
