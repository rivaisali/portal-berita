<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Visitor_counter 
{
    private $ci = NULL;
    private $_count = array();
    
    public $table = 'pengunjung';
    public $cookie_name = 'vcntr';
    
    public function __construct() 
    {
       $this->ci = &get_instance();
       // $this->cookie_name = add_prefix($this->cookie_name);
       if ( $json_count = $this->ci->input->cookie($this->cookie_name) ) {
            $json_count = stripslashes($json_count);
            $this->_count = json_decode($json_count, TRUE);
       } else {
            $this->_get_count();  
       }      
    }
    
    public function count()
    { 
        $table = $this->ci->db->dbprefix($this->table);
        $today = date('Y-m-d');
        if ( !$this->ci->input->cookie(config_item('cookie_prefix').$this->cookie_name) ) {
            $added = $this->ci->db->query("INSERT INTO {$table} VALUES('{$today}', jumlah+1) 
                ON DUPLICATE KEY UPDATE jumlah = jumlah+1");
            if ($added) {
                $this->_count['d']+= 1;
                $this->_count['w']+= 1;
                $this->_count['m']+= 1;
                $this->_count['a']+= 1;
                $json_count = json_encode($this->_count);
                $this->ci->input->set_cookie($this->cookie_name, $json_count, '3600');
            }
        }
    }  
    
    private function _get_count()
    {
        $table = $this->ci->db->dbprefix($this->table);
        // Hari ini
        $date = date('Y-m-d');        
        $qw = $this->ci->db->get_where($this->table, array('tanggal'=>$date), 1);
        $this->_count['d'] = $qw->num_rows() > 0 ? $qw->row()->jumlah : 0;
        // Kemarin
        $date = date('Y-m-d', strtotime('-1 days'));
        $qw = $this->ci->db->get_where($this->table, array('tanggal'=>$date), 1);
        $this->_count['y'] = $qw->num_rows() > 0 ? $qw->row()->jumlah : 0;  
        // Minggu ini
        $qw = $this->ci->db->query("SELECT SUM(jumlah) AS jumlah FROM {$table} WHERE
             tanggal > DATE_SUB(NOW(), INTERVAL 1 WEEK)");  
        $this->_count['w'] = $qw->row()->jumlah == NULL ? 0 : $qw->row()->jumlah;    
        // Bulan ini
        $qw = $this->ci->db->query("SELECT SUM(jumlah) AS jumlah FROM {$table} WHERE 
            tanggal > DATE_SUB(NOW(), INTERVAL 1 MONTH)");  
        $this->_count['m'] = $qw->row()->jumlah == NULL ? 0 : $qw->row()->jumlah; 
        // Total
        $qw = $this->ci->db->query("SELECT SUM(jumlah) AS jumlah FROM {$table}");  
        $this->_count['a'] = $qw->row()->jumlah == NULL ? 0 : $qw->row()->jumlah;  
    }
    
    public function get_count($type = 'a')
    {
        return $this->_count[$type];
    } 
}

/* End of file Visitor_counter.php */
/* Location: ./app/libraries/Visitor_counter.php */