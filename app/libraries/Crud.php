<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crud
{
    private $CI;
    private $postdata = array();
    var $error_delimiter = array('', ', ');

    function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('form_validation');
        $this->CI->load->helper('language');
        $this->CI->load->model('mcrud');
        $this->CI->lang->load('message');
    }

    function insert($table)
    {
        $error = '';
        $data = array('ok'=>FALSE, 'type'=>'insert', 'msg'=>'', 'id'=>0, 'code'=>0);
        $err0 = $this->error_delimiter[0];
        $err1 = $this->error_delimiter[1];
        $this->CI->mcrud->before_validation($table, 'insert');
        $this->CI->form_validation->set_error_delimiters($err0, $err1);
        if ($this->CI->form_validation->run($table) == TRUE) {
            $this->CI->mcrud->after_validation($table, 'insert');
            $this->postdata = $this->getpost($table);            
            $error.= $this->CI->mcrud->before_post($table, 'insert', 0, $this->postdata);
            if ($error == '') {
                if ($this->CI->db->insert($table, $this->postdata)) {
                    $newid = $this->CI->db->insert_id();
                    $msg = $this->CI->mcrud->after_post($table, 'insert', $newid, $this->postdata);
                    $data['ok'] = TRUE;
                    if ($msg != '')
                        $data['msg'] = $msg; else
                        $data['msg'] = lang('msg_insert_ok');
                    $data['id']  = $newid;
                }
            }
        } else {
            $error.= substr(validation_errors(), 0, -3);
        } 
        if ($error != '') {
            $data['ok'] = FALSE;
            $data['msg'] = $error;
            $data['id'] = 0;
        }        
        return $data;
    }

    function update($table, $id)
    {
        $error = '';
        $data = array('ok'=>FALSE, 'type'=>'update', 'msg'=>'', 'id'=>$id, 'code'=>0);
        $err0 = $this->error_delimiter[0];
        $err1 = $this->error_delimiter[1];
        $this->CI->mcrud->before_validation($table, 'update', $id);
        $this->CI->form_validation->set_error_delimiters($err0, $err1);
        if($this->CI->form_validation->run($table) == true) {
            $this->CI->mcrud->after_validation($table, 'update', $id);
            $this->postdata = $this->getpost($table);
            $error.= $this->CI->mcrud->before_post($table, 'update', $id, $this->postdata);
            $this->CI->config->load('table_pkey');
            $pkey = $this->CI->config->item('pkey');
            if (empty($pkey[$table])) $pkey[$table] = 'id';
            unset($this->postdata[$pkey[$table]]);            
            if ($error == '') {
                $this->CI->db->where($pkey[$table], $id);
                if ($this->CI->db->update($table, $this->postdata)) {
                    $msg = $this->CI->mcrud->after_post($table, 'update', $id, $this->postdata);
                    $data['ok'] = $msg == '';
                    if ($msg != '')
                        $data['msg'] = $msg; else
                        $data['msg'] = lang('msg_update_ok');
                }
            }            
        } else {
            $error.= substr(validation_errors(), 0, -3);
        }
        if ($error != '') {
            $data['msg'] = $error;
        }
        return $data;
    }

    function delete($table, $id)
    {
        $data = array('ok'=>FALSE, 'type'=>'delete', 'msg'=>'', 'id'=>0, 'code'=>0);
        $this->CI->config->load('table_pkey');
        $pkey = $this->CI->config->item('pkey');
        if ( empty($pkey[$table]) ) $pkey[$table] = 'id';
        $this->CI->mcrud->before_delete($table, $id);
        if ($this->CI->db->delete($table, array($pkey[$table] => $id))) {
            $this->CI->mcrud->after_delete($table, $id);
            $data['ok'] = TRUE;
            $data['msg'] = 'ass';
            $data['id'] = $id;            
        } else {
            $data['msg'] = lang('msg_unknown_error');
        } $data['ok'] = TRUE;
        return $data;
    }
    
    function apbdes()
    {
        $data = array('type'=>'apbdes', 'id'=>0, 'ok'=>FALSE);
        $err0 = $this->error_delimiter[0];
        $err1 = $this->error_delimiter[1];
        $this->CI->form_validation->set_error_delimiters($err0, $err1);
        if ($this->CI->form_validation->run('apbdes') == TRUE) {
            $this->CI->load->helper('array');
            $this->postdata = post_to_array();
            $this->CI->config->force_save = FALSE;
            if ($_FILES['file']['error'] ===  UPLOAD_ERR_OK)  {
                $this->CI->load->helper('string');
                $filename = random_string('alnum', 15); 
                $filename = strtolower($filename); 
                $upload = $this->CI->mcrud->upload_file($filename, './uploads/apbdes/');
                if ( $upload['result'] == 'ok' ) {
                    if ( $old_file = config_item('apbdes_file') ) {
                        if ( file_exists('./uploads/apbdes/'.$old_file))
                            unlink('./uploads/apbdes/'.$old_file);
                    }
                    $this->postdata['apbdes_file'] = $upload['msg']['file_name'];
                }
            }
            $this->CI->config->update($this->postdata);
            $data['ok'] = TRUE;
            $data['msg'] = lang('msg_update_config_ok');
        } else {
            $error_msg = validation_errors();
            $data['msg'] = substr($error_msg, 0, -3);
        } 
        return $data;       
    }
    
    function updateconfig()
    {
        $data = array('type'=>'update_config', 'id'=>0, 'ok'=>FALSE);
        $err0 = $this->error_delimiter[0];
        $err1 = $this->error_delimiter[1];
        $this->CI->form_validation->set_error_delimiters($err0, $err1);
        if ($this->CI->form_validation->run('config') == TRUE) {
            $this->CI->load->helper('array');
            $this->postdata = post_to_array();
            $this->CI->config->force_save = FALSE;
            $this->CI->config->update($this->postdata);
            $data['ok'] = TRUE;
            $data['msg'] = lang('msg_update_config_ok');
        } else {
            $error_msg = validation_errors();
            $data['msg'] = substr($error_msg, 0, -3);
        } 
        return $data;       
    }
    
    function updatepassword($id)
    {        
        $data = array('ok'=>FALSE, 'type'=>'update_password', 'msg'=>'', 'id'=>0, 'code'=>0);
        $err0 = $this->error_delimiter[0];
        $err1 = $this->error_delimiter[1];
        $this->CI->form_validation->set_error_delimiters($err0, $err1);
        if ($this->CI->form_validation->run('password') == TRUE) {   
            $this->postdata['password'] = $this->CI->input->post('baru');                    
            $this->CI->db->where('user_id',$id);
            $updt = $this->CI->db->update('user', $this->postdata);
            if($updt) {
                $data['ok'] = TRUE;
                $data['msg'] = lang('msg_change_password_ok');
            }          
        } else {
           $error_msg = validation_errors();
           $data['msg'] = substr($error_msg, 0, -3);
        }   
        return $data;     
    }
    
    function resetpassword($id)
    {
        $data = array();
        $this->CI->load->library(array('encrypt','parser'));
        $this->CI->load->helper('string');
        $qw = $this->CI->db->get_where('user', array('id'=>$id));
        if($qw->num_rows()>0) {  
            $row=$qw->row();
            $newpass = random_string();
            $tpldata=array(
                'nama'=>$row->nama,
                'username'=>$row->username,
                'password'=>$newpass,
                'site_name'=>myconfig('judul'));
        $pesan=$this->CI->parser->parse('template/email_resetadminpass', $tpldata, TRUE);
        kirim_email($row->email, 'Password Baru', $pesan); 
        $newpass = $this->CI->encrypt->encode($newpass);
        $this->CI->db->where('id', $id);
        $updt = $this->CI->db->update(USER_TABLE, array('password'=>$newpass));
        if($updt) {
            $data['result'] = 'ok';
            $data['msg'] = lang('msg_reset_password_ok');
            $data['id'] = $id;
            $data['type'] = 'reset_password';
        }
        }
        return $data;
    }       
    
    private function getpost($table)
    {
        $except = array('jaringan');
        if(!$this->CI->db->table_exists($table)) {
            exit;
        }
        $postdata=array();
        $fields=$this->CI->db->list_fields($table);
        foreach($fields as $field) {
            if ( $this->CI->input->post($field) || in_array($table, $except) )
                $postdata[$field] = $this->CI->input->post($field); 
        }
        return $postdata;
    }

}

/* End of file crud.php */
/* Location: ./app/libraries/crud.php */
