<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Post 
{
    public $default_category = 'Tanpa Kategori';
    
    private $_registered_types = array();    
    private $ci = NULL;
    
    public function __construct() 
    {
        $this->ci = &get_instance();
        $this->ci->load->library(array());
        $this->ci->load->helper(array('array','bootstrap'));
    }
    
    /**
     * Jika $post_id > 0, edit
     */
    public function save_post($post_data = array(), $post_id = 0)
    {
        $ret = 0;
        $post_cats = $post_data['post_cats'];
        unset($post_data['post_cats']);
        if ( empty($post_data['post_slug']) ) 
            $post_data['post_slug'] = $post_data['post_title'];
			$post_data['post_slug']  = $this->generate_post_slug($post_data['post_slug'], $post_id);
        if ($post_id == 0) {
            if ( $this->ci->db->insert('post', $post_data) ) {
                $ret = $this->ci->db->insert_id();
                $post_id = $ret;
            }
        } else { 
            $this->ci->db->where('post_id', $post_id);
            if ( $this->ci->db->update('post', $post_data) )
                $ret = $post_id;
        }        
        $this->assign_term($post_id, $post_cats);   
        return array('id'=>$ret, 'slug'=>$post_data['post_slug']);
    }
    
    public function delete_post($post_ids = 0, $permanent = FALSE)
    {        
        if ( !is_array($post_ids) ) $post_ids = array($post_ids);
        $cek = $this->ci->db->where_in('post_id', $post_ids)->select('tax_id')->get_where('term_rel');
        if ($cek->num_rows() > 0) {
            $old_tax_ids = array();
            foreach ($cek->result() as $r) {
                $old_tax_ids[] = $r->tax_id;
            }
            $this->ci->db->where_in('tax_id', $old_tax_ids)->set('count', 'count - 1', FALSE)->update('term_tax');
        }   
        $this->ci->db->where_in('post_id', $post_ids)->delete('term_rel');       
        if ($permanent) 
            return $this->ci->db->where_in('post_id', $post_ids)->delete('post'); else 
            return $this->ci->db->where_in('post_id', $post_ids)->update('post', array('trash'=>1)); 
    }
    
    public function restore_post($post_ids = 0)
    {
        if ( !is_array($post_ids) ) $post_ids = array($post_ids);
        return $this->ci->db->where_in('post_id', $post_ids)->update('post', array('trash'=>0));
    }
    
    public function set_post_status($post_ids = 0, $post_status = 'publish')
    {
        if ( !is_array($post_ids) ) $post_ids = array($post_ids);
        return $this->ci->db->where_in('post_id', $post_ids)->update('post', array('post_status'=>$post_status));
    }
    
    /**
     * Set term post
     * @param int $post_id ID post
     * @param int/array $tax_ids ID term tax. untuk un-assign, set NULL/array()
     * @return bool
     */
    public function assign_term($post_id = 0, $tax_ids = array())
    {        
        if ( !is_array($tax_ids) ) {
            if (empty($tax_ids)) 
                $tax_ids = array(); else
                $tax_ids = array($tax_ids);
        }
        $cek = $this->ci->db->select('tax_id')->get_where('term_rel', array('post_id'=>$post_id));
        if ($cek->num_rows() > 0) {
            $old_tax_ids = array();
            foreach ($cek->result() as $r) {
                $old_tax_ids[] = $r->tax_id;
            }
            $this->ci->db->where_in('tax_id', $old_tax_ids)->set('count', 'count - 1', FALSE)->update('term_tax');
        }
        $this->ci->db->delete('term_rel', array('post_id'=>$post_id));
        if ( !empty($tax_ids) ) {
            $pdata = array();
            foreach($tax_ids as $tax_id) {
                $pdata[] = array('post_id'=>$post_id, 'tax_id'=>$tax_id);
            }        
            if ($this->ci->db->insert_batch('term_rel', $pdata)) {
                $this->ci->db->where_in('tax_id', $tax_ids)->set('count', 'count + 1', FALSE)->update('term_tax');    
            } 
        }
    }
    
    public function get_result($post_type = 'news', $params = NULL)
    {
        $params = $this->_merge_params(
            array('trash'       => 0, // 1 untuk yg sudah terhapus
                  'status'      => 'publish', // [all|publish|draft]
                  'date'        => NULL, // d=hari ini, w=minggu ini, m=bulan ini 
                                         // 'Y-m-d' atau array('Y-m-d','Y-m-d') untuk range
                  'cat_id'      => 0, // kategori berdasar id
                  'cat_slug'    => '', // kategori berdasar slug
                  'page'        => 1,
                  'perpage'     => 0,
                  'search'      => '',
                  'deep_search' => FALSE, // pencarian sampai ke post_content
                  'has_thumb'   => 0, // 1 jika hanya yg ada post_thumb
                  'order_by'    => 'post_date DESC'), $params);
        extract($params);  
        $where = array("trash={$trash}", "post_type='{$post_type}'");
        if ($status != 'all' && $trash == 0)
            $where[] = "post_status = '{$status}'";
        if ($date != NULL) {
            switch ($date) {
                case 'd':
                    $date = date('Y-m-d'); 
                    $where[] = "(DATE(post_date) = '{$date}')";
                break;
                case 'w':
                    $where[] = "(post_date > DATE_SUB(NOW(), INTERVAL 1 WEEK))";   
                break;
                case 'm':
                    $where[] = "(post_date > DATE_SUB(NOW(), INTERVAL 1 MONTH))";   
                break;
                default:
                    if (is_array($date)) {
                        $where[] = "(post_date BETWEEN '{$date[0]}' AND '{$date[1]}')"; 
                    } else {
                        $where[] = "(post_date = '{$date}')";
                    }
            }
        }        
        if ($cat_id > 0) {
            $cat_id = $this->ci->db->escape_like_str($cat_id);
            $where[] = "(post_categories LIKE '{$cat_id};%' OR post_categories LIKE '%|{$cat_id};%')";
        }
        if ($cat_slug != '') {
            $cat_slug = $this->ci->db->escape_like_str($cat_slug);
            $where[] = "(post_categories LIKE '%;{$cat_slug};%')";
        }
        if ($has_thumb > 0)
            $where[] = "post_thumb IS NOT NULL";     
        if ($search != '') {
            $search = $this->ci->db->escape_like_str($search);
            $qlike = "post_title LIKE '%{$search}%'";
            if ($deep_search)
                $qlike.= " OR post_content LIKE '%{$search}%'";
            $where[] = "({$qlike})";
        }
        $ret = array('page'=>$page, 'rows'=>array(), 'next_page'=>0, 'prev_page'=>0);
        $where_str = implode(" AND ", $where);
        $offset = $perpage * ($page-1); 
        $ret['data_from'] = $offset + 1; 
        $ret['data_to'] = $ret['data_from'] + $perpage - 1;
        $ret['data_total'] = $this->ci->db->where($where_str, NULL, FALSE)->count_all_results('vpost');        
        if ($ret['data_to'] > $ret['data_total'])
            $ret['data_to'] = $ret['data_total'];
        $ret['page_total'] = $perpage > 0 ? ceil($ret['data_total'] / $perpage) : 1;  
        if ($perpage > 0) {
            $this->ci->db->limit($perpage, $offset);
            $ret['prev_page'] = $page > 1 ? $page-1 : 0;
            $ret['next_page'] = $page < $ret['page_total'] ? $page+1 : 0;
        }
        $qw = $this->ci->db->where($where_str, NULL, FALSE)->order_by($order_by)->get('vpost');
        if ($qw->num_rows() > 0) {
            $ret['rows'] = $this->_generate_post_row($qw->result_array()); 
        }       
        return $ret;
    }
    
    /**
     * $post_id bisa id atau slug
     */
    public function get_row($post_id = 0, $params = NULL)
    {
        $params = $this->_merge_params(
            array('trash'       => 0, // 1 untuk yg sudah terhapus
                  'status'      => 'publish', // [all|publish|draft]
                  'type'        => '',
                  'has_thumb'   => 0), $params);
        extract($params);   
        $this->ci->db->where('trash', $trash);
        if ($status != 'all')
            $this->ci->db->where('post_status', $status);
        if ($type != '')
            $this->ci->db->where('post_type', $type);
        if ($has_thumb > 0)
            $this->ci->db->where('post_thumb IS NOT ', 'NULL', FALSE);     
        if (is_numeric($post_id))    
            $this->ci->db->where('post_id', $post_id); else
            $this->ci->db->where('post_slug', $post_id);
        $qw = $this->ci->db->get('vpost', 1);
        if ($qw->num_rows() == 0) return FALSE;
        return $this->_generate_post_row($qw->row_array());
    }
    
    public function update_term($tax_id = 0, $term_name = '', $parent = 0, $description = NULL)
    {
        if ($term_id = $this->_get_term_id($term_name)) {
            if ($tax_id == $parent) $parent = 0;
            $data = compact('term_id','parent');
            if ( !is_null($description) ) 
                $data['description'] = $description;
            return $this->ci->db->update('term_tax', $data, array('tax_id'=>$tax_id));
        }
        return FALSE;
    }
    
    public function add_term($tax_name = '', $term_name = '', $parent = '0', 
        $description = '', $post_type = 'news')
    {
        $tax_id = FALSE;
        $term_name = trim($term_name);
        if (empty($term_name) || empty($tax_name) || empty($post_type)) return FALSE;    
        if ($term_id = $this->_get_term_id($term_name)) {
            $where = compact('tax_name', 'post_type', 'term_id');
            $qw = $this->ci->db->select('tax_id')->get_where('term_tax', $where);
            if ($qw->num_rows() > 0) {
                $tax_id = $qw->row()->tax_id; 
            } else {
                $this->ci->db->insert('term_tax', compact('term_id','tax_name', 'post_type', 'description', 'parent'));
                $tax_id = $this->ci->db->insert_id();
            }
        }
        return $tax_id;
    }
    
    public function delete_term($tax_id = 0)
    {
        if ($tax_id > 0) {
            $get = $this->ci->db->select('term_id')->get_where('term_tax', array('tax_id'=>$tax_id), 1);
            if ($get->num_rows() > 0) {
                $term_id = $get->row()->term_id; 
                $this->ci->db->where('term_id', $term_id);
                $used_count = $this->ci->db->count_all_results('term_tax'); 
                if ($used_count <= 1) {
                    $this->ci->db->delete('term', array('term_id'=>$term_id));
                }
            } 
            $del1 = $this->ci->db->delete('term_tax', array('tax_id'=>$tax_id));
            $del2 = $this->ci->db->delete('term_rel', array('tax_id'=>$tax_id));
            $del3 = $this->ci->db->update('term_tax', array('parent'=>0), array('parent'=>$tax_id));
            return ($del1 && $del2 && $del3);
        }
        return FALSE;
    }
    
    /**
     * isi $exclude_id saat edit
     */
    public function generate_post_slug($title = '', $exclude_id = 0)
    {
        $slug = url_title($title, '-', TRUE);
        $i = 1;
        do {
            $new_slug = $i > 1 ? "$slug-$i" : $slug;
            $this->ci->db->where('post_slug', $new_slug);   
            if ($exclude_id > 0) 
                $this->ci->db->where('post_id !=', $exclude_id);
            $count =  $this->ci->db->count_all_results('post');
            $i++;
        } while ($count > 0);
        return $new_slug;
    }
    
    public function register_type($name = '', $label = '', $params = NULL)
    {
        $options = $this->_merge_params(
            array('name'          => $name,
                  'label'         => $label,
                  'cat_support'   => TRUE,
                  'tag_support'   => TRUE,
                  'cat_label'     => 'Kategori',
                  'tag_label'     => 'Tags',
                  'thumb_support' => TRUE,
                  'has_archive'   => TRUE,
                  'wyswyg_mode'   => 'full',
                  'add_new_label' => "Tulis {$label} Baru",
                  'edit_label'    => "Edit {$label}",
                  'icon'          => 'fa-bullhorn'), $params);
        $this->_registered_types[$name] = $options;
    }
    
    /**
     * Semua post_type yang terdaftar
     * jika $type diisi, return options dari type dimaksud
     */
    public function get_registered_types($type = NULL)
    {
        $types = $this->_registered_types;
        if ($type == NULL) 
            return $types; else 
            return isset($types[$type]) ? $types[$type] : FALSE;        
    }
    
    public function generate_post_menu($type, $base_uri = 'admin', $submenu_icon = '', $add_new_menu = TRUE)
    {
        $types = $this->get_registered_types($type);
        $menus = array(
            'url'   => "{$base_uri}/post?t={$type}",
            'label' => $types['label'],
            'icon'  => $types['icon'],
            'sub'   => array(
                array(
                    'url'   => "{$base_uri}/post?t={$type}",
                    'label' => 'Lihat Daftar',
                    'icon'  => $submenu_icon
                )                
            ));
            if ($add_new_menu) {
                $menus['sub'][] = array(
                    'url'   => "{$base_uri}/post?t={$type}&a=add",
                    'label' => 'Buat Baru',
                    'icon'  => $submenu_icon);
            }
            if ($types['cat_support']) {
                $menus['sub'][] = array(
                    'url'   => "{$base_uri}/post?t={$type}&a=category",
                    'label' => $types['cat_label'],
                    'icon'  => $submenu_icon);
            }
            if ($types['tag_support']) {
                $menus['sub'][] = array(
                    'url'   => "{$base_uri}/post?t={$type}&a=tag",
                    'label' => $types['tag_label'],
                    'icon'  => $submenu_icon);
            }
        return $menus;
    }
    
    public function terms_checkbox($params = NULL)
    {        
        $params = $this->_merge_params(
            array('tax_name'    => 'category',
                  'post_type'   => 'news',
                  'name'        => 'post-cats[]',
                  'class'       => '',
                  'checkeds'    => array(),
                  'order_by'    => 'term_name',
                  'wrapper'     => array('<div class="checkbox">','</div>'),
                  'attr'        => array()), $params);
        extract($params); 
        $attr  = $this->_merge_params($attr, array('name'=>$name));
        if ($class != '') $attr['class'] = $class;
        $attr  = generate_attr($attr);
        if ( !is_array($checkeds) )
            $checkeds = explode(',', $checkeds);
        $terms = $this->get_terms($tax_name, $post_type, $order_by);
        $data  = array('checkbox_items' => array());
        $ret = '';
        foreach ($terms as $tax_id => $vals) {
            $ret.= isset($wrapper[0]) ? $wrapper[0] : '';
            for ($i=1; $i<$vals['level']; $i++)
                $ret.= '&nbsp;&nbsp;&nbsp;&nbsp;';
            $ret.= '<label><input type="checkbox" value="'.$tax_id.'"'.$attr;
            if (in_array($tax_id, $checkeds))
                $ret.= ' checked';
            $ret.= '>&nbsp;<span>'.$vals['name'].'</span></label>';  
            $ret.= isset($wrapper[1]) ? $wrapper[1] : '';   
        }        
        return $ret;
    }
    
    public function terms_dropdown($params = NULL)
    {
        $params = $this->_merge_params(
            array('tax_name'    => 'category',
                  'post_type'   => 'news',
                  'first_label' => '', 
                  'name'        => 'post-cat',
                  'id'          => 'post-cat',
                  'class'       => '',
                  'value_field' => 'id',
                  'selected'    => '0',
                  'order_by'    => 'term_name',
                  'opts_only'   => FALSE,
                  'exclude_def' => FALSE, 
                  'attr'        => array()), $params); 
        extract($params);    
        $attr  = $this->_merge_params($attr, array('name'=>$name, 'id'=>$id));
        if ($class != '') $attr['class'] = $class;
        $attr  = generate_attr($attr);
        $terms = $this->get_terms($tax_name, $post_type, $order_by);
        $ret = '';
        if (!$opts_only)
            $ret.= '<select'.$attr.'>';
        $ret.= '<option value="0">'.$first_label.'</option>';
        foreach ($terms as $tax_id => $vals) {
            if ($exclude_def && ($vals['name'] == $this->default_category)) continue;
            $ret.= '<option value="'.$vals[$value_field].'"';
            if ($selected == $vals[$value_field])
                $ret.= ' selected';
            $ret.= '>';
            for ($i=1; $i<$vals['level']; $i++)
                $ret.= '&nbsp;&nbsp;&nbsp;';
            $ret.= $vals['name'].'</option>';
        }
        if (!$opts_only) $ret.= '</select>';
        return $ret;
    }    
          
    public function get_terms($tax_name = 'category', $post_type = 'news', $order_by = 'term_name') 
    {
        $arr = array();
        $this->ci->db->select('tax.tax_id, tax.term_id, tr.term_slug, tr.term_name, 
            tax.description, tax.parent, tax.count')
        ->from('term_tax tax')
        ->join('term tr', 'tax.term_id=tr.term_id')
        ->where('tax.tax_name', $tax_name)
        ->where('tax.post_type', $post_type)
        ->order_by($order_by);         
        $qw = $this->ci->db->get();
        if ($qw->num_rows() == 0) {
            $this->add_term($tax_name, $this->default_category, 0, '', $post_type);
            return $this->get_terms($tax_name, $post_type, $order_by);
        } 
        foreach($qw->result() as $row) {
            $arr[$row->tax_id] = array(
                'id'         => $row->tax_id,
                'name'       => $row->term_name, 
                'parent'     => $row->parent, 
                'slug'       => $row->term_slug,
                'description'=> $row->description,
                'count'      => $row->count);
        }
        $arr = $this->_sort_by_parents($arr);
        return $arr;
    }
    
    private function _sort_by_parents($items, $parents = array(), $level = 1) 
    {
        $results  = $parents;
        $childs = array();
        $def_idx = -1; $def_slug = ''; 
        $def_count = ''; $def_desc = '';
        foreach ($items as $key => $vals) {
            if ($vals['name'] == $this->default_category) {
                $def_idx = $key;
                $def_slug = $vals['slug'];
                $def_count = $vals['count'];
                $def_desc = $vals['description'];
                continue;
            }
            $vals['level'] = $level;
            if ($vals['parent'] > 0 && array_key_exists($vals['parent'], $parents))
               $results = insert_after_array_key($vals['parent'], $results, array($key=>$vals)); 
            else {
                if ($vals['parent'] == 0)
                    $results[$key] = $vals; else
                    $childs[$key] = $vals;  
            }
        }
        if (!empty($childs)) {
            $level++;
            $childs = array_reverse($childs, TRUE); 
            $results = $this->_sort_by_parents($childs, $results, $level);       
        }     
        // set default kategori di terakhir   
        if ($def_idx >= 0) {
            unset($results[$def_idx]);
            $results[$def_idx] = array('id'=>$def_idx, 'name'=>$this->default_category, 'count'=>$def_count,
                'slug'=>$def_slug, 'description'=>'', 'parent'=>0, 'level'=>1);
        }  
        return $results;
    }
    
    private function _generate_post_row($rows = array())
    {
        if ( isset($rows['post_id']) ) {
            $is_single = TRUE;
            $rows = array($rows);
        }
        foreach ($rows as &$row) {
            $row['category_ids'] = array();
            $row['category_slugs'] = array();
            $row['category_names'] = array();
            if (!empty($row['post_categories'])) {
                $items = explode('|', $row['post_categories']);
                foreach($items as $item) {
                    list($id, $slug, $name) = explode(';', $item);
                    $row['category_ids'][] = $id;
                    $row['category_slugs'][] = $slug;
                    $row['category_names'][] = $name;
                }
            }
            $row['categories'] = implode(', ', $row['category_names']);
            unset($row['post_categories']);    
        }    
        return isset($is_single) ? $rows[0] : $rows;
    }
    
    private function _get_term_id($term_name = '')
    {
        $term_id = FALSE;
        $qw = $this->ci->db->select('term_id')->get_where('term', array('term_name'=>$term_name));
        if ($qw->num_rows() > 0) 
            $term_id = $qw->row()->term_id; 
        else { 
            $term_slug = $this->generate_post_slug($term_name);
            $this->ci->db->insert('term', compact('term_name','term_slug'));
            $term_id = $this->ci->db->insert_id();
        }
        return $term_id;
    }
        
    private function _merge_params()
    {
        $args = func_get_args();
        $arr_to_merge = array();
        foreach ($args as $arg) {
            if ($arg == NULL || $arg == '') continue;
            if ( !is_array($arg) ) 
                parse_str($arg, $arg);
            $arr_to_merge[] = $arg;            
        }
        return call_user_func_array('array_merge', $arr_to_merge);
    }
      
}

/* End of file Post.php */
/* Location: ./app/libraries/Post.php */