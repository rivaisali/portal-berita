<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DS_Form_validation extends CI_Form_validation
{
    
    public function __construct($config)
    {
        $_POST = (count($_FILES) > 0) ? array_merge($_POST, $_FILES) : $_POST;
        parent::__construct($config);
    } 
    
    public function valid_time($value)
    {
        $pattern1 = '/^(0?\d|1\d|2[0-3]):[0-5]\d$/';
        $pattern2 = '/^(0?\d|1[0-2]):[0-5]\d\s(am|pm)$/i';
        return preg_match($pattern1, $value) || preg_match($pattern2, $value);
    }
    
    /**
     * harus ada file yang diupload
     * @return boolean
     */
    public function file_required($str)
    {
        return ($str['error'] == UPLOAD_ERR_NO_FILE) ? FALSE : TRUE;
    }

    /**
     * file yang boleh di upload
     * @param string $types extensi file, batasi dengan "," ex: jpg,gif,png
     * @return boolean
     */
    public function allowed_types($str, $types)
    {
        $types=strtolower($types);
        $type = explode(',', $types);
        $filetype = pathinfo($str['name'], PATHINFO_EXTENSION);
        $filetype=strtolower($filetype);
        if($str['error'] == UPLOAD_ERR_NO_FILE) 
            return TRUE; else 
            return (in_array($filetype, $type)) ? TRUE : FALSE;
    }
          
    /**
     * batas ukuran file maksimal yang diupload
     * @param int $size ukuran file dalam KB
     * @return boolean
     */
    public function max_file_size($str, $size)
    {
        $size = $size*1024;
        return ($str['size'] <= $size) ? TRUE : FALSE;
    }
    
    /**
     * validasi captcha, sesuaikan dengan captcha helper
     * @param string $sesname 
     * @return boolean
     */
    public function captcha($str, $sesname='CAPTCHA')
    {
        $code = get_userdata($sesname);
        if($str == $code) 
            return TRUE;  else          
            return FALSE;
    }
    
    /**
     * sama dengan $this->encrypt->encode()
     * @return string
     */
    public function encrypt($str)
    {
        $CI = &get_instance();
        $CI->load->library('encrypt');
        return $CI->encrypt->encode($str);
    }
    
    function valid_date($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    
    /**
     * sama dengan $this->encrypt->md5()
     * @return string
     */
    public function md5encrypt($str)
    {
        $CI = &get_instance();
        $CI->load->library('encrypt');
        return $CI->encrypt->md5($str);
    }
    
    /**
     * Cocok bila field satunya tidak kosong
     * @param string $other field yang satunya 
     * @return boolean
     */
    public function matches_if_filled($str, $other)
    {
        $CI = &get_instance();
        $other = $CI->input->post($other);
        if($other != '' && $str == $other) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /**
     * menghilangkan substring
     * @param string $substring yang ingin dihapus
     * @return string
     */
    public function remove_string($str, $substring)
    {
        $str = str_replace($substring, '', $str);
        return $str;
    }
    
    /**
     * format tanggal dari d/m/Y ke Y-m-d H:i:s
     * @return string
     */
    public function ymdhis($str)
    {
        $pattern="/^(\d{2})\/(\d{2})\/(\d{4})$/";
        $str=preg_replace($pattern, "$3-$2-$1 ".date('H:i:s'), $str);
        return $str;
    }
    
    /**
     * merubah "," ke "."
     * @return string
     */
    public function comma_to_dot($str)
    {
        $str=str_replace(',','.',$str);
        return $str;
    }
    
    /**
     * merubah "." ke ","
     * @return string
     */
    public function dot_to_comma($str)
    {
        $str = str_replace('.',',',$str);
        return $str;
    }

    /**
     * nilai unik field pada tabel
     * @param string $params tabel.field,pkey - ex: unique[kecamatan.nama,id_kec]
     * @return boolean
     */
    public function unique($value, $params)
    {
        $CI = &get_instance();
        list($attr, $pkey) = explode(",", $params, 2);             
        list($table, $field) = explode(".", $attr, 2);   
        $keyval = $CI->input->post($pkey); 
        if (!$keyval || $keyval=='') {
            $keyval = $CI->uri->segment($CI->uri->total_segments());
        }
        $query = $CI->db->query("SELECT $field FROM ".db_prefix($table)." WHERE $pkey <> '$keyval' AND $field = '$value' LIMIT 1");      
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    /**
     * memanggil fungsi lain 
     * @param string $custom nama_fungsi,pesan_error(optional) - ex: call[cekid,%id harus 3]
     * @return tergantung dari fungsi
     */
    public function call($str, $custom_function)
    {
        $exp = explode(',', $custom_function, 2);
        $count = count($exp);
        if ($count > 1) {
            $custom_function = $exp[0];
            $custom_message = '';
            for ($i=1; $i<$count-1; $i++) {
                $custom_message.= ', '.$exp[$i];
            }            
            $this->set_message('call', $custom_message);
        }
        return call_user_func($custom_function, $str);
    }
    
}

/* End of file DS_Form_validation.php */
/* Location: ./application/libraries/DS_Form_validation.php */
