<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User 
{    
    public $table = 'user';
    public $id_length = 10;
    public $remember_expire = '1209600';  // 2 minggu  
    public $remember_cookie = 'usrmb';   
    public $multiple_login = FALSE;  // perbolehkan beberapa tipe login sekaligus
        
    private $ci;    
    private $_types = array();
    private $_userdata = 'logged_in_user';
    
    function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->helper(array('language','session','global','cookie'));
        $this->ci->lang->load('message'); 
        $this->remember_cookie = add_prefix($this->remember_cookie);
        $this->_get_remember_user();
    }
    
    /**
     * Proses login
     * @param string $user_type 
     * @return array ok=>FALSE, action=>login, type=>user_type, msg=>'', 'd=>0, code=>0) 
     */
    public function login($user_type = '') 
    {
        $error = ''; 
        $result = array(
            'ok'  => FALSE, 'action' => 'login', 'type' => $user_type,
            'msg' => '',    'id'     => 0,       'code' => 0 );
        $username = post_value('username');
        $password = post_value('password');
        $md5password = md5encrypt($password);
        $type_attr = $this->get_type_attr($user_type);
        if ( empty($username) || empty($password) ) {
            $error = lang('msg_logindata_empty');
        } else {
            $this->ci->db->where('password', $md5password); 
            $this->ci->db->where('username', $username); 
            if ($type_attr) 
                $this->ci->db->where_in('level', $type_attr['level']);
            $query = $this->ci->db->get($this->table, 1);
            if ($query->num_rows == 0) {
                $error = lang('msg_wrong_login');
            } else {
                $row = $query->row();
                if ($user_type == '')
                    $user_type = $this->get_type_by_level($row->level);
                $this->_set_user_login($user_type, $row->user_id);
                $result['code'] = $row->level;
                $result['type'] = $user_type;
                if ($this->ci->input->post('remember')) {
                    $data = encrypt($user_type.' '.$row->user_id);                  
                    $this->ci->input->set_cookie($this->remember_cookie, $data, $this->remember_expire);
                }                    
                $this->ci->db->where('user_id', $row->user_id);
                $this->ci->db->update( $this->table, array('last_login' => date('Y-m-d H:i:s')) );   
            }
        }        
        if ($error == '') {
            $result['ok'] = TRUE;
            $result['msg'] = lang('msg_login_ok');            
            $result['id'] = $row->user_id;
        } else {
            $result['msg'] = $error;
        }
        return $result;
    } 
    
    /**
     * Logout user
     * @param string $user_type 
     * @return void
     */
    public function logout($user_type = NULL) 
    {
        if ($user_type == NULL) {
            unset_userdata($this->_userdata);
        } else {
            $this->_set_user_login($user_type);
        }        
        $this->ci->input->set_cookie($this->remember_cookie);
    }
    
    public function register( $username = '', $password = '', $level = 2, $extra_fields = array() )
    {
        $result = array('ok'=>FALSE, 'action'=>'user_register', 'msg'=>'', 'id'=>0, 'code'=>0);
        $table = $this->ci->db->dbprefix . $this->table;
        $password = md5encrypt($password);
        $data = compact('username', 'password', 'level');
        $data+= $extra_fields;
        $this->ci->db->db_debug = FALSE;
        if ( !$this->ci->db->insert($table, $data) ) {
            $result['msg'] = $this->ci->db->_error_message();
            $result['code'] = $this->ci->db->_error_number();
        } else {
            $result['id'] = $this->ci->db->insert_id();
            $result['ok'] = TRUE;
        }    
        return $result;    
    }
    
    public function update( $user_id = 0, $data = array() )
    {
        $result = array('ok'=>FALSE, 'action'=>'user_update', 'msg'=>'', 'id'=>$user_id, 'code'=>0);
        $data['updated'] = date('Y-m-d H:i:s');
        $this->ci->db->db_debug = FALSE;
        if ( !$this->ci->db->update($this->table, $data) ) {
            $result['msg'] = $this->ci->db->_error_message();
            $result['code'] = $this->ci->db->_error_number();
        } else {
            $result['ok'] = TRUE;
        }
        return $result;  
    }
    
    /**
     * Mengambil data user dalam bentuk array berdasarkan ID
     * @param string userdata name default 'user'
     * @param string/array $join_table nama tabel dengan 'user_id' sebagai acuan
     * @return array
     */
    public function get_user_login($user_type = '', $join_table = NULL) 
    {
        $udata = get_userdata($this->_userdata, array());
        if ( !isset($udata[$user_type]) ) return FALSE;
        $this->ci->db->where('user_id', $udata[$user_type]['id']);        
        if ($join_table != NULL && $join_table != '') {
            if ( !is_array($join_table) ) $join_table = array($join_table);
            foreach ($join_table as $table) {
                $this->ci->db->join($table, "{$table}.user_id={$this->table}.user_id");
            }
        }
        $query = $this->ci->db->get($this->table, 1);
        if ($query->num_rows() == 0) return FALSE;
        $row = $query->row_array(); 
        unset($row['password']);
        return $row;
    }
    
    public function is_logged_in($user_type = NULL)
    {        
        if ($user_type == NULL) {
            $udata = get_userdata($this->_userdata, array());
            return !empty($udata);
        } else {
            $udata = get_userdata($this->_userdata);            
            return isset( $udata[$user_type] );
        }        
    }
    
    /**
     * Mendaftarkan tipe user = uri
     * @param string $name tipe user
     * @param string/array $level level dari tipe
     * @param string $theme tema yang digunakan
     * @param array $login_data return login data if assigned and logged in
     */
    public function register_type($name = '', $level = array(), $theme = '', 
        $resource = NULL, $join_table = NULL, &$login_data = FALSE)
    {
        if ( !is_array($level) ) $level = array($level);
        $this->_types[] = compact('name', 'level', 'theme');
        if ( !empty($resource) ) {
            foreach ($resource as $res => $items) {
                $this->ci->load->$res($items);
            }
        }        
        if ( $login_data !== FALSE ) 
            $login_data = $this->get_user_login($name, $join_table);
    }
    
    /**
     * Mendapatkan attribut dari tipe berdasar nama (level, theme)
     * @param string $name tipe user
     * @return string(theme) or array(level) or false
     */
    public function get_type_attr($name = '')
    {
        foreach ($this->_types as $row) {              
            if ( $row['name'] == $name ) {
                return $row;
                break;
            }
        }
        return FALSE;
    } 
    
    /**
     * Mendapatkan tipe user berdasar level
     * @param string $level level user
     * @return string(tipe user) or false
     */
    public function get_type_by_level($level = 0)
    {
        foreach ($this->_types as $row) {              
            if ( in_array($level, $row['level']) ) {
                return $row['name'];
                break;
            }
        }
        return FALSE;
    } 
    
    /**
     * Mendapatkan list tipe user
     * @param string $item (name|level|theme)
     * @return array
     */
    public function type_list($item = NULL)
    {
        if ( $item != NULL ) {
            $ret = array();
            foreach ($this->_types as $row) {              
                $ret[] = $row[$item];
            }
            return $ret;
        } else
            return $this->_types;
    }
    
    public function create_table( $extra_fields = array() ) 
    {        
        $table = $this->ci->db->dbprefix . $this->table;
        if ( $this->ci->db->table_exists($table) ) return FALSE;
        $dbcollat = $this->ci->db->dbcollat;
        switch (TRUE) {
            case ($this->id_length < 3) :
                $field_id_type = 'TINYINT';
            break;
            case ($this->id_length < 11) :
                $field_id_type = 'INT';
            break;
            default:
                $field_id_type = 'MEDIUMINT';
        }   
        $unique = array('username');     
        $sql = "CREATE TABLE IF NOT EXISTS `{$table}` (
            `user_id` {$field_id_type}({$this->id_length}) UNSIGNED NOT NULL AUTO_INCREMENT,
            `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,            
        	`last_login` DATETIME NULL,
        	`username` VARCHAR(50) NOT NULL,
        	`password` VARCHAR(50) NOT NULL,
        	`level` TINYINT(1) NOT NULL DEFAULT 2,";
        foreach ($extra_fields as $field) {
            if ( !empty($field['unique']) && $field['unique'] == TRUE ) {
               $unique[]= $field['name']; 
            }
            $sql.= "`{$field['name']}` {$field['attr']},";
        }
        $sql.= "PRIMARY KEY (`user_id`)";
        foreach ($unique as $item) {
            $sql.= ", UNIQUE INDEX `{$item}` ({$item})";
        }        
        $sql.= ") COLLATE='{$dbcollat}' ENGINE=InnoDB";
        return $this->ci->db->simple_query($sql);
    }
    
    private function _get_remember_user() 
    {
        $cookie_name = config_item('cookie_prefix').$this->remember_cookie;
        if ( $cookie_data = $this->ci->input->cookie($cookie_name) ) {
            $cookie_data = decrypt($cookie_data);
            list ($user_type, $user_id) = explode(' ', $cookie_data);
            if ( !$this->is_logged_in($user_type) ) {  
                $this->_set_user_login($user_type, $user_id);                             
            }            
        }
    }    
    
    private function _set_user_login($user_type, $id = NULL, $time = NULL)
    {
        if ($time == NULL) $time = time();
        $udata = get_userdata($this->_userdata, array());
        if ($id == NULL) 
            unset ( $udata[$user_type] ); else
            $udata[$user_type] = array('id' => $id, 'time' => $time); 
        set_userdata($this->_userdata, $udata);
    }
    
}

/* End of file user.php */
/* Location: ./app/libraries/user.php */