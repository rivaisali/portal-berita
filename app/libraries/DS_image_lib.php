<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DS_Image_lib extends CI_Image_lib
{
    var $ci = NULL;
    var $img_lib = 'gd2';
    var $img_lib_path = '';
    
    var $small_size = '50';
    var $medium_size = '150';
    var $large_size = '900';
    
    var $small_path = 'small/';
    var $medium_path = 'medium/';
    var $large_path = 'large/';
    
    public function __construct()
    {
        parent::__construct();
    } 
    
    public function resize_and_thumbnail()
    {
        
    }
    
    public function create_thumbnail($source='')
    {
        
    }
    
    public function resize_image($source='', $width='', $height='', $maintain_ratio=TRUE)
    {
        $config['image_library'] = $this->$img_lib;
        $config['source_image']	= $source;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = $maintain_ratio;
        $config['width'] = $width;
        $config['height'] = $height;
        $this->initialize($config); 
        if ($this->resize()) 
            return TRUE; else 
            return $this->display_errors('', ', ');
    }
    
    public function crop_image($source='', $width='', $height='', $maintain_ratio=TRUE)
    {
        $config['image_library'] = $this->$img_lib;
        $config['source_image']	= $source;
        $config['maintain_ratio'] = $maintain_ratio;
        $config['width'] = $width;
        $config['height'] = $height;
        $config['x_axis'] = '0';
        $config['y_axis'] = '0';
        $this->initialize($config); 
        if ($this->crop()) 
            return TRUE; else 
            return $this->display_errors('', ', ');
    }
    
    public function square_crop($source='', $size='50')
    {
        return $this->crop_image($source, $size, $size, FALSE);
    }

}

/* End of file DS_Image_lib.php */
/* Location: ./app/libraries/DS_Image_lib.php */
