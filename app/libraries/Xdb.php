<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Xdb 
{
    private $ci = NULL;
    
    public function __construct()
    {
         $this->ci = &get_instance();
    }
    
    /**
     * Simpan data, jika key duplikat, abaikan
     *
     * @param string $table nama tabel
     * @param array $data array data
     * @param bool $exec eksekusi atau tampilkan sql
     */
    public function insert_ignore($table, $data, $exec = TRUE)
    {
        $sql = $this->ci->db->insert_string($table, $data);
        $sql = substr_replace($sql, 'INSERT IGNORE INTO', 0, 11);
        return $exec ? $this->ci->db->query($sql) : $sql;
    }
    
    /**
     * Simpan data, jika key duplikat, update
     *
     * @param string $table nama tabel
     * @param array $data array of array data
     * @param bool $exec eksekusi atau tampilkan sql
     */
    public function insert_update($table, $data, $exec = TRUE)
    {
        $fields = array();
        foreach ($data as $key => $val) {
            $fields[] = "`{$key}` = VALUES(`{$key}`)";
        }
        $fields = implode(', ', $fields);
        $sql = $this->ci->db->insert_string($table, $data);
        $sql.= " ON DUPLICATE KEY UPDATE {$fields}"; 
        return $exec ? $this->ci->db->query($sql) : $sql;
    }
    
    /**
     * Simpan batch data, jika key duplikat, update
     *
     * @param string $table nama tabel
     * @param array $data array of array data
     * @param bool $exec eksekusi atau tampilkan sql
     */
    public function insert_batch_update($table, $data, $exec = TRUE)
    {
        $fields = array();
        foreach ($data[0] as $key => $val) {
            $fields[] = "`{$key}` = VALUES(`{$key}`)";
        }
        $fields = implode(', ', $fields);
        $sql = $this->insert_batch_ignore($table, $data, FALSE);
        $sql = substr_replace($sql, 'INSERT INTO', 0, 18);
        $sql.= " ON DUPLICATE KEY UPDATE {$fields}"; 
        return $exec ? $this->ci->db->query($sql) : $sql;
    }
    
    /**
     * Simpan batch data, jika key duplikat, abaikan
     *
     * @param string $table nama tabel
     * @param array $data array of array data
     * @param bool $exec eksekusi atau tampilkan sql
     */
    public function insert_batch_ignore($table, $data, $exec = TRUE)
    {
        $table = $this->ci->db->dbprefix($table);
        $values = array();
        for ($i=0; $i < count($data); $i++) {
            if ($i == 0) {
                $arr_keys = array();
                foreach ($data[$i] as $key => $val) {
                    $arr_keys[] = "`{$key}`";            
                }
                $fields = "(" . implode(', ', $arr_keys) . ")";
            }
            $arr_vals = array();
            foreach ($data[$i] as $key => $val) {
                if ( !is_int($val) && !is_float($val) ) $val = "'{$val}'"; 
                $arr_vals[] = "{$val}";            
            }
            $values[] = "(" . implode(', ', $arr_vals) . ")";
        }
        $sql = "INSERT IGNORE INTO `{$table}` {$fields} VALUES " . implode(', ', $values);
        return $exec ? $this->ci->db->query($sql) : $sql;
    }
}

/* End of file Xdb.php */
/* Location: ./app/libraries/Xdb.php */