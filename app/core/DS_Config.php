<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DS_Config extends CI_Config
{     
    
    private $table = 'config';
    private $ci = NULL;  

    public function __construct()
    {
        parent::__construct();  
    }
    
    /**
     * Mengambil settings dari database tanpa memasukkannya ke runtime config
     *
     * @param array/string $keys nama config
     * @param $default default jika kosong. jika $keys array, return array, nilai default per item
     */
    public function db_items($keys, $default = FALSE)
    {
        if ( $this->item('config_use_db') == FALSE ) return FALSE;
        if ( is_null($this->ci) ) $this->ci = &get_instance();
        $this->ci->db->select('config_key, config_value');
        if ( is_array($keys) ) {            
            $this->ci->db->where_in('config_key', $keys);
            $qw = $this->ci->db->get($this->table);
            $ret = array();         
            foreach ($qw->result() as $row) {
                $ret[$row->config_key] = $this->_get_value($row->config_value);
            }     
            foreach ($keys as $key) {
                if ( !array_key_exists($key, $ret) ) {
                    $ret[$key] = $default;
                }
            }
            return $ret;      
        } else {
            $qw = $this->ci->db->get_where($this->table, array('config_key'=>$keys), 1);
            if ($qw->num_rows() == 0) {
                return $default;
            } else {
                $row = $qw->row();
                return $this->_get_value($row->config_value);
            } 
        }        
    } 
    
    /**
     * Mengambil settings dari database dan memasukkannya ke runtime config
     *
     * @param array/string key=>value
     * @return void
     */
    public function load_db_items()
    {
        $keys = func_get_args(); 
        $items = $this->db_items($keys);
        if ($items) {
            foreach ($items as $key => $value) {
                $this->set_item($key, $value);
            }
        }
    }
    
    /**
     * Register/menambahkan config db
     *
     * @param ($key, $value, $autoload) atau ($arr[key=>val], $autoload)
     * @return void
     */  
    public function register()
    {
        if ( $this->item('config_use_db' ) == FALSE) return FALSE;
        if ( is_null($this->ci) ) $this->ci = &get_instance();
        $items = func_get_arg(0);
        $pdata = array();
        if ( is_array($items) ) {
            $autoload = @func_get_arg(1) ? 'yes' : 'no';
            foreach ($items as $key => $value) {
                $pdata[] = array(
                    'config_key'    => $key,
                    'config_value'  => $this->_set_value($value),
                    'autoload'      => $autoload );
            }
        } else {
            $autoload = @func_get_arg(2) ? 'yes' : 'no';
            $pdata[] = array(
                'config_key'    => $items,
                'config_value'  => $this->_set_value( func_get_arg(1) ),
                'autoload'      => $autoload );
        }
        $this->ci->xdb->insert_batch_ignore($this->table, $pdata);
    }  
    
    /**
     * Update config db
     *
     * @param ($key, $value) atau ($arr[key=>val])
     * @return void
     */ 
    public function update()
    {
        if ( $this->item('config_use_db' ) == FALSE) return FALSE;
        if ( is_null($this->ci) ) $this->ci = &get_instance();
        $items = func_get_arg(0);
        $pdata = array();
        if ( is_array($items) ) {
            foreach ($items as $key => $value) {
                $pdata[] = array(
                    'config_key'    => $key,
                    'config_value'  => $this->_set_value($value) );
            }
        } else {
            $pdata[] = array(
                'config_key'    => $items,
                'config_value'  => $this->_set_value( func_get_arg(1) ) );
        }
        $this->ci->db->update_batch($this->table, $pdata, 'config_key');
    } 
    
    public function autoload_db_items()
    {   
        if ( $this->item('config_use_db' ) == FALSE) return FALSE;
        if ( is_null($this->ci) ) $this->ci = &get_instance();
        if ($this->ci->db->table_exists($this->table)) {
            $this->ci->db->select('config_key, config_value');
            $query = $this->ci->db->get_where( $this->table, array('autoload'=>'yes') );
            foreach ($query->result() as $row) {
                $this->set_item( $row->config_key, $this->_get_value($row->config_value) );
            }
        }
    }
    
    public function create_table()
    {
        if ( $this->item('config_use_db') == FALSE ) return FALSE;
        if ( is_null($this->ci) ) $this->ci = &get_instance();
        $dbcollat = $this->ci->db->dbcollat;
        $table = $this->ci->db->dbprefix . $this->table;
        $qstr = "CREATE TABLE IF NOT EXISTS `{$table}` (
            `config_id` INT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
            `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        	`config_key` VARCHAR(100) NOT NULL,
        	`config_value` LONGTEXT NOT NULL,
        	`autoload` VARCHAR(3) NOT NULL DEFAULT 'yes',
        	PRIMARY KEY (`config_id`),
            UNIQUE INDEX `key` (`config_key`))
            COLLATE='{$dbcollat}'
            ENGINE=InnoDB";
        $this->ci->db->simple_query($qstr);
    }
    
    private function _get_value($value)
    {
        $array = @unserialize($value);
        if ($array !== FALSE)
            return $array; else
            return $value;
    }
    
    private function _set_value($value)
    {
        if ( is_array($value) )
            $value = serialize($value);
        return $value;
    }
    
}

/* End of file DS_Config.php */
/* Location: ./app/core/DS_Config.php */