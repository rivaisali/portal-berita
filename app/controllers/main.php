<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller 
{
    private $reserved_uri = array('ajax', 'action', 'popup', 'theme_loader', 'include', 'templates');
    private $data = array();
    
    function __construct()
    {
        parent::__construct();   
        $this->data = array(
            'base_url' => base_url(),
            'jscss_version' => config_item('jscss_version')
        );   
        $this->user->register_type('admpt', array(1, 2), 'admin', 
            array('library' => array('form'),
                  'helper'  => array('post')), NULL, $this->data['admin']);  
        $this->post->register_type('post', 'Berita', 
            array('tag_support'   => FALSE,
                  'icon'          => 'fa-thumb-tack',
                  'add_new_label' => 'Buat Tulisan Baru'));     
        $this->post->register_type('pengumuman', 'Pengumuman', 
            array('tag_support'   => FALSE,
                  'cat_support'   => FALSE,
                  'thumb_support' => FALSE,
                  'icon'          => 'fa-bullhorn',    
                  'wyswyg_mode'   => 'simple',              
                  'add_new_label' => 'Tulis Pengumuman Baru'));           
        $this->post->register_type('page', 'Halaman', 
            array('tag_support'   => FALSE,
                  'cat_support'   => TRUE,
                  'thumb_support' => FALSE,
                  'icon'          => 'fa-files-o',
                  'has_archive'   => FALSE,
                  'add_new_label' => 'Buat Halaman Baru')); 
    } 
       
	public function index() 
    {
        //$this->load->library('tools'); echo '<pre>'.print_r($this->tools->generate_assets_config(array('assets/js/', 'assets/css/', 'assets/custom/')), 1).'</pre>';
        $this->route('index');
	}
    
    public function ajax($action = '', $extra = '') 
    {   
        if ( !$this->input->is_ajax_request() ) exit;
        header("Content-type: application/json");   
        $action = str_replace('-', '_', $action);
        $this->load->model('majax');
		$this->majax->$action($extra, $this->data);
	}  
    
    public function action($action = '', $extra = '') 
    {        
        if ( empty($_POST) && empty($_GET) && empty($_FILES) ) exit; 
        $action = str_replace('-', '_', $action);
        $this->load->model('maction');
		$this->maction->$action($extra, $this->data);
	} 
    
    public function popup($name = '') 
    {        
        $this->load->view('popup/' . $name, $this->data);
    } 

    public function download($name = '') 
    {        
		switch($name) {
            case 'esurat':
                if ( ! $this->user->is_logged_in()) {
                    show_error('Anda tidak mempunyai hak akses ke halaman ini');
                    exit;
                }
                $idfile = (int) get_value('file');
                $qw = $this->db->get_where('esurat_file', array('file_ID'=>$idfile), 1);
                if ($qw->num_rows() > 0) {
                    $row = $qw->row();
                    $filePath = './uploads/esurat/'.$row->filename;
                    $fileName = basename($filePath);
                    $fileSize = filesize($filePath);
                    // Output headers.
                    header("Cache-Control: private");
                    header("Content-Type: application/stream");
                    header("Content-Length: ".$fileSize);
                    header("Content-Disposition: attachment; filename=".$row->nama_asli);
                    // Output file.
                    readfile ($filePath);        
                } else {
                    show_404();
                }
                exit;
            break;
        }
	} 
    
    public function load_assets($type = '')
    {   
        if ($ids = $this->input->get('ids')) {
            $this->content->combine_assets($type, urldecode($ids)); 
        } else {
            show_404();
        }
    }
    
    public function route($uri = '', $extra = '') 
    {
        if ( in_array($uri, $this->reserved_uri) ) return;    
        page_title( config_item('site_title') ); 
        page_description( config_item('site_description') ); 
        page_imageurl(base_url().config_item('website_card_img'));
        if ( $type = $this->user->get_type_attr($uri) ) {
            $this->data['theme'] = $type['theme'];
            $this->_user($uri);
        } else {            
            $this->data['theme'] = 'main';
            $post_types = $this->post->get_registered_types();
            if (array_key_exists($uri, $post_types)) { 
                $this->data['post_type'] = $uri;
                $this->data['post_slug'] = $extra;
                $this->_load_content('post'); 
            } else {
                $view_path = $uri == 'index' ? 'index' : $this->uri->uri_string();
                $this->_load_content($view_path);
            }
            $this->visitor_counter->count();
        }        
	}  
    
    private function _user($user_type = '')
    {
        if ( $this->uri->segment(2) == 'login') {
            $this->_login($user_type);
        } else {            
            if ( !$this->user->is_logged_in($user_type) ) {
                $curr_uri = urlencode( current_uri(TRUE) );
                $redir_uri = "{$user_type}/login?redirect={$curr_uri}";
                redirect($redir_uri);
                exit;
            }
            $this->data['user_type'] = $user_type;
            $uri_string = str_replace( $user_type, '', $this->uri->uri_string() );
            if ($uri_string == '') $uri_string = 'index';
            $this->_load_content("{$user_type}/{$uri_string}");
        }        
    } 
    
    private function _login($user_type = '')
    {
        if ( $this->user->is_logged_in($user_type) ) {
            redirect($user_type);
            exit;
        }
        if ( $this->user->multiple_login == FALSE ) {
            if ( $this->user->is_logged_in() )
                redirect ( base_url() );
        }
        $custom_login = config_item('theme_path') . "login-{$user_type}";
        $this->data['theme'] = file_exists($custom_login) ? file_exists($custom_login) : 'login';
        $this->data['user_type'] = $user_type;
        //$this->data['main_content'] = $this->load->view('include/login_form', $this->data, TRUE);
        $this->load->view('theme_loader', $this->data);
    }  
    
    private function _load_content($view_path = '')
    {
        $view_path = str_replace('-', '_', $view_path);
        $view_file = APPPATH . 'views/' . $view_path . EXT; 
        if ( file_exists($view_file) ) { 
            $this->data['main_content'] = $this->load->view($view_path, $this->data, TRUE);
            $this->load->view('theme_loader', $this->data);
        } else {
            show_404();
        }
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */