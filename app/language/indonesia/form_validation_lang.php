<?php

$lang['required']			= "%s harus diisi";
$lang['isset']				= "Field %s tidak boleh kosong";
$lang['valid_email']		= "Format Email tidak valid";
$lang['valid_emails']		= "Format Email tidak valid";
$lang['valid_url']			= "Format URL tidak valid";
$lang['valid_ip']			= "Format IP tidak valid";
$lang['min_length']			= "%s minimal harus %s karakter";
$lang['max_length']			= "%s tidak boleh lebih dari %s karakter";
$lang['exact_length']		= "%s harus terdiri dari %s karakter";
$lang['alpha']				= "Hanya boleh karakter alphabetical";
$lang['alpha_numeric']		= "Hanya boleh karakter alpha-numeric";
$lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes";
$lang['numeric']			= "Hanya boleh karakter angka";
$lang['is_numeric']			= "The %s field must contain only numeric characters";
$lang['integer']			= "The %s field must contain an integer.";
$lang['regex_match']		= "The %s field is not in the correct format";
$lang['matches']			= "Konfirmasi Password tidak cocok";
$lang['captcha']			= "Kode keamanan salah";
$lang['is_natural']			= "The %s field must contain only positive numbers";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero";
$lang['allowed_types']      = "Tipe file tidak didukung";
$lang['max_file_size']      = "Ukuran file melebihi batas maksimal";
$lang['unique']             = "%s sudah terdaftar";
$lang['file_required']		= "%s belum dipilih";
$lang['valid_date']		    = "Format %s tidak valid";
$lang['valid_time']		    = "Format %s tidak valid";


/* End of file form_validation_lang.php */
/* Location: ./sys/language/english/form_validation_lang.php */