<?php

// Crud
$lang['msg_insert_ok'] = "Data berhasil ditambahkan..";
$lang['msg_update_ok'] = "Data berhasil di-update..";
$lang['msg_delete_ok'] = "Data berhasil dihapus..";
$lang['msg_change_password_ok'] = "Password berhasil diganti..";
$lang['msg_update_config_ok'] = "Konfigurasi berhasil di-update..";
$lang['msg_updateakun_ok'] = "Akun anda berhasil di-update..";
$lang['msg_old_password_empty'] = "Password lama harus diisi!!";
$lang['msg_wrong_old_password'] = "Password lama salah!!!";
$lang['msg_new_password_empty'] = "Password baru/Konfirmasi harus diisi!!";
$lang['msg_password_not_match'] = "Konfirmasi password tidak cocok!!!";
$lang['msg_min_password_char'] = "Password minimal harus %d karakter";
$lang['msg_change_password_ok'] = "Password berhasil di-update..";
$lang['msg_reset_password_ok'] = "Password berhasil di-reset..";

// Login
$lang['msg_login_ok'] = "Login berhasil..";
$lang['msg_logindata_empty'] = "Username/Password Masih Kosong!!!";
$lang['msg_user_not_registered'] = "Username tidak terdaftar!!!";
$lang['msg_wrong_login'] = "Username/Password salah!!!";
$lang['msg_logout_ok'] = "Anda berhasil logout..";

// Other
$lang['msg_must_login'] = '<p>Anda harus login untuk melihat halaman ini</p>';
$lang['msg_unknown_error'] = "Terjadi kesalahan!";


/* End of file message_lang.php */
/* Location: ./app/language/indonesia/message_lang.php */