<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['pkey'] = array(
    'post'   => 'post_id',
    'vpost'   => 'post_id',
    'penduduk' => 'ID_pend',
    'harga_komoditi' => 'ID',
    'album' => 'album_id',
    'foto' => 'foto_id',
    'vfoto' => 'foto_id',
    'polling' => 'poll_id',
    'user' => 'user_id',
    'vuser' => 'user_id',
    'kontak' => 'kontak_id',
    'pengaduan' => 'peng_id',
    'menu' => 'menu_id',
    'download' => 'ID',
    'vcategory' => 'tax_id',
    'kategori' => 'kat_id',
    'esurat' => 'surat_ID',
    'esuratinbox' => 'kirim_ID',
    'esuratoutbox' => 'kirim_ID',
    'esurat_kirim' => 'kirim_ID',
    'vesurat' => 'kirim_ID',
    'event' => 'event_id',
);


/* End of file table_pkey.php */
/* Location: ./application/config/table_pkey.php */