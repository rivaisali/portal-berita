<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['config_use_db'] = TRUE;
$config['global_prefix'] = 'wd_';
$config['assets_use_cdn'] = FALSE;

$config['assets_path'] = 'assets/';
$config['lib_path'] = 'assets/lib/';
$config['theme_path'] = 'assets/theme/';
$config['img_path'] = 'assets/img/';

$config['my_random_char'] = 'axrtyu34gh';
$config['jscss_version'] = '240518';
$config['post_type_comment'] = array('post','artikel');
$config['website_card_img'] = 'uploads/content-slider/Xbu9YDePcLtK.jpg';
$config['facebook_appid'] = '931527986953963';

/* End of file config.php */
/* Location: ./app/config/config.php */