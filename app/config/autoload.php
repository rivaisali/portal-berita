<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database', 'xdb', 'session', 'content', 'user', 'post', 'visitor_counter');

$autoload['helper'] = array('global', 'url', 'content', 'bootstrap', 'number', 'date', 'text', 'snippet', 'custom', 'html');

$autoload['config'] = array('app');

$autoload['language'] = array();

$autoload['model'] = array();


/* End of file autoload.php */
/* Location: ./application/config/autoload.php */