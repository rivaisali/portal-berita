<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
     'config' => array(
        array(
            'field' => 'site_title',
            'label' => 'Judul Website',
            'rules' => 'trim|required|strip_tags'),
        // array(
        //     'field' => 'owner_name',
        //     'label' => 'Nama Instansi',
        //     'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'owner_addr',
            'label' => 'Alamat',
            'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'owner_email',
            'label' => 'Email',
            'rules' => 'trim|valid_email|required|strip_tags')
     ),
     'download' => array(
        array(
            'field' => 'nama',
            'label' => 'Nama Dokumen',
            'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'file',
            'label' => 'File',
            'rules' => 'allowed_types[jpg,jpeg,png,bmp,pdf,doc,docx,xls,xlsx,zip,rar,7z]|max_file_size[20480]')
     ),
     'polling' => array(
        array(
            'field' => 'pertanyaan',
            'label' => 'Pertanyaan',
            'rules' => 'trim|required|strip_tags')
     ),
     'kategori' => array(
        array(
            'field' => 'nmkat',
            'label' => 'Nama Kategori',
            'rules' => 'trim|required|strip_tags')
     ),
     'harga_komoditi' => array(
        array(
            'field' => 'komoditi',
            'label' => 'Nama Komoditas',
            'rules' => 'trim|required|strip_tags|unique[harga_komoditi.komoditi,ID]'),
        array(
            'field' => 'harga',
            'label' => 'Harga',
            'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'unit',
            'label' => 'Unit',
            'rules' => 'trim|required|strip_tags'),
     ),
     'menu' => array(
        array(
            'field' => 'nm_menu',
            'label' => 'Nama Menu',
            'rules' => 'trim|required|strip_tags')
     ),
     'kontak' => array(
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|strip_tags'),
        array(
            'field' => 'subjek',
            'label' => 'Subjek',
            'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'pesan',
            'label' => 'Pesan',
            'rules' => 'trim|required|strip_tags')
     ),
     'user' => array(
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required'),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email|unique[user.email,user_id]'),
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|strip_tags|required|min_length[5]|unique[user.username,user_id]'),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[5]|md5encrypt'),
        array(
            'field' => 'password2',
            'label' => 'Konfirmasi Password',
            'rules' => 'required|matches[password]')        
    ),
    'user2' => array(
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required'),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email|unique[user.email,user_id]'),
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|strip_tags|required|min_length[5]|unique[user.username,user_id]'),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'min_length[5]|md5encrypt'), 
        array(
            'field' => 'password2',
            'label' => 'Konfirmasi Password',
            'rules' => 'md5encrypt'),   
        array(
            'field' => 'file',
            'label' => 'Foto',
            'rules' => 'allowed_types[jpg,jpeg,gif,png,bmp]|max_file_size[5120]')
    ),
    'esurat' => array(
        array(
            'field' => 'judul',
            'label' => 'Judul',
            'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'keterangan',
            'label' => 'Keterangan',
            'rules' => 'trim|strip_tags|nl2br'),
        array(
            'field' => 'tujuan[]',
            'label' => 'Tujuan',
            'rules' => 'required'),
        array(
            'field' => 'userfile',
            'label' => 'File',
            'rules' => 'allowed_types[jpg,jpeg,gif,png,bmp,pdf,zip,rar,doc,docx,xls,xlsx,ppt,pptx]|max_file_size[5120]')
    ),
    'personiluser' => array(
        array(
            'field' => 'nip',
            'label' => 'NIP',
            'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'nama',
            'label' => 'Nama Lengkap',
            'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'tmp_lahir',
            'label' => 'Tempat Lahir',
            'rules' => 'trim'),
        array(
            'field' => 'tgl_lahir',
            'label' => 'Tanggal Lahir',
            'rules' => 'ymdhis'),
        array(
            'field' => 'jabatan',
            'label' => 'Jabatan',
            'rules' => 'required'),
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required'),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email|unique[user.email,user_id]'),
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|strip_tags|required|min_length[5]|unique[user.username,user_id]'),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'min_length[5]|md5encrypt'), 
        array(
            'field' => 'password2',
            'label' => 'Konfirmasi Password',
            'rules' => 'md5encrypt'), 
        array(
            'field' => 'file',
            'label' => 'Foto',
            'rules' => 'allowed_types[jpg,jpeg,gif,png,bmp]|max_file_size[5120]')
    ),
	'event' => array(
        array(
            'field' => 'judul',
            'label' => 'Judul',
            'rules' => 'trim|required|strip_tags'),
        array(
            'field' => 'waktu_mulai',
            'label' => 'Waktu Mulai',
            'rules' => 'ymdhis'),
        array(
            'field' => 'waktu_selesai',
            'label' => 'Waktu Selesai',
            'rules' => 'ymdhis'),
        array(
            'field' => 'tempat',
            'label' => 'Tempat',
            'rules' => 'required'),
        array(
            'field' => 'cp',
            'label' => 'Contact Person',
            'rules' => 'required'),
        array(
            'field' => 'deskripsi',
            'label' => 'Deskripsi',
            'rules' => 'required'),
		array(
            'field' => 'file',
            'label' => 'Gambar',
            'rules' => 'allowed_types[jpg,jpeg,gif,png,bmp]|max_file_size[5120]')
    ),
);


/* End of file form_validation.php */
/* Location: ./application/config/form_validation.php */