<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "main";
$route['404_override'] = '';
$route['ajax/(:any)'] = "main/ajax/$1";
$route['action/(:any)'] = "main/action/$1";
$route['popup/(:any)'] = "main/popup/$1";
$route['download/(:any)'] = "main/download/$1";
$route['loadjs'] = "main/load_assets/js";
$route['loadcss'] = "main/load_assets/css";
$route['(:any)'] = "main/route/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */