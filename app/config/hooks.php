<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$hook['post_controller_constructor'] = array(
    'class'    => 'DS_Config',
    'function' => 'autoload_db_items',
    'filename' => 'DS_Config.php',
    'filepath' => 'core'
);

/* End of file hooks.php */
/* Location: ./app/config/hooks.php */