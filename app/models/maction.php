<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Maction extends CI_Model
{
    function __construct()
    {
        parent::__construct();           
    } 
    
    function reply_msg($tipe = '')
    {
        $res = array('msg'=>'Terjaadi kesalahan!', 'ok'=>FALSE);
        $email = post_value('email');
        $pesan = post_value('pesan', '');
        $obj_id = post_value('obj_id', '');
        if (trim($pesan)=='') {
            $res['msg'] = 'Pesan masih kosong!';
        } else {
            $pkey = $tipe == 'pengaduan' ? 'peng_id' : 'kontak_id';
            $subjek = $tipe == 'pengaduan' ? 'jenis' : 'subjek';
            $qw = $this->db->get_where($tipe, array($pkey=>$obj_id), 1);
            if ($qw->num_rows() > 0) {
                $row = $qw->row();
                $pesan_email = '<p style="margin-bottom:15px">'.$pesan.'</p><p>Pesan anda <em>('.hari_tanggal($row->waktu).')</em> :<br/>
                    <span style="color:gray;font-style:italic">'.$row->pesan.'</span></p>';
                $this->load->library('email');
                $this->email->from(config_item('owner_email'), config_item('owner_name'));
                $this->email->to($email);                 
                $this->email->subject('Balasan: "'.$row->$subjek.'"');
                $this->email->message($pesan_email); 
                if ($this->email->send()) {
                    $udata = $this->user->get_user_login('admpt');
                    $pdata = compact('tipe','pesan','obj_id');
                    $pdata['user_id'] = $udata['user_id'];
                    $this->db->insert('balasan', $pdata);
                    $res['ok'] = TRUE;
                    $res['msg'] = 'Pesan berhasil dikirim...';
                }   
            }            
        }
        echo json_encode($res);
    }
    
    function resetpass($action = '')
    {
        $res = array('ok'=>FALSE, 'msg'=>'Terjadi kesalahan sistem!');
        switch ($action):
        case 'verification':
            $email = trim(post_value('email'));
            if ( $email == '' || !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
                $res['msg'] = 'Format email tidak valid!';
            } else {
                $qw = $this->db->get_where('user', array('email'=>$email), 1);
                if ($qw->num_rows() == 0) {
                    $res['msg'] = 'Email tidak terdaftar!';
                } else {
                    $row = $qw->row();
                    $base_url = base_url();
                    $site_title = config_item('site_title');
                    $this->load->helper('string');
                    $verfcode = random_string('numeric', 6);
                    $linkverf = post_value('verf-url', $base_url);
                    $linkverf = "<a href='{$linkverf}'>{$linkverf}</a>"; 
                    $this->load->library('email');
                    $servname = $_SERVER["SERVER_NAME"] == 'localhost' ? 'localhost.com' : $_SERVER["SERVER_NAME"];
                    $this->email->from('noreply@'.$servname, $site_title);
                    $this->email->to($email);                 
                    $this->email->subject('Kode Verifikasi Untuk Ubah Pasword');
                    $this->email->message("Hi {$row->nama},<br>sistem telah menerima permintaan
                        perubahaan password anda. Username anda adalah <strong>{$row->username}</strong> dan kode 
                        vefirikasi adalah <strong>{$verfcode}</strong>. Ketikkan kode tersebut pada form yang 
                        disediakan atau bisa juga melalui link berikut : <br>{$linkverf}<br>
                        Perlu diperhatikan bahwa kode ini hanya berlaku selama 1 jam. Jika melebihi waktu tersebut, anda
                        harus melakukan permintaan perubahan password lagi.
                        <br><br><a href='{$base_url}'>{$base_url}</a>");	                
                    if ($this->email->send()) {
                        set_userdata('verfcode', $verfcode);
                        $this->input->set_cookie('rstp', $email, 3600);
                        $res['ok'] = TRUE;
                        $res['msg'] = 'Kode verifikasi telah dikirim ke '.$email;
                    }
                }
            } 
        break;
        default:
            if ($email = $this->input->cookie('rstp')) {
                $pass  = post_value('password');
                $pass2 = post_value('password2');
                $kode  = post_value('kode');
                if (empty($pass) || empty($pass2) || empty($kode)) {
                    $res['msg'] = 'Masih ada field yang kosong!';
                } else {
                    if ($pass != $pass2)
                        $res['msg'] = 'Konfirmasi password tidak cocok!';
                    elseif ($kode !== get_userdata('verfcode')) 
                        $res['msg'] = 'Kode verifikasi tidak cocok!';
                    else {
                        $updt = $this->db->where('email', $email)->update('user', array('password'=>md5encrypt($pass)));
                        if ($updt) {
                            $res['ok'] = TRUE;
                            $res['msg'] = 'Password anda berhasil diubah..';
                            $this->input->set_cookie('rstp');
                            unset_userdata('verfcode');
                        }
                    }
                }
            } else {
                $res['msg'] = 'Session expire. Silahkan kirim ulang kode verifikasi!';
            }
        endswitch;
        echo json_encode($res);
    }
    
    function crud($action = '', $global_data = '') 
    {        
        $table = get_value('n');
        if ( !$this->user->is_logged_in('admpt') ) {
            if ($action != 'insert' || !in_array($table, array('kontak','pengaduan')))
            show_404();
        }
        $id    = $this->input->get_post('id', 0);
        $this->load->library('crud');
        $result = $this->crud->$action($table, $id);
        $this->_show_result($result, $action);
    }
    
    function upload($name = '', $global_data = '')
    {
        $this->load->library('image');
        $this->load->helper('string');
        $filename = random_string('alnum', 25); 
        $filename = strtolower($filename); 
        switch($name) {
            case 'headerimg':
                header("Content-type: application/json"); 
                $res = array('ok'=>FALSE);   
                $upload = $this->upload_file($filename, './uploads/header-img/');
                if ( $upload['result'] == 'ok' ){
                    $filename = $upload['msg']['file_name'];
                    $nm = pathinfo($filename, PATHINFO_FILENAME); 
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $thumb = "{$nm}_thumb.{$ext}";
                    $src = $upload['msg']['full_path'];
                    $this->image->source_img = $src;
                    $this->image->resize(400, 400, $upload['msg']['file_path'].$thumb, '');  
                    $this->image->clear();  
                    $res['img'] = $filename;
                    $res['thumb'] = $thumb;
                    $res['ok'] = TRUE;
                }
                $res['ok'] = TRUE;
                echo json_encode($res);
            break;
            case 'fpbanner':
                header("Content-type: application/json"); 
                $res = array('ok'=>FALSE);   
                $upload = $this->upload_file($filename, './uploads/banner/');
                if ( $upload['result'] == 'ok' ){
                    $filename = $upload['msg']['file_name'];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $src = $upload['msg']['full_path'];
                    if ($ext != 'gif') {
                        $this->image->source_img = $src;
                        $this->image->resize(1000, 1000, $upload['msg']['full_path'], '');  
                        $this->image->clear();  
                    }
                    $judul = post_value('judul');
                    $link = post_value('link');
                    if (!empty($link)) $link = prep_url($link);
                    if ($this->db->insert('banner', array('judul'=>$judul, 'link'=>$link,
                        'grup'=>'Halaman Depan','url'=>$filename))) {
                        $res['img'] = $filename;
                        $res['ok'] = TRUE; 
                        $res['judul'] = $judul;   
                        $res['link'] = $link; 
                        $res['id'] = $this->db->insert_id();
                    }                    
                }
                $res['ok'] = TRUE;
                echo json_encode($res);
            break;
            case 'banner':     
                header("Content-type: application/json"); 
                $res = array('ok'=>FALSE);           
                $action = post_value('action');
                $pdata = array(
                    'judul' => trim(post_value('judul')),
                    'grup' => trim(post_value('banner_grup')),
                    'link' => prep_url(post_value('link')) );
                if ($action == 'insert') {
                    $upload = $this->upload_file($filename, './uploads/banner/');
                    if ( $upload['result'] == 'ok' ) {
                        $filename = $upload['msg']['file_name'];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        $src = $upload['msg']['full_path'];
                        if ($ext != 'gif') {
                            $this->image->source_img = $src;
                            $this->image->resize(1000, 1000, $upload['msg']['full_path'], '');  
                            $this->image->clear();  
                        }
                        $pdata['url'] = $filename;
                        if ($this->db->insert('banner', $pdata)) {
                            $res['ok'] = TRUE;
                            $res['msg'] = 'Banner berhasil ditambahkan..';
                        }
                    }
                } else {
                    $ban_id = post_value('ban_id', 0);
                    if ($this->db->update('banner', $pdata, array('ban_id'=>$ban_id))) {
                        $res['ok'] = TRUE;
                        $res['msg'] = 'Banner berhasil di-update..';
                    }
                }
                echo json_encode($res);
            break;
            case 'gallery':
                $upload = $this->upload_file($filename, './uploads/gallery/');
                if ( $upload['result'] == 'ok' ) {    
                    $filename = $upload['msg']['file_name'];
                    $src = $upload['msg']['full_path'];
                    $this->image->source_img = $src;
                    $this->image->resize(1024, 1024, $upload['msg']['full_path'], '');  
                    $this->image->resize(320, 240, $upload['msg']['file_path'].'thumbs/'.$filename, 'crop');  
                    $this->image->clear();  
                    $album_id = get_value('album', 0);
                    $pdata = array('url'=>$filename, 'album_id'=>$album_id); 
                    if ($this->db->insert('foto', $pdata)) {
                        $tbalbmm = $this->db->dbprefix('album');
                        $this->db->query("UPDATE {$tbalbmm} SET jum_foto=jum_foto+1 WHERE album_id={$album_id}");
                    }             
                }
            break;
        }
    }
    
    private function upload_file($filename='', $path='')
    {
        $res = array();
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $config['file_name'] = $filename;
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file')) {
            $res['result']='ok'; 
            $res['msg']=$this->upload->data();           
        } else {
            $res['msg']=$this->upload->display_errors('', ', ');  
            $res['result']='error';
        }
        return $res;
    }
    
    function post($name = '', $global_data = '')
    {
        $this->load->library('post');
        $this->load->helper('array');
        $result = array('ok'=>FALSE); 
        if ( !$this->user->is_logged_in('admpt') ) {
            if (!is_ajax()) {
                $red = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
                if ($red != '')
                    $red = '?redirect='.urlencode(str_replace(base_url(), '', $red));
                redirect('admpt/login'.$red);
                exit;
            }            
        }
        switch ($name) {
            case 'add-category':
                $term_name = post_value('cat_name');
                $parent    = post_value('cat_parent');
                $post_type = post_value('post_type');
                $description = post_value('description', '');
                $term_name = preg_replace('/[\;\|]/', '', $term_name);
                $term_name = trim($term_name);
                if ($tax_id = $this->post->add_term('category', $term_name, $parent, $description, $post_type)) {
                    $result['ok'] = TRUE;
                    $result['id'] = $tax_id;
                    $result['code'] = $parent;
                    $result['val'] = $term_name;
                    $result['msg'] = 'Kategori berhasil ditambahkan';
                }        
            break;
            case 'update-category':
                if ($tax_id = get_value('id')) {
                    $term_name = post_value('cat_name');
                    $parent    = post_value('cat_parent');
                    $description = $this->input->post('description') ? $this->input->post('description') : NULL;
                    $result['ok'] = $this->post->update_term($tax_id, $term_name, $parent, $description);
                    $result['id'] = $tax_id;
                    $result['msg'] = 'Kategori berhasil di-update';
                }                
            break;
            case 'delete-term':
                if ($tax_id = get_value('id')) {
                    $result['ok'] = $this->post->delete_term($tax_id);
                    $result['id'] = $tax_id;
                }
            break;
            case 'add-tags':
                $tags = post_value('tags_name');
                $tags = explode(',', $tags);
                $post_type = post_value('post_type');
                foreach ($tags as $term_name) {
                    $term_name = trim($term_name);
                    $this->post->add_term('tag', $term_name, 0, '', $post_type);
                } 
                $result['ok'] = TRUE;       
            break;
            case 'generate-slug':
                $post_id    = $this->input->get_post('post-id');
                $post_title = $this->input->get_post('post-title');
                $post_slug  = $this->post->generate_post_slug($post_title, $post_id);
                $result['ok']  = TRUE; 
                $result['val'] = $post_slug;
            break;
            case 'submit-post':				
                $post_id    = post_value('post-id');
                $post_date  = post_value('post-date');
                if (strtolower($post_date) == 'otomatis')
                    $post_date = date('Y-m-d H:i:s');
                $post_title = post_value('post-title', '(Tanpa Judul)');
                $post_content = post_value('post-content', '');
                $post_type = post_value('post-type');
                $post_status = post_value('btn-submit-post');
                $post_thumb = post_value('post-thumb', NULL);
                $post_slug = post_value('post-slug', '');
                $post_cats = post_value('post-cats', array());
                $user_id = $global_data['admin']['user_id'];
                $pdata = compact('post_title','post_content','post_type','post_status',
                    'post_thumb','post_date','post_slug','user_id', 'post_cats');
                $ret = $this->post->save_post($pdata, $post_id);
                $post_type_label = post_value('post-type-label');
                $act_msg = $post_id > 0 ? 'di-update' : 'diterbitkan';
                if ($post_status == 'draft')
                    $result['msg'] = $post_type_label.' disimpan sebagai draft '; else
                    $result['msg'] = $post_type_label.' berhasil '.$act_msg.'. <a class="alert-link" target="_blank" 
                        href="'.base_url("{$post_type}/{$ret['slug']}").'">Lihat '.$post_type_label.'</a>'; 
                $result['ok'] = TRUE;
                set_flashdata('post_submit_result', $result);
                redirect(post_value('redirect')."&a=edit&id={$ret['id']}");
                exit;
            break;
            case 'preview-post':
                $post = array(
                    'post_title'   => post_value('post-title'),
                    'post_content' => post_value('post-content'),
                    'post_thumb'   => post_value('post-thumb')
                );
                $this->load->library('encrypt');
                $cache_id = md5encrypt($post['post_title']);
                $this->load->driver('cache'); 
                if ($this->cache->file->save('postprev-'.$cache_id, $post, 1800)) {
                    $result['id'] = $cache_id;    
                    $result['ok'] = TRUE;      
                }                               
            break;
            case 'restore-post':
                if ($post_id = get_value('id')) {
                    $this->post->restore_post($post_id);
                    $result['ok'] = TRUE;
                    $result['id'] = $post_id;
                    $result['msg'] = '1 Data berhasil dikembalikan...';
                }
            break;
            case 'trash-post':
                if ($post_id = get_value('id')) {
                    $this->post->delete_post($post_id);
                    $result['ok'] = TRUE;
                    $result['id'] = $post_id;
                    $restore_url = base_url('action/post/restore-post?id='.$post_id);
                    $result['msg'] = '1 data dipindahkan ke tempat sampah. <a href="'.$restore_url.'" class="alert-link">Batalkan</a>';
                }
            break;
            case 'publish-post':
                if ($post_id = get_value('id')) {
                    $this->post->set_post_status($post_id, 'publish');
                    $result['ok'] = TRUE;
                    $result['id'] = $post_id;
                    $result['msg'] = '1 data berhasil diterbitkan..';
                }
            break;
            case 'draft-post':
                if ($post_id = get_value('id')) {
                    $this->post->set_post_status($post_id, 'draft');
                    $result['ok'] = TRUE;
                    $result['id'] = $post_id;
                    $result['msg'] = '1 data disimpan sebagai draft..';
                }
            break;
            case 'delete-post':
                if ($post_id = get_value('id')) {
                    $this->post->delete_post($post_id, TRUE);
                    $result['ok'] = TRUE;
                    $result['id'] = $post_id;
                    $result['msg'] = '1 Data berhasil dihapus permanen...';
                }
            break;
        }
        $this->_show_result($result, $name);
    }
    
    function login($user_type = '')
    {
        $result = $this->user->login($user_type);
        $redirect = post_value('redirect') != '' ? post_value('redirect') : $result['type'];
        if ($this->input->is_ajax_request()) {
            header("Content-type: application/json");
            $result['redirect'] = base_url($redirect);
            echo json_encode($result);
        } else {
            if ( !$result['ok'] ) {
                if ( $result['type'] != '' ) {
                    $red_uri = urlencode($redirect);
                    $redirect = $result['type'] . '/login?redirect=' . $red_uri;                
                } else {
                    $redirect = base_url();
                }
                set_flashdata( 'action_msg', $result['msg'] );
            }
            redirect($redirect);
        }
    }    
    
    function logout($user_type = '')
    { 
        $this->user->logout($user_type);
        if ( $redirect = get_value('redirect') ) {
            $redirect = in_array($redirect, array('home', 'index', '1')) ? '' : $redirect;
            redirect(add_qstring($redirect, 'log-time='.time())); 
        } else {
            if ($user_type == '')
                redirect("?logout=true"); else
                redirect("{$user_type}/login?log-time=".time());
        }
    }
    
    private function _show_result($result, $action)
    {
        $is_ajax = $this->input->is_ajax_request(); 
        if ( !empty($_SERVER['HTTP_REFERER']) )
            $ref = $_SERVER['HTTP_REFERER']; else 
            $ref = base_url();
        $redirto = $this->input->get_post('redirect');
        if (!$redirto) $redirto = $ref;
        $result = array_merge(
            array('ok'   => FALSE,
                  'msg'  => '',
                  'type' => $action,
                  'id'   => 0,
                  'val'  => '',
                  'code' => 0), $result); 
        if ($is_ajax) {
            echo json_encode($result);
        } else { 
            $this->load->helper('array');
            $post_data = post_to_array();
            if (!$result['ok'])
                set_flashdata('post_data', $post_data);            
            set_flashdata('result', $result); 
            $redirect = $result['ok'] ? $redirto : $ref; 
            redirect($redirect);    
        } 
    }
}

/* End of file maction.php */
/* Location: ./app/models/maction.php */