<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Majax extends CI_Model
{
    private $udata = array();
    
    function __construct()
    {
        parent::__construct();    
    }

    function contentslider($action = '')
    {
        $res = array('ok'=>FALSE, 'msg'=>'Terjadi kesalahan','type'=>$action);
        if ($action === 'sort') {
            $items = post_value('items');
            if (is_array($items) && count($items) > 0) {
                $pdata = array();
                $i = 1;
                foreach ($items as $id) {
                    $pdata[] = array('ID'=>$id,'urutan'=>$i);
                    $i++;
                }
                if ( $this->db->update_batch('content_slider', $pdata, 'ID') !== FALSE ) {
                    $res['msg'] = 'Data berhasil di-update';
                    $res['ok'] = TRUE;
                }                
            }
        } elseif ($action === 'delete') {
            if ($id = post_value('id')) {
                $this->db->delete('content_slider', array('ID'=>$id));
                $this->load->helper('file');
                $fn = post_value('fn');
                $filepath = './uploads/content-slider/'.$fn;
                if (file_exists($filepath)) unlink($filepath);
                $filepath = './uploads/content-slider/'.add_filename_prefix($fn, '_thumb');
                if (file_exists($filepath)) unlink($filepath);
                $res['ok'] = TRUE;
                $res['msg'] = 'Data dihapus';
            }
        } elseif ($action === 'add' OR $action === 'edit') {
            $imageData = trim(post_value('foto_dataurl'));
            if ( ! empty($imageData) ) {
                $img_uploaded = TRUE;
                $pdata = array(
                    'judul' => trim(strip_tags(post_value('judul'))),
                    'deskripsi' => trim(strip_tags(post_value('deskripsi'))),
                    'link' => prep_url(trim(post_value('link')))
                );
                if (post_value('newimg') === 'yes') {
                    $imageData = substr($imageData, strpos($imageData, ',')+1);
                    $imageData = str_replace(' ', '+', $imageData);
                    $imageData = base64_decode($imageData);
                    $this->load->helper('string');
                    $filename = random_string('alnum', 12).'.jpg';
                    $filepath = './uploads/content-slider/'.$filename;
                    if ( file_put_contents($filepath, $imageData) ) {
                        $this->load->helper('file');
                        $thumb_filename = add_filename_prefix($filename, '_thumb');
                        $thumb_filepath = './uploads/content-slider/'.$thumb_filename;
                        $this->load->library('image');
                        $this->image->source_img = $filepath; 
                        $this->image->resize(450, 450, $thumb_filepath, '');  
                        $this->image->clear(); 
                        $pdata['nama_file'] = $filename;
                        if ($old_filename = post_value('prev_imgsrc')) {
                            $old_filepath = './uploads/content-slider/'.$old_filename;
                            if (file_exists($old_filepath)) unlink($old_filepath);
                            $old_filepath = './uploads/content-slider/'.add_filename_prefix($old_filename, '_thumb');
                            if (file_exists($old_filepath)) unlink($old_filepath);
                        }
                    } else {
                        $img_uploaded = FALSE;
                    }
                }                
                if ($img_uploaded) {    
                    if ($action === 'add') {               
                        if ($this->db->insert('content_slider', $pdata)) {
                            $res['msg'] = 'Data berhasil ditambahkan';
                            $res['ok'] = TRUE;
                        } 
                    } else {
                        if ($this->db->update('content_slider', $pdata, array('ID'=>post_value('id')))) {
                            $res['msg'] = 'Data berhasil di-update';
                            $res['ok'] = TRUE;
                        } 
                    }
                }
            } else {
                $res['msg'] = 'Foto belum dipilih';
            }
        }
        echo json_encode($res);
    }
    
    function setaktifpers()
    {
        $res = array('ok'=>FALSE, 'msg'=>'Terjadi kesalahan!');
        if ($id = post_value('id')) {
            $tbl = $this->db->dbprefix('personil');
            $res['ok'] = $this->db->query("UPDATE {$tbl} SET aktif = IF(aktif='Ya','Tidak','Ya') WHERE pers_id={$id}");            
        }
        echo json_encode($res);
    }
    
    function pencarian($type = '')
    {
        $res = array('html'=>'', 'total'=>0);      
        $perpage = 15;
        $page = get_value('p', 1);
        $kword = get_value('k', '');
        $posts = $this->post->get_result($type, array(
            'search'=>$kword, 'deep_search'=>TRUE, 'perpage'=>$perpage, 'page'=>$page));
        $tlabel = $type == 'news' ? 'Tulisan' : 'Pengumuman';
        if ($posts['data_total'] == 0) {
            $res['html'].= '<div class="alert alert-danger">Tidak ada '.$tlabel.' yang sesuai dengan kata kunci!</div>';
        } else {
            $res['html'].= '<ul class="media-list">';
            foreach ($posts['rows'] as $post) {
				$res['html'].='<div class="blog-item-one">';
                $post_url = base_url($post['post_type'].'/'.$post['post_slug']);
				$res['html'].='<div class="content-text clearfix animation-item" data-animation-type="fadeInUp">
                        <a href="'.$post_url.'" title="Baca selengkapnya">
                    <h5 class="media-heading post-title">'
						.$post['post_title'].'
                    </h5>
						</a>
						<p class="post-summary" style="margin-bottom:5px">'.word_limiter(strip_tags($post['post_content']), 20).'</p>
                    <ul class="text-left">
                        <li><i class="fa fa-clock-o"></i> '.xtime($post['post_date']).'</li>
                        <li><i class="fa fa-user"></i> '.$post['namauser'].'</li>
                    </ul>
                </div>';
                // $res['html'].= '<li class="media post-item"><h4 class="media-heading large post-title"><a href="'.$post_url.'" title="Baca selengkapnya">'.$post['post_title'].'</a></h4>
                    // <p class="post-summary" style="margin-bottom:5px">'.word_limiter(strip_tags($post['post_content']), 20).'</p>
                    // <small class="post-meta text-muted"><i class="fa fa-clock-o"></i> '.xtime($post['post_date']).' 
                    // &nbsp;|&nbsp; <i class="fa fa-user"></i> '.$post['namauser'].'</small></li>';
				$res['html'].='</div>';
            }
            $res['html'].= '</ul>';
            $res['html'].= '<ul class="page_pagination">';
            if ($posts['prev_page'] > 0) 
                $res['html'].= '<li class="previous"><a href="#" data-page="'.$posts['prev_page'].'">&laquo; Sebelumnya</a></li>';
            if ($posts['next_page'] > 0) 
                $res['html'].= '<li class="next"><a href="#" data-page="'.$posts['next_page'].'">Berikut &raquo;</a></li>';            
            $res['html'].= '</ul>';
        }
        $res['total'] = $posts['data_total'];
        echo json_encode($res);
    }
    
    function headerimg($action = '')
    {
        $res = array('ok'=>FALSE, 'val'=>'', 'id'=>0, 'action'=>'');
        switch($action) {
            case 'use':
                $img = post_value('img');
                $this->config->update('header_img', $img);
                $res['ok'] = TRUE;
            break;
            case 'delete':
                $img = post_value('img');
                $nm = pathinfo($img, PATHINFO_FILENAME); 
                $ext = pathinfo($img, PATHINFO_EXTENSION);
                $large = "./uploads/header-img/{$img}";
                $thumb = "./uploads/header-img/{$nm}_thumb.{$ext}";
                if (file_exists($large)) unlink($large);
                if (file_exists($thumb)) unlink($thumb);
                $res['ok'] = TRUE;
            break;
        }
        echo json_encode($res);
    }
    
    function frontpageitem($action = '')
    {
        $res = array('ok'=>FALSE, 'val'=>'', 'id'=>0, 'action'=>'');
        switch($action) {
            case 'save':
                $items = post_value('items');
                $this->config->update('front_page', trim($items));
                $res['ok'] = TRUE;
            break;
        }
        echo json_encode($res);
    }
    
    function sidebaritem($action = '')
    {
        $res = array('ok'=>FALSE, 'val'=>'', 'id'=>0, 'action'=>'');
        switch($action) {
            case 'save':
                $left = post_value('left');
                $right = post_value('right');
                $this->config->update('sidebar_left', trim($left));
                $this->config->update('sidebar_right', trim($right));
                $res['ok'] = TRUE;
            break;
        }
        echo json_encode($res);
    }
    
    function menuman($action = '') 
    {
        $res = array('ok'=>FALSE, 'val'=>'', 'id'=>0, 'action'=>'');
        switch($action) {
            case 'load':
                $menuid = get_value('id');
                if ($menuid) {
                    $res['val'] = '';
                    $qw = $this->db->get_where('menu', array('menu_id'=>$menuid));
                    if ($qw->num_rows() > 0) {
                        $row = $qw->row();
                        $res['ok'] = TRUE;
                        $res['val'] = @json_decode($row->items); 
                    }
                }
            break;
            case 'save':
                $nm_menu = post_value('nm_menu');
                $nm_menu = trim(strip_tags($nm_menu));
                $subact = post_value('action');
                $menuid = post_value('menu_id');
                $res['action'] = $subact;
                if (trim($nm_menu) != '') {
                    if ($subact == 'insert') {
                        $this->db->insert('menu', array('nm_menu'=>$nm_menu));
                        $res['id'] = $this->db->insert_id();
                    } else {
                        $this->db->update('menu', array('nm_menu'=>$nm_menu), array('menu_id'=>$menuid)); 
                        $res['id'] = $menuid;
                    }   
                    $res['ok'] = TRUE;
                    $res['val'] = $nm_menu;
                }
            break;
            case 'save-item':
                $items = post_value('menuitem'); 
                $menuid = post_value('menuid');
                if ($this->db->update('menu', array('items'=>$items), array('menu_id'=>$menuid))) {
                    $res['ok'] = TRUE;
                } 
            break;
            case 'delete':
                $menuid = get_value('id');
                if ($menuid && $menuid > 1) {
                    $this->db->delete('menu', array('menu_id'=>$menuid));
                    $res['ok'] = TRUE;
                }
            break;
        }
        echo json_encode($res);
    }
    
    function loadopts($name = '')
    {
        $res = array('html'=>'');
        switch($name) {
            case 'category':
                $type = get_value('t');
                $res['html'] = categories_dropdown($type, '', 
                    array('class'       => 'form-control',
                      'first_label' => '',
                      'name'        => 'cat_parent',
                      'opts_only'   => TRUE,
                      'id'          => 'iparent',
                      'exclude_def' => TRUE)); 
            break;
        }
        echo json_encode($res);
    }
    
    function polling($action = '')
    {
        $res = array('ok'=>FALSE);
        switch($action) {
            case 'submit':
                $pilihan = post_value('pilihan');
                $poll_id = post_value('poll_id');
                if ($pilihan && $poll_id) {
                    $this->db->where('opsi_id', $pilihan)
                     ->set('jumlah', 'jumlah+1', FALSE)
                     ->update('polling_opsi');
                    $this->db->where('poll_id', $poll_id)
                     ->set('total_suara', 'total_suara+1', FALSE)
                     ->update('polling');
                    $this->input->set_cookie('pllg', $pilihan, '864000'); // 10 hari
                    $res['ok'] = TRUE;
                }
            break;
        }
        echo json_encode($res);
    }
        
    function load_banner($grup = '')
    {
        $grup = urldecode($grup);
        $res = array('html'=>'', 'count'=>0);
        if ($grup != '') $this->db->where('grup', $grup);
        $qw = $this->db->get('banner');
        if ($qw->num_rows() == 0) {
            $res['html'] = '<div class="well">Belum banner!</div>';
        } else {
            $res['count'] = $qw->num_rows();
            $res['html'] = '<ul class="banner-list">';
            foreach ($qw->result() as $row) {
                $ban_img = base_url('uploads/banner/'.$row->url);
                $link = $row->link == '' ? '' : ' href="'.$row->link.'" title="Klik untuk mengunjungi '.$row->link.'" target="_blank"';
                $res['html'].= '<li data-id="'.$row->ban_id.'"><a class="img-container"'.$link;
                if ($row->judul != '')
                    $res['html'].= ' title="'.$row->judul.'"';
                $res['html'].= '><span class="valign-helper"></span><img src="'.$ban_img.'"></a>';
                $res['html'].= '<div class="banner-btns">';
                if (has_akses('banner-edit'))
                    $res['html'].= '<a href="#" class="btn-edit-banner" data-id="'.$row->ban_id.'" data-grup="'.$row->grup.'"
                     data-judul="'.$row->judul.'" data-url="'.$row->url.'" data-link="'.$row->link.'" title="Edit">
                     <i class="glyphicon glyphicon-edit"></i></a> ';
                if (has_akses('banner-del'))
                    $res['html'].= '<a href="#" class="btn-del-banner" data-id="'.$row->ban_id.'" data-grup="'.$row->grup.'"
                     data-judul="'.$row->judul.'" data-url="'.$row->url.'" data-link="'.$row->link.'" title="Hapus">
                    <i class="glyphicon glyphicon-trash"></i></a>';
                $res['html'].= '</div></li>';
            }
            $res['html'].= '</ul>';
        }
        echo json_encode($res);
    }
    
    function load_gallery($album_id = 0)
    {
        $res = array('html'=>'', 'count'=>0);
        $qw = $this->db->get_where('foto', array('album_id'=>$album_id));
        if ($qw->num_rows() == 0) {
            $res['html'] = '<div class="well">Belum ada foto di album ini!</div>';
        } else {
            $res['count'] = $qw->num_rows();
            $res['html'] = '<ul class="foto-list">';
            foreach ($qw->result() as $row) {
                $thumb_url = base_url('uploads/gallery/thumbs/'.$row->url);
                $large_url = base_url('uploads/gallery/'.$row->url);
                $res['html'].= '<li data-id="'.$row->foto_id.'"><a class="img-container" href="'.$large_url.'"';
                if ($row->judul != '')
                    $res['html'].= ' title="'.$row->judul.'"';
                $res['html'].= '><img src="'.$thumb_url.'"></a>';
                if ($row->judul != '')
                    $res['html'].= '<span class="foto-title">'.$row->judul.'</span>';
                $res['html'].= '<div class="foto-btns">';
                if (has_akses('galeri-edit')) {
                    $res['html'].= '<a href="#" class="btn-edit-foto" data-id="'.$row->foto_id.'" data-album-id="'.$row->album_id.'"
                         data-judul="'.$row->judul.'" data-url="'.$row->url.'" title="Edit">
                         <i class="glyphicon glyphicon-edit"></i></a> ';
                }
                if (has_akses('galeri-del')) {
                    $res['html'].= '<a href="#" class="btn-del-foto" data-id="'.$row->foto_id.'" data-album-id="'.$row->album_id.'"
                         data-judul="'.$row->judul.'" data-url="'.$row->url.'" title="Hapus">
                        <i class="glyphicon glyphicon-trash"></i></a>';
                }
                $res['html'].= '</div></li>';
            }
            $res['html'].= '</ul>';
        }
        echo json_encode($res);
    }
    
    function updatefoto()
    {
        $res = array('ok'=>FALSE);
        $pdata = array(
            'judul' => trim(post_value('new_judul')),
            'album_id' => post_value('new_album_id'));    
        $foto_id = post_value('edit_foto_id');
        if ( $this->db->update('foto', $pdata, array('foto_id'=>$foto_id)) ) {
            $res['ok'] = TRUE;
            $res['album_id'] = $pdata['album_id'];
            $res['judul'] = $pdata['judul'];
        }    
        echo json_encode($res);   
    }
    
    function deletefoto()
    {
        $res = array('ok'=>FALSE);
        $foto_id = post_value('foto_id');
        $album_id = post_value('album_id'); 
        $nmfile = post_value('url'); 
        $foto_path = './uploads/gallery/'.$nmfile;
        $thumb_path = './uploads/gallery/thumbs/'.$nmfile;
        if (file_exists($foto_path)) unlink($foto_path);
        if (file_exists($thumb_path)) unlink($thumb_path);
        $this->db->delete('foto', array('foto_id'=>$foto_id));
        $tbalbmm = $this->db->dbprefix('album');
        $this->db->query("UPDATE {$tbalbmm} SET jum_foto=jum_foto-1 WHERE album_id={$album_id}");
        $res['ok'] = TRUE;        
        echo json_encode($res);
    }
    
    function deletebanner()
    {
        $res = array('ok'=>FALSE);
        $ban_id = post_value('ban_id');
        $nmfile = post_value('url'); 
        $foto_path = './uploads/banner/'.$nmfile;
        if (file_exists($foto_path)) unlink($foto_path);
        $this->db->delete('banner', array('ban_id'=>$ban_id));
        $res['ok'] = TRUE;        
        echo json_encode($res);
    }
        
    function savealbum()
    {
        $res = array('ok'=>FALSE, 'msg'=>'');
        $action = post_value('new_album_action');
        $album_id = post_value('new_album_id', 0);
        $nm_album = trim(post_value('new_album_name'));
        if ($nm_album == '') {
            $res['msg'] = "Nama Album masih kosong!";
        } else {
            $where = array('nm_album'=>$nm_album);
            if ($album_id > 0)
                $where['album_id <>'] = $album_id;
            $cek = $this->db->get_where('album', $where, 1);
            if ($cek->num_rows() == 0) {
                if ($action == 'update')
                    $this->db->update('album', array('nm_album'=>$nm_album), array('album_id'=>$album_id)); else
                    $this->db->insert('album', array('nm_album'=>$nm_album));
                $res['ok'] = TRUE;
            } else {
                $res['msg'] = "Album {$nm_album} sudah ada!";
            }
        }
        echo json_encode($res);
    }
    
    function category_pers_table()
    {
        $res = array('html'=>'');
        $qw = $this->db->order_by('urutan ASC')->get('kategori');
        if ($qw->num_rows() == 0) {
            $res['html'].= '<tr><td style="text-align:center;padding:20px 0" colspan="3">(Belum ada data)</td></tr>';
        } else {
            $no = 1;
            foreach ($qw->result() as $row) {
                $res['html'].= '<tr>';
                $res['html'].= '<td style="text-align:center">'.$no.'</td>';
                $res['html'].= '<td data-name="nama"><span>'.$row->nmkat.'</span>';
                $res['html'].= '<div class="cat-actions" style="display:none">
                    <button class="btn btn-success btn-xs btn-edit-cat" type="button" data-id="'.$row->kat_id.'">
                        <i class="glyphicon glyphicon-edit"></i> Edit</button> 
                    <a class="btn btn-danger btn-xs btn-del-cat" href="../action/crud/delete?n=kategori&id='.$row->kat_id.'">
                        <i class="glyphicon glyphicon-trash"></i> Hapus</a>
                    <a class="btn btn-info btn-xs btn-move-cat" data-move="naik" data-urut="'.$row->urutan.'" href="#">
                        <i class="fa fa-arrow-up"></i> Ke atas</a>
                    <a class="btn btn-info btn-xs btn-move-cat" data-move="turun" data-urut="'.$row->urutan.'" href="#">
                        <i class="fa fa-arrow-down"></i> Ke bawah</a>
                </div></td>';
                $res['html'].= '</tr>';
                $no++;
            }
        }
        echo json_encode($res);
    }
    
    function urutpersonil()
    {
        $lama = (int) get_value('u');
        $dir = get_value('d');
        $res = array('ok'=>FALSE);
        $baru = $dir == 'down' ? $lama + 1 : $lama - 1;
        $tabel = $this->db->dbprefix('personil');
        $this->db->query("UPDATE {$tabel} a
            INNER JOIN {$tabel} b ON a.pers_id <> b.pers_id
            SET a.urutan = b.urutan
            where a.urutan in ({$lama},{$baru}) and b.urutan in ({$lama},{$baru})");
        $res['ok'] = TRUE;
        echo json_encode($res);
    }
    
    function movekategori($tuju = '')
    {
        $res = array('ok'=>FALSE);
        $lama = get_value('u', 1);
        $baru = $tuju == 'naik' ? $lama - 1 : $lama + 1;
        if ($lama > 0 && $baru > 0) {
            $tabel = $this->db->dbprefix('kategori');
            $this->db->query("UPDATE {$tabel} a
                INNER JOIN {$tabel} b ON a.kat_id <> b.kat_id
                SET a.urutan = b.urutan
                where a.urutan in ({$lama},{$baru}) and b.urutan in ({$lama},{$baru})");
            $res['ok'] = TRUE;
        }
        echo json_encode($res);
    }
    
    function category_table($post_type = 'news')
    {
        $res = array('html'=>'');
        $cats = get_categories($post_type);
        if (count($cats) == 0) {
            $res['html'].= '<tr><td style="text-align:center;padding:20px 0" colspan="3">(Belum ada data)</td></tr>';
        } else {
            foreach ($cats as $cat_id => $cat) {
                $res['html'].= '<tr data-parent="'.$cat['parent'].'">';
                $res['html'].= '<td>';                            
                for ($i=1; $i<$cat['level']; $i++)
                    $res['html'].= '&mdash;';
                $res['html'].= ' <span>'.$cat['name'].'</span>';
                $res['html'].= '<div class="cat-actions" style="display:none">
                    <button class="btn btn-success btn-xs btn-edit-cat" type="button" data-id="'.$cat_id.'">
                        <i class="glyphicon glyphicon-edit"></i> Edit</button> 
                    <a class="btn btn-danger btn-xs btn-del-cat" href="../action/post/delete-term?id='.$cat_id.'">
                        <i class="glyphicon glyphicon-trash"></i> Hapus</a>
                    <a class="btn btn-info btn-xs btn-view-cat" href="?t='.$post_type.'&c='.$cat_id.'">
                        <i class="glyphicon glyphicon-search"></i> Lihat</a> 
                </div></td>';
                $res['html'].= '<td style="color:#666;font-style:italic">'.$cat['description'].'</td>';
                $res['html'].= '<td style="text-align:center">'.$cat['count'].'</td>';
                $res['html'].= '</tr>';
            }
        }
        echo json_encode($res);
    }
    
    function dstable($name = '')
    {   
        $this->load->helper('db');
        $udata = $this->user->get_user_login('admpt');
        $return  = array('rows' => array());
        $page    = $this->input->get_post('page');
        $perpage = $this->input->get_post('perpage');
        $filters = $this->input->get_post('filters');
        $orderby = $this->input->get_post('orderby');
        $wheres  = array();
        if ($name == 'esuratinbox' OR $name == 'esuratoutbox')
            $table = 'vesurat'; else
            $table   = $this->db->dbprefix($name);
        $this->config->load('table_pkey');
        $pkey_cfg = $this->config->item('pkey');
		$pkey = $pkey_cfg[$name];
        if ($perpage == 0) $perpage = 20;
        if (is_array($filters) && !empty($filters)) {           
            foreach($filters as $filter) {
                $exp_filter = explode(' ', $filter);
                $filter_key = $exp_filter[0];
                $fiter_op   = $exp_filter[1];
                unset($exp_filter[0], $exp_filter[1]);
                $fiter_val  = implode(' ', $exp_filter);
                if (($name == 'vperkara' || $name == 'perkara') && $filter_key == 'no_perk') {
                    $str_where = "no_perk LIKE '%{$fiter_val}%' OR nm_terdakwa LIKE '%{$fiter_val}%'";
                } else {
                    $str_where = $filter_key;
                    if ($fiter_val == 'null') {
                        $str_where.= " IS NULL";
                    } else {                        
                        if ($fiter_op == '%')
                            $str_where.= " LIKE '%{$fiter_val}%'"; 
                        elseif ($fiter_op == '=') 
                            $str_where.= " = '{$fiter_val}'";
                        else $str_where.= " {$fiter_op} {$fiter_val}"; 
                    } 
                }
                $wheres[] = "({$str_where})";                   
            }
        }
        $group_by = '';
        $extra_select = '';
        switch($name) {
            case 'esuratinbox':
                $wheres[] = 'tujuan = '.(empty($udata['unitkrj_ID']) ? '0' : $udata['unitkrj_ID']);
                $wheres[] = 'pengirim <> '.$udata['user_id'];
            break;
            case 'esuratoutbox':
                $wheres[] = 'pengirim = '.$udata['user_id'];
                $wheres[] = "hapus = 'N'";
                $extra_select = "GROUP_CONCAT(nm_tujuan SEPARATOR '<br/>') AS nm_tujuans";
                $group_by = 'surat_ID';
            break;
        }
        $where = '';
        if (!empty($wheres)) 
            $where = " WHERE ".implode(" AND ", $wheres); 
        if ($group_by != '')
            $group_by = " GROUP BY {$group_by}";
        if ($extra_select != '')
            $extra_select = ",{$extra_select}";
        $sql = "SELECT COUNT(*) AS total FROM {$table}{$where}{$group_by}";
        $qw_total = $this->db->query($sql);
        $total_data = ($qw_total->num_rows() == 0) ? 0 : $qw_total->row()->total;
        $total_page = ceil($total_data / $perpage);  
        $offset = $perpage * ($page-1); 
        $sql = "SELECT *{$extra_select} FROM {$table}{$where}{$group_by}";
        if ($orderby != '')
            $sql.= " ORDER BY {$orderby}";
        $sql.= " LIMIT {$offset},{$perpage} ";
        $query = $this->db->query($sql);
        $ind = 0;
        foreach($query->result_array() as $row) {
            $row['id'] = $row[$pkey];
            switch ($name) {
                case 'vperkara': case 'perkara':
                    $row['status'] = empty($row['tgl_putus']) || $row['tgl_putus'] == '0000-00-00' ? 
                        '<span style="color:#A21010">Proses</span>' : '<span style="color:#1F9032">Diputus</span>';
                break;
                case 'penduduk':
                    $row['menikah'] = $row['menikah'] == 'Y' ? 'Ya' : 'Tidak';
                    $row['tmp_tgl_lahir'] = $row['tmp_lahir'].', '.ddmmy($row['tgl_lahir']);
                break;
                case 'vapbdes_realisasi':
                    $row['persen'] = $row['anggaran'] == 0 ? 0 : ribuan(($row['jumlah']/$row['anggaran'])*100);
                break;
				case 'user':
					$row['last_login'] = xtime($row['last_login']);	
				break;
            }
            $return['rows'][] = $row;
        }
        $return['data_from'] = $offset + 1;            
        $return['data_total'] = $total_data;
        $return['data_to'] = $return['data_from'] + $perpage - 1;
        if ($return['data_to'] > $return['data_total'])
            $return['data_to'] = $return['data_total'];
        $return['page_total'] = $total_page;
        $return['page'] = $page;
        echo json_encode($return);
    }        
    
}

/* End of file majax.php */
/* Location: ./app/models/majax.php */