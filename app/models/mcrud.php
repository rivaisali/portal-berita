<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mcrud extends CI_Model
{
    private $temp = '';
    private $data = array();
    
    function __construct()
    {
        parent::__construct();
    }
       
    function before_post(&$table='', $action='', $id='', &$postdata)
    { 
        $result = '';
        switch ($table) {
            case 'kategori': case 'personil':
                $tbl = $this->db->dbprefix($table);
                if ( empty($postdata['urutan']) ) {
                    $qwx = $this->db->query("SELECT IFNULL(max(urutan)+1,1) AS urut From {$tbl}");
                    $postdata['urutan'] = $qwx->row()->urut;
                }
                if ($action == 'update') {
                    unset($postdata['urutan']);
                }
            break;
            case 'kontak':
                $empty = post_value('empty-field');
                $random = post_value('random-field');
                if ($empty != '' OR $random != config_item('my_random_char')) {
                    $result = 'Terjadi kesalahan!';
                }
            break;
            case 'pengaduan':
                $empty = post_value('empty-field');
                $random = post_value('random-field');
                if ($empty != '' OR $random != config_item('my_random_char')) {
                    $result = 'Terjadi kesalahan!';
                }
            break;
            case 'harga_komoditi':
                $postdata['last_update'] = date('Y-m-d');
            break;
            case 'esurat':
                $udata = $this->user->get_user_login('admpt');
                $postdata['pengirim'] = $udata ['user_id'];
                $postdata['hapus'] = 'N';
            break;
            case 'user':
                unset($postdata['last_login']);
                unset($postdata['updated']);
                if ( !$this->input->post('hak') ) 
                    unset($postdata['hak']); else
                    $postdata['hak'] = serialize( post_value('hak') );
                if ($action == 'update') {
                    $pass = post_value('password');
                    $konf = post_value('password2');
                    if ( trim($pass) == '' ) {
                        unset($postdata['password']);
                    } else {
                        if ($pass != $konf) $result = 'Konfirmasi password tidak cocok!'; 
                    }
                    unset($postdata['level']);                    
                } 
            break;
        }
        return $result;
    }

    function after_post(&$table='', $action='', $id='', &$postdata)
    {
        $result = '';
        $pkey = $this->config->item('pkey');
        switch ($table) {	
            case 'esurat':
                $tujuans = post_value('tujuan');
                $pdata = array();
                foreach ($tujuans as $tuj) {
                    $pdata[] = array('surat_ID'=>$id, 'wkt_kirim'=>ymdhis(), 'wkt_baca'=>NULL, 'tujuan'=>$tuj);
                }
                $this->db->insert_batch('esurat_kirim', $pdata);
                if (isset($_FILES['userfile']) && $_FILES['userfile']['error'] ===  UPLOAD_ERR_OK) {
                    $filename = random_string('alnum', 25); 
                    $filename = strtolower($filename); 
                    $upload = $this->upload_file($filename, './uploads/esurat/', 'userfile');
                    if ( $upload['result'] == 'ok' ) {
                        $pdata = array(
                            'surat_ID' => $id,
                            'nama_asli' => trim(str_replace(array(';','|','_'), ' ', $_FILES['userfile']['name'])),
                            'filename' => $upload['msg']['file_name']
                        );
                        $this->db->insert('esurat_file', $pdata);
                    }
                }
            break;		
            case 'kontak':
                if (config_item('kontak_send_email') == 'yes') {
                    $this->load->library('email');
                    $this->email->from($postdata['email'], $postdata['nama']);
                    $this->email->to(config_item('owner_email'));   
                    $this->email->reply_to($postdata['email']);              
                    $this->email->subject('Kontak  - '.$postdata['subjek']);
                    $this->email->message($postdata['pesan']);
                    $this->email->send();
                }
            break;
            case 'polling':
                $opsi_baru = post_value('opsi_baru', array());
                $pdata = array();
                foreach ($opsi_baru as $opsi) {
                    if (trim($opsi) == '') continue;
                    $pdata[] = array('opsi'=>$opsi, 'poll_id'=>$id);
                }
                if (!empty($pdata))
                    $this->db->insert_batch('polling_opsi', $pdata);
                if ($action == 'update') {
                    $pdata = array();
                    $opsi_lama = post_value('opsi_lama', array());
                    foreach ($opsi_lama as $key => $val) {
                        if (trim($val) == '') continue;
                        $pdata[] = array('opsi_id'=>$key, 'opsi'=>$val);
                    }
                    if (!empty($pdata))
                        $this->db->update_batch('polling_opsi', $pdata, 'opsi_id');
                    $opsi_to_delete = post_value('opsi_to_delete');
                    $arr_opsi_id = explode(',', $opsi_to_delete);
                    foreach($arr_opsi_id as $k => $ids) {
                        $ids = trim($ids);
                        if ($ids == '') unset($arr_opsi_id[$k]);
                    }
                    if (!empty($arr_opsi_id)) {
                        $this->db->where_in('opsi_id', $arr_opsi_id);
                        $this->db->delete('polling_opsi');
                    }
                }   
            break;
            case 'personil': case 'user':
                if (isset($_FILES['file']) && $_FILES['file']['error'] ===  UPLOAD_ERR_OK) {
                    $this->load->helper('string');
                    $filename = random_string('alnum', 25); 
                    $filename = strtolower($filename); 
                    $upload = $this->upload_file($filename, './uploads/personil/');
                    if ( $upload['result'] == 'ok' ) {
                        if ($action == 'update') {
                            $this->before_delete($table, $id);
                            $this->after_delete($table, $id);
                        }
                        $this->load->library('image');
                        $filename = $upload['msg']['file_name'];
                        $this->image->source_img = $upload['msg']['full_path']; 
                        $this->image->resize(600, 800, $upload['msg']['full_path'], 'crop'); 
                        $this->image->resize(120, 160, $upload['msg']['file_path'].'thumbs/'.$filename, 'crop');  
                        $this->image->clear();  
                        if ($table == 'personil') {                            
                            $this->db->update('personil', array('foto'=>$filename), array('pers_id'=>$id));                           
                        } else {
                            $this->db->update('user', array('foto'=>$filename), array('user_id'=>$id));   
                        }                        
                    }
                }
                if ($table == 'personil' && post_value('user_id')) {  
                    $tb2 = 'user';
                    $pdata = $this->getpost($tb2);
                    $result = $this->before_post($tb2, 'update', $id, $pdata);
                    unset($pdata['user_id']);   
                    $this->db->where('user_id', post_value('user_id'));
                    if (!$this->db->update($tb2, $pdata))
                        $result = 'Unknown Error!';                                             
                }
            break;
            case 'download':
                if ($_FILES['file']['error'] ===  UPLOAD_ERR_OK) {
                    $this->load->helper('string');
                    $filename = random_string('alnum', 15); 
                    $filename = strtolower($filename); 
                    $upload = $this->upload_file($filename, './uploads/dokumen/');
                    if ( $upload['result'] == 'ok' ) {
                        $filename = $upload['msg']['file_name'];
                        if ($action == 'update') {
                            $this->before_delete($table, $id);
                            $this->after_delete($table, $id);
                        }
                        $this->db->update($table, array('filename'=>$filename), array('ID'=>$id));                           
                    }
                }
            break;
            case 'event':
				if (isset($_FILES['file']) && $_FILES['file']['error'] ===  UPLOAD_ERR_OK) {
                    $this->load->helper('string');
                    $filename = random_string('alnum', 25); 
                    $filename = strtolower($filename); 
                    $upload = $this->upload_file($filename, './uploads/event/');
                    if ( $upload['result'] == 'ok' ) {
                        if ($action == 'update') {
                            $this->before_delete($table, $id);
                            $this->after_delete($table, $id);
                        }
                        $this->load->library('image');
                        $filename = $upload['msg']['file_name'];
						$this->db->update('event', array('gambar'=>$filename), array('event_id'=>$id));    
                    }
                }
				
			break;
        }
        return $result;
    }

    function before_validation(&$table='', $action='')
    {
        switch ($table) {
            
        }
    }
    
    function after_validation(&$table='', $action='')
    {
        switch ($table) {
           case 'user2': 
                $table = 'user';                
           break; 
           case 'personiluser': 
                $table = 'personil';
           break;
        }
    }
    
    function before_delete(&$table='', $id='')
    {
        $pkey = $this->config->item('pkey');
        switch ($table) {
            case 'personil':
                $qw = $this->db->select('foto')->get_where('personil', array('pers_id'=>$id), 1);
                $this->temp = $qw->row()->foto;
            break;   
            case 'download':
                $qw = $this->db->select('filename')->get_where('download', array('ID'=>$id), 1);
                $this->temp = $qw->row()->filename;
            break;    
        }
    }
    
    function after_delete(&$table='', $id='')
    {
        switch ($table) {
            case 'album':
                $qw = $this->db->select('url')->get_where('foto', array('album_id'=>$id));
                foreach($qw->result() as $row) {
                    $foto_path = './uploads/gallery/'.$row->url;
                    $thumb_path = './uploads/gallery/thumbs/'.$row->url;
                    if (file_exists($foto_path)) unlink($foto_path);
                    if (file_exists($thumb_path)) unlink($thumb_path);
                }
                $this->db->delete('foto', array('album_id'=>$id));
            break;
            case 'polling':
                $this->db->delete('polling_opsi', array('poll_id'=>$id));
            break;
            case 'kontak':
                $this->db->delete('balasan', array('obj_id'=>$id,'tipe'=>'kontak'));
            break;
            case 'personil':
                if (!empty($this->temp)) {
                    $path1 = './uploads/personil/'.$this->temp;
                    $path2 = './uploads/personil/thumbs/'.$this->temp;
                    @unlink($path1); @unlink($path2);
                }
            break;
            case 'download':
                if (!empty($this->temp)) {
                    if (file_exists('./uploads/dokumen/'.$this->temp))
                        unlink('./uploads/dokumen/'.$this->temp);
                }
            break;
        }
    }
    
    public function upload_file($filename='', $path='', $input_name = 'file')
    {
        $res = array();
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $config['file_name'] = $filename;
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload($input_name)) {
            $res['result']='ok'; 
            $res['msg']=$this->upload->data();           
        } else {
            $res['msg']=$this->upload->display_errors('', ', ');  
            $res['result']='error';
        }
        return $res;
    }
    
    private function getpost($table)
    {
        $except = array('jaringan');
        $postdata=array();
        $fields=$this->db->list_fields($table);
        foreach($fields as $field) {
            if ( $this->input->post($field) || in_array($table, $except) )
                $postdata[$field] = $this->input->post($field); 
        }
        return $postdata;
    }
    
    
}

/* End of file mcrud.php */
/* Location: ./app/models/mcrud.php */